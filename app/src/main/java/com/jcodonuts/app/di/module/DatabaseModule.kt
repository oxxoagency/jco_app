package com.jcodonuts.app.di.module

import android.content.Context
import androidx.room.Room
import com.jcodonuts.app.data.local.room.CartDao
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @ApplicationScope
    @Provides
    fun provideWofDatabase(context: Context): JcoDatabase {
        return Room.databaseBuilder(context, JcoDatabase::class.java, "jco_db")
            .fallbackToDestructiveMigration().build()
    }

    @ApplicationScope
    @Provides
    fun provideProductDao(jcoDatabase: JcoDatabase): CartDao {
        return jcoDatabase.cartDao()
    }
}