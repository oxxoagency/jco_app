package com.jcodonuts.app.di.module

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jcodonuts.app.di.module.factory.InjectingFragmentFactory
import com.jcodonuts.app.di.scope.FragmentKey
import com.jcodonuts.app.ui.add_address.AddAddressFragment
import com.jcodonuts.app.ui.add_address.DialogCity
import com.jcodonuts.app.ui.auth.change_password.ChangePasswordFragment
import com.jcodonuts.app.ui.auth.forgot_password.ForgotPasswordFragment
import com.jcodonuts.app.ui.auth.login.LoginFragment
import com.jcodonuts.app.ui.auth.register.RegisterFragment
import com.jcodonuts.app.ui.contact_us.ContactUsFragment
import com.jcodonuts.app.ui.delivery.DeliveryFragment
import com.jcodonuts.app.ui.add_address.DialogSearchLocation
import com.jcodonuts.app.ui.banner_detail.BannerDetailFragment
import com.jcodonuts.app.ui.banner_detail.DialogBannerDetail
import com.jcodonuts.app.ui.detail_promo.DetailPromoFragment
import com.jcodonuts.app.ui.edit_profile.EditProfileFragment
import com.jcodonuts.app.ui.hot_promo.HotPromoFragment
import com.jcodonuts.app.ui.jcor.add_address.AddAddressJcoRFragment
import com.jcodonuts.app.ui.jcor.add_address.DialogCityJcoR
import com.jcodonuts.app.ui.jcor.add_address.DialogSearchLocationJcoR
import com.jcodonuts.app.ui.jcor.banner_detail.BannerDetailJcoRFragment
import com.jcodonuts.app.ui.jcor.banner_detail.DialogBannerDetailJcoR
import com.jcodonuts.app.ui.jcor.base.MainJcoRFragment
import com.jcodonuts.app.ui.jcor.change_password.ChangePasswordJcoRFragment
import com.jcodonuts.app.ui.jcor.contact_us.ContactUsJcoRFragment
import com.jcodonuts.app.ui.jcor.delivery.DeliveryJcoRFragment
import com.jcodonuts.app.ui.jcor.edit_profile.EditProfileJcoRFragment
import com.jcodonuts.app.ui.jcor.hot_promo.HotPromoJcoRFragment
import com.jcodonuts.app.ui.jcor.language.LanguageJcoRFragment
import com.jcodonuts.app.ui.jcor.main.cart.CartJcoRFragment
import com.jcodonuts.app.ui.jcor.main.home.HomeJcoRFragment
import com.jcodonuts.app.ui.jcor.main.profile.ProfileJcoRFragment
import com.jcodonuts.app.ui.jcor.main.recent_order.RecentOrderJcoRFragment
import com.jcodonuts.app.ui.jcor.main.wishlist.WishlistJcoRFragment
import com.jcodonuts.app.ui.jcor.map.MapJcoRFragment
import com.jcodonuts.app.ui.jcor.menu_search.MenuSearchJcoRFragment
import com.jcodonuts.app.ui.jcor.payment.choose_payment.ChoosePaymentJcoRFragment
import com.jcodonuts.app.ui.jcor.pickup.PickUpJcoRFragment
import com.jcodonuts.app.ui.jcor.product_detail.ProductDetailJcoRFragment
import com.jcodonuts.app.ui.jcor.transaction_detail.TransactionDetailJcoRFragment
import com.jcodonuts.app.ui.jcor.voucher.VoucherJcoRFragment
import com.jcodonuts.app.ui.jcor.voucher.claim_voucher.ClaimVoucherJcoRFragment
import com.jcodonuts.app.ui.jcor.webview.WebViewJcoRJPointFragment
import com.jcodonuts.app.ui.jpoint.JPointFragment
import com.jcodonuts.app.ui.jpoint.jpoint_receive.JPointReceiveFragment
import com.jcodonuts.app.ui.language.LanguageFragment
import com.jcodonuts.app.ui.location.LocationFragment
import com.jcodonuts.app.ui.main.base.MainFragment
import com.jcodonuts.app.ui.main.cart.CartFragment
import com.jcodonuts.app.ui.main.home.HomeFragment
import com.jcodonuts.app.ui.main.menu_search.MenuSearchFragment
import com.jcodonuts.app.ui.main.notification.NotificationFragment
import com.jcodonuts.app.ui.main.profile.ProfileFragment
import com.jcodonuts.app.ui.main.recent_order.RecentOrderFragment
import com.jcodonuts.app.ui.main.wishlist.WishlistFragment
import com.jcodonuts.app.ui.map.MapFragment
import com.jcodonuts.app.ui.midtrans.MidtransFragment
import com.jcodonuts.app.ui.order.OrderFragment
import com.jcodonuts.app.ui.order_status.OrderStatusFragment
import com.jcodonuts.app.ui.payment.choose_payment.ChoosePaymentFragment
import com.jcodonuts.app.ui.payment.linkaja.LinkajaFragment
import com.jcodonuts.app.ui.payment.payment_detail.PaymentDetailFragment
import com.jcodonuts.app.ui.payment.topup.TopupFragment
import com.jcodonuts.app.ui.pickup.PickupFragment
import com.jcodonuts.app.ui.product_detail.DialogVariantDonut
import com.jcodonuts.app.ui.product_detail.ProductDetailFragment
import com.jcodonuts.app.ui.splash.SplashFragment
import com.jcodonuts.app.ui.tracking.TrackingFragment
import com.jcodonuts.app.ui.transaction_detail.TransactionDetailFragment
import com.jcodonuts.app.ui.voucher.VoucherFragment
import com.jcodonuts.app.ui.voucher.claim_voucher.ClaimVoucherFragment
import com.jcodonuts.app.ui.webview.WebViewFragment
import com.jcodonuts.app.ui.webview.WebViewJCOFragment
import com.jcodonuts.app.ui.webview.WebViewJPointFragment
import com.jcodonuts.app.ui.zzexample.article.ArticleFragment
import com.jcodonuts.app.ui.zzexample.detail.DetailFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class FragmentBindingModule {

    @Binds
    @IntoMap
    @FragmentKey(SplashFragment::class)
    abstract fun bindSplashFragment(splashFragment: SplashFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MainFragment::class)
    abstract fun bindMainFragment(mainFragment: MainFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HomeFragment::class)
    abstract fun bindHomeFragment(homeFragment: HomeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CartFragment::class)
    abstract fun bindCartFragment(cartFragment: CartFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WishlistFragment::class)
    abstract fun bindWishlistFragment(fragment: WishlistFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(NotificationFragment::class)
    abstract fun bindNotificationFragment(notificationFragment: NotificationFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProfileFragment::class)
    abstract fun bindProfileFragment(profileFragment: ProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LoginFragment::class)
    abstract fun bindLoginFragment(fragment: LoginFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ForgotPasswordFragment::class)
    abstract fun bindForgotPasswordFragment(fragment: ForgotPasswordFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RegisterFragment::class)
    abstract fun bindRegisterFragment(fragment: RegisterFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LinkajaFragment::class)
    abstract fun bindLinkajaFragment(fragment: LinkajaFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TopupFragment::class)
    abstract fun bindTopupFragment(fragment: TopupFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProductDetailFragment::class)
    abstract fun bindProductDetailFragment(fragment: ProductDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PickupFragment::class)
    abstract fun bindPickupFragment(fragment: PickupFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DeliveryFragment::class)
    abstract fun bindDeliveryFragment(fragment: DeliveryFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MenuSearchFragment::class)
    abstract fun bindMenuSearchFragment(fragment: MenuSearchFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChoosePaymentFragment::class)
    abstract fun bindChoosePaymentFragment(fragment: ChoosePaymentFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PaymentDetailFragment::class)
    abstract fun bindPaymentDetailFragment(fragment: PaymentDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TrackingFragment::class)
    abstract fun bindTrackingFragment(fragment: TrackingFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HotPromoFragment::class)
    abstract fun bindHotPromoFragment(fragment: HotPromoFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChangePasswordFragment::class)
    abstract fun bindChangePasswordFragment(fragment: ChangePasswordFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EditProfileFragment::class)
    abstract fun bindEditProfileFragment(fragment: EditProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LanguageFragment::class)
    abstract fun bindLanguageFragment(fragment: LanguageFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrderFragment::class)
    abstract fun bindOrderFragment(fragment: OrderFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ContactUsFragment::class)
    abstract fun bindContactUsFragment(fragment: ContactUsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ContactUsJcoRFragment::class)
    abstract fun bindContactUsJcoRFragment(fragment: ContactUsJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DetailPromoFragment::class)
    abstract fun bindDetailPromoFragment(framgent: DetailPromoFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LocationFragment::class)
    abstract fun bindLocationFragment(fragment: LocationFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DialogVariantDonut::class)
    abstract fun bindDialogVariantDonut(fragment: DialogVariantDonut): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(DialogSearchLocation::class)
    abstract fun bindDialogSearchLocation(fragment: DialogSearchLocation): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(DialogSearchLocationJcoR::class)
    abstract fun bindDialogSearchLocationJcoR(fragment: DialogSearchLocationJcoR): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(DialogBannerDetail::class)
    abstract fun bindDialogBannerDetail(fragment: DialogBannerDetail): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(DialogBannerDetailJcoR::class)
    abstract fun bindDialogBannerDetailJcoR(fragment: DialogBannerDetailJcoR): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(DialogCity::class)
    abstract fun bindDialogCity(fragment: DialogCity): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(DialogCityJcoR::class)
    abstract fun bindDialogCityJcoR(fragment: DialogCityJcoR): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(MapFragment::class)
    abstract fun bindMapFragment(fragment: MapFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RecentOrderFragment::class)
    abstract fun bindRecentOrderFragment(fragment: RecentOrderFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransactionDetailFragment::class)
    abstract fun bindTransactionDetailFragment(fragment: TransactionDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VoucherFragment::class)
    abstract fun bindVoucherFragment(fragment: VoucherFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ClaimVoucherFragment::class)
    abstract fun bindClaimVoucherFragment(fragment: ClaimVoucherFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MidtransFragment::class)
    abstract fun bindMidtransFragment(fragment: MidtransFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BannerDetailFragment::class)
    abstract fun bindBannerDetailFragment(fragment: BannerDetailFragment): Fragment


    @Binds
    @IntoMap
    @FragmentKey(MainJcoRFragment::class)
    abstract fun bindMainJcoRFragment(fragment: MainJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HomeJcoRFragment::class)
    abstract fun bindHomeJcoRFragment(fragment: HomeJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RecentOrderJcoRFragment::class)
    abstract fun bindRecentOrderJcoRFragment(fragment: RecentOrderJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CartJcoRFragment::class)
    abstract fun bindCartJcoRFragment(fragment: CartJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WishlistJcoRFragment::class)
    abstract fun bindWishlistJcoRFragment(fragment: WishlistJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProfileJcoRFragment::class)
    abstract fun bindProfileJcoRFragment(fragment: ProfileJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProductDetailJcoRFragment::class)
    abstract fun bindProductDetailJcoRFragment(fragment: ProductDetailJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BannerDetailJcoRFragment::class)
    abstract fun bindBannerDetailJcoRFragment(fragment: BannerDetailJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HotPromoJcoRFragment::class)
    abstract fun bindHotPromoJcoRFragment(fragment: HotPromoJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MenuSearchJcoRFragment::class)
    abstract fun bindMenuSearchJcoRFragment(fragment: MenuSearchJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DeliveryJcoRFragment::class)
    abstract fun bindDeliveryJcoRFragment(fragment: DeliveryJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AddAddressJcoRFragment::class)
    abstract fun bindAddAddressJcoRFragment(fragment: AddAddressJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MapJcoRFragment::class)
    abstract fun bindMapJcoRFragment(fragment: MapJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChoosePaymentJcoRFragment::class)
    abstract fun bindChoosePaymentJcoRFragment(fragment: ChoosePaymentJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VoucherJcoRFragment::class)
    abstract fun bindVoucherJcoRFragment(fragment: VoucherJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ClaimVoucherJcoRFragment::class)
    abstract fun bindClaimVoucherJcoRFragment(fragment: ClaimVoucherJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PickUpJcoRFragment::class)
    abstract fun bindPickUpJcoRFragment(fragment: PickUpJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransactionDetailJcoRFragment::class)
    abstract fun bindTransactionDetailJcoRFragment(fragment: TransactionDetailJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EditProfileJcoRFragment::class)
    abstract fun bindEditProfileJcoRFragment(fragment: EditProfileJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChangePasswordJcoRFragment::class)
    abstract fun bindChangePasswordJcoRFragment(fragment: ChangePasswordJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LanguageJcoRFragment::class)
    abstract fun bindLanguageJcoRFragment(fragment: LanguageJcoRFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ArticleFragment::class)
    abstract fun bindArticleFragment(articleFragment: ArticleFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DetailFragment::class)
    abstract fun bindDetailFragment(detailFragment: DetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AddAddressFragment::class)
    abstract fun bindAddAddressFragment(addAddressFragment: AddAddressFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(JPointFragment::class)
    abstract fun bindJPointFragment(jPointFragment: JPointFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WebViewFragment::class)
    abstract fun bindWebViewFragment(webViewFragment: WebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WebViewJPointFragment::class)
    abstract fun bindWebViewJpointFragment(webViewFragment: WebViewJPointFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WebViewJcoRJPointFragment::class)
    abstract fun bindWebViewJcoRJpointFragment(webViewJcoRJPointFragment: WebViewJcoRJPointFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrderStatusFragment::class)
    abstract fun bindOrderStatusFragment(orderStatusFragment: OrderStatusFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WebViewJCOFragment::class)
    abstract fun bindWebViewJCOFragment(webViewJCOFragment: WebViewJCOFragment): Fragment

//    @Binds
//    @IntoMap
//    @FragmentKey(JPointReceiveFragment::class)
//    abstract fun bindJPointReceiveFragment(jPointReceiveFragment: JPointReceiveFragment): Fragment

    @Binds
    abstract fun bindFragmentFactory(factory: InjectingFragmentFactory): FragmentFactory
}