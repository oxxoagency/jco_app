package com.jcodonuts.app.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jcodonuts.app.di.module.factory.ViewModelFactory
import com.jcodonuts.app.di.scope.ApplicationScope
import com.jcodonuts.app.di.scope.ViewModelKey
import com.jcodonuts.app.ui.add_address.AddAddressViewModel
import com.jcodonuts.app.ui.auth.change_password.ChangePasswordViewModel
import com.jcodonuts.app.ui.auth.forgot_password.ForgotPasswordViewModel
import com.jcodonuts.app.ui.auth.login.LoginViewModel
import com.jcodonuts.app.ui.auth.register.RegisterViewModel
import com.jcodonuts.app.ui.banner_detail.BannerDetailViewModel
import com.jcodonuts.app.ui.contact_us.ContactUsViewModel
import com.jcodonuts.app.ui.delivery.DeliveryViewModel
import com.jcodonuts.app.ui.detail_promo.DetailPromoViewModel
import com.jcodonuts.app.ui.edit_profile.EditProfileViewModel
import com.jcodonuts.app.ui.hot_promo.HotPromoViewModel
import com.jcodonuts.app.ui.jcor.banner_detail.BannerDetailJcoRViewModel
import com.jcodonuts.app.ui.jcor.base.MainJcoRViewModel
import com.jcodonuts.app.ui.jcor.delivery.DeliveryJcoRViewModel
import com.jcodonuts.app.ui.jcor.hot_promo.HotPromoJcoRViewModel
import com.jcodonuts.app.ui.jcor.main.cart.CartJcoRViewModel
import com.jcodonuts.app.ui.jcor.main.home.HomeJcoRViewModel
import com.jcodonuts.app.ui.jcor.main.profile.ProfileJcoRViewModel
import com.jcodonuts.app.ui.jcor.main.recent_order.RecentOrderJcoRViewModel
import com.jcodonuts.app.ui.jcor.main.wishlist.WishlistJcoRViewModel
import com.jcodonuts.app.ui.jcor.menu_search.MenuSearchJcoRViewModel
import com.jcodonuts.app.ui.jcor.add_address.AddAddressJcoRViewModel
import com.jcodonuts.app.ui.jcor.contact_us.ContactUsJcoRViewModel
import com.jcodonuts.app.ui.jcor.map.MapJcoRViewModel
import com.jcodonuts.app.ui.jcor.payment.choose_payment.ChoosePaymentJcoRViewModel
import com.jcodonuts.app.ui.jcor.pickup.PickUpJcoRViewModel
import com.jcodonuts.app.ui.jcor.product_detail.ProductDetailJcoRViewModel
import com.jcodonuts.app.ui.jcor.transaction_detail.TransactionDetailJcoRViewModel
import com.jcodonuts.app.ui.jcor.voucher.VoucherJcoRViewModel
import com.jcodonuts.app.ui.jcor.voucher.claim_voucher.ClaimVoucherJcoRViewModel
import com.jcodonuts.app.ui.jpoint.JPointViewModel
import com.jcodonuts.app.ui.language.LanguageViewModel
import com.jcodonuts.app.ui.location.LocationViewModel
import com.jcodonuts.app.ui.main.base.MainViewModel
import com.jcodonuts.app.ui.main.cart.CartViewModel
import com.jcodonuts.app.ui.main.home.HomeViewModel
import com.jcodonuts.app.ui.main.menu_search.MenuSearchViewModel
import com.jcodonuts.app.ui.main.notification.NotificationViewModel
import com.jcodonuts.app.ui.main.profile.ProfileViewModel
import com.jcodonuts.app.ui.main.recent_order.RecentOrderViewModel
import com.jcodonuts.app.ui.main.wishlist.WishlistViewModel
import com.jcodonuts.app.ui.map.MapViewModel
import com.jcodonuts.app.ui.midtrans.MidtransViewModel
import com.jcodonuts.app.ui.order.OrderViewModel
import com.jcodonuts.app.ui.order_status.OrderStatusViewModel
import com.jcodonuts.app.ui.payment.choose_payment.ChoosePaymentViewModel
import com.jcodonuts.app.ui.payment.linkaja.LinkajaViewModel
import com.jcodonuts.app.ui.payment.payment_detail.PaymentDetailViewModel
import com.jcodonuts.app.ui.payment.topup.TopupViewModel
import com.jcodonuts.app.ui.pickup.PickupViewModel
import com.jcodonuts.app.ui.product_detail.ProductDetailViewModel
import com.jcodonuts.app.ui.splash.SplashViewModel
import com.jcodonuts.app.ui.tracking.TrackingViewModel
import com.jcodonuts.app.ui.transaction_detail.TransactionDetailViewModel
import com.jcodonuts.app.ui.voucher.VoucherViewModel
import com.jcodonuts.app.ui.voucher.claim_voucher.ClaimVoucherViewModel
import com.jcodonuts.app.ui.webview.WebViewViewModel
import com.jcodonuts.app.ui.zzexample.article.ArticleViewModel
import com.jcodonuts.app.ui.zzexample.detail.DetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @ApplicationScope
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun providesSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun providesMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun providesHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CartViewModel::class)
    internal abstract fun providesCartViewModel(viewModel: CartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WishlistViewModel::class)
    internal abstract fun providesWishlistViewModel(viewModel: WishlistViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotificationViewModel::class)
    internal abstract fun providesNotificationViewModel(viewModel: NotificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun providesProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun providesLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordViewModel::class)
    internal abstract fun providesForgotViewModel(viewModel: ForgotPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    internal abstract fun providesRegisterViewModel(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LinkajaViewModel::class)
    internal abstract fun providesLinkajaViewModel(viewModel: LinkajaViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TopupViewModel::class)
    internal abstract fun providesTopupViewModel(viewModel: TopupViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    internal abstract fun providesProductDetailViewModel(viewModel: ProductDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PickupViewModel::class)
    internal abstract fun providesPickupViewModel(viewModel: PickupViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DeliveryViewModel::class)
    internal abstract fun providesDeliveryViewModel(viewModel: DeliveryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MenuSearchViewModel::class)
    internal abstract fun providesMenuSearchViewModel(viewModel: MenuSearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChoosePaymentViewModel::class)
    internal abstract fun providesChoosePaymentViewModel(viewModel: ChoosePaymentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PaymentDetailViewModel::class)
    internal abstract fun providesPaymentDetailViewModel(viewModel: PaymentDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrackingViewModel::class)
    internal abstract fun providesTrackingViewModel(viewModel: TrackingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HotPromoViewModel::class)
    internal abstract fun providesHotPromoViewModel(viewModel: HotPromoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePasswordViewModel::class)
    internal abstract fun providesChangePasswordViewModel(viewModel: ChangePasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditProfileViewModel::class)
    internal abstract fun providesEditProfileViewModel(viewModel: EditProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LanguageViewModel::class)
    internal abstract fun providesLanguageViewModel(viewModel: LanguageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderViewModel::class)
    internal abstract fun providesOrderViewModel(viewModel: OrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactUsViewModel::class)
    internal abstract fun providesContactUsViewModel(viewModel: ContactUsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactUsJcoRViewModel::class)
    internal abstract fun providesContactUsJcoRViewModel(viewModel: ContactUsJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddAddressViewModel::class)
    internal abstract fun providesAddAddressViewModel(viewModel: AddAddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailPromoViewModel::class)
    internal abstract fun provideDetailPromoViewModel(viewModel: DetailPromoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LocationViewModel::class)
    internal abstract fun provideLocationViewModel(viewModel: LocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    internal abstract fun provideMapViewModel(viewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecentOrderViewModel::class)
    internal abstract fun provideRecentOrderViewModel(viewModel: RecentOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherViewModel::class)
    internal abstract fun provideRecentVoucherViewModel(viewModel: VoucherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ClaimVoucherViewModel::class)
    internal abstract fun provideClaimVoucherViewModel(viewModel: ClaimVoucherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MidtransViewModel::class)
    internal abstract fun provideMidtransViewModel(viewModel: MidtransViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BannerDetailViewModel::class)
    internal abstract fun provideBannerDetailViewModel(viewModel: BannerDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransactionDetailViewModel::class)
    internal abstract fun provideTransactionDetailViewModel(viewModel: TransactionDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainJcoRViewModel::class)
    internal abstract fun provideMainJcoRViewModel(viewModel: MainJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeJcoRViewModel::class)
    internal abstract fun provideHomeJcoRViewModel(viewModel: HomeJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecentOrderJcoRViewModel::class)
    internal abstract fun provideRecentOrderJcoRViewModel(viewModel: RecentOrderJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CartJcoRViewModel::class)
    internal abstract fun provideCartJcoRViewModel(viewModel: CartJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WishlistJcoRViewModel::class)
    internal abstract fun provideWishlistJcoRViewModel(viewModel: WishlistJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileJcoRViewModel::class)
    internal abstract fun provideProfileJcoRViewModel(viewModel: ProfileJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailJcoRViewModel::class)
    internal abstract fun provideProductDetailJcoRViewModel(viewModel: ProductDetailJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BannerDetailJcoRViewModel::class)
    internal abstract fun provideBannerDetailJcoRViewModel(viewModel: BannerDetailJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HotPromoJcoRViewModel::class)
    internal abstract fun provideHtoPromoJcoRViewModel(viewModel: HotPromoJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MenuSearchJcoRViewModel::class)
    internal abstract fun provideMenuSearchJcoRViewModel(viewModel: MenuSearchJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DeliveryJcoRViewModel::class)
    internal abstract fun provideDeliveryJcoRViewModel(viewModel: DeliveryJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddAddressJcoRViewModel::class)
    internal abstract fun provideAddAddressJcoRViewModel(viewModel: AddAddressJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChoosePaymentJcoRViewModel::class)
    internal abstract fun provideChoosePaymentJcoRViewModel(viewModel: ChoosePaymentJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapJcoRViewModel::class)
    internal abstract fun provideMapJcoRViewModel(viewModel: MapJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherJcoRViewModel::class)
    internal abstract fun provideVoucherJcoRViewModel(viewModel: VoucherJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ClaimVoucherJcoRViewModel::class)
    internal abstract fun provideClaimVoucherJcoRViewModel(viewModel: ClaimVoucherJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PickUpJcoRViewModel::class)
    internal abstract fun providePickUpJcoRViewModel(viewModel: PickUpJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransactionDetailJcoRViewModel::class)
    internal abstract fun provideTransactionDetailJcoRViewModel(viewModel: TransactionDetailJcoRViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(JPointViewModel::class)
    internal abstract fun provideJPointViewModel(viewModel: JPointViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderStatusViewModel::class)
    internal abstract fun provideOrderStatusViewModel(viewModel: OrderStatusViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WebViewViewModel::class)
    internal abstract fun provideWebViewViewModel(viewModel: WebViewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ArticleViewModel::class)
    internal abstract fun providesArticleViewModel(viewModel: ArticleViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun providesDetailViewModel(viewModel: DetailViewModel): ViewModel

}