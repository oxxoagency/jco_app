package com.jcodonuts.app.di.module

import android.app.Application
import com.jcodonuts.app.data.remote.api.*
import com.jcodonuts.app.data.repository.*
import com.jcodonuts.app.di.scope.ApplicationScope
import com.jcodonuts.app.utils.SharedPreference
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    @ApplicationScope
    fun provideSharePref(app: Application): SharedPreference {
        return SharedPreference(app.applicationContext)
    }

    @Provides
    @ApplicationScope
    fun provideCountriesRepository(): MenuRepository {
        return MenuRepositoryImpl()
    }

    @Provides
    @ApplicationScope
    fun provideNewsRepository(service: NewsApi): NewsRepository {
        return NewsRepositoryImpl(service)
    }

    @Provides
    @ApplicationScope
    fun provideHomeRepository(service: JcoApi, application: Application): HomeRepository {
        return HomeRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun provideTopupRepository(service: JcoPayment, application: Application): PaymentRepository {
        return PaymentRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun provideProductRepository(service: JcoApi, application: Application): ProductRepository {
        return ProductRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun providePickupRepository(application: Application, service: JcoApi): PickupRepository {
        return PickupRepositoryImpl(application, service)
    }

    @Provides
    @ApplicationScope
    fun provideDeliveryRepository(service: JcoApi, application: Application): DeliveryRepository {
        return DeliveryRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun provideAuthRepository(service: JcoUserApi): AuthRepository {
        return AuthRepositoryImpl(service)
    }

    @Provides
    @ApplicationScope
    fun provideAuthJcoRepository(service: DirectJcoApi): AuthJcoRepository {
        return AuthJcoRepositoryImpl(service)
    }

    @Provides
    @ApplicationScope
    fun provideDirectJcoRepository(service: DirectJcoApi): DirectJcoRepository {
        return DirectJcoRepositoryImpl(service)
    }

    @Provides
    @ApplicationScope
    fun provideCartRepository(service: JcoAdminApi, application: Application): CartRepository {
        return CartRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun provideAddAddressRepository(
        service: JcoApi,
        application: Application
    ): AddAddressRepository {
        return AddAddressRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun provideAddProductPromoRepository(
        service: JcoApi,
        application: Application
    ): ProductPromoRepository = ProductPromoRepositoryImpl(service, application)

    @Provides
    @ApplicationScope
    fun provideAddTransactionRepository(
        service: JcoAdminApi,
        application: Application
    ): TransactionRepository = TransactionRepositoryImpl(service, application)

    @Provides
    @ApplicationScope
    fun provideCouponRepository(
        service: JcoApi,
        application: Application
    ): CouponRepository = CouponRepositoryImpl(service, application)
}