package com.jcodonuts.app.ui.main.cart

import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.carousel
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*

/**
 * Showcases [EpoxyController] with sticky header support
 */
class CartController(
    private val listener: CartControllerListener,
) : EpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var pickUp = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var note = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var paymentMethod = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var paymentIcon = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var menuSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var enableOrderButton = true
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is CartSwitch -> addCartSwitch(listener, cellData)
                is CartDeliveryAddress -> addCartDeliveryAddress(cellData, index, listener)
                is CartPickupAddress -> addCartPickupAddress(cellData, index, listener)
                is CartProduct -> addCartProduct(cellData, index, listener)
                is CartProductPickUp -> addCartProductPickUp(cellData, index, listener)
                is CartDetail -> addCartDetail(cellData, index, listener)
                is CartRecommendations -> addRecommendation(cellData, listener)
                is EmptyCart -> addEmptyCart(cellData)
                is CartTotal -> addCartTotal(cellData, index, listener)
                is HeaderRecommendationMenu -> addHeaderRecommendationMenu()
            }
        }
    }

    private fun addCartSwitch(listener: CartControllerListener, cellData: CartSwitch) {
        cartSwitch {
            id("switch_cart")
            listener(listener)
            data(cellData)
        }
    }


    private fun addHeaderRecommendationMenu(){
        cartHeaderRecommendation{
            id("header-recommendation")
        }
    }

    private fun addCartDeliveryAddress(
        cellData: CartDeliveryAddress,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartDeliveryAddress {
            id("delivery_address")
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCartPickupAddress(
        cellData: CartPickupAddress,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartPickupAddress {
            id("pickup_address")
            data(cellData)
            index(index)
            listener(listener)
            pickUp(pickUp)
        }
    }

    private fun addCartProduct(
        cellData: CartProduct,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartProduct {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCartProductPickUp(
        cellData: CartProductPickUp,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartProductPickUp {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCartDetail(cellData: CartDetail, index: Int, listener: CartControllerListener) {
        cartDetail {
            id("cart_detail")
            data(cellData)
            index(index)
            listener(listener)
            note(note)
            paymentMethod(paymentMethod)
            paymentIcon(paymentIcon)
        }
    }

    private fun addCartTotal(cellData: CartTotal, index: Int, listener: CartControllerListener) {
        cartTotal {
            id("cart_detail")
            data(cellData)
            isEnable(enableOrderButton)
            index(index)
            listener(listener)
        }
    }

    private fun addRecommendation(
        cellData: CartRecommendations,
        listener: CartControllerListener,
    ) {
        val models = cellData.recommendations.map {
            CartRecommendationBindingModel_()
                .id(it.menuCode)
                .data(it)
                .listener(listener)
                .selected(it.menuCode == menuSelected)
        }
        carousel {
            id("menuHeader")
            padding(Carousel.Padding.dp(16, 4, 16, 16, 8))
            models(models)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addEmptyCart(cellData: EmptyCart) {
        cartEmpty {
            id("empty-cart")
            data(cellData)
        }
    }
}
