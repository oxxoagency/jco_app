package com.jcodonuts.app.ui.main.cart

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.webkit.WebView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.*
import com.jcodonuts.app.data.repository.*
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.*
import com.midtrans.sdk.corekit.models.ItemDetails
import io.reactivex.Completable
import io.reactivex.Flowable
import org.greenrobot.eventbus.EventBus
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class CartViewModel @Inject constructor(
    private val cartRepository: CartRepository,
    private val transactionRepository: TransactionRepository,
    private val paymentRepository: PaymentRepository,
    private val productRepository: ProductRepository,
    private val directJcoRepository: DirectJcoRepository,
    private val schedulers: SchedulerProvider,
    private val jcoDatabase: JcoDatabase,
    private val sharedPreference: SharedPreference,
    private val context: Context,
) : BaseViewModel(),
    CartControllerListener {

    val datas = MutableLiveData<MutableList<BaseCell>>()

    private val _showDelivery = MutableLiveData<SingleEvents<String>>()
    val showDelivery: LiveData<SingleEvents<String>>
        get() = _showDelivery

    private val _showPickup = MutableLiveData<SingleEvents<String>>()
    val showPickup: LiveData<SingleEvents<String>>
        get() = _showPickup

    private val _showDialogNote = MutableLiveData<SingleEvents<String>>()
    val showDialogNote: LiveData<SingleEvents<String>>
        get() = _showDialogNote

    private val _showChangePayment = MutableLiveData<SingleEvents<String>>()
    val showChangePayment: LiveData<SingleEvents<String>>
        get() = _showChangePayment

    private val _showPaymentDetail = MutableLiveData<SingleEvents<String>>()
    val showPaymentDetail: LiveData<SingleEvents<String>>
        get() = _showPaymentDetail

    private val _showAddOrder = MutableLiveData<SingleEvents<String>>()
    val showAddOrder: LiveData<SingleEvents<String>>
        get() = _showAddOrder

    private val _removeFromCart = MutableLiveData<SingleEvents<String>>()
    val removeFromCart: LiveData<SingleEvents<String>>
        get() = _removeFromCart

    private val _reFetchCart = MutableLiveData<SingleEvents<String>>()
    val reFetchCart: LiveData<SingleEvents<String>>
        get() = _reFetchCart

    private val _showLoading = MutableLiveData<SingleEvents<Boolean>>()
    val showLoading: LiveData<SingleEvents<Boolean>>
        get() = _showLoading

    private val _orderId = MutableLiveData<Int?>()
    val orderId: LiveData<Int?>
        get() = _orderId

    private val _alertChoosePickUp = MutableLiveData<SingleEvents<String>>()
    val alertChoosePickUp: LiveData<SingleEvents<String>>
        get() = _alertChoosePickUp

    private val _showVoucher = MutableLiveData<SingleEvents<String>>()
    val showVoucher: LiveData<SingleEvents<String>>
        get() = _showVoucher

    private val _errorMessage = MutableLiveData<SingleEvents<String>>()
    val errorMessage: LiveData<SingleEvents<String>>
        get() = _errorMessage

    private val _showMidtrans = MutableLiveData<SingleEvents<String>>()
    val showMidtrans: LiveData<SingleEvents<String>>
        get() = _showMidtrans

    private val _alertTime = MutableLiveData<SingleEvents<String>>()
    val alertTIme: LiveData<SingleEvents<String>>
        get() = _alertTime

    private val _grossAmount = MutableLiveData<Double>()
    val grossAmount: LiveData<Double>
        get() = _grossAmount

    private val _cartOrders = MutableLiveData<MutableList<ItemDetails>>()
    val cartOrders: LiveData<MutableList<ItemDetails>>
        get() = _cartOrders

    private val _openProductDetail = MutableLiveData<SingleEvents<String>>()
    val openProductDetail: LiveData<SingleEvents<String>>
        get() = _openProductDetail

    private val _stateRefreshCartDetail = MutableLiveData<Boolean>()
    val stateRefreshCartDetail: LiveData<Boolean>
        get() = _stateRefreshCartDetail

    private val _enableOrderClick = MutableLiveData<SingleEvents<Boolean>>()
    val enableOrderClick: LiveData<SingleEvents<Boolean>>
        get() = _enableOrderClick

    val menuSelected = MutableLiveData<String>()

    var state = false

    fun cartProduct(): Flowable<List<CartProduct>> =
        jcoDatabase.cartDao().getAllCartProduct()

    fun cartProductPickUp(): Flowable<List<CartProductPickUp>> =
        jcoDatabase.cartDao().getAllCartProductPickUp()

    private fun updateCart(cartProduct: CartProduct): Completable =
        jcoDatabase.cartDao().updateCart(cartProduct)

    private fun updateCartPickUp(cartProduct: CartProductPickUp): Completable =
        jcoDatabase.cartDao().updateCartPickUp(cartProduct)

    private fun deleteCart(cartProduct: CartProduct): Completable =
        jcoDatabase.cartDao().deleteCart(cartProduct)

    private fun deleteCartPickUp(cartProduct: CartProductPickUp): Completable =
        jcoDatabase.cartDao().deleteCartPickUp(cartProduct)

    private fun deleteAll(): Completable =
        jcoDatabase.cartDao().deleteAll()

    private fun deleteAllPickUp(): Completable =
        jcoDatabase.cartDao().deleteAllPickUp()

    private var orderDetail: MutableList<CreateOrderDetailReq> = mutableListOf()

    private var deliveryType = 0
    private var isUseJPoint = false

//    private fun cartDonut(): Flowable<List<CartDetailVariant>> =
//        jcoDatabase.cartDao().getAllDonut()


    init {
        datas.value = mutableListOf()
    }

    private fun loadCreateOrder(deliveryType: Int?) {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val currentDateAndTime: String = sdf.format(Date())
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = pInfo.versionName
        val body = if (deliveryType == 0) {
            DirectCreateOrderReq(
                order_type = "8",
                payment_type = "3",
                delivery_type = deliveryType,
                member_name = sharedPreference.loadName(),
                member_email = sharedPreference.loadEmail(),
                member_phone = sharedPreference.loadPhoneNumber(),
                member_phone2 = sharedPreference.loadSelectPhoneNumber(),
                order_trx_time = currentDateAndTime,
                order_city = sharedPreference.loadSelectCity(),
                order_address = sharedPreference.loadAddress(),
                order_address_info = sharedPreference.loadDetailAddress(),
                order_postcode = sharedPreference.loadOrderPosttCodeForDelivery(), //hardcode for test emu
                order_latitude = sharedPreference.loadSelectLatitudeAddress(), //hardcode for test emu
                order_longitude = sharedPreference.loadSelectLongitudeAddress(), //hardcode for test emu
                order_note = sharedPreference.loadNotes(),
                order_company = "JID",
                order_outlet_ID = sharedPreference.loadOutletIdForDelivery(), //hardcode for test emu
                order_outlet_code = sharedPreference.loadOutletCodeForDelivery(), //hardcode for test emu
                order_ip = NetworkUtils.getIPAddress(true),
                order_useragent = "${WebView(context).settings.userAgentString} $version",
                order_detail = orderDetail,
                coupon_code = sharedPreference.loadCodeVoucher(),
                order_payment = sharedPreference.loadPaymentMethod(),
                order_brand = "JCO",
                order_recipient_name = sharedPreference.loadRecipientName(),
                order_recipient_phone = sharedPreference.loadSelectPhoneNumber(),
                use_jpoint = if (isUseJPoint) 1 else 0
            )
        } else {
            DirectCreateOrderReq(
                order_type = "8",
                payment_type = "3",
                delivery_type = deliveryType,
                member_name = sharedPreference.loadName(),
                member_email = sharedPreference.loadEmail(),
                member_phone = sharedPreference.loadPhoneNumber(),
                member_phone2 = sharedPreference.loadSelectPhoneNumber(),
                order_trx_time = currentDateAndTime,
                order_city = sharedPreference.loadPickUpCity(),
                order_address = sharedPreference.loadPickUpAddress(),
                order_address_info = sharedPreference.loadDetailAddress(),
                order_postcode = sharedPreference.loadPostCode(), //hardcode for test emu
                order_latitude = sharedPreference.loadSelectLatitudePickUp(), //hardcode for test emu
                order_longitude = sharedPreference.loadSelectLongitudePickup(), //hardcode for test emu
                order_note = sharedPreference.loadNotes(),
                order_company = "JID",
                order_outlet_ID = sharedPreference.loadOutletId(), //hardcode for test emu
                order_outlet_code = sharedPreference.loadOutletCode(), //hardcode for test emu
                order_ip = NetworkUtils.getIPAddress(true),
                order_useragent = "${WebView(context).settings.userAgentString} $version",
                order_detail = orderDetail,
                coupon_code = sharedPreference.loadCodeVoucher(),
                order_payment = sharedPreference.loadPaymentMethod(),
                order_brand = "JCO",
                order_recipient_name = sharedPreference.loadName(),
                order_recipient_phone = sharedPreference.loadPhoneNumber(),
                use_jpoint = if (isUseJPoint) 1 else 0
            )
        }

        lastDisposable = directJcoRepository.createOrder(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _showLoading.value = SingleEvents(false)
                try {
                    _orderId.value = it.data.order_id
                    itemSubmitMidtrans(it)
                } catch (e: Exception) {
                    _orderId.value = 0
                }
                if (it.status_code == 200) {
                    if (isUseJPoint && sharedPreference.loadJPoint()
                            ?.toInt() ?: 0 >= _grossAmount.value?.toInt() ?: 0
                    ) {
                        _showPaymentDetail.value = SingleEvents("show-payment-detail")
                    } else {
                        _showMidtrans.value = SingleEvents("show-midtrans")
                    }
                } else {
                    Log.d("CHECK ERROR", "CHECK ERROR")
                    if (!it.error?.order_address.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.order_address.toString())
                    } else if (!it.error?.order_outlet_code.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.order_outlet_code.toString())
                    } else if (!it.error?.member_phone2.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.member_phone2.toString())
                    } else if (!it.error?.order_payment.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.order_payment.toString())
                    } else if (!it.error?.message.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.message.toString())
                    } else {
                        _errorMessage.value =
                            SingleEvents("Harap ubah pesanan Anda karena produk ini tidak tersedia")
                    }
                }

            }, {
                _showLoading.value = SingleEvents(false)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadMenuRecommendation(cartInfo: MutableList<CartInfo>, city: String) {
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = pInfo.versionCode
        val body = MenuRecommendationReq(
            city = city,
            order_brand = "JCO",
            cart_info = cartInfo,
            app_os = "android",
            app_version = version
        )
        lastDisposable = directJcoRepository.getMenuRecommendation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                val response = data.data
                val recommendation = mutableListOf<CartRecommendation>()
                datas.value.let {
                    response?.map { product ->
                        recommendation.add(CartRecommendation(
                            menuCode = product.menu_code,
                            name = localizationMenuName(product),
                            image = product.menu_img,
                            price = Converter.thousandSeparator(product.menu_price),
                            isFavorite = false
                        ))
                    }

                    it?.add(CartRecommendations(recommendation))
                    datas.value = it
                }
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadCartUseJpoint(cartInfo: MutableList<CartInfo>, city: String, jPoint: Int) {
        val body = CartOrder(
            1,
            city,
            deliveryType,
            sharedPreference.loadCodeVoucher(),
            sharedPreference.loadPaymentMethod(),
            "JCO",
            cartInfo,
            jPoint,
            sharedPreference.loadAddress(),
            sharedPreference.loadDetailAddress(),
            sharedPreference.loadSelectLatitudeAddress(),
            sharedPreference.loadSelectLongitudeAddress()
        )
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = pInfo.versionCode
        val bodyRecommendation = MenuRecommendationReq(
            city = city,
            order_brand = "JCO",
            cart_info = cartInfo,
            app_os = "android",
            app_version = version
        )
        lastDisposable = cartRepository.getCartOrders(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .flatMap { model ->
                datas.value.let {
                    if (model.status_code == 200) {
                        if (!model.data.error.message.isNullOrEmpty()) {
                            _errorMessage.value = SingleEvents(model.data.error.message)
                            sharedPreference.removeCodeVoucher()
                            sharedPreference.removeVoucher()
                        } else if (!model.data.error.coupon_code.isNullOrEmpty()) {
                            _errorMessage.value = SingleEvents(model.data.error.coupon_code)
                            sharedPreference.removeCodeVoucher()
                            sharedPreference.removeVoucher()
                        }
                        saveCartDetail(model)
                        _grossAmount.value = model.data.grandtotal?.toDouble()


                        if (!model.data.invalid_menu.isNullOrEmpty()) {
                            var items = ""
                            model.data.invalid_menu.map { invalidMenu ->
                                items = invalidMenu.menu_name.toString()
                            }
                            _errorMessage.value =
                                SingleEvents("Harap ubah pesanan Anda karena produk ini tidak tersedia: $items")
                        }

                    } else {
                        sharedPreference.removeCodeVoucher()
                        sharedPreference.removeVoucher()
                        _errorMessage.value =
                            SingleEvents("Pesanan kamu belum memenuhi syarat dan ketentuan voucher yang berlaku")
                        _grossAmount.value = sharedPreference.loadGrossAmount()?.toDouble()
                    }
                    datas.value = it
                }
                directJcoRepository.getMenuRecommendation(bodyRecommendation)
            }
            .subscribe({ data ->
                val response = data.data
                val recommendation = mutableListOf<CartRecommendation>()
                datas.value.let {
                    response?.map { product ->
                        recommendation.add(CartRecommendation(
                            menuCode = product.menu_code,
                            name = localizationMenuName(product),
                            image = product.menu_img,
                            price = Converter.thousandSeparator(product.menu_price),
                            isFavorite = false
                        ))
                    }

                    if (sharedPreference.loadItemDetail() != null) {
                        it?.removeAll { data -> data is CartTotal }
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetail()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetail()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetail()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointEarn = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                                jpointUsed = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_used
                                    ?: 0,
                            )
                        )
                    }
                    Log.d("cekrecom", it.toString())
                    datas.value = it
                }

            }, {
                handleError(it)
                datas.value.let {
                    if (sharedPreference.loadItemDetail() != null) {
                        it?.removeAll { data -> data is CartTotal }
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetail()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetail()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetail()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointUsed = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_used
                                    ?: 0,
                                jpointEarn = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                            )
                        )
                        datas.value = it
                    }
                }
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadCart(cartInfo: MutableList<CartInfo>, city: String, jPoint: Int) {
        val body = CartOrder(
            1,
            city,
            deliveryType,
            sharedPreference.loadCodeVoucher(),
            sharedPreference.loadPaymentMethod(),
            "JCO",
            cartInfo,
            jPoint,
            sharedPreference.loadAddress(),
            sharedPreference.loadDetailAddress(),
            sharedPreference.loadSelectLatitudeAddress(),
            sharedPreference.loadSelectLongitudeAddress()
        )
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = pInfo.versionCode
        val bodyRecommendation = MenuRecommendationReq(
            city = city,
            order_brand = "JCO",
            cart_info = cartInfo,
            app_os = "android",
            app_version = version
        )
        lastDisposable = cartRepository.getCartOrders(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .flatMap { model ->
                datas.value.let {
                    if (model.status_code == 200) {
                        if (!model.data.error.coupon_code.isNullOrEmpty()) {
                            _errorMessage.value = SingleEvents(model.data.error.coupon_code)
                            sharedPreference.removeCodeVoucher()
                            sharedPreference.removeVoucher()
                        }
                        saveCartDetail(model)
                        it?.add(
                            CartDetail(
                                paymentName = sharedPreference.loadPaymentName().toString(),
                                paymentIcon = sharedPreference.loadPaymentIcon().toString(),
                                price = Converter.rupiah(
                                    model.data.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    model.data.total_delivery_fee?.toDouble() ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    model.data.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                notes = sharedPreference.loadNotes(),
                                ecobag = model.data.ecoBag?.ecobag_name,
                                ecobagPrice = Converter.rupiah(
                                    model.data.ecoBag?.ecobag_price.toString(),
                                ),
                                ecobagSubTotal = Converter.rupiah(
                                    model.data.ecoBag?.ecobag_subtotal?.toDouble() ?: 0.0
                                ),
                                ecobagQty = model.data.ecoBag?.ecobag_quantity.toString(),
                                voucher = sharedPreference.loadVoucher(),
                                promo = model.data.promo?.promo_value,
                                freeDeliveryText = Converter.rupiah(
                                    model.data.freeDelivery?.toDouble() ?: 0.0
                                ),
                                freeDelivery = model.data.freeDelivery,
                                deliveryService = model.data.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    model.data.delivery_fee?.toDouble() ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jPoint = Converter.thousandSeparator(sharedPreference.loadJPoint()
                                    ?: ""),
                                jPointInt = sharedPreference.loadJPoint()?.toInt() ?: 0
                            )
                        )


                        if (!model.data.invalid_menu.isNullOrEmpty()) {
                            var items = ""
                            model.data.invalid_menu.map { invalidMenu ->
                                items = invalidMenu.menu_name.toString()
                            }
                            _errorMessage.value =
                                SingleEvents("Harap ubah pesanan Anda karena produk ini tidak tersedia: $items")
                        }

                    } else if (model.status_code == 401) {
                        EventBus.getDefault().post(LogoutEvent(model.error.toString()))
                    } else {
                        sharedPreference.removeCodeVoucher()
                        sharedPreference.removeVoucher()
                        it?.add(
                            CartDetail(
                                paymentName = sharedPreference.loadPaymentName().toString(),
                                paymentIcon = sharedPreference.loadPaymentIcon().toString(),
                                price = sharedPreference.loadPriceCart(),
                                deliveryServiceText = sharedPreference.loadTotalDeliveryFee(),
                                totalPrice = sharedPreference.loadTotalPriceCart(),
                                notes = sharedPreference.loadNotes(),
                                ecobag = sharedPreference.loadEcobagCart(),
                                ecobagPrice = sharedPreference.loadEcobagPriceCart(),
                                ecobagSubTotal = sharedPreference.loadEcobagSubtotalCart(),
                                ecobagQty = sharedPreference.loadEcobagQtyCart(),
                                voucher = sharedPreference.loadVoucher(),
                                promo = 0,
                                freeDeliveryText = sharedPreference.loadFreeDelivery(),
                                freeDelivery = sharedPreference.loadFreeDelivery()?.toInt(),
                                deliveryService = sharedPreference.loadDeliveryFeeCart()?.toInt(),
                                totalDeliveryFee = sharedPreference.loadDeliveryFeeCart(),
                                isDelivery = deliveryType == 0,
                                jPoint = Converter.thousandSeparator(sharedPreference.loadJPoint()
                                    ?: ""),
                                jPointInt = sharedPreference.loadJPoint()?.toInt() ?: 0
                            )
                        )
                        _errorMessage.value =
                            SingleEvents("Pesanan kamu belum memenuhi syarat dan ketentuan voucher yang berlaku")
                    }
                    datas.value = it
                }
                directJcoRepository.getMenuRecommendation(bodyRecommendation)
            }
            .subscribe({ data ->
                val response = data.data
                val recommendation = mutableListOf<CartRecommendation>()
                datas.value.let {
                    response?.map { product ->
                        recommendation.add(CartRecommendation(
                            menuCode = product.menu_code,
                            name = localizationMenuName(product),
                            image = product.menu_img,
                            price = Converter.thousandSeparator(product.menu_price),
                            isFavorite = false
                        ))
                    }
                    if (!recommendation.isNullOrEmpty()) {
                        it?.add(HeaderRecommendationMenu())
                        it?.add(CartRecommendations(recommendation))
                    }

                    if (sharedPreference.loadItemDetail() != null) {
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetail()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetail()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetail()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointUsed = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_used
                                    ?: 0,
                                jpointEarn = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                            )
                        )
                    }
                    Log.d("cekrecom", it.toString())
                    datas.value = it
                }

            }, {
                handleError(it)
                datas.value.let {
                    if (sharedPreference.loadItemDetail() != null) {
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetail()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetail()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetail()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetail()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetail()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointUsed = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_used
                                    ?: 0,
                                jpointEarn = sharedPreference.loadItemDetail()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                            )
                        )
                        datas.value = it
                    }
                }
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getAllItemCartForSwitch(deliveryType: Int?) {
        datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            it?.removeAll { data -> data is EmptyCart }
            it?.removeAll { data -> data is CartRecommendations }
            it?.removeAll { data -> data is CartTotal }
            it?.removeAll { data -> data is HeaderRecommendationMenu }
            datas.value = it
        }
        if (deliveryType == 0) {
            lastDisposable = cartProduct().subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ data ->
                    Log.d("cekcart", data.toString())
                    datas.value.let {
                        if (data.isNullOrEmpty()) {
                            it?.add(EmptyCart(context.getString(R.string.cart_is_empty)))
                        }
                        data.map { cartProduct ->
                            it?.add(
                                CartProduct(
                                    cartProduct.id,
                                    cartProduct.menuCode,
                                    cartProduct.name,
                                    cartProduct.imgURL,
                                    cartProduct.price,
                                    cartProduct.productType,
                                    cartProduct.notes,
                                    cartProduct.qty,
                                    cartProduct.priceOriginal,
                                    cartProduct.variantname,
                                    cartProduct.detailVariant
                                )
                            )
                        }
                        val cartInfo = mutableListOf<CartInfo>()
                        data.map { cartProduct ->
                            cartInfo.add(
                                CartInfo(
                                    cartProduct.menuCode.toString(),
                                    cartProduct.qty.toString()
                                )
                            )
                        }
                        orderDetail.clear()
                        data.map { cartProduct ->
                            orderDetail.add(
                                CreateOrderDetailReq(
                                    menu_code = cartProduct.menuCode.toString(),
                                    menu_name = cartProduct.name,
                                    menu_quantity = cartProduct.qty.toString(),
                                    material_unit_price = cartProduct.priceOriginal.toString(),
                                    material_sub_total = cartProduct.price.toString(),
                                    menu_detail = cartProduct.getMenuDetail()
                                )
                            )
                        }
                        loadCart(cartInfo, sharedPreference.loadSelectCity() ?: "", 0)
//                        loadMenuRecommendation(cartInfo, sharedPreference.loadSelectCity() ?: "")
//                    loadCreateOrder(0)

                        datas.value = it
                    }
                }, {

                })
        } else {
            lastDisposable = cartProductPickUp().subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ data ->
                    Log.d("cekcart", data.toString())
                    datas.value.let {
                        data.map { cartProduct ->
                            it?.add(
                                CartProductPickUp(
                                    cartProduct.id,
                                    cartProduct.menuCode,
                                    cartProduct.name,
                                    cartProduct.imgURL,
                                    cartProduct.price,
                                    cartProduct.productType,
                                    cartProduct.notes,
                                    cartProduct.qty,
                                    cartProduct.priceOriginal,
                                    cartProduct.variantname,
                                    cartProduct.detailVariant
                                )
                            )
                        }
                        val cartInfo = mutableListOf<CartInfo>()
                        data.map { cartProduct ->
                            cartInfo.add(
                                CartInfo(
                                    cartProduct.menuCode.toString(),
                                    cartProduct.qty.toString()
                                )
                            )
                        }
                        orderDetail.clear()
                        data.map { cartProduct ->
                            orderDetail.add(
                                CreateOrderDetailReq(
                                    menu_code = cartProduct.menuCode.toString(),
                                    menu_name = cartProduct.name,
                                    menu_quantity = cartProduct.qty.toString(),
                                    material_unit_price = cartProduct.priceOriginal.toString(),
                                    material_sub_total = cartProduct.price.toString(),
                                    menu_detail = cartProduct.getMenuDetail()
                                )
                            )
                        }
                        loadCart(cartInfo, sharedPreference.loadPickUpCity() ?: "Jakarta Barat", 0)
//                        loadMenuRecommendation(cartInfo,
//                            sharedPreference.loadPickUpCity() ?: "Jakarta Barat")
//                    loadCreateOrder(0)

                        datas.value = it
                    }
                }, {

                })
        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }


    fun getOrderPayment(orderId: String) {
        val body = OrderPaymentReq(
            brand = 1,
            order_id = orderId
        )

        lastDisposable = paymentRepository.orderPayment(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }


    fun getAllItemCart() {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({ data ->
                sharedPreference.saveDeliveryType("0")
                datas.value?.let {
                    it.add(CartSwitch(false))
                    it.add(
                        CartDeliveryAddress(
                            sharedPreference.loadAddress().toString(),
                            sharedPreference.loadPickUp().toString()
                        )
                    )
                    if (data.isNullOrEmpty()) {
                        it.add(EmptyCart(context.getString(R.string.cart_is_empty)))
                    }
                    data.map { cartProduct ->
                        it.add(
                            CartProduct(
                                cartProduct.id,
                                cartProduct.menuCode,
                                cartProduct.name,
                                cartProduct.imgURL,
                                cartProduct.price,
                                cartProduct.productType,
                                cartProduct.notes,
                                cartProduct.qty,
                                cartProduct.priceOriginal,
                                cartProduct.variantname,
                                cartProduct.detailVariant
                            )
                        )
                    }
                    val cartInfo = mutableListOf<CartInfo>()
                    data.map { cartProduct ->
                        cartInfo.add(
                            CartInfo(
                                cartProduct.menuCode.toString(),
                                cartProduct.qty.toString()
                            )
                        )
                    }

                    orderDetail.clear()
                    data.map { cartProduct ->
                        orderDetail.add(
                            CreateOrderDetailReq(
                                menu_code = cartProduct.menuCode.toString(),
                                menu_name = cartProduct.name,
                                menu_quantity = cartProduct.qty.toString(),
                                material_unit_price = cartProduct.priceOriginal.toString(),
                                material_sub_total = cartProduct.price.toString(),
                                menu_detail = cartProduct.getMenuDetail()
                            )
                        )
                    }
                    loadCart(cartInfo, sharedPreference.loadSelectCity() ?: "", 0)
//                    loadMenuRecommendation(cartInfo, sharedPreference.loadSelectCity() ?: "")
//                    loadCreateOrder(0)

                    datas.value = it
                }
            }, {
                handleError(it)
            })


    }

    private fun getAllItemCartUseJpoint(jPoint: Int) {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({ data ->
                Log.d("tag", data.toString())
                datas.value?.let {
                    val cartInfo = mutableListOf<CartInfo>()
                    data.map { cartProduct ->
                        cartInfo.add(
                            CartInfo(
                                cartProduct.menuCode.toString(),
                                cartProduct.qty.toString()
                            )
                        )
                    }

                    orderDetail.clear()
                    data.map { cartProduct ->
                        orderDetail.add(
                            CreateOrderDetailReq(
                                menu_code = cartProduct.menuCode.toString(),
                                menu_name = cartProduct.name,
                                menu_quantity = cartProduct.qty.toString(),
                                material_unit_price = cartProduct.priceOriginal.toString(),
                                material_sub_total = cartProduct.price.toString(),
                                menu_detail = cartProduct.getMenuDetail()
                            )
                        )
                    }
//                    it.removeAll { data -> data is CartTotal }
                    loadCartUseJpoint(cartInfo, sharedPreference.loadSelectCity() ?: "", jPoint)

                    datas.value = it
                }
            }, {
                handleError(it)
            })


    }

    fun midtransPayment(transactionId: String?) {
        if (deliveryType == 0) {
            val body = PaymentReq(
                1,
                transactionId,
                sharedPreference.loadEmail(),
                sharedPreference.loadPhoneNumber(),
                sharedPreference.loadFcmToken(),
                sharedPreference.loadOutletIdForDelivery()
            )
            lastDisposable = paymentRepository.getMidtransPayment(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({

                }, {

                })
        } else {
            val body = PaymentReq(
                1,
                transactionId,
                sharedPreference.loadEmail(),
                sharedPreference.loadPhoneNumber(),
                sharedPreference.loadFcmToken(),
                sharedPreference.loadOutletId()
            )
            lastDisposable = paymentRepository.getMidtransPayment(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({

                }, {

                })
        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    //TODO add recommendation
    fun getOrderRecommendation() {
        val body =
            RecommendationReq(
                1,
                sharedPreference.loadPhoneNumber(),
                sharedPreference.loadSelectCity(),
                3
            )
        lastDisposable =
            productRepository.getRecommendation(body).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui()).subscribe({
                    val data = it.data
                    datas.value.let {
                        data.map { product ->
                            it?.add(
                                CartRecommendation(
                                    menuCode = product.menu_code,
                                    name = product.menu_name,
                                    image = product.menu_image,
                                    price = Converter.rupiah(product.menu_price),
                                    isFavorite = product.is_favorite == "1"
                                )
                            )
                        }
                    }
                }, {

                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun saveCartDetail(model: CartOrderRes) {
        sharedPreference.apply {
            savePriceCart(
                Converter.rupiah(
                    model.data.subtotal?.toDouble()
                        ?: 0.0
                )
            )
            saveDeliveryFeeCart(
                Converter.rupiah(
                    model.data.total_delivery_fee?.toDouble() ?: 0.0
                )
            )
            saveTotalPriceCart(
                Converter.rupiah(
                    model.data.grandtotal?.toDouble()
                        ?: 0.0
                )
            )
            saveEcobagCart(model.data.ecoBag?.ecobag_name.toString())
            saveEcobagPriceCart(
                Converter.rupiah(
                    model.data.ecoBag?.ecobag_price.toString(),
                )
            )
            saveEcobagSubtotalCart(
                Converter.rupiah(
                    model.data.ecoBag?.ecobag_subtotal?.toDouble() ?: 0.0
                )
            )
            saveEcobagQtyCart(model.data.ecoBag?.ecobag_quantity.toString())
            saveGrossAmount(model.data.grandtotal ?: 0)
            saveItemDetail(model.data)
            saveFreeDelivery(Converter.rupiah(model.data.freeDelivery?.toDouble()
                ?: 0.0))
            saveTotalDeliveryFee(Converter.rupiah(model.data.delivery_fee?.toDouble()
                ?: 0.0))
        }
    }

    private fun doUpdateCart(cartProduct: CartProduct) {
        lastDisposable =
            updateCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
//                    getAllItemCartForReloadDetail()
                }, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doUpdateCartPickUp(cartProduct: CartProductPickUp) {
        lastDisposable =
            updateCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
//                    getAllItemCartForReloadDetail()
                }, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doDeleteCart(cartProduct: CartProduct) {
        sharedPreference.removeItemDetail()
        lastDisposable =
            deleteCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({}, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    private fun doDeleteCartPickUp(cartProduct: CartProductPickUp) {
        sharedPreference.removeItemDetail()
        lastDisposable =
            deleteCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({}, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    private fun itemSubmitMidtrans(model: CreateOrderRes) {
        val tempCart = mutableListOf<ItemDetails>()
        model.data.order_detail?.map { cartProduct ->
            tempCart.add(
                ItemDetails(
                    cartProduct.menu_code.toString(),
                    cartProduct.menu_unitprice?.toDouble() ?: 0.0,
                    cartProduct.menu_quantity?.toInt() ?: 0,
                    cartProduct.menu_name?.take(50)
                )
            )
        }
        if (model.data.order_fee ?: 0 >= 0) {
            tempCart.add(
                ItemDetails(
                    "1111111111-000-1",
                    model.data.order_fee?.toDouble() ?: 0.0,
                    1,
                    "Delivery Fee"
                )
            )
        }

        if (model.data.order_promo != 0) {
            if (model.data.order_promo ?: 0 > 0) {
                tempCart.add(
                    ItemDetails(
                        "1111111111-000-2",
                        model.data.order_promo?.toDouble()?.times(-1) ?: 0.0,
                        1,
                        "Promo"
                    )
                )
            } else {
                tempCart.add(
                    ItemDetails(
                        "1111111111-000-2",
                        model.data.order_promo?.toDouble() ?: 0.0,
                        1,
                        "Promo"
                    )
                )
            }

        }

        if (isUseJPoint) {
            val point = model.data.order_loyalty?.toDouble() ?: 0.0
            tempCart.add(
                ItemDetails(
                    "1111111111-000-3",
                    -point,
                    1,
                    "Potongan JPoint"
                )
            )
        }
        _grossAmount.value = model.data.order_total?.toDouble()
        _cartOrders.value = tempCart
    }

    fun doDeleteAllCart() {
        datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            it?.removeAll { data -> data is CartRecommendations }
            it?.removeAll { data -> data is CartTotal }
            it?.removeAll { data -> data is HeaderRecommendationMenu }
            datas.value = it
        }
        lastDisposable = deleteAll().subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({}, {})
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun doDeleteAllCartPickUp() {
        datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            it?.removeAll { data -> data is CartRecommendations }
            it?.removeAll { data -> data is CartTotal }
            it?.removeAll { data -> data is HeaderRecommendationMenu }
            datas.value = it
        }
        lastDisposable = deleteAllPickUp().subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({}, {})
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun removeAfterPickup() {
        datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is EmptyCart }
            datas.value = it
        }
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    override fun onClick(index: Int) {

    }

    override fun onSwitchChange(checked: Boolean) {
        datas.value?.let {
            if (checked) {
                it[0] = CartSwitch(true)
                it[1] = CartPickupAddress(sharedPreference.loadPickUp().toString())
                deliveryType = 1
                sharedPreference.saveDeliveryType("1")
                getAllItemCartForSwitch(1)

//                loadCreateOrder(1)
            } else {
                it[0] = CartSwitch(false)
                it[1] = CartDeliveryAddress(
                    sharedPreference.loadAddress().toString(),
                    sharedPreference.loadPickUp().toString()
                )
                deliveryType = 0
                sharedPreference.saveDeliveryType("0")
                getAllItemCartForSwitch(0)
//                loadCreateOrder(0)
            }
            datas.postValue(it)
        }
    }

    override fun onPickupAddressClick(index: Int) {
        _showPickup.value = SingleEvents("pickup")
    }

    override fun onDeliveryAddressClick(index: Int) {
        _showDelivery.value = SingleEvents("delivery")
    }

    override fun onOrderClick(index: Int) {
//        if (_orderId.value == 0) {
//            _alertChoosePickUp.value = SingleEvents("alert-choose-pick-up")
//        } else {
//            _showPaymentDetail.value = SingleEvents("payment_detail")
//        }
        _enableOrderClick.value = SingleEvents(false)
        Handler(Looper.getMainLooper()).postDelayed({
            _enableOrderClick.value = SingleEvents(true)
        }, 5000)
        if (deliveryType == 1) {
            val currentTime = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
            if (currentTime in 22 downTo 9) {
                _showLoading.value = SingleEvents(true)
                loadCreateOrder(deliveryType)

            } else {
                _alertTime.value =
                    SingleEvents("Outlet sedang tutup pada jam 21:00 - 10:00")
            }

        } else {
            _showLoading.value = SingleEvents(true)
            loadCreateOrder(deliveryType)
        }
    }

    override fun onChangePaymentClick(index: Int) {
        _showChangePayment.value = SingleEvents("change_payment")
    }

    override fun onProductClick(index: Int) {

    }

    override fun onProductPlusClick(index: Int) {
        datas.value?.let {
            val content = (it[index] as CartProduct).copy()
            content.qty++
            content.price = content.priceOriginal * content.qty
            it[index] = content
            doUpdateCart(content)
            doUpdateCartPickUp(
                CartProductPickUp(
                    menuCode = content.menuCode,
                    id = content.id,
                    name = content.name,
                    imgURL = content.imgURL,
                    price = content.price,
                    productType = content.productType,
                    notes = "",
                    qty = content.qty,
                    variantname = content.variantname,
                    detailVariant = content.detailVariant,
                    priceOriginal = content.priceOriginal
                )
            )
            Handler(Looper.getMainLooper()).postDelayed({
                _reFetchCart.value = SingleEvents("removeFromCart")
            }, 3000)
            datas.value = it
        }
    }

    override fun onProductMinusClick(index: Int) {
        datas.value?.let {
            val content = (it[index] as CartProduct).copy()
            if (content.qty > 1) {
                content.qty--
                content.price = content.priceOriginal * content.qty
                val price = content.priceOriginal.times(content.qty)
                Log.d("cekqyaaa", content.qty.toString())
                it[index] = content
                doUpdateCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                doUpdateCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _reFetchCart.value = SingleEvents("removeFromCart")
                }, 1500)
            } else {
                doDeleteCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                doDeleteCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _removeFromCart.value = SingleEvents("removeFromCart")
                }, 0)
            }
            datas.value = it
        }
    }

    override fun onProductNotesClick(index: Int) {
        _showDialogNote.value = SingleEvents("notes")
    }

    override fun onAddOrderClick() {
        _showAddOrder.value = SingleEvents("showAddOrder")
    }

    override fun onDeleteAllClick() {
        doDeleteAllCart()
        doDeleteAllCartPickUp()
        sharedPreference.removeItemDetail()
        Handler(Looper.getMainLooper()).postDelayed({
            _removeFromCart.value = SingleEvents("removeFromCart")
        }, 1)
    }

    override fun onVoucherClick() {
        _showVoucher.value = SingleEvents("show-voucher")
    }

    override fun onProductPlusPickUpClick(index: Int) {
        datas.value?.let {
            val content = (it[index] as CartProductPickUp).copy()
            content.qty++
            content.price = content.priceOriginal * content.qty
            it[index] = content
            doUpdateCartPickUp(content)
            doUpdateCart(
                CartProduct(
                    menuCode = content.menuCode,
                    id = content.id,
                    name = content.name,
                    imgURL = content.imgURL,
                    price = content.price,
                    productType = content.productType,
                    notes = "",
                    qty = content.qty,
                    variantname = content.variantname,
                    detailVariant = content.detailVariant,
                    priceOriginal = content.priceOriginal
                )
            )
            Handler(Looper.getMainLooper()).postDelayed({
                _reFetchCart.value = SingleEvents("removeFromCart")
            }, 3000)
            datas.value = it
        }
    }

    override fun onProductMinusPickUpClick(index: Int) {
        datas.value?.let {
            val content = (it[index] as CartProductPickUp).copy()
            if (content.qty > 1) {
                content.qty--
                content.price = content.priceOriginal * content.qty
                val price = content.priceOriginal.times(content.qty)
                Log.d("cekqyaaa", content.qty.toString())
                it[index] = content
                doUpdateCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                doUpdateCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _reFetchCart.value = SingleEvents("removeFromCart")
                }, 1500)
            } else {
                doDeleteCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                doDeleteCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _removeFromCart.value = SingleEvents("removeFromCart")
                }, 0)
            }
            datas.value = it
        }
    }

    override fun onMenuItemClick(cartRecommendation: CartRecommendation) {
        datas.value.let {
            menuSelected.postValue(cartRecommendation.menuCode ?: "")
            _openProductDetail.value = SingleEvents(cartRecommendation.menuCode ?: "")
            Log.d("recomend", "clicked")
        }
    }

    override fun onUsedJPoint(checked: Boolean) {
        datas.value.let {
            if (checked) {
                isUseJPoint = true
                getAllItemCartUseJpoint(1)
            } else {
                isUseJPoint = false
                getAllItemCartUseJpoint(0)
            }

            datas.postValue(it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        _cartOrders.value = mutableListOf()
    }
}