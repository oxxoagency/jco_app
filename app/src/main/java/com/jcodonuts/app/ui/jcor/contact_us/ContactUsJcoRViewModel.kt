package com.jcodonuts.app.ui.jcor.contact_us

import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.ProfileMenu
import com.jcodonuts.app.data.local.ProfileMenuHeader
import com.jcodonuts.app.data.local.SocmedMenu
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.ui.contact_us.ContactUsViewModel
import com.jcodonuts.app.ui.main.profile.ProfileControllerListener
import javax.inject.Inject

class ContactUsJcoRViewModel @Inject constructor() :  BaseViewModel(), ProfileControllerListener {

    val datas = MutableLiveData<MutableList<BaseCell>>()

    init {
        datas.value = mutableListOf()
    }

    fun loadData(){
        datas.value?.let {
            it.add(ProfileMenuHeader("Contact Us"))
            it.add(ProfileMenu(R.drawable.ic_baseline_language, R.string.website_jco, ContactUsViewModel.WEB))
            it.add(ProfileMenu(R.drawable.ic_mail, R.string.email_jco, ContactUsViewModel.EMAIL))
            it.add(ProfileMenu(R.drawable.ic_call, R.string.number_jco, ContactUsViewModel.PHONE))
            it.add(ProfileMenuHeader("Social Media"))
            it.add(SocmedMenu(R.drawable.ic_facebook, "iamjcolovers", ContactUsViewModel.FACEBOOK))
            it.add(SocmedMenu(R.drawable.ic_instagram, "jcoindonesia", ContactUsViewModel.INSTAGRAM))
            it.add(SocmedMenu(R.drawable.ic_youtube, "J.CO Donuts & Coffee", ContactUsViewModel.YOUTUBE))
            datas.postValue(it)
        }
    }

    override fun onMenuClick(index: Int) {}

    override fun onSignOut() {}

    override fun onEditProfile() {}

    override fun onLinkAjaClick() {}

    override fun onSocialMediaClick(index: Int) {

    }
}