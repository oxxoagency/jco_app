package com.jcodonuts.app.ui.jcor.banner_detail

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.CartProductJcoR
import com.jcodonuts.app.data.remote.model.res.RelatedMenuRes
import com.jcodonuts.app.databinding.FragmentBannerDetailJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import de.mateware.snacky.Snacky
import javax.inject.Inject


class BannerDetailJcoRFragment @Inject constructor() :
    BaseFragment<FragmentBannerDetailJcoRBinding, BannerDetailJcoRViewModel>() {

    override fun getViewModelClass(): Class<BannerDetailJcoRViewModel> =
        BannerDetailJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_banner_detail_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadData(it1) }
            }
        }

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = BannerDetailJcoRController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText("Sukses ditambahkan ke keranjang")
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setActionText(getString(R.string.close))
                    .setActionTextSize(12f)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.jco_r_secondary
                        )
                    )
                    .success()
                    .show()
            }
        })

        viewModel.showMenu.observe(this, { showMenu ->
            viewModel.relaatedMenus.observe(this, { data ->
                showMenu.getContentIfNotHandled()?.let {
                    val dlg = DialogBannerDetailJcoR()
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        "BannerDetailFragment",
                        object : DialogBannerDetailJcoR.DialogBannerDetailListener {
                            override fun promoMenu(): List<RelatedMenuRes> {
                                return data
                            }

                            override fun getPromo(relatedMenuRes: RelatedMenuRes) {
                                viewModel.doInsertToCart(
                                    CartProductJcoR(
                                        menuCode = relatedMenuRes.menu_code?.toInt(),
                                        name = relatedMenuRes.menu_name,
                                        imgURL = relatedMenuRes.menu_image,
                                        price = relatedMenuRes.menu_price?.toDouble() ?: 0.0,
                                        productType = "",
                                        notes = "",
                                        qty = 1,
                                        priceOriginal = relatedMenuRes.menu_price?.toDouble() ?: 0.0
                                    )
                                )
                                Snacky.builder()
                                    .setActivity(activity)
                                    .setText("Sukses ditambahkan ke keranjang")
                                    .setDuration(Snacky.LENGTH_SHORT)
                                    .setBackgroundColor(
                                        ContextCompat.getColor(
                                            requireContext(),
                                            R.color.jco_r_secondary
                                        )
                                    )
                                    .success()
                                    .show()
                            }
                        }
                    )
                }
            })
        })
    }
}