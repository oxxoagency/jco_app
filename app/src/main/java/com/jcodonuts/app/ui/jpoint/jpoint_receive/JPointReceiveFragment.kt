package com.jcodonuts.app.ui.jpoint.jpoint_receive

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jcodonuts.app.data.remote.model.res.JPointRes
import com.jcodonuts.app.databinding.FragmentJPointReceiveBinding
import com.jcodonuts.app.ui.add_address.CityAdapter
import com.jcodonuts.app.utils.SharedPreference

class JPointReceiveFragment : Fragment() {

    private lateinit var binding: FragmentJPointReceiveBinding
    private val adapter by lazy { JPointReceiveAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        binding = FragmentJPointReceiveBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreference = SharedPreference(requireContext())
        if (sharedPreference.loadJPointReceived() != null) {
            adapter.setJPoints(sharedPreference.loadJPointReceived()?.data as ArrayList<JPointRes>)
        } else {
            binding.labelNotFound.visibility = View.VISIBLE
        }

        binding.apply {
            recyclerView.apply {
                adapter = this@JPointReceiveFragment.adapter
                layoutManager = LinearLayoutManager(context)
            }
        }
    }
}