package com.jcodonuts.app.ui.jcor.main.profile

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.repository.AuthRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.ui.main.profile.ProfileControllerListener
import com.jcodonuts.app.ui.main.profile.ProfileViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class ProfileJcoRViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val jcoDatabase: JcoDatabase
) : BaseViewModel(), ProfileControllerListener {

    val datas = MutableLiveData<MutableList<BaseCell>>()

    private val _showEditProfile = MutableLiveData<SingleEvents<String>>()
    val showEditProfile: LiveData<SingleEvents<String>>
        get() = _showEditProfile

    private val _showMenuDetail = MutableLiveData<SingleEvents<Int>>()
    val showMenuDetail: LiveData<SingleEvents<Int>>
        get() = _showMenuDetail

    private fun deleteAll(): Completable =
        jcoDatabase.cartDao().deleteAllJcoR()

    init {
        datas.value = mutableListOf()
    }

    fun loadData() {
        datas.value?.let {
            lastDisposable = authRepository.memberMe()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ model ->
                    if (model.status_code == 200) {
                        it.add(
                            ProfileHeader(
                                "https://drive.google.com/uc?id=1gtrQXUFKsrkexSwWA9eN3Mh2Xtau3S3p",
                                model.data.member_name.toString(),
                                "",
                                model.data.member_point.toString(),
                                model.data.member_phone ?: ""
                            )
                        )

                        it.add(ProfileMenuHeader("Account"))
                        it.add(
                            ProfileMenu(
                                R.drawable.ic_lock,
                                R.string.change_password,
                                ProfileViewModel.CHANGE_PASSWORD
                            )
                        )
//                        it.add(ProfileMenu(R.drawable.ic_orders, R.string.order, ProfileViewModel.ORDER))
                        it.add(
                            ProfileMenu(
                                R.drawable.ic_baseline_language,
                                R.string.language,
                                ProfileViewModel.LANGUAGE
                            )
                        )
                        it.add(ProfileMenu(R.drawable.ic_call, R.string.contact_us, ProfileViewModel.CONTACT_US))
                        it.add(ProfileMenuHeader("General"))
                        it.add(
                            ProfileMenu(
                                R.drawable.ic_term,
                                R.string.term_conditions,
                                ProfileViewModel.TERM_CONDITION
                            )
                        )
                        it.add(
                            ProfileMenu(
                                R.drawable.ic_privacy,
                                R.string.privacy_policy,
                                ProfileViewModel.PRIVACY_POLICY
                            )
                        )
                        it.add(ProfileFooter())
                    } else if (model.status_code == 401) {
                        EventBus.getDefault().post(LogoutEvent(model.error.toString()))
                    }
                    datas.postValue(it)

                }, { throwable ->
                    handleError(throwable)
                })

        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun showLoadingLogout(show: Boolean) {
        datas.value?.let {
            val data = (it[it.size - 1] as ProfileFooter).copy()
            data.showLoading = show
            it[it.size - 1] = data
            lastDisposable = deleteAll().subscribeOn(schedulers.io())
                .observeOn(schedulers.io())
                .subscribe({}, {})
            lastDisposable?.let { compositeDisposable.add(it) }
            datas.postValue(it)

        }
    }

    override fun onMenuClick(index: Int) {
        datas.value?.let {
            val data = it[index] as ProfileMenu
            _showMenuDetail.value = SingleEvents(data.type)
        }
    }

    override fun onSignOut() {
        showLoadingLogout(true)

        lastDisposable = authRepository.logout()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                EventBus.getDefault().post(LogoutEvent())
                sharedPreference.apply {
                    removeAddress()
                    removeCity()
                    removePickUp()
                }

            }, { e ->
                showLoadingLogout(false)
                handleError(e)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onEditProfile() {
        _showEditProfile.value = SingleEvents("edit_profile")
    }

    override fun onLinkAjaClick() {}

    override fun onSocialMediaClick(index: Int) {

    }

    companion object {
        const val CHANGE_PASSWORD = 1
        const val ORDER = 2
        const val LANGUAGE = 3
        const val CONTACT_US = 4
        const val TERM_CONDITION = 5
        const val PRIVACY_POLICY = 6
    }
}