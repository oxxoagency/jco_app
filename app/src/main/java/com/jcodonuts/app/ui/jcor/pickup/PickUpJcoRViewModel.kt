package com.jcodonuts.app.ui.jcor.pickup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.LocationSearch
import com.jcodonuts.app.data.local.PickupItem
import com.jcodonuts.app.data.remote.model.req.CityReq
import com.jcodonuts.app.data.remote.model.req.CitySearchReq
import com.jcodonuts.app.data.remote.model.req.OutletCityReq
import com.jcodonuts.app.data.remote.model.req.OutletReq
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.data.repository.AddAddressRepository
import com.jcodonuts.app.data.repository.PickupRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import java.math.RoundingMode
import javax.inject.Inject

class PickUpJcoRViewModel @Inject constructor(
    private val homeRepository: PickupRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val addAddressRepository: AddAddressRepository
) : BaseViewModel(), PickUpJcoRControllerListener {

    val datas = MutableLiveData<MutableList<BaseCell>>()

    private val _openToCart = MutableLiveData<SingleEvents<String>>()
    val openToCart: LiveData<SingleEvents<String>>
        get() = _openToCart

    private val _cities = MutableLiveData<List<CityDataRes>>()
    val cities: LiveData<List<CityDataRes>>
        get() = _cities

    private val _showCity = MutableLiveData<SingleEvents<String>>()
    val showCity: LiveData<SingleEvents<String>>
        get() = _showCity

    init {
        datas.value = mutableListOf()
    }

    fun loadLocations() {
        val body = OutletReq(2, sharedPreference.loadLatitude(), sharedPreference.loadLongitude(), 1)
        lastDisposable = homeRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                datas.value?.let {
                    it.add(LocationSearch("temp"))
                    if (data.status_code == 200) {
                        data.data.map { model ->
                            it.add(
                                PickupItem(
                                    _id = model.outlet_id.toString(),
                                    address = model.outlet_address.toString(),
                                    distance = "${
                                        model.distance?.toDouble()?.toBigDecimal()
                                            ?.setScale(1, RoundingMode.UP)
                                            ?.setScale(1, RoundingMode.UP)
                                    }Km",
                                    placeName = model.outlet_name,
                                    postCode = model.outlet_postcode.toString(),
                                    outletCode = model.outlet_code,
                                    city = model.outlet_city.toString(),
                                    latitude = model.outlet_latitude.toString(),
                                    longiutde = model.outlet_longitude.toString()
                                )
                            )
                        }

                    } else {
                        it.add(EmptyCart(data.error.toString()))
                    }
                    datas.postValue(it)
                }

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getCity() {
        val body = CityReq(2, "indonesia")
        lastDisposable = addAddressRepository.getCity(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _cities.postValue(it.data)
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getSearchCity(city: String?) {
        val body = CitySearchReq(2, "indonesia", city)
        lastDisposable = addAddressRepository.getCitySearch(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({
                _cities.postValue(it.data)
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getOutletByCity(city: String?) {
        datas.value?.let {
            val body = OutletCityReq(2, city)
            lastDisposable = homeRepository.getOutletByCity(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ res ->
                    it.removeAll { data -> data is PickupItem }
                    it.removeAll { data -> data is EmptyCart }
                    res.data.map { model ->
                        it.add(
                            PickupItem(
                                _id = model.outlet_id.toString(),
                                address = model.outlet_address.toString(),
                                distance = "${
                                    model.distance?.toDouble()?.toBigDecimal()
                                        ?.setScale(1, RoundingMode.UP)?.setScale(1, RoundingMode.UP)
                                }Km",
                                placeName = model.outlet_name,
                                postCode = model.outlet_postcode.toString(),
                                outletCode = model.outlet_code,
                                city = model.outlet_city.toString(),
                                latitude = model.outlet_latitude.toString(),
                                longiutde = model.outlet_longitude.toString()
                            )
                        )
                    }
                    datas.value = it
                }, {

                })
        }
    }

    override fun onClick(index: Int) {
        datas.value.let {
            val pickUp = (it?.get(index) as PickupItem).copy()
            sharedPreference.savePickUpJcoR(pickUp.placeName)
            sharedPreference.savePostCode(pickUp.postCode)
            sharedPreference.saveOutletCode(pickUp.outletCode)
            sharedPreference.saveOutletId(pickUp._id)
            sharedPreference.savePickUpAddress(pickUp.address)
            sharedPreference.savePickUpCity(pickUp.city)
            sharedPreference.saveSelectLatitudePickUp(pickUp.latitude)
            sharedPreference.saveSelectLongitudePickup(pickUp.longiutde)
            _openToCart.value = SingleEvents("openToCart")
        }
    }

    override fun onSearchClick() {
        _showCity.value = SingleEvents("show-city")
    }
}