package com.jcodonuts.app.ui.main.home

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.jcodonuts.app.BuildConfig
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.req.HomeReq
import com.jcodonuts.app.data.remote.model.req.OutletReq
import com.jcodonuts.app.data.remote.model.req.ProductByCategoryReq
import com.jcodonuts.app.data.remote.model.req.ProductFavoriteReq
import com.jcodonuts.app.data.remote.model.res.HomeRes
import com.jcodonuts.app.data.remote.model.res.Product
import com.jcodonuts.app.data.remote.model.res.UpdateVersionRes
import com.jcodonuts.app.data.remote.model.res.User
import com.jcodonuts.app.data.repository.AuthRepository
import com.jcodonuts.app.data.repository.DirectJcoRepository
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.data.repository.PickupRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val authRepository: AuthRepository,
    private val pickupRepository: PickupRepository,
    private val directJcoRepository: DirectJcoRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val app: Application,
    private val gson: Gson,
) : BaseViewModel(), HomeControllerListener {
    private val TAG = "HomeViewModel"

    val datas = MutableLiveData<MutableList<BaseCell>>()
    val menuSelected = MutableLiveData<String>()

    private var _promoSelected = MutableLiveData<SingleEvents<String>>()
    val promoSelected: LiveData<SingleEvents<String>>
        get() = _promoSelected

    private val _showDialogCannotOrder = MutableLiveData<SingleEvents<String>>()
    val showDialogCannotOrder: LiveData<SingleEvents<String>>
        get() = _showDialogCannotOrder

    private val _showLinkaja = MutableLiveData<SingleEvents<String>>()
    val showLinkaja: LiveData<SingleEvents<String>>
        get() = _showLinkaja

    private val _showQRcode = MutableLiveData<SingleEvents<String>>()
    val showQRcode: LiveData<SingleEvents<String>>
        get() = _showQRcode

    private val _showPickup = MutableLiveData<SingleEvents<String>>()
    val showPickup: LiveData<SingleEvents<String>>
        get() = _showPickup

    private val _showMenuSearch = MutableLiveData<SingleEvents<String>>()
    val showMenuSearch: LiveData<SingleEvents<String>>
        get() = _showMenuSearch

    private val _showChangeApp = MutableLiveData<SingleEvents<String>>()
    val showChangeApp: LiveData<SingleEvents<String>>
        get() = _showChangeApp

    private val _showHotPromo = MutableLiveData<SingleEvents<String>>()
    val showHotPromo: LiveData<SingleEvents<String>>
        get() = _showHotPromo

    private val _openProductDetail = MutableLiveData<SingleEvents<HomeMenuItem>>()
    val openProductDetail: LiveData<SingleEvents<HomeMenuItem>>
        get() = _openProductDetail

    private val _showNotification = MutableLiveData<SingleEvents<String>>()
    val showNotification: LiveData<SingleEvents<String>>
        get() = _showNotification

    private val _showUpdatePopUp = MutableLiveData<SingleEvents<String>>()
    val showUpdatePopUp: LiveData<SingleEvents<String>>
        get() = _showUpdatePopUp

    private val _showUpdatePopUpMandatory = MutableLiveData<SingleEvents<String>>()
    val showUpdatePopUpMandatory: LiveData<SingleEvents<String>>
        get() = _showUpdatePopUpMandatory

    private var _updateVersion = MutableLiveData<UpdateVersionRes>()
    val updateVersion: LiveData<UpdateVersionRes>
        get() = _updateVersion

    private val _showJPoint = MutableLiveData<SingleEvents<String>>()
    val showJPoint: LiveData<SingleEvents<String>>
        get() = _showJPoint

    private val _menus = mutableListOf<MenuCategory>()

    init {
        datas.value = mutableListOf()
        loadDataJPoint()
    }

    fun checkFromLogin(phone: String, latitude: String?, longitude: String?) {
        if (sharedPreference.getValueBoolean(SharedPreference.FROM_LOGIN, false)) {
            fetchHome(phone, latitude, longitude)
            sharedPreference.save(SharedPreference.FROM_LOGIN, false)
        }
    }

    private fun updateVersion() {
        lastDisposable = homeRepository.getUpdateNotification()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _updateVersion.value = it
            }, {

            })
    }

    fun fetchHome(phone: String, latitude: String?, longitude: String?) {
        val data = sharedPreference.getValueString(SharedPreference.DATA_HOME)
        if (data != null) {
            val model = gson.fromJson(data, HomeRes::class.java)
            parseFetchHome(model)
        } else {

            //add loading page
            val loading = mutableListOf<BaseCell>()
            loading.add(LoadingPage())
            datas.value = loading

            fetchHomeFromAPI()
        }
    }

    fun fetchHomeFromAPI() {

        datas.value.let {
            it?.add(LoadingPage())
            datas.value = it
        }

        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionCode
        val body = HomeReq(
            1,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber(),
            "android",
            pInfo.versionCode
        )
        lastDisposable = homeRepository.fetchHome(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                parseFetchHome(model)
                checkTokenExpired(model.user)
                sharedPreference.saveVersion(BuildConfig.VERSION_CODE)
                sharedPreference.saveVersion(model.version?.version_build_latest ?: 0)
                if (version < model.version?.version_build_latest ?: 0) {
                    if (model.version?.must_update == true) {
                        _showUpdatePopUpMandatory.value =
                            SingleEvents("show-update-pop-up-mandatory")
                        updateVersion()
                    } else {
                        _showUpdatePopUp.value = SingleEvents("show-update-pop-up")
                        updateVersion()
                    }

                }
                //save json to sharepreference
                val data = gson.toJson(model)
                sharedPreference.save(SharedPreference.DATA_HOME, data)

            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
        menuSelected.postValue("")

    }

    private fun getProductByCategory(category: String) {
        datas.value?.let {
            it.removeAll { data -> data is HomeMenuItem }
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            datas.value = it
        }

        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionCode
        val body = ProductByCategoryReq(
            category,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber() ?: "",
            1,
            "android",
            version
        )
        lastDisposable = homeRepository.getProductByCategory(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) {
                    val temp: MutableList<BaseCell> =
                        datas.value?.toMutableList() ?: mutableListOf()
                    temp.removeAll { it is LoadingProductGrid }

                    model.data.map {
                        temp.add(
                            HomeMenuItem(
                                localizationMenuName(it),
                                it.menu_image,
                                it.menu_price,
                                Converter.thousandSeparator(it.menu_price),
                                false,
                                it.is_promo == "1",
                                it.is_freedelivery == "1",
                                false,
                                it.menu_code,
                                Converter.thousandSeparator(it.menu_normalprice.toString())
                            )
                        )
                    }
                    datas.value = temp
                }
                Log.d("DATA__", gson.toJson(model))
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun parseFetchHome(model: HomeRes) {
        val temp = mutableListOf<BaseCell>()
        temp.removeAll { it is LoadingPage }
        if (isLoggedIn()) {
            temp.add(HomeHeadSection(model.user.member_name,
                Converter.thousandSeparator(model.jpoint?.point.toString()),
                model.jpoint?.point_tier?.name))
        } else {
            temp.add(Divider16())
            temp.add(Divider16())
            temp.add(Divider16())
        }

        temp.add(
            HomeSearchSection(
                "test",
                sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN) == null
            )
        )
        temp.add(HomePromoHeader("test"))
        val promos = mutableListOf<PromoBanner>()
        model.promos?.map {
            promos.add(PromoBanner(it.banner_id.toString(), it.banner_img.toString()))
        }
        temp.add(HomePromos(promos))

        temp.add(HomeMenuHeader(app.getString(R.string.what_are_you_looking_for)))

        val menus = mutableListOf<MenuCategory>()
        model.category.map {
            if (it.category_name == "all") {
                menus.add(
                    MenuCategory(
                        it.category_name, it.category_title, it.category_img,
                        true
                    )
                )
                menuSelected.postValue(it.category_name)
            } else {
                menus.add(
                    MenuCategory(
                        it.category_name, it.category_title, it.category_img,
                        false
                    )
                )
            }
        }

        temp.add(HomeMenuCategories(menus))

        model.products.map {
            temp.add(
                HomeMenuItem(
                    localizationMenuName(it),
                    it.menu_image,
                    it.menu_price,
                    Converter.thousandSeparator(it.menu_price),
                    false,
                    it.is_promo == "1",
                    it.is_freedelivery == "1",
                    it.is_favorite == "1",
                    it.menu_code,
                    Converter.thousandSeparator(it.menu_normalprice.toString())
                )
            )
        }
        if (model.user.status_code != 401) {
            sharedPreference.savePhoneNumber(model.user.member_phone)
            sharedPreference.savePoints(model.user.member_point)
            sharedPreference.saveEmail(model.user.member_email)
            sharedPreference.saveName(model.user.member_name)
            sharedPreference.saveJPoint(model.jpoint?.point.toString())
        }

        datas.value = temp
    }

    fun loadDataJPoint() {
        lastDisposable = directJcoRepository.getJPointMutation(

            pointStatus = 1,
            page = 1
        )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ jpoint ->
                sharedPreference.saveJPointReceived(jpoint)

            }, {
                handleError(it)

            })
        lastDisposable?.let { compositeDisposable.add(it) }

        lastDisposable = directJcoRepository.getJPointMutation(

            pointStatus = 2,
            page = 1
        )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ jpoint ->
                sharedPreference.saveJPointUsed(jpoint)

            }, {
                handleError(it)

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    fun loadLocation() {
        val body = OutletReq(1, sharedPreference.loadLatitude(), sharedPreference.loadLongitude())
        lastDisposable = pickupRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status_code == 200) {
                    sharedPreference.saveOutletCodeForDelivery(it.data[0].outlet_code)
                    sharedPreference.saveOutletIdForDelivery(it.data[0].outlet_id.toString())
                    sharedPreference.saveOrderPostCodeForDelivery(it.data[0].outlet_postcode.toString())
                }
            }, {

            })
    }

    private fun checkTokenExpired(user: User?) {
        if (user?.status_code == 401) {
            refreshToken()
        } else {
            sharedPreference.savePhoneNumber(
                user?.member_phone ?: sharedPreference.loadPhoneNumber()
            )
            sharedPreference.savePoints(user?.member_point)
            sharedPreference.saveEmail(user?.member_email)
            sharedPreference.saveName(user?.member_name)
        }
    }

    private fun refreshToken() {
        lastDisposable = authRepository.getRefreshToken()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status == 200) {
                    sharedPreference.save(SharedPreference.ACCESS_TOKEN, it.access_token)
                }
            }, {

            })
    }

    override fun onLinkajaClick() {
        _showLinkaja.value = SingleEvents("linkaja")
    }

    override fun onQrCodeClick() {
        _showQRcode.value = SingleEvents("qrcode")
    }

    override fun onPickupClick() {
        if (isLoggedIn()) {
            _showPickup.value = SingleEvents("show_pickup")
        } else {
            showDlgCannotOrder()
        }
    }

    override fun onSearchClick() {
        if (isLoggedIn()) {
            _showMenuSearch.value = SingleEvents("menu_search")
        } else {
            showDlgCannotOrder()
        }
    }

    override fun onSwitchAppClick() {
        _showChangeApp.value = SingleEvents("change_app")
    }

    override fun onBannerPromoClick(promoBanner: PromoBanner) {

    }

    override fun onPromoSeeAllClick() {
        if (isLoggedIn()) {
            _showHotPromo.value = SingleEvents("hot_promo")
        } else {
            showDlgCannotOrder()
        }
    }

    override fun onMenuCategoryClick(menuCategory: MenuCategory) {
        if (isLoggedIn()) {
            if (menuCategory.name != menuSelected.value) {
                menuSelected.postValue(menuCategory.name)
                if (menuCategory.name != "all") {
                    getProductByCategory(menuCategory.name)
                } else {
                    getProductByCategory("")
                }
            }
        } else {
            showDlgCannotOrder()
        }

    }

    override fun onMenuItemClick(index: Int) {
        if (isLoggedIn()) {
            datas.value?.let {
                _openProductDetail.value = SingleEvents(it[index] as HomeMenuItem)
            }
        } else {
            showDlgCannotOrder()
        }
    }

    override fun onMenuItemFavoriteClick(index: Int) {
        if (isLoggedIn()) {
            datas.value?.let {
                val temp = (it[index] as HomeMenuItem).copy()
                val favorite = if (temp.isFavorite) "0" else "1"
                val body = ProductFavoriteReq(
                    favorite,
                    sharedPreference.loadPhoneNumber().toString(),
                    temp.menuCode
                )
                lastDisposable = homeRepository.setProductFavorite(body)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe({ model ->
                        temp.isFavorite = !temp.isFavorite
                        it[index] = temp
                        datas.postValue(it)
                    }, {
                        handleError(it)
                    })

                lastDisposable?.let { compositeDisposable.add(it) }
            }
        } else {
            showDlgCannotOrder()
        }
    }

    override fun onPromosItemClick(promoBanner: PromoBanner) {
        if (isLoggedIn()) {
            datas.value?.let {
                _promoSelected.postValue(SingleEvents(promoBanner.menuCode ?: ""))
            }
        } else {
            showDlgCannotOrder()
        }
        Log.d("cekclickpromo", "cekclickpromo")
    }

    override fun onSearchItem(query: String) {}

    override fun onSearchMenuCategoryClick(menuSearchTagName: MenuSearchTagName, position: Int) {}

    override fun onNotificationClick() {
        _showNotification.value = SingleEvents("showNotification")
    }

    override fun onJPointClick() {
        _showJPoint.value =
            SingleEvents(sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN) ?: "")
    }

    private fun showDlgCannotOrder() {
        _showDialogCannotOrder.value = SingleEvents("_showDialogCannotOrder")
    }

    private fun isLoggedIn(): Boolean {
        val data = sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)
        return data != null && data != ""
    }
}