package com.jcodonuts.app.ui.jcor.voucher.claim_voucher

import android.net.Uri
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentClaimVoucherJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragmentWithoutBackPressDispatcher
import de.mateware.snacky.Snacky
import javax.inject.Inject


class ClaimVoucherJcoRFragment @Inject constructor() :
    BaseFragmentWithoutBackPressDispatcher<FragmentClaimVoucherJcoRBinding, ClaimVoucherJcoRViewModel>() {

    override fun getViewModelClass(): Class<ClaimVoucherJcoRViewModel> =
        ClaimVoucherJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_claim_voucher_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.topBar.btnBack.setOnClickListener {
            val url = getString(R.string.linkVoucherFragmentJcoR)
            val uri = Uri.parse(url)
            navigatePopupInclusiveTo(R.id.voucherFragmentJcoR, uri)
        }


        initRecyclerView()
        initObserver()
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val url = getString(R.string.linkVoucherFragmentJcoR)
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.voucherFragmentJcoR, uri)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun initRecyclerView() {
        val controller = ClaimVoucherJcoRController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.claimVoucher.observe(this, {
            it.getContentIfNotHandled()?.let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText(R.string.claimed)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_text_secondary
                        )
                    )
                    .success()
                    .show()
            }
        })
    }
}