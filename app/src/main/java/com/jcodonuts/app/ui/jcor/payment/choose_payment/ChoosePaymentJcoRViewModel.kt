package com.jcodonuts.app.ui.jcor.payment.choose_payment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.Payment
import com.jcodonuts.app.data.repository.AuthJcoRepository
import com.jcodonuts.app.data.repository.PaymentRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class ChoosePaymentJcoRViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val repository: AuthJcoRepository,
    private val schedulers: SchedulerProvider
) : BaseViewModel(), ChoosePaymentJcoRControllerListener {

    private val _datas = MutableLiveData<List<BaseCell>>()
    val datas: LiveData<List<BaseCell>>
        get() = _datas

    private val _openCart = MutableLiveData<SingleEvents<String>>()
    val openCart: LiveData<SingleEvents<String>>
        get() = _openCart

    fun loadData() {
        val temp = _datas.value?.toMutableList() ?: mutableListOf()
        lastDisposable = repository.paymentList("JID")
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                response.data.map { paymentList ->
                    temp.add(
                        Payment(
                            paymentMethod = paymentList.payment_method,
                            paymentIcon = paymentList.payment_icon,
                            paymentName = paymentList.payment_name
                        )
                    )
                }
                _datas.postValue(temp)
            }, {

            })

    }

    override fun onClick(index: Int) {
        _datas.value.let {
            val payment = it?.get(index) as Payment
            sharedPreference.savePaymentMethod(payment.paymentMethod.toString())
            sharedPreference.savePaymentName(payment.paymentName.toString())
            sharedPreference.savePaymentIcon(payment.paymentIcon.toString())
            _openCart.value = SingleEvents("open-cart")
        }
    }
}