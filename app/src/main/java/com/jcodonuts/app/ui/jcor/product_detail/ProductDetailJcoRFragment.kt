package com.jcodonuts.app.ui.jcor.product_detail

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.data.remote.model.res.MenuImage
import com.jcodonuts.app.databinding.FragmentProductDetailJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.product_detail.ProductSliderAdapter
import com.jcodonuts.app.utils.Converter
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import javax.inject.Inject


class ProductDetailJcoRFragment @Inject constructor() :
    BaseFragment<FragmentProductDetailJcoRBinding, ProductDetailJcoRViewModel>() {

    private val adapter by lazy { ProductSliderAdapter() }

    override fun getViewModelClass(): Class<ProductDetailJcoRViewModel> =
        ProductDetailJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_product_detail_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        binding.btnBack.setOnClickListener {
            onBackPress()
        }

        binding.viewModel = viewModel
        binding.executePendingBindings()

        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadDetail(it1) }
            }

        }

        binding.slider.apply {
            setSliderAdapter(adapter)
            startAutoCycle()
            setInfiniteAdapterEnabled(true)
            setIndicatorAnimation(IndicatorAnimationType.WORM)
            setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        }

        binding.home.setOnClickListener {
            val action = ProductDetailJcoRFragmentDirections.actionFromProductToMainFragment("")
            findNavController().navigate(
                action,
                FragmentNavigator.Extras.Builder()
                    .build()
            )
        }

        binding.cart.setOnClickListener {
            val action = ProductDetailJcoRFragmentDirections.actionFromProductToMainFragment("cart")
            findNavController().navigate(
                action,
                FragmentNavigator.Extras.Builder()
                    .build()
            )
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        viewModel.productDetail.observe(viewLifecycleOwner, { detail ->
            adapter.setSlider((detail.menu_image ?: mutableListOf()) as MutableList<MenuImage>)
            binding.slider.apply {
                setSliderAdapter(adapter)
                startAutoCycle()
                setInfiniteAdapterEnabled(true)
                setIndicatorAnimation(IndicatorAnimationType.WORM)
                setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            }
        })

        val controller = ProductDetailJcoRController(viewModel)
        binding.recyclerview.setController(controller)
        binding.recyclerview.itemAnimator = null

        viewModel.datas.observe(viewLifecycleOwner, {
            controller.data = it
        })

        viewModel.price.observe(viewLifecycleOwner, {
            if (!it.isNullOrEmpty()) {
                controller.price = Converter.thousandSeparator(it)
            }
        })
    }

    private fun initObserver() {
        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { _ ->
                val dlg = DialogAddToCartJcoR(requireContext())
                dlg.showPopup({
                    dlg.dismissPopup()
                    onBackPress()
                },
                    {
                        dlg.dismissPopup()
                        val action =
                            ProductDetailJcoRFragmentDirections.actionFromProductToMainFragment(
                                "cart")
                        findNavController().navigate(
                            action,
                            FragmentNavigator.Extras.Builder()
                                .build()
                        )
                    })
            }

        })

        viewModel.sizeCart.observe(viewLifecycleOwner, {
            binding.apply {
                if (it <= 0) {
                    bgQtyCart.visibility = View.GONE
                    qtyCart.visibility = View.GONE
                } else {
                    bgQtyCart.visibility = View.VISIBLE
                    qtyCart.visibility = View.VISIBLE
                    qtyCart.text = it.toString()
                }
            }
        })
    }
}