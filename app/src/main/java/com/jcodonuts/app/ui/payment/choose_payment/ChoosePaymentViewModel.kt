package com.jcodonuts.app.ui.payment.choose_payment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.req.PaymentListReq
import com.jcodonuts.app.data.repository.AuthJcoRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class ChoosePaymentViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val repository: AuthJcoRepository,
    private val schedulers: SchedulerProvider,
) :
    BaseViewModel(), ChoosePaymentControllerListener {

    private val _datas = MutableLiveData<List<BaseCell>>()
    val datas: LiveData<List<BaseCell>>
        get() = _datas

    private val _paymentClick = MutableLiveData<SingleEvents<ChoosePaymentData>>()
    val paymentClick: LiveData<SingleEvents<ChoosePaymentData>>
        get() = _paymentClick

    private val _openCart = MutableLiveData<SingleEvents<String>>()
    val openCart: LiveData<SingleEvents<String>>
        get() = _openCart

    fun loadData() {
        val temp = _datas.value?.toMutableList() ?: mutableListOf()
        lastDisposable = repository.paymentList("JID")
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                response.data.map { paymentList ->
                    temp.add(
                        Payment(
                            paymentMethod = paymentList.payment_method,
                            paymentIcon = paymentList.payment_icon,
                            paymentName = paymentList.payment_name
                        )
                    )
                }
                _datas.postValue(temp)
            }, {

            })

    }

    override fun onClickEWallet(index: Int) {
        _datas.value.let {
            val payment = it?.get(index) as EWallet
            sharedPreference.savePaymentMethod(payment.payment.toString())
            sharedPreference.savePaymentName(payment.name.toString())
            _openCart.value = SingleEvents("open-cart")
        }
    }

    override fun onClickTransferVirtualAccount(index: Int) {
        _datas.value.let {
            val payment = it?.get(index) as TransferVirtualAccount
            sharedPreference.savePaymentMethod(payment.payment.toString())
            sharedPreference.savePaymentName(payment.name.toString())
            _openCart.value = SingleEvents("open-cart")
        }
    }

    override fun onClickOtherPayment(index: Int) {
        _datas.value.let {
            val payment = it?.get(index) as OtherPayment
            sharedPreference.savePaymentMethod(payment.payment.toString())
            sharedPreference.savePaymentName(payment.name.toString())
            _openCart.value = SingleEvents("open-cart")
        }
    }

    override fun onClick(index: Int) {
        _datas.value.let {
            val payment = it?.get(index) as Payment
            sharedPreference.savePaymentMethod(payment.paymentMethod.toString())
            sharedPreference.savePaymentName(payment.paymentName.toString())
            sharedPreference.savePaymentIcon(payment.paymentIcon.toString())
            sharedPreference.saveRefreshStateCartDetail(true)
            _openCart.value = SingleEvents("open-cart")
        }
    }
}