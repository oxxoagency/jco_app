package com.jcodonuts.app.ui.main.recent_order

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.RecentOrderReq
import com.jcodonuts.app.data.remote.model.res.ProductDetail
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class RecentOrderViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val jcoDatabase: JcoDatabase,
    private val app: Application
) : BaseViewModel(), RecentOrderListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _showDetail = MutableLiveData<SingleEvents<RecentOrder>>()
    val showDetail: LiveData<SingleEvents<RecentOrder>>
        get() = _showDetail

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private fun insertToCart(cartProduct: CartProduct): Completable =
        jcoDatabase.cartDao().insertCart(cartProduct)

    private fun insertToCartPickUp(cartProduct: CartProductPickUp): Completable =
        jcoDatabase.cartDao().insertCartPickUp(cartProduct)

    private fun doInsertToCart(cartProduct: CartProduct) {
        compositeDisposable.add(
            insertToCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    private fun doInsertToCartPickUp(cartProduct: CartProductPickUp) {
        compositeDisposable.add(
            insertToCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun loadData() {
        val temp = mutableListOf<BaseCell>()
        temp.add(LoadingPage())
        _datas.value = temp
        sharedPreference.removeCodeVoucher()
        sharedPreference.removeVoucher()
        val body = RecentOrderReq(1, sharedPreference.loadEmail(), "Jakarta Barat")
        lastDisposable = homeRepository.getRecentOrder(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                temp.removeAll { it is LoadingPage }
                if (response.data.isNullOrEmpty()) {
                    temp.add(EmptyCart(app.getString(R.string.you_havent_ordered)))
                }


                response.data?.map { product ->
                    temp.add(
                        RecentOrder(
                            orderId = product.order_id,
                            outletName = product.order_outlet_code,
                            menuImage = product.menu_img,
                            orderTime = product.order_time,
                            orderDetail = product.order_detail,
                            price = Converter.rupiah(product.order_total.toString()),
                            menuName = product.menu_list,
                            orderStatus = product.order_status,
                            totalOrder = product.order_total_item,
                            orderPaymentStatus = product.order_payment_status
                        )
                    )

                }
                _datas.postValue(temp)
                Log.d("cekrecentorder", datas.toString())

            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }


    private fun localizationMenuName(product: ProductDetail): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    override fun onRecentOrderClick(position: Int) {
        _datas.value.let {
            _showDetail.value = SingleEvents(it?.get(position) as RecentOrder)
        }
    }

    override fun onReOrderClick(position: Int) {
        _datas.value.let {
            val recentOrder = (it?.get(position) as RecentOrder).copy()
            recentOrder.orderDetail?.map { detail ->
                if (detail.menu_name?.contains("eco bag", true) == false) {
                    doInsertToCart(
                        CartProduct(
                            menuCode = detail.menu_code?.toInt(),
                            name = detail.menu_name,
                            imgURL = detail.menu_image[0].image,
                            price = detail.menu_price?.toDouble() ?: 0.0,
                            productType = "",
                            notes = "",
                            qty = detail.menu_quantity?.toInt() ?: 0,
                            priceOriginal = detail.menu_unitprice?.toDouble() ?: 0.0,
                            detailVariant = mutableListOf()
                        )
                    )
                    doInsertToCartPickUp(
                        CartProductPickUp(
                            menuCode = detail.menu_code?.toInt(),
                            name = detail.menu_name,
                            imgURL = detail.menu_image[0].image,
                            price = detail.menu_price?.toDouble() ?: 0.0,
                            productType = "",
                            notes = "",
                            qty = detail.menu_quantity?.toInt() ?: 0,
                            priceOriginal = detail.menu_unitprice?.toDouble() ?: 0.0,
                            detailVariant = mutableListOf()
                        )
                    )
                }
            }
            _addToCart.value = SingleEvents("add-to-cart")
            _datas.value = it
        }
    }
}