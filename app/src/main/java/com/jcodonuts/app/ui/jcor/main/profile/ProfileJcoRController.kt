package com.jcodonuts.app.ui.jcor.main.profile

import com.airbnb.epoxy.EpoxyAsyncUtil
import com.airbnb.epoxy.TypedEpoxyController
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.ui.main.profile.ProfileControllerListener

class ProfileJcoRController(
    private val listener: ProfileControllerListener
) : TypedEpoxyController<List<BaseCell>>(
    EpoxyAsyncUtil.getAsyncBackgroundHandler(),
    EpoxyAsyncUtil.getAsyncBackgroundHandler()
) {

    override fun buildModels(data: List<BaseCell>?) {
        data?.forEachIndexed { index, cellData ->
            when (cellData) {
                is ProfileHeader -> addProfileHeader(cellData, listener)
                is ProfileMenuHeader -> addProfileMenuHeader(cellData)
                is ProfileMenu -> addProfileMenu(cellData, index, listener)
                is ProfileFooter -> addProfileFooter(cellData, listener)
            }
        }
    }

    private fun addProfileHeader(cellData: ProfileHeader, listener: ProfileControllerListener) {
        profileHeaderJcoR {
            id("switch_cart")
            data(cellData)
            listener(listener)
        }
    }

    private fun addProfileMenuHeader(cellData:ProfileMenuHeader){
        profileMenuHeaderJcoR {
            id("delivery_address")
            data(cellData)
        }
    }

    private fun addProfileMenu(cellData:ProfileMenu, index:Int, listener:ProfileControllerListener){
        profileMenuJcoR {
            id("pickup_address")
            data(cellData)
            index(index)
            listener(listener)
        }
    }


    private fun addProfileFooter(cellData:ProfileFooter, listener:ProfileControllerListener){
        profileFooterJcoR {
            id("profile_footer")
            data(cellData)
            listener(listener)
        }
    }
}