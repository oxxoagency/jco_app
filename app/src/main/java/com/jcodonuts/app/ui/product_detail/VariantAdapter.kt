package com.jcodonuts.app.ui.product_detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jcodonuts.app.data.remote.model.res.DetailVariant
import com.jcodonuts.app.databinding.ItemVariantBinding

class VariantAdapter : RecyclerView.Adapter<VariantAdapter.VariantViewHolder>() {

    private var detailVariant = emptyList<DetailVariant>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VariantAdapter.VariantViewHolder {
        return VariantViewHolder(
            ItemVariantBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = detailVariant.size

    override fun onBindViewHolder(holder: VariantViewHolder, position: Int) {
        val current = detailVariant[position]
        holder.apply {
            bind(current)
            val childLayoutManager =
                LinearLayoutManager(holder.recyclerView.context, RecyclerView.VERTICAL, false)
            recyclerView.apply {
                layoutManager = childLayoutManager
                adapter = DonutAdapter(current.package_donut_list ?: emptyList())
            }
        }
    }

    inner class VariantViewHolder(private val binding: ItemVariantBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(detailVariant: DetailVariant) {
            binding.apply {
                data = detailVariant
                variant.text = detailVariant.package_name
                expandMore.setOnClickListener {
                    if (recyclerViewDonut.visibility == View.GONE) recyclerViewDonut.visibility =
                        View.VISIBLE
                    else recyclerViewDonut.visibility = View.GONE
                }
                executePendingBindings()
            }
        }

        val recyclerView = binding.recyclerViewDonut
    }

    internal fun setVariant(detailVariant: List<DetailVariant>) {
        this.detailVariant = detailVariant
        notifyDataSetChanged()
    }
}
