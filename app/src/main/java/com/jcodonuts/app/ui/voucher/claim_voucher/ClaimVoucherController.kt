package com.jcodonuts.app.ui.voucher.claim_voucher

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.cartEmpty
import com.jcodonuts.app.claimVoucherItems
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.Voucher

class ClaimVoucherController(private val listener: ClaimVoucherControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is Voucher -> addCouponList(cellData, index, listener)
                is EmptyCart -> addEmptyCart(cellData)
            }
        }
    }

    private fun addCouponList(
        cellData: Voucher,
        index: Int,
        listener: ClaimVoucherControllerListener
    ) {
        claimVoucherItems {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart){
        cartEmpty {
            id("empty-cart")
            data(cellData)
        }
    }
}