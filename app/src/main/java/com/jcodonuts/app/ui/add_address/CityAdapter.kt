package com.jcodonuts.app.ui.add_address

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.databinding.ItemCityBinding

class CityAdapter(val data: (CityDataRes) -> Unit) :
    RecyclerView.Adapter<CityAdapter.CityViewHolder>() {

    private var cities = ArrayList<CityDataRes>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityAdapter.CityViewHolder {
        return CityViewHolder(ItemCityBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: CityAdapter.CityViewHolder, position: Int) {
        val city = cities[position]
        holder.bind(city)
    }

    override fun getItemCount(): Int = cities.size

    inner class CityViewHolder(private val binding: ItemCityBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(city: CityDataRes) {
            binding.apply {
                data = city
                executePendingBindings()
            }
            itemView.setOnClickListener { data(city) }
        }
    }

    internal fun setCities(cities: ArrayList<CityDataRes>) {
        this.cities = cities
        notifyDataSetChanged()
    }

    internal fun setSearchCities(cities: ArrayList<CityDataRes>) {
        this.cities.clear()
        this.cities.addAll(cities)
        notifyDataSetChanged()
    }

    internal fun deleteCities() {
        this.cities.clear()
        notifyDataSetChanged()
    }
}