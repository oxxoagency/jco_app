package com.jcodonuts.app.ui.jcor.delivery

interface DeliveryJcoRControllerListener {
    fun onClick(index: Int)
    fun onFavoriteClick(index: Int)
    fun onClickUpdateAddress(index: Int)
    fun onClickAddAddress()
    fun onClickSaveDeliveryAddress(index: Int)
    fun onDeleteAddress(index: Int)
}