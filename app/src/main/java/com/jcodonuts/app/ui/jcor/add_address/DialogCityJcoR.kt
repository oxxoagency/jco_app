package com.jcodonuts.app.ui.jcor.add_address

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jcodonuts.app.R
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.databinding.DlgCityJcoRBinding
import com.jcodonuts.app.utils.KeyboardUtil
import com.jcodonuts.app.utils.SharedPreference
import javax.inject.Inject

class DialogCityJcoR @Inject constructor() : BottomSheetDialogFragment() {

    private lateinit var binding: DlgCityJcoRBinding
    private lateinit var listener: DialogCityListener
    private val adapter by lazy { CityJcoRAdapter { saveCity(it) } }

    @Inject
    lateinit var sharedPreference: SharedPreference

    override fun getTheme(): Int {
        return R.style.DialogFullWidth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DlgCityJcoRBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        KeyboardUtil(requireActivity(), binding.root)
        isCancelable = false
        sharedPreference = SharedPreference(requireContext())

        val searchStream = RxTextView.textChanges(binding.searchLocation)
            .map {
                it.toString()
            }

        searchStream.subscribe { query ->
            if (!listener.searchCity(query).isNullOrEmpty()) {
                adapter.setSearchCities(listener.searchCity(query) as ArrayList<CityDataRes>)
            }
        }


        adapter.setCities(listener.getCity() as ArrayList<CityDataRes>)
        binding.apply {
            btnDlgClose.setOnClickListener { dismissDialog() }
            recyclerViewCity.apply {
                adapter = this@DialogCityJcoR.adapter
                layoutManager = LinearLayoutManager(context)
            }
        }
    }

    private fun saveCity(city: CityDataRes) {
        listener.saveCity(city)
        dismissDialog()
    }

    fun showDialog(
        fragmentManager: FragmentManager,
        tag: String?,
        listener: DialogCityListener
    ) {
        if (fragmentManager.findFragmentByTag(tag)?.isAdded == true) {
            return
        }
        show(fragmentManager, tag)
        this.listener = listener

    }

    fun dismissDialog() {
        dialog?.cancel()
    }

    interface DialogCityListener {
        fun getCity(): List<CityDataRes>
        fun saveCity(city: CityDataRes): String
        fun searchCity(city: String): List<CityDataRes>
    }
}