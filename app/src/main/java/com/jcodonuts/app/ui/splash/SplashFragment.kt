package com.jcodonuts.app.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentSplashBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.fcm.PromoActivity
import com.jcodonuts.app.ui.fcm.WebViewJPointActivity
import javax.inject.Inject

class SplashFragment @Inject constructor() :
    BaseFragment<FragmentSplashBinding, SplashViewModel>() {

    override fun getViewModelClass(): Class<SplashViewModel> {
        return SplashViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_splash
    }

    override fun onViewReady(savedInstance: Bundle?) {
        viewModel.loadData()
        viewModel.setCleanDB()
        viewModel.complete.observe(this, Observer {
            var idBanner = activity?.intent?.getStringExtra("id") ?: ""
            if (idBanner.isEmpty() || idBanner.equals("home", ignoreCase = true)) {
                val action = SplashFragmentDirections.actionToMainFragment("")
                findNavController().navigate(
                    action,
                    FragmentNavigator.Extras.Builder()
                        .build()
                )
            } else if (idBanner.equals("loyalty", ignoreCase = true)) {
                val intent = Intent(activity, WebViewJPointActivity::class.java)
                activity?.finish()
                startActivity(intent)
            } else {
                val intent = Intent(activity, PromoActivity::class.java)
                intent.putExtra("id", idBanner)
                activity?.finish()
                startActivity(intent)
            }
        })
    }
}