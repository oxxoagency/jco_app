package com.jcodonuts.app.ui.location

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.jcodonuts.app.databinding.ItemLocationBinding
import java.math.RoundingMode

class LocationAdapter(val data: (AutocompletePrediction) -> Unit) :
    RecyclerView.Adapter<LocationAdapter.ProgramViewHolder>() {

    private var program = emptyList<AutocompletePrediction>()

    private var selectedItemPosition: Int? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LocationAdapter.ProgramViewHolder {
        return ProgramViewHolder(ItemLocationBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun getItemCount(): Int = program.size

    override fun onBindViewHolder(holder: LocationAdapter.ProgramViewHolder, position: Int) {
        val current = program[position]
        holder.apply {
            bind(current, position)
//            itemView.setOnClickListener {
//                selectedItemPosition = position
//                notifyDataSetChanged()
//            }
        }
    }

    inner class ProgramViewHolder(private val binding: ItemLocationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(autocompletePrediction: AutocompletePrediction, position: Int) {
            binding.apply {
                data = autocompletePrediction
                locationPrimary.text = autocompletePrediction.getPrimaryText(null)
                locationSecondary.text = autocompletePrediction.getSecondaryText(null)
                if (autocompletePrediction.distanceMeters != null) {
                    distance.text =
                        "${
                            autocompletePrediction.distanceMeters?.toDouble()?.div(1000.0)
                                ?.toBigDecimal()?.setScale(1, RoundingMode.UP)
                        }Km"
                }
                if (position == selectedItemPosition) {
                    locationPrimary.setTextColor(Color.parseColor("#F25A17"))
                    locationSecondary.setTextColor(Color.parseColor("#F25A17"))
                    distance.setTextColor(Color.parseColor("#F25A17"))
                    data(autocompletePrediction)
                } else {
                    locationPrimary.setTextColor(Color.parseColor("#000000"))
                    locationSecondary.setTextColor(Color.parseColor("#000000"))
                    distance.setTextColor(Color.parseColor("#8E8E8E"))
                }
                executePendingBindings()
            }
            itemView.setOnClickListener{data(autocompletePrediction)}
        }
    }

    internal fun setPrograms(programs: List<AutocompletePrediction>) {
        this.program = programs
        notifyDataSetChanged()
    }
}
