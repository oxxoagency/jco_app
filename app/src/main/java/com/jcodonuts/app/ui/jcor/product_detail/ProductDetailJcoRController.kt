package com.jcodonuts.app.ui.jcor.product_detail

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.ProductDetailContent
import com.jcodonuts.app.productDetailContentsJcoR

class ProductDetailJcoRController(private val listener: ProductDetailJcoRControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var price = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is ProductDetailContent -> addProductDetailContent(cellData, listener, index)
            }
        }
    }

    private fun addProductDetailContent(
        cellData: ProductDetailContent,
        listener: ProductDetailJcoRControllerListener,
        index: Int
    ) {
        productDetailContentsJcoR {
            id("content")
            data(cellData)
            onClickListener(listener)
            index(index)
            price(price)
            spanSizeOverride { _, _, _ -> 4 }
        }
    }
}