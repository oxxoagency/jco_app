package com.jcodonuts.app.ui.map

import com.jcodonuts.app.data.remote.model.req.OutletReq
import com.jcodonuts.app.data.repository.PickupRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import javax.inject.Inject

class MapViewModel @Inject constructor(
    private val pickupRepository: PickupRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : BaseViewModel() {

    fun loadLocation(lat: String, lng: String) {
        val body = OutletReq(1, lat, lng)
        lastDisposable = pickupRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status_code == 200) {
                    sharedPreference.saveCity(it.data[0].outlet_city.toString())
                }
            }, {

            })
    }
}