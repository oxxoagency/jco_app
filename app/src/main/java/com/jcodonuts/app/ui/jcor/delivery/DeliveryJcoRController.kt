package com.jcodonuts.app.ui.jcor.delivery

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.addressAddSavePlaceJcoR
import com.jcodonuts.app.addressItemJcoR
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.deliveryItemJcoR

class DeliveryJcoRController(private val listener: DeliveryJcoRControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is MemberAddressItem -> addDeliveryItem(cellData, listener, index)
                is AddressItem -> addAddressItem(cellData, listener, index)
                is SavePlace -> savePlace(listener)
            }
        }
    }

    private fun addDeliveryItem(
        cellData: MemberAddressItem,
        listener: DeliveryJcoRControllerListener,
        index: Int
    ) {
        deliveryItemJcoR {
            id(cellData._id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addAddressItem(
        cellData: AddressItem,
        listener: DeliveryJcoRControllerListener,
        index: Int
    ) {
        addressItemJcoR {
            id(cellData.id)
            data(cellData)
            listener(listener)
            index(index)
        }
    }

    private fun savePlace(listener: DeliveryJcoRControllerListener) {
        addressAddSavePlaceJcoR {
            id("add-save-place")
            listener(listener)
        }
    }
}