package com.jcodonuts.app.ui.delivery

import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.EpoxyController
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*

/**
 * Showcases [EpoxyController] with sticky header support
 */
class DeliveryController(
    private val listener: DeliveryControllerListener
) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is LocationSearch -> addLocationSearch()
                is MemberAddressItem -> addDeliveryItem(cellData, listener, index)
                is DeliveryWebView -> addDeliveryWebview(cellData)
                is AddressItem -> addAddressItem(cellData, listener, index)
                is SavePlace -> savePlace(listener)
            }
        }
    }

    private fun addLocationSearch() {
        locationSearchview {
            id("locationSearch")
        }
    }

    private fun addDeliveryItem(
        cellData: MemberAddressItem,
        listener: DeliveryControllerListener,
        index: Int
    ) {
        deliveryItem {
            id(cellData._id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addDeliveryWebview(cellData: DeliveryWebView) {
        deliveryWebview {
            id("https://google.com")
            wv(cellData)
        }
    }

    private fun addAddressItem(
        cellData: AddressItem,
        listener: DeliveryControllerListener,
        index: Int
    ) {
        addressItem {
            id(cellData.id)
            data(cellData)
            listener(listener)
            index(index)
        }
    }

    private fun savePlace(listener: DeliveryControllerListener) {
        addressAddSavePlace {
            id("add-save-place")
            listener(listener)
        }
    }

//    private fun addDeliveryItemz(cellData: MemberAddressItem, listener: DeliveryControllerListener, index:Int){
//        deliveryItemz {
//            id(cellData._id)
//            data(cellData)
//            index(index)
//            listener(listener)
//        }
//    }

}
