package com.jcodonuts.app.ui.main.wishlist

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.req.ProductFavReq
import com.jcodonuts.app.data.remote.model.req.ProductFavoriteReq
import com.jcodonuts.app.data.remote.model.res.ProductFavorite
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.ui.main.home.HomeControllerListener
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class WishlistViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : HomeControllerListener, BaseViewModel() {

    val datas = MutableLiveData<MutableList<BaseCell>>()

    private val _whistList = MutableLiveData<List<ProductFavorite>>()
    val whistList: LiveData<List<ProductFavorite>>
        get() = _whistList

    private val _openProductDetail = MutableLiveData<SingleEvents<HomeMenuItem>>()
    val openProductDetail: LiveData<SingleEvents<HomeMenuItem>>
        get() = _openProductDetail

    init {
        datas.value = mutableListOf()
    }

    @SuppressLint("CheckResult")
    val body = ProductFavReq(sharedPreference.loadPhoneNumber().toString(), "Jakarta Barat",1)
    fun loadLocations() {
        lastDisposable = homeRepository.getProductFavorite(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                _whistList.postValue(data.data)
                data.data.map { model ->
                    datas.value?.let {
                        it.add(
                            HomeMenuItem(
                                name = localizationMenuName(model),
                                imgURL = model.menu_image.toString(),
                                price = model.menu_price.toString(),
                                priceText = Converter.rupiah(model.menu_price.toString()),
                                isStartFrom = false,
                                isPromo = model.is_promo == "1",
                                isFreeDelivery = model.is_freedelivery == "1",
                                isFavorite = model.is_favorite == 1,
                                menuCode = model.menu_code.toString(),
                                normalPrice = Converter.thousandSeparator(model.menu_normalprice.toString())
                            )
                        )
                        datas.postValue(it)
                    }
                }

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationMenuName(product: ProductFavorite): String =
        if (sharedPreference.loadLanguage()
                ?.contains("indonesia", true) == true
        ) product.menu_name.toString()
        else product.menu_name_en.toString()

    override fun onLinkajaClick() {

    }

    override fun onQrCodeClick() {

    }

    override fun onPickupClick() {

    }

    override fun onSearchClick() {

    }

    override fun onSwitchAppClick() {

    }

    override fun onBannerPromoClick(promoBanner: PromoBanner) {

    }

    override fun onPromoSeeAllClick() {

    }

    override fun onMenuCategoryClick(menuCategory: MenuCategory) {

    }

    override fun onMenuItemClick(index: Int) {
        datas.value?.let {
            _openProductDetail.value = SingleEvents(it[index] as HomeMenuItem)
        }
    }

    override fun onMenuItemFavoriteClick(index: Int) {
        datas.value?.let { data ->
            val temp = (data[index] as HomeMenuItem).copy()
            val favorite = if (temp.isFavorite) "0" else "1"
            val body = ProductFavoriteReq(
                favorite,
                sharedPreference.loadPhoneNumber().toString(),
                temp.menuCode
            )
            lastDisposable = homeRepository.setProductFavorite(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    temp.isFavorite = !temp.isFavorite
                    data[index] = temp
                    data.removeAt(index)
                    datas.postValue(data)
                }, {

                })

            lastDisposable?.let { compositeDisposable.add(it) }
        }
    }

    override fun onPromosItemClick(promoBanner: PromoBanner) {

    }

    override fun onSearchItem(query: String) {

    }

    override fun onSearchMenuCategoryClick(menuSearchTagName: MenuSearchTagName, position: Int) {}

    override fun onNotificationClick() {}

    override fun onJPointClick() {

    }
}