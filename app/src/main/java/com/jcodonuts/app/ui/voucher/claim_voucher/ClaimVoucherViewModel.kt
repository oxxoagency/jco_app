package com.jcodonuts.app.ui.voucher.claim_voucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.Voucher
import com.jcodonuts.app.data.remote.model.req.CouponClaimReq
import com.jcodonuts.app.data.remote.model.req.CouponReq
import com.jcodonuts.app.data.repository.CouponRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import com.jcodonuts.app.utils.parseDateWithFormat
import javax.inject.Inject

class ClaimVoucherViewModel @Inject constructor(
    private val couponRepository: CouponRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : BaseViewModel(), ClaimVoucherControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _claimVoucher = MutableLiveData<SingleEvents<String>>()
    val claimVoucher: LiveData<SingleEvents<String>>
        get() = _claimVoucher

    fun loadData() {
        val body = CouponReq(1, "2", "1")
        lastDisposable = couponRepository.getCouponList(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                val data = it.data
                val temp = mutableListOf<BaseCell>()
                if (it.status_code == 200) {
                    data.map { coupon ->
                        temp.add(
                            Voucher(
                                id = coupon.coupon_id,
                                name = coupon.coupon_name,
                                image = coupon.image,
                                date = "${coupon.start_time?.parseDateWithFormat("dd MMM yyyy")} - ${
                                    coupon.end_time?.parseDateWithFormat("dd MMM yyyy")
                                }",
                                description = coupon.description?.replace("\\n", "\n"),
                                code = coupon.code,
                                isClaim = coupon.is_claim,
                                isUsed = coupon.is_used
                            )
                        )
                    }
                } else {
                    temp.add(EmptyCart(it.error.toString()))
                }
                _datas.postValue(temp)
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onClaimClick(position: Int) {
        _datas.value.let {
            val coupon = (it?.get(position) as Voucher)

            val body = CouponClaimReq(1, coupon.code, sharedPreference.loadPhoneNumber())
            lastDisposable = couponRepository.claimCoupon(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    if (coupon.isClaim == 0) {
                        _claimVoucher.value = SingleEvents("claim-voucher")
                    }
                }, {
                    handleError(it)
                })

        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }
}