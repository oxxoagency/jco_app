package com.jcodonuts.app.ui.jcor.voucher.claim_voucher

interface ClaimVoucherJcoRControllerListener {
    fun onClaimClick(position: Int)
}