package com.jcodonuts.app.ui.jcor.map

import android.location.Geocoder
import android.os.Bundle
import androidx.core.os.bundleOf
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentMapJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject


class MapJcoRFragment @Inject constructor() :
    BaseFragment<FragmentMapJcoRBinding, MapJcoRViewModel>(),
    OnMapReadyCallback {

    override fun getViewModelClass(): Class<MapJcoRViewModel> = MapJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_map_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        val position = LatLng(
            sharedPreference.loadLatitude()?.toDouble() ?: 0.0,
            sharedPreference.loadLongitude()?.toDouble() ?: 0.0
        )

        val geocoder = Geocoder(context)
        var address = geocoder.getFromLocation(
            sharedPreference.loadLatitude()?.toDouble() ?: 0.0,
            sharedPreference.loadLongitude()?.toDouble() ?: 0.0, 1
        )

        googleMap?.addMarker(
            MarkerOptions()
                .position(position)
                .draggable(true)
                .title(address[0].getAddressLine(0))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_jco_r))
        )

        binding.btnSave.setOnClickListener {
            parentFragmentManager.setFragmentResult(
                "address", // Same request key FragmentA used to register its listener
                bundleOf("address" to address[0].getAddressLine(0)) // The data to be passed to FragmentA
            )
            parentFragmentManager.popBackStack()
        }

        googleMap?.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    sharedPreference.loadLatitude()?.toDouble() ?: 0.0,
                    sharedPreference.loadLongitude()?.toDouble() ?: 0.0
                ), 17f
            )
        )

        googleMap?.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(p0: Marker?) {
                address = geocoder.getFromLocation(
                    p0?.position?.latitude ?: 0.0,
                    p0?.position?.longitude ?: 0.0,
                    1
                )
                p0?.title = address[0].getAddressLine(0)
                onAddressSave(p0)
            }

            override fun onMarkerDrag(p0: Marker?) {
                address = geocoder.getFromLocation(
                    p0?.position?.latitude ?: 0.0,
                    p0?.position?.longitude ?: 0.0,
                    1
                )
                p0?.title = address[0].getAddressLine(0)
                onAddressSave(p0)
            }

            override fun onMarkerDragEnd(p0: Marker?) {
                address = geocoder.getFromLocation(
                    p0?.position?.latitude ?: 0.0,
                    p0?.position?.longitude ?: 0.0,
                    1
                )
                p0?.title = address[0].getAddressLine(0)
                onAddressSave(p0)
            }
        })
    }

    private fun onAddressSave(marker: Marker?) {
        binding.btnSave.setOnClickListener {
            parentFragmentManager.setFragmentResult(
                "address", // Same request key FragmentA used to register its listener
                bundleOf("address" to marker?.title)
            )
            parentFragmentManager.popBackStack()
        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }
}