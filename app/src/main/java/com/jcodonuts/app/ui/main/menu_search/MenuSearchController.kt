package com.jcodonuts.app.ui.main.menu_search

import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.EpoxyController
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.homeMenuItem
import com.jcodonuts.app.lytLoadingProductGrid
import com.jcodonuts.app.menuSearchTagview
import com.jcodonuts.app.searchview
import com.jcodonuts.app.ui.main.home.HomeControllerListener

/**
 * Showcases [EpoxyController] with sticky header support
 */
class MenuSearchController(
    private val listener: HomeControllerListener
) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var menuSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when(cellData) {
                is CommonSearch -> addCommonSearch(listener)
                is MenuSearchTagName -> addMenuSearchTagName(cellData, index, listener)
                is HomeMenuItem -> addHomeMenuItem(cellData, listener, index)
                is LoadingProductGrid -> addLoadingProductGrid(cellData)
            }
        }
    }

    private fun addHomeMenuItem(cellData: HomeMenuItem, listener: HomeControllerListener, index:Int) {
        homeMenuItem {
            id(cellData.menuCode)
            data(cellData)
            index(index)
            onClickListener(listener)
        }
    }

    private fun addCommonSearch( listener: HomeControllerListener){
        searchview {
            id("menuSearchTagview")
            listener(listener)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addMenuSearchTagName(cellData: MenuSearchTagName, position: Int, listener: HomeControllerListener){
        menuSearchTagview {
            id(cellData.name[position])
            data(cellData)
            spanSizeOverride { _, _, _ -> 2 }
            selected(cellData.name[position] == menuSelected)
            onClickListener(listener)
            position(position)
        }
    }

    private fun addLoadingProductGrid(cellData: LoadingProductGrid) {
        lytLoadingProductGrid {
            id(cellData.hashCode())
        }
    }

}
