package com.jcodonuts.app.ui.banner_detail

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.bannerDetailPromo
import com.jcodonuts.app.bannerDetailRelatedMenu
import com.jcodonuts.app.data.local.Banner
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.RelatedMenu

class BannerDetailController(private val listener: BannerDetailControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is Banner -> addDetailBannerPromo(cellData, listener, index)
            }
        }
    }

    private fun addDetailBannerPromo(
        cellData: Banner,
        listener: BannerDetailControllerListener,
        position: Int
    ) {
        bannerDetailPromo {
            id(cellData.id)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}