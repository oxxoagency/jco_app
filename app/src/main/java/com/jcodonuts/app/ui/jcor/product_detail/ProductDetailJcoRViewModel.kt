package com.jcodonuts.app.ui.jcor.product_detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.ProductDetailReq
import com.jcodonuts.app.data.remote.model.req.ProductFavoriteReq
import com.jcodonuts.app.data.remote.model.res.ProductDetail
import com.jcodonuts.app.data.remote.model.res.ProductDetailRes
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.data.repository.ProductRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class ProductDetailJcoRViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val productRepository: ProductRepository,
    private val schedulers: SchedulerProvider,
    private val homeRepository: HomeRepository,
    private val jcoDatabase: JcoDatabase
) : BaseViewModel(), ProductDetailJcoRControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _productDetail = MutableLiveData<ProductDetail>()
    val productDetail: LiveData<ProductDetail>
        get() = _productDetail

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private val _nameProduct = MutableLiveData<String>()
    val nameProduct: LiveData<String>
        get() = _nameProduct

    private val _sizeCart = MutableLiveData<Int>()
    val sizeCart: LiveData<Int>
        get() = _sizeCart

    private fun insertToCart(cartProductJcoR: CartProductJcoR): Completable =
        jcoDatabase.cartDao().insertCartProductJcoR(cartProductJcoR)

    private fun cartProduct(): Flowable<List<CartProductJcoR>> =
        jcoDatabase.cartDao().getAllCartProductJcoR()

    private var tempCartProduct: CartProductJcoR? = null

    var price = MutableLiveData<String>()

    private lateinit var menuCode: String

    init {
        getAllItemCart()
    }

    fun loadDetail(id: String) {
        menuCode = id
        val loading = mutableListOf<BaseCell>()
        loading.add(LoadingPage())
        _datas.postValue(loading)

        val body = ProductDetailReq(
            "2",
            sharedPreference.loadSelectCity().toString(),
            id,
            sharedPreference.loadPhoneNumber().toString()
        )
        lastDisposable = productRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                val temp = mutableListOf<BaseCell>()
                _productDetail.postValue(model.data)
                price.postValue(model.data.menu_price.toString())
                _nameProduct.value = localizationMenuName(model)
                defaultTempCartProduct(model, model.data.menu_price.toString(), "")
                temp.add(
                    ProductDetailContent(
                        model.data.menu_code,
                        model.data.id_menu.toInt(),
                        "#FAC1A8",
                        "#F25A17",
                        model.data.menu_desc,
                        "",
                        model.data.menu_price,
                        price.value ?: "",
                        localizationMenuName(model),
                        1,
                        0,
                        model.data.menu_amount.toInt(),
                        model.data.category_title,
                        model.data.menu_image?.get(0)?.image,
                        model.data.details,
                        model.data.variant_num,
                        Converter.thousandSeparator(model.data.menu_normalprice.toString())
                    )
                )

                _datas.postValue(temp)
            }, {

            })

    }

    private fun defaultTempCartProduct(
        content: ProductDetailRes,
        price: String,
        variantName: String?
    ) {
        tempCartProduct = CartProductJcoR(
            menuCode = content.data.menu_code.toInt(),
            name = localizationMenuName(content),
            imgURL = content.data.menu_image?.get(0)?.image,
            price = price.toDouble(),
            productType = content.data.category_title,
            notes = "",
            qty = 1,
            priceOriginal = price.toDouble(),
            variantname = variantName.toString()
        )
        Log.d("cekqty", tempCartProduct.toString())
    }

    private fun localizationMenuName(product: ProductDetailRes): String =
        if (sharedPreference.loadLanguage()
                ?.contains("indonesia", true) == true
        ) product.data.menu_name
        else product.data.menu_name_en

    private fun doInsertToCart(cartProductJcoR: CartProductJcoR, quantity: Int) {
        if (quantity > 0) {
            compositeDisposable.add(
                insertToCart(cartProductJcoR).subscribeOn(schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {
                        handleError(it)
                    })
            )
        }
    }

    private fun getAllItemCart() {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers. ui())
            .subscribe({ data ->
                var totalQty = 0
                data.forEach {
                    Log.d("tag", it.qty.toString())
                    totalQty += it.qty
                }
                _sizeCart.postValue(totalQty)
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    override fun onPlusClick(position: Int) {
        _datas.value?.let {
            val content = (it[position] as ProductDetailContent).copy()
            content.quantity++

            val fixPrice = content.price?.toInt()?.times(content.quantity)

            tempCartProduct = CartProductJcoR(
                menuCode = content.menuCode?.toInt(),
                name = content.productName,
                imgURL = content.menu_image,
                price = fixPrice?.toDouble() ?: 0.0,
                productType = content.type,
                notes = "",
                qty = content.quantity,
                priceOriginal = content.price?.toDouble() ?: 0.0,
            )

            it[position] = content
            _datas.postValue(it)
        }

    }

    override fun onMinusClick(position: Int) {
        _datas.value?.let {
            val content = (it[position] as ProductDetailContent).copy()
            if (content.quantity > 0) {
                content.quantity--
                it[position] = content

                val fixPrice = content.price?.toInt()?.times(content.quantity)

                tempCartProduct = CartProductJcoR(
                    menuCode = content.menuCode?.toInt(),
                    name = content.productName,
                    imgURL = content.menu_image,
                    price = fixPrice?.toDouble() ?: 0.0,
                    productType = content.type,
                    notes = "",
                    qty = content.quantity,
                    priceOriginal = content.price?.toDouble() ?: 0.0,
                )
            }

            _datas.postValue(it)
        }
    }

    override fun onAddToCart() {
        _addToCart.value = SingleEvents("addToCart")
        doInsertToCart(tempCartProduct ?: return, tempCartProduct?.qty ?: 0)
    }

    override fun onBtnFavoriteClick() {
        productDetail.value?.let { modelData ->
            val favorite = if (modelData.is_favorite == "1") "0" else "1"
            val body = ProductFavoriteReq(
                favorite,
                sharedPreference.loadPhoneNumber().toString(),
                menuCode
            )
            lastDisposable = homeRepository.setProductFavorite(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    modelData.is_favorite = favorite
                    _productDetail.postValue(modelData)
                }, {
                    handleError(it)
                })

            lastDisposable?.let { compositeDisposable.add(it) }
        }
    }

    override fun onSizeBeverageClick(sizeBeverage: SizeBeverage) {}

    override fun onVariantBeverageClick(variant: Variant) {}
}