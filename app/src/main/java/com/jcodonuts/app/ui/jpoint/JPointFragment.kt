package com.jcodonuts.app.ui.jpoint

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentJPointBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.jpoint.jpoint_receive.JPointReceiveFragment
import com.jcodonuts.app.ui.jpoint.jpoint_use.JPointUseFragment
import javax.inject.Inject


class JPointFragment @Inject constructor() :
    BaseFragment<FragmentJPointBinding, JPointViewModel>() {

    private lateinit var pagerAdapter: ViewPagerAdapter
    override fun getViewModelClass(): Class<JPointViewModel> = JPointViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_j_point

    override fun onViewReady(savedInstance: Bundle?) {
        setupActionBar()
        initPagerAdapter()
        initTabLayout()
    }

    private fun setupActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initPagerAdapter() {
        pagerAdapter = ViewPagerAdapter(childFragmentManager, lifecycle)
        binding.viewPager.apply {
            offscreenPageLimit = 2
            adapter = pagerAdapter
        }
    }


    private fun initTabLayout() {
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = when (position) {
                0 -> getString(R.string.jpoint_received)
                else -> getString(R.string.jpoint_used)
            }
        }.attach()
    }


    inner class ViewPagerAdapter(
        fm: FragmentManager,
        lifecycle: Lifecycle,
    ) : FragmentStateAdapter(fm, lifecycle) {

        override fun getItemCount(): Int {
            return 2
        }

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> JPointReceiveFragment()
                else -> JPointUseFragment()
            }

        }
    }

}