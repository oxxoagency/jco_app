package com.jcodonuts.app.ui.jcor.hot_promo

import com.jcodonuts.app.data.local.ModelHotPromo

interface HotPromoJcoRControllerListener {
    fun onClick(modelHotPromo: ModelHotPromo)
}