package com.jcodonuts.app.ui.edit_profile

import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.ProfileData
import com.jcodonuts.app.databinding.FragmentEditProfileBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.language.LanguageFragmentDirections
import com.jcodonuts.app.utils.KeyboardUtil
import net.glxn.qrgen.android.QRCode
import javax.inject.Inject

class EditProfileFragment @Inject constructor() :
    BaseFragment<FragmentEditProfileBinding, EditProfileViewModel>() {

    override fun getViewModelClass(): Class<EditProfileViewModel> {
        return EditProfileViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_edit_profile
    }

    override fun onViewReady(savedInstance: Bundle?) {
        KeyboardUtil(requireActivity(), binding.root)
        setupActionBar()
        initObserver()

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.btnBirthDate.setOnClickListener {
            DialogBirthDate().showDialog(
                requireActivity().supportFragmentManager,
                object : DialogBirthDate.OnDialogClickListener {
                    override fun onChange(data: String) {
                        binding.birthDate.setText(data)
                    }
                })
        }
        binding.btnGender.setOnClickListener {
            DialogGender().showDialog(
                requireActivity().supportFragmentManager,
                object : DialogGender.OnDialogClickListener {
                    override fun onChangeMale(gender: String) {
                        binding.gender.setText(gender)
                    }

                    override fun onChangeFemale(gender: String) {
                        binding.gender.setText(gender)
                    }
                })
        }
        binding.btnCancel.setOnClickListener {
            onBackPress()
        }

        binding.btnSave.setOnClickListener{
            viewModel.editProfile(ProfileData(
                img = "https://drive.google.com/uc?id=1gtrQXUFKsrkexSwWA9eN3Mh2Xtau3S3p",
                name = binding.name.text.toString(),
                phone = binding.phoneNumber.text.toString(),
                gender = binding.gender.text.toString(),
                dateOfBirth = binding.birthDate.text.toString()
            ))
        }
    }

    private fun setupActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initObserver() {
        viewModel.datas.observe(this, {
            binding.viewModel = viewModel
            binding.executePendingBindings()

            val bitmap =  QRCode.from(it.phone).bitmap()
            binding.qrCode.setImageBitmap(bitmap)
        })

        viewModel.showProfile.observe(this,{
            it.getContentIfNotHandled().let {
                val action =
                    EditProfileFragmentDirections.actionFromEditToMainFragment("profile")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })
    }
}