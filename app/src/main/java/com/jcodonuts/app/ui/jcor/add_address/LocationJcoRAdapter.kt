package com.jcodonuts.app.ui.jcor.add_address

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.jcodonuts.app.databinding.ItemLocationJcoRBinding
import java.math.RoundingMode

class LocationJcoRAdapter(val data: (AutocompletePrediction) -> Unit) :
    RecyclerView.Adapter<LocationJcoRAdapter.ProgramViewHolder>() {

    private var program = emptyList<AutocompletePrediction>()

    private var selectedItemPosition: Int? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): LocationJcoRAdapter.ProgramViewHolder {
        return ProgramViewHolder(ItemLocationJcoRBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun getItemCount(): Int = program.size

    override fun onBindViewHolder(holder: LocationJcoRAdapter.ProgramViewHolder, position: Int) {
        val current = program[position]
        holder.apply {
            bind(current, position)
//            itemView.setOnClickListener {
//                selectedItemPosition = position
//                notifyDataSetChanged()
//            }
        }
    }

    inner class ProgramViewHolder(private val binding: ItemLocationJcoRBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(autocompletePrediction: AutocompletePrediction, position: Int) {
            binding.apply {
                data = autocompletePrediction
                locationPrimary.text = autocompletePrediction.getPrimaryText(null)
                locationSecondary.text = autocompletePrediction.getSecondaryText(null)
                if (autocompletePrediction.distanceMeters != null) {
                    distance.text =
                        "${
                            autocompletePrediction.distanceMeters?.toDouble()?.div(1000.0)
                                ?.toBigDecimal()?.setScale(1, RoundingMode.UP)
                        }Km"
                }
                if (position == selectedItemPosition) {
                    locationPrimary.setTextColor(Color.parseColor("#86754D"))
                    locationSecondary.setTextColor(Color.parseColor("#86754D"))
                    distance.setTextColor(Color.parseColor("#86754D"))
                    data(autocompletePrediction)
                } else {
                    locationPrimary.setTextColor(Color.parseColor("#FFFFFF"))
                    locationSecondary.setTextColor(Color.parseColor("#FFFFFF"))
                    distance.setTextColor(Color.parseColor("#8E8E8E"))
                }
                executePendingBindings()
            }
            itemView.setOnClickListener { data(autocompletePrediction) }
        }
    }

    internal fun setPrograms(programs: List<AutocompletePrediction>) {
        this.program = programs
        notifyDataSetChanged()
    }
}
