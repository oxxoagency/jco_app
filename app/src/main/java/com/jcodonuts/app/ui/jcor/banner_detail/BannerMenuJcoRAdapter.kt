package com.jcodonuts.app.ui.jcor.banner_detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jcodonuts.app.data.remote.model.res.RelatedMenuRes
import com.jcodonuts.app.databinding.ItemBannerMenuJcoRBinding
import com.jcodonuts.app.utils.Converter

class BannerMenuJcoRAdapter (private val data : (RelatedMenuRes) -> Unit) : RecyclerView.Adapter<BannerMenuJcoRAdapter.BannerMenuViewHolder>() {

    private var relatedMenu = emptyList<RelatedMenuRes>()
    private var lastCheckedPosition = -1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BannerMenuJcoRAdapter.BannerMenuViewHolder {
        return BannerMenuViewHolder(ItemBannerMenuJcoRBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: BannerMenuJcoRAdapter.BannerMenuViewHolder, position: Int) {
        holder.bind(relatedMenu[position], position)
    }

    override fun getItemCount(): Int {
        return relatedMenu.size
    }

    inner class BannerMenuViewHolder(private val binding: ItemBannerMenuJcoRBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(relatedMenu: RelatedMenuRes, position: Int) {
            binding.apply {
                data = relatedMenu
                price.text = Converter.rupiah(relatedMenu.menu_price.toString())

                radioButton.setOnClickListener {
                    val copyOfLastCheckPosition = lastCheckedPosition
                    lastCheckedPosition = position
                    notifyItemChanged(copyOfLastCheckPosition)
                    notifyItemChanged(lastCheckedPosition)
                    this@BannerMenuJcoRAdapter.data(relatedMenu)
                }
                radioButton.isChecked = position == lastCheckedPosition
                executePendingBindings()
            }
        }
    }

    internal fun setData(relatedMenu: List<RelatedMenuRes>) {
        this.relatedMenu = relatedMenu
        notifyDataSetChanged()
    }
}