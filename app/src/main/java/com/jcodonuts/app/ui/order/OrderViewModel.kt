package com.jcodonuts.app.ui.order

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.req.RecentOrderReq
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.ui.main.recent_order.RecentOrderListener
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class OrderViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val app: Application
) : BaseViewModel(), RecentOrderListener {

    private val _datas = MutableLiveData<List<BaseCell>>()
    val datas: LiveData<List<BaseCell>>
        get() = _datas

    private val _orderClick = MutableLiveData<SingleEvents<ModelOrder>>()
    val orderClick: LiveData<SingleEvents<ModelOrder>>
        get() = _orderClick

    private val _showDetail = MutableLiveData<SingleEvents<RecentOrder>>()
    val showDetail: LiveData<SingleEvents<RecentOrder>>
        get() = _showDetail

    fun loadOrder() {
        val body = RecentOrderReq(1, sharedPreference.loadEmail(), sharedPreference.loadSelectCity())
        lastDisposable = homeRepository.getRecentOrder(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                val temp = mutableListOf<BaseCell>()

                if (response.data.isNullOrEmpty()) {
                    temp.add(EmptyCart(app.getString(R.string.you_havent_ordered)))
                }


                response.data?.map { product ->
                    temp.add(
                        RecentOrder(
                            orderId = product.order_id,
                            outletName = product.order_outlet_code,
                            menuImage = product.menu_img,
                            orderTime = product.order_time,
                            orderDetail = product.order_detail,
                            price = Converter.rupiah(product.order_total.toString()),
                            menuName = product.menu_list,
                            orderStatus = product.order_status,
                            totalOrder = product.order_total_item,
                            orderPaymentStatus = product.order_payment_status
                        )
                    )

                }
                _datas.postValue(temp)

            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onRecentOrderClick(position: Int) {
        _datas.value.let {
            _showDetail.value = SingleEvents(it?.get(position) as RecentOrder)
        }
    }

    override fun onReOrderClick(position: Int) {}
}