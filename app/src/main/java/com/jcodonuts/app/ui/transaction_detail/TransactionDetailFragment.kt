package com.jcodonuts.app.ui.transaction_detail

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.TransactionDetailShippingInformation
import com.jcodonuts.app.databinding.FragmentTransactionDetailBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.order_status.OrderStatusFragment
import javax.inject.Inject


class TransactionDetailFragment @Inject constructor() :
    BaseFragment<FragmentTransactionDetailBinding, TransactionDetailViewModel>() {

    override fun getViewModelClass(): Class<TransactionDetailViewModel> =
        TransactionDetailViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_transaction_detail

    override fun onViewReady(savedInstance: Bundle?) {

        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadData(it1) }
            }
        }

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = TransactionDetailController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.onCopy.observe(this, {
            it.getContentIfNotHandled().let { text ->
                val clipboard =
                    context?.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
                val clip = ClipData.newPlainText("Copied Text", text)
                clipboard.setPrimaryClip(clip)
                Toast.makeText(context, getString(R.string.copied), Toast.LENGTH_SHORT).show()
            }

        })

        viewModel.onCopyVa.observe(this, {
            it.getContentIfNotHandled().let { va ->
                val clipboard =
                    context?.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
                val clip = ClipData.newPlainText("Copied Text", va)
                clipboard.setPrimaryClip(clip)
                Toast.makeText(context, getString(R.string.copied), Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val action =
                    TransactionDetailFragmentDirections.actionFromDetailToMainFragment("cart")
                findNavController().navigate(
                    action,
                    FragmentNavigator.Extras.Builder()
                        .build()
                )
            }
        })

        viewModel.showHotline.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val url = "https://api.whatsapp.com/send?phone=+6281288008990"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        })


        viewModel.checkOrderStatus.observe(viewLifecycleOwner, {
            viewModel.checkOrderStatus.observe(viewLifecycleOwner, {
                it.getContentIfNotHandled()?.let { data ->
//                    val url = getString(R.string.linkOrderStatusFragment)
//                    val uri = Uri.parse(url)
//                    navigateTo(uri)
//                    view?.findNavController()?.navigate(R.id.orderStatusFragment)
                    val dlg = OrderStatusFragment()
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        "BannerDetailFragment",
                        object : OrderStatusFragment.DialogOrderStatusListener {
                            override fun orderStatus(): TransactionDetailShippingInformation {
                                return data
                            }
                        }
                    )
                }
            })
        })
    }
}