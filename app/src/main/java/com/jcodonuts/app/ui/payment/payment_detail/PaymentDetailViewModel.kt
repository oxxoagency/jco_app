package com.jcodonuts.app.ui.payment.payment_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.OrderDetail
import com.jcodonuts.app.data.local.OrderProduct
import com.jcodonuts.app.data.local.OrderTotal
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.DetailRecentOrderReq
import com.jcodonuts.app.data.remote.model.res.Order
import com.jcodonuts.app.data.repository.TransactionRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.*
import javax.inject.Inject

class PaymentDetailViewModel @Inject constructor(
        private val repository: TransactionRepository,
        private val schedulers: SchedulerProvider,
        private val jcoDatabase: JcoDatabase,
        private val sharedPreference: SharedPreference
) : BaseViewModel(), PaymentDetailControllerListener {

    private val _datas = MutableLiveData<List<BaseCell>>()
    val datas: LiveData<List<BaseCell>>
        get() = _datas

    private val _trackingClick = MutableLiveData<SingleEvents<String>>()
    val trackingClick: LiveData<SingleEvents<String>>
        get() = _trackingClick

    private val _orderAgainClick = MutableLiveData<SingleEvents<String>>()
    val orderAgainClick: LiveData<SingleEvents<String>>
        get() = _orderAgainClick

    fun loadData(id: String) {
        val temp = _datas.value?.toMutableList() ?: mutableListOf()
        val body = DetailRecentOrderReq(1, id)
        lastDisposable = repository.getTransactionDetail(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ response ->
                    temp.add(
                            OrderDetail(
                                    addressName = addressName(response.data ?: return@subscribe),
                                    addressDetail = addressDetail(response.data ?: return@subscribe),
                                    orderNumber = response.data.order_id.toString(),
                                    purchaseDate = response.data.order_time?.parseDateAndTimeWithFormat("dd MMM yyyy").toString(),
                                    deliveryType = response.data.delivery_type.toString()
                            )
                    )

                    response.data.order_detail.map { detail ->
                        temp.add(OrderProduct(
                                name = detail.menu_name.toString(),
                                qty = detail.menu_quantity.toString()
                        ))
                    }

                    temp.add(OrderTotal(
                            subTotal = Converter.rupiah(response.data.subtotal.toString()),
                            deliveryFee = Converter.rupiah(response.data.delivery_fee.toString()),
                            total = Converter.rupiah(response.data.grandtotal.toString())
                    ))
                    _datas.value = temp
                }, {

                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun addressName(order: Order): String {
        return if (order.delivery_type?.contains("0", true) == true) {
            order.order_address.toString()
        } else {
            order.outlet_name.toString()
        }
    }

    private fun addressDetail(order: Order): String {
        return if (order.delivery_type?.contains("0", true) == true) {
            order.order_addressinfo.toString()
        } else {
            order.outlet_address.toString()
        }
    }

    override fun onOrderNumberCopy(index: Int) {

    }

    override fun onBtnOrderClick(index: Int) {
        _orderAgainClick.value = SingleEvents("orderAgainClick");
    }

    override fun onBtnTrackingClick(index: Int) {
        _trackingClick.value = SingleEvents("tracking");
    }

}