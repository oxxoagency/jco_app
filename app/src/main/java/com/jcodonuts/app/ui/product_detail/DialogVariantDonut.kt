package com.jcodonuts.app.ui.product_detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jcodonuts.app.R
import com.jcodonuts.app.data.remote.model.res.DetailVariant
import com.jcodonuts.app.databinding.DialogVariantDonutBinding
import javax.inject.Inject


class DialogVariantDonut @Inject constructor() : BottomSheetDialogFragment() {

    private lateinit var binding: DialogVariantDonutBinding
    private lateinit var listener: DialogVariantListener
    private val adapter by lazy { VariantAdapter() }

    override fun getTheme(): Int {
        return R.style.DialogFullWidth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogVariantDonutBinding.inflate(inflater)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        isCancelable = false
        adapter.setVariant(listener.variantDonut())
        Log.d("cekvariant",listener.variantDonut().toString())
        binding.name.text = listener.nameDonut()
        binding.sizeVariant.text = "All Variant ${listener.sizeDonut()}X"
        binding.recyclerViewVariant.apply {
            adapter = this@DialogVariantDonut.adapter
            layoutManager = LinearLayoutManager(context)
        }
        binding.btnDlgClose.setOnClickListener {
            dissmissDialog()
        }
    }


    fun showDialog(
        fragmentManager: FragmentManager,
        tag: String?,
        listener: DialogVariantListener
    ) {
        show(fragmentManager, tag)
        this.listener = listener
    }

    fun dissmissDialog() {
        dialog?.cancel()
    }

    interface DialogVariantListener {
        fun onClose()
        fun nameDonut(): String
        fun sizeDonut(): String
        fun variantDonut(): List<DetailVariant>
    }

}