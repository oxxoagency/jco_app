package com.jcodonuts.app.ui.product_detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jcodonuts.app.data.remote.model.res.PackageDonut
import com.jcodonuts.app.databinding.ItemDonutBinding

class DonutAdapter(private val donuts: List<PackageDonut>) :
    RecyclerView.Adapter<DonutAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            ItemDonutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val donut = donuts[position]
        holder.bind(donut)
    }

    override fun getItemCount(): Int {
        return donuts.size
    }

    inner class ViewHolder(private val binding: ItemDonutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(packageDonut: PackageDonut) {
            binding.apply {
                data = packageDonut
            }
        }
    }
}