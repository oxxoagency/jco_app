package com.jcodonuts.app.ui.payment.choose_payment

interface ChoosePaymentControllerListener {
    fun onClickEWallet(index: Int)
    fun onClickTransferVirtualAccount(index: Int)
    fun onClickOtherPayment(index: Int)
    fun onClick(index: Int)
}