package com.jcodonuts.app.ui.jcor.edit_profile

import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.ProfileData
import com.jcodonuts.app.databinding.FragmentEditProfileJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.edit_profile.DialogGender
import com.jcodonuts.app.ui.edit_profile.EditProfileFragmentDirections
import com.jcodonuts.app.ui.edit_profile.EditProfileViewModel
import com.jcodonuts.app.utils.KeyboardUtil
import net.glxn.qrgen.android.QRCode
import javax.inject.Inject


class EditProfileJcoRFragment @Inject constructor() :
    BaseFragment<FragmentEditProfileJcoRBinding, EditProfileViewModel>() {

    override fun getViewModelClass(): Class<EditProfileViewModel> = EditProfileViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_edit_profile_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        KeyboardUtil(requireActivity(), binding.root)
        setupActionBar()
        initObserver()

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.btnBirthDate.setOnClickListener {
            DialogBirthDateJcoR().showDialog(
                requireActivity().supportFragmentManager,
                object : DialogBirthDateJcoR.OnDialogClickListener {
                    override fun onChange(data: String) {
                        binding.birthDate.setText(data)
                    }
                })
        }
        binding.btnGender.setOnClickListener {
            DialogGender().showDialog(
                requireActivity().supportFragmentManager,
                object : DialogGender.OnDialogClickListener {
                    override fun onChangeMale(gender: String) {
                        binding.gender.setText(gender)
                    }

                    override fun onChangeFemale(gender: String) {
                        binding.gender.setText(gender)
                    }
                })
        }
        binding.btnCancel.setOnClickListener {
            onBackPress()
        }

        binding.btnSave.setOnClickListener {
            viewModel.editProfile(
                ProfileData(
                    img = "https://drive.google.com/uc?id=1gtrQXUFKsrkexSwWA9eN3Mh2Xtau3S3p",
                    name = binding.name.text.toString(),
                    phone = binding.phoneNumber.text.toString(),
                    gender = binding.gender.text.toString(),
                    dateOfBirth = binding.birthDate.text.toString()
                )
            )
        }
    }

    private fun setupActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initObserver() {
        viewModel.datas.observe(this, {
            binding.viewModel = viewModel
            binding.executePendingBindings()

            val bitmap =  QRCode.from(it.phone).bitmap()
            binding.qrCode.setImageBitmap(bitmap)
        })

        viewModel.showProfile.observe(this, {
            it.getContentIfNotHandled().let {
                val action =
                    EditProfileFragmentDirections.actionFromEditToMainFragment("profile")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })
    }
}