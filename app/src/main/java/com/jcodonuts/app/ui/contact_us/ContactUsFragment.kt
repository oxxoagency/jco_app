package com.jcodonuts.app.ui.contact_us

import android.content.Intent
import android.net.Uri
import android.os.Bundle

import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentContactUsBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject
import android.content.ActivityNotFoundException
import android.util.Log
import java.lang.Exception


class ContactUsFragment @Inject constructor() :
    BaseFragment<FragmentContactUsBinding, ContactUsViewModel>() {
    private val TAG = "ProfileFragment"

    override fun getViewModelClass(): Class<ContactUsViewModel> {
        return ContactUsViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_contact_us
    }

    override fun onViewReady(savedInstance: Bundle?) {
        initRecyclerview()
        initObserver()

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }
    }

    private fun initRecyclerview() {
        val controller = ContactUsController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.setData(it)
        })
    }

    private fun initObserver() {
        viewModel.showMenuDetail.observe(this, {
            it.getContentIfNotHandled().let {
                val url = "https://api.whatsapp.com/send?phone=+6281288008990"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            }
        })

        viewModel.showWebView.observe(this, {
            it.getContentIfNotHandled()?.let { urlWeb ->
                val url = getString(R.string.linkWebViewJCOFragment).replace("{url}", urlWeb)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })

        viewModel.openMail.observe(this, {
            it.getContentIfNotHandled()?.let { email ->
                try {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:$email"))
                    intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject")
                    intent.putExtra(Intent.EXTRA_TEXT, "your_text")
                    startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    Log.d("Error", e.message.toString())
                }
            }
        })

        viewModel.openFacebook.observe(this, {
            it.getContentIfNotHandled()?.let { fb ->
                try {
                    context?.packageManager?.getPackageInfo("com.facebook.katana", 0)
                    Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/$fb"))
                } catch (e: Exception) {
                    Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.facebook.com/$fb"))
                }
            }
        })

        viewModel.openInstagram.observe(this, {
            it.getContentIfNotHandled()?.let { ig ->
                val uri = Uri.parse("http://instagram.com/_u/$ig")
                val likeIng = Intent(Intent.ACTION_VIEW, uri)

                likeIng.setPackage("com.instagram.android")

                try {
                    startActivity(likeIng)
                } catch (e: ActivityNotFoundException) {
                    startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://instagram.com/$ig")))
                }

            }
        })

    }
}