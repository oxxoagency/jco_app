package com.jcodonuts.app.ui.product_detail

import android.util.Log
import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.carousel
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*

class ProductDetailController(private val listener: ProductDetailControllerListener) :
    AsyncEpoxyController() {
    private val TAG = "TopupController"

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var sizeSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var variantSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var price = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var max = 0
        set(value) {
            field = value
            requestModelBuild()
        }

    var mainQty = 0
        set(value) {
            field = value
            requestModelBuild()
        }

    var qtyDonut : MutableList<Int> = mutableListOf()
        set(value) {
            field = value
            requestModelBuild()
        }

    var maxCustomDonut = 0
        set(value) {
            field = value
            requestModelBuild()
        }

    var totalAmount = 0
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is ProductDetailContent -> addProductDetailContent(cellData, listener, index)
//                is DetailVariant -> addProductDetailVariant(cellData)
                is ProductDetailChooseItemHeader -> addProductDetailChooseSizeHeader()
                is HeaderVariantBeverage -> addHeaderVariantBeverage()
                is ProductDetailVariant -> {
                    addProductDetailDonut(cellData, listener, index)
                    addProductDetailVariantDonutCustom(cellData, listener, index)
                }
                is ProductDetailPackage -> {
                    addProductDetailPackage(cellData, listener, index)
                }
                is DonutCustom -> {
                    addDonutCustom(cellData, listener, index)
                }
                is SizeVariantBeverage -> addProductDetailBeverage(
                    cellData,
                    listener,
                    index
                )
                is VariantBeverage -> addProductDetailVariantBeverage(
                    cellData,
                    listener,
                    index
                )
                is Divider16 -> addDivider16(cellData)
                is LoadingPage -> addLoadingPage(cellData)
//                is ProductDetailVariantCustom -> addProductDetailVariantDonutCustom(cellData)
            }
        }


    }

    private fun addDivider16(cellData: Divider16) {
        divider16 {
            id(cellData.hashCode())
            spanSizeOverride { _, _, _ -> 4 }
        }
    }

    private fun addLoadingPage(cellData: LoadingPage) {
        lytLoadingShimmer {
            id(cellData.hashCode())
            spanSizeOverride { _, _, _ -> 4 }
        }
    }

    private fun addProductDetailContent(
        cellData: ProductDetailContent,
        listener: ProductDetailControllerListener,
        index: Int
    ) {
        productDetailContents {
            id("content")
            data(cellData)
            onClickListener(listener)
            index(index)
            price(price)
            spanSizeOverride { _, _, _ -> 4 }
        }
    }

//    private fun addProductDetailVariant(cellData: DetailVariant, listener:ProductDetailControllerListener, index:Int){
//        productDetailDonutItem {
//            id("variant")
//            data(cellData)
//            onClickListener(listener)
//            index(index)
//        }
//    }

    private fun addProductDetailChooseSizeHeader() {
        productDetailChooseSizeHeader {
            id("choose-size-header")
            spanSizeOverride { _, _, _ -> 4 }
        }
    }

    private fun addHeaderVariantBeverage() {
        productDetailVariantHeader {
            id("choose-variant-header")
            spanSizeOverride { _, _, _ -> 4 }
        }
    }

    private fun addWebview(cellData: DeliveryWebView) {
        deliveryWebview {
            id("https://google.com")
            wv(cellData)
        }
    }

    private fun addProductDetailPackage(
        cellData: ProductDetailPackage,
        listener: ProductDetailControllerListener,
        index: Int
    ) {
        productPackage {
            id(cellData.packageName)
            data(cellData)
            onClickListener(listener)
            index(index)
            spanSizeOverride { _, _, _ -> 4 }
        }
    }

    private fun addDonutCustom(
        cellData: DonutCustom,
        listener: ProductDetailControllerListener,
        index: Int
    ) {
        productDonutCustom {
            id(cellData.menuName)
            data(cellData)
            onClickListener(listener)
            index(index)
        }
    }

    private fun addProductDetailDonut(
        cellData: ProductDetailVariant,
        listener: ProductDetailControllerListener,
        index: Int
    ) {
        variantDonut {
            id(cellData.variant)
            data(cellData)
            index(index)
            onClickListener(listener)
            spanSizeOverride { _, _, _ -> 4 }
            isEmpty(cellData.quantity == 0 && mainQty >= max)
            isHide(cellData.variant.contains("sendiri", true))

//            cellData.variantDonut.forEach { packageDonut ->
//                productDetailDonut {
//                    id("${cellData.variant} ${packageDonut.menu_img}")
//                    data(packageDonut)
//                    dataProduct(cellData)
//                }
//            }
        }
    }

    private fun addProductDetailVariantDonutCustom(
        cellData: ProductDetailVariant,
        listener: ProductDetailControllerListener,
        index: Int
    ) {
        productDetailVariantDonutCustom {
            id(cellData.variant)
            data(cellData)
            index(index)
            onClickListener(listener)
            isEmpty(cellData.quantity == 0 && mainQty >= max)
            isHide(!cellData.variant.contains("sendiri", true))
            quantity(qtyDonut)
            maxCustomDonut(maxCustomDonut)
            totalAmount(totalAmount)
            Log.d("cekdonut", qtyDonut.toString())
            spanSizeOverride { _, _, _ -> 4 }
            Log.d(
                "cekvarqty",
                "${cellData.variant} quantity = $qtyDonut , max = $maxCustomDonut"
            )
        }
    }

    private fun addProductDetailBeverage(
        cellData: SizeVariantBeverage,
        listener: ProductDetailControllerListener,
        index: Int
    ) {
        val models = cellData.listSize.map {
            ProductDetailBeverageItemBindingModel_()
                .id(it.menuCode)
                .data(it)
                .index(index)
                .onClickListener(listener)
                .isSelected(it.size == sizeSelected)

        }
        carousel {
            id("detail-beverage")
            models(models)
            padding(Carousel.Padding.dp(16, 4, 16, 16, 8))
            spanSizeOverride { _, _, _ -> 4 }
        }
    }

    private fun addProductDetailVariantBeverage(
        cellData: VariantBeverage,
        listener: ProductDetailControllerListener,
        index: Int
    ) {
        val models = cellData.listVariant.map {
            ProductDetailVariantBeverageBindingModel_()
                .id(it.variant)
                .data(it)
                .index(index)
                .onClickListener(listener)
                .isSelected(it.variant == variantSelected)
        }
        carousel {
            id("detail-variant-beverage-carousel")
            models(models)
            padding(Carousel.Padding.dp(16, 4, 16, 16, 8))
            spanSizeOverride { _, _, _ -> 4 }
        }
    }
}