package com.jcodonuts.app.ui.jcor.add_address

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.databinding.ItemCityJcoRBinding


class CityJcoRAdapter(val data: (CityDataRes) -> Unit) :
    RecyclerView.Adapter<CityJcoRAdapter.CityViewHolder>() {

    private var cities = ArrayList<CityDataRes>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CityJcoRAdapter.CityViewHolder {
        return CityViewHolder(ItemCityJcoRBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: CityJcoRAdapter.CityViewHolder, position: Int) {
        val city = cities[position]
        holder.bind(city)
    }

    override fun getItemCount(): Int = cities.size

    inner class CityViewHolder(private val binding: ItemCityJcoRBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(city: CityDataRes) {
            binding.apply {
                data = city
                executePendingBindings()
            }
            itemView.setOnClickListener { data(city) }
        }
    }

    internal fun setCities(cities: ArrayList<CityDataRes>) {
        this.cities = cities
        notifyDataSetChanged()
    }

    internal fun setSearchCities(cities: ArrayList<CityDataRes>) {
        this.cities.clear()
        this.cities.addAll(cities)
        notifyDataSetChanged()
    }

    internal fun deleteCities() {
        this.cities.clear()
        notifyDataSetChanged()
    }
}