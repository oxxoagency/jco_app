package com.jcodonuts.app.ui.add_address

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.AddressRes
import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.data.repository.AddAddressRepository
import com.jcodonuts.app.data.repository.AuthRepository
import com.jcodonuts.app.data.repository.PickupRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject
import kotlin.math.ln

class AddAddressViewModel @Inject constructor(
    private val addAddressRepository: AddAddressRepository,
    private val authRepository: AuthRepository,
    private val pickupRepository: PickupRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : BaseViewModel() {


    private val _successAddAddress = MutableLiveData<SingleEvents<String>>()
    val successAddAddress: LiveData<SingleEvents<String>>
        get() = _successAddAddress

    private val _memberAddress = MutableLiveData<AddressRes>()
    val memberAddress: LiveData<AddressRes>
        get() = _memberAddress

    private val _errorMessage = MutableLiveData<SingleEvents<String>>()
    val errorMessage: LiveData<SingleEvents<String>>
        get() = _errorMessage

    private val _cities = MutableLiveData<List<CityDataRes>>()
    val cities: LiveData<List<CityDataRes>>
        get() = _cities

    fun setAddress(body: AddressReq) {
        lastDisposable = addAddressRepository.setAddress(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) {
                    _successAddAddress.value = SingleEvents("success_add_address")
                } else {
                    _errorMessage.value =
                        SingleEvents("Alamat tidak bisa ditambahkan, karena diluar radius pengantaran")
                }
            }, {
                handleError(it)
                _errorMessage.value =
                    SingleEvents("Alamat tidak bisa ditambahkan, karena diluar radius pengantaran")
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getAddress(id: Int) {
        lastDisposable = addAddressRepository.getAddress(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) _memberAddress.postValue(model)

            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun updateAddress(body: UpdateAddressReq) {
        lastDisposable = addAddressRepository.updateAddress(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) _successAddAddress.value =
                    SingleEvents("success_add_address")
            }, {
                handleError(it)
            })
        _successAddAddress.value = SingleEvents("success_add_address")
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getCity() {
        val body = CityReq(1, "indonesia")
        lastDisposable = addAddressRepository.getCity(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({
                _cities.postValue(it.data)
            }, {
                handleError(it)
            })
    }

    fun getSearchCity(city: String?) {
        val body = CitySearchReq(1, "indonesia", city)
        lastDisposable = addAddressRepository.getCitySearch(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({
                _cities.postValue(it.data)
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun loadLocation(lat: String, lng: String) {
        val body = OutletReq(1, lat, lng)
        lastDisposable = pickupRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status_code == 200) {
                    sharedPreference.saveCity(it.data[0].outlet_city.toString())
                    sharedPreference.savePostCode(it.data[0].outlet_postcode.toString())
                } else {
                    sharedPreference.saveCity("")
                }
            }, {

            })
    }
}