package com.jcodonuts.app.ui.banner_detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.CartProduct
import com.jcodonuts.app.data.local.CartProductPickUp
import com.jcodonuts.app.data.remote.model.res.RelatedMenuRes
import com.jcodonuts.app.databinding.FragmentBannerDetailBinding
import com.jcodonuts.app.ui.base.BaseFragment
import de.mateware.snacky.Snacky
import javax.inject.Inject

class BannerDetailFragment @Inject constructor() :
    BaseFragment<FragmentBannerDetailBinding, BannerDetailViewModel>() {

    override fun getViewModelClass(): Class<BannerDetailViewModel> =
        BannerDetailViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_banner_detail

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadData(it1) }
            }
        }

        binding.viewModel = viewModel
        binding.executePendingBindings()

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = BannerDetailController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText("Sukses ditambahkan ke keranjang")
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_text_secondary
                        )
                    )
                    .success()
                    .show()
            }
        })

        viewModel.showMenu.observe(this, { showMenu ->
            viewModel.relaatedMenus.observe(this, { data ->
                showMenu.getContentIfNotHandled()?.let {
                    val dlg = DialogBannerDetail()
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        "BannerDetailFragment",
                        object : DialogBannerDetail.DialogBannerDetailListener {
                            override fun promoMenu(): List<RelatedMenuRes> {
                                return data
                            }

                            override fun getPromo(relatedMenuRes: RelatedMenuRes) {
                                viewModel.doInsertToCart(
                                    CartProduct(
                                        menuCode = relatedMenuRes.menu_code?.toInt(),
                                        name = relatedMenuRes.menu_name,
                                        imgURL = relatedMenuRes.menu_image,
                                        price = relatedMenuRes.menu_price?.toDouble() ?: 0.0,
                                        productType = "",
                                        notes = "",
                                        qty = 1,
                                        priceOriginal = relatedMenuRes.menu_price?.toDouble()
                                            ?: 0.0,
                                        detailVariant = mutableListOf()
                                    )
                                )

                                viewModel.doInsertToCartPickUp(
                                    CartProductPickUp(
                                        menuCode = relatedMenuRes.menu_code?.toInt(),
                                        name = relatedMenuRes.menu_name,
                                        imgURL = relatedMenuRes.menu_image,
                                        price = relatedMenuRes.menu_price?.toDouble() ?: 0.0,
                                        productType = "",
                                        notes = "",
                                        qty = 1,
                                        priceOriginal = relatedMenuRes.menu_price?.toDouble()
                                            ?: 0.0,
                                        detailVariant = mutableListOf()
                                    )
                                )
                                Snacky.builder()
                                    .setActivity(activity)
                                    .setText("Sukses ditambahkan ke keranjang")
                                    .setDuration(Snacky.LENGTH_SHORT)
                                    .setActionText(R.string.close)
                                    .setBackgroundColor(
                                        ContextCompat.getColor(
                                            requireContext(),
                                            R.color.c_text_secondary
                                        )
                                    )
                                    .success()
                                    .show()
                            }
                        }
                    )
                }
            })
        })
        viewModel.update.observe(viewLifecycleOwner,{
            openAppOnPlayStore(requireContext(),null)
        })
    }

    private fun openAppOnPlayStore(ctx: Context, packageName: String?) {
        var packageName = packageName
        if (packageName == null) {
            packageName = ctx.packageName
        }
        val uri = Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
        openURI(ctx, uri, "Play Store not found in your device")
    }

    private fun openURI(
        ctx: Context,
        uri: Uri?,
        errorMessage: String?
    ) {
        val i = Intent(Intent.ACTION_VIEW, uri)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (ctx.packageManager.queryIntentActivities(i, 0).size > 0) {
            ctx.startActivity(i)
        } else if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        }
    }
}