package com.jcodonuts.app.ui.jcor.main.home

import android.net.Uri
import android.os.Bundle
import com.airbnb.epoxy.Carousel
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentHomeJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.main.home.HomeSpacingDecoration
import javax.inject.Inject


class HomeJcoRFragment @Inject constructor() :
    BaseFragment<FragmentHomeJcoRBinding, HomeJcoRViewModel>() {

    override fun getViewModelClass(): Class<HomeJcoRViewModel> = HomeJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_home_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        viewModel.fetchHomeFromAPI()
        viewModel.loadLocation()

        binding.refresh.setOnRefreshListener { viewModel.fetchHomeFromAPI() }

        initObserver()
        initRecyclerview()
    }

    private fun initRecyclerview() {
        val controller = HomeController(viewModel)
        controller.spanCount = 2
        Carousel.setDefaultGlobalSnapHelperFactory(null)
        binding.homeRecyclerview.setController(controller)
        binding.homeRecyclerview.addItemDecoration(HomeSpacingDecoration())
        viewModel.datas.observe(this, {
            controller.data = it
            binding.refresh.isRefreshing = false
        })

        viewModel.promoSelected.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                controller.promoSelected = data
                val url = getString(R.string.linkBannerDetailJcoRFragment).replace("{id}", data)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }

        })

        viewModel.menuSelected.observe(this, {
            controller.menuSelected = it
        })
    }

    private fun initObserver() {
        viewModel.changeApp.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateToWithAnimation(R.string.linkMainFragment)
            }
        })

        viewModel.openProductDetail.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkProductDetailJcoR).replace("{id}", data.menuCode)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })


        viewModel.showHotPromo.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkHotPromoJcoRFragment)
            }
        })

        viewModel.showMenuSearch.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkMenuSearchJcoRFragment)
            }
        })

        viewModel.showJPoint.observe(this, {
            it.getContentIfNotHandled()?.let { token ->
                val baseUrl = "https://test.jcodelivery.com/loyalty/detail?point_status=1&token="
                val url = getString(R.string.linkWebViewJcoRJPointFragment).replace("{url}", baseUrl + token)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }

    override fun onBackPress() {
        requireActivity().finish()
    }
}