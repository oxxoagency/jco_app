package com.jcodonuts.app.ui.jcor.main.home

import com.jcodonuts.app.data.local.MenuCategory
import com.jcodonuts.app.data.local.MenuSearchTagName
import com.jcodonuts.app.data.local.PromoBanner

interface HomeControllerListener {
    fun onSwitchAppClick()
    fun onNotificationClick()
    fun onSearchClick()
    fun onPromoSeeAllClick()
    fun onPromosItemClick(promoBanner: PromoBanner)
    fun onMenuCategoryClick(menuCategory: MenuCategory)
    fun onMenuItemClick(index:Int)
    fun onMenuItemFavoriteClick(index:Int)
    fun onSearchItem(query:String)
    fun onSearchMenuCategoryClick(menuSearchTagName: MenuSearchTagName, position: Int)
    fun onJPointClick()
}