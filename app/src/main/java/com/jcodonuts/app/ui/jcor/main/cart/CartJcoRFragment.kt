package com.jcodonuts.app.ui.jcor.main.cart

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.util.Preconditions
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.BuildConfig
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentCartJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.base.InjectingNavHostFragment
import com.jcodonuts.app.ui.jcor.base.MainJcoRFragment
import com.jcodonuts.app.utils.DlgLoadingProgressBarJcoR
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback
import com.midtrans.sdk.corekit.core.MidtransSDK
import com.midtrans.sdk.corekit.core.PaymentMethod
import com.midtrans.sdk.corekit.core.TransactionRequest
import com.midtrans.sdk.corekit.core.UIKitCustomSetting
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme
import com.midtrans.sdk.corekit.models.*
import com.midtrans.sdk.corekit.models.snap.Authentication
import com.midtrans.sdk.corekit.models.snap.CreditCard
import com.midtrans.sdk.corekit.models.snap.TransactionResult
import com.midtrans.sdk.uikit.SdkUIFlowBuilder
import de.mateware.snacky.Snacky
import java.util.*
import javax.inject.Inject


class CartJcoRFragment @Inject constructor() :
    BaseFragment<FragmentCartJcoRBinding, CartJcoRViewModel>(), TransactionFinishedCallback {

    var dialog: DlgLoadingProgressBarJcoR? = null

    override fun getViewModelClass(): Class<CartJcoRViewModel> = CartJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_cart_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        dialog = DlgLoadingProgressBarJcoR(requireContext())
        if (!isFragmentFromPaused) {
            viewModel.getAllItemCart()
        }

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
        initRecyclerview()
        initObserver()
        initMidtransSdk()
    }

    @SuppressLint("RestrictedApi")
    private fun initRecyclerview() {
        val controller = CartJcoRController(viewModel)
        binding.recyclerView.setController(controller)
        viewModel.datas.observe(this, {
            controller.data = it
        })

        parentFragmentManager.setFragmentResultListener("pickup", this,
            { requestKey, result ->
                Preconditions.checkState("pickup" == requestKey)
                controller.pickUp = sharedPreference.loadPickUpJcoR().toString()
            }
        )

        viewModel.showDialogNote.observe(this, {
            it.getContentIfNotHandled()?.let {
                val dlg = DialogNoteJcoR()
                dlg.showDialog(
                    requireActivity().supportFragmentManager,
                    "CartFragment",
                    object : DialogNoteJcoR.OnDialogClickListener {
                        override fun onSaveClick(notes: String) {
                            sharedPreference.saveNotesJcoR(notes)
                            dlg.dissmissDialog()
                            controller.note = notes
                        }

                        override fun onNotes(): String? = sharedPreference.loadNotesJcoR()
                    })
            }
        })

        viewModel.openProductDetail.observe(this,{
            it.getContentIfNotHandled()?.let { cartRecommendation ->
//                controller.menuSelected = cartRecommendation.menuCode ?: ""
                val url = getString(R.string.linkProductDetailJcoR).replace("{id}", cartRecommendation ?: "")
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })

        controller.pickUp = sharedPreference.loadPickUpJcoR().toString()
        controller.paymentMethod = sharedPreference.loadPaymentName().toString()
        controller.paymentIcon = sharedPreference.loadPaymentIcon().toString()
        controller.note = sharedPreference.loadNotesJcoR().toString()
    }

    private fun initObserver() {
        viewModel.showDelivery.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkDeliveryJcoRFragment)
            }
        })

        viewModel.reFetchCart.observe(viewLifecycleOwner, {
            val action =
                CartJcoRFragmentDirections.actionFromProductToMainFragment()
            findNavController()
                .navigate(
                    action,
                    FragmentNavigator.Extras.Builder()
                        .build()
                )
        })

        viewModel.removeFromCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled().let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText("Item berhasil dihapus")
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setActionText(getString(R.string.close))
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_text_secondary
                        )
                    )
                    .success()
                    .show()
                val action =
                    CartJcoRFragmentDirections.actionFromProductToMainFragment()
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })

        viewModel.showChangePayment.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkChoosePaymentFragmentJcoR)
            }
        })

        viewModel.showVoucher.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkVoucherFragmentJcoR)
            }
        })

        viewModel.showPickup.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkPickupFragmentJcoR)
            }
        })

        viewModel.showLoading.observe(this, {
            it.getContentIfNotHandled()?.let { state ->
                if (state) {
                    dialog?.showPopUp()
                } else {
                    dialog?.dismissPopup()
                }
            }
        })

        viewModel.showMidtrans.observe(this, {
            it.getContentIfNotHandled()?.let {
                MidtransSDK.getInstance().apply {
                    transactionRequest = initTransactionRequest()
                    var paymentMethod: PaymentMethod? = null
                    when (sharedPreference.loadPaymentMethod()) {
                        "credit_cards" -> paymentMethod = PaymentMethod.CREDIT_CARD
                        "bca_va" -> paymentMethod = PaymentMethod.BANK_TRANSFER_BCA
                        "echannel" -> paymentMethod = PaymentMethod.BANK_TRANSFER_MANDIRI
                        "cimb_clicks" -> paymentMethod = PaymentMethod.CIMB_CLICKS
                        "gopay" -> paymentMethod = PaymentMethod.GO_PAY
                        "bca_klikbca" -> paymentMethod = PaymentMethod.KLIKBCA
                        "bca_klikpay" -> paymentMethod = PaymentMethod.BCA_KLIKPAY
                        "bri_epay" -> paymentMethod = PaymentMethod.EPAY_BRI
                        "permata_va" -> paymentMethod = PaymentMethod.BANK_TRANSFER_PERMATA
                        "bni_va" -> paymentMethod = PaymentMethod.BANK_TRANSFER_BNI
                        "bri_va" -> paymentMethod = PaymentMethod.EPAY_BRI
                        "other_va" -> paymentMethod = PaymentMethod.BANK_TRANSFER_OTHER
                        "indomaret" -> paymentMethod = PaymentMethod.INDOMARET
                        "danamon_online" -> paymentMethod = PaymentMethod.DANAMON_ONLINE
                        "akulaku" -> paymentMethod = PaymentMethod.AKULAKU
                        "shopeepay" -> paymentMethod = PaymentMethod.SHOPEEPAY
                    }

                    startPaymentUiFlow(context, paymentMethod)
                }

            }
        })

        viewModel.alertTIme.observe(this, {
            it.getContentIfNotHandled()?.let { message ->
                Snacky.builder()
                    .setActivity(activity)
                    .setText(message)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setActionText(getString(R.string.close))
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_favorite_true
                        )
                    )
                    .error()
                    .show()
            }
        })

        viewModel.showAddOrder.observe(viewLifecycleOwner, {
            val action = CartJcoRFragmentDirections.actionCartToHome()
            findNavController().navigate(
                action,
                FragmentNavigator.Extras.Builder()
                    .build()
            )
        })

        viewModel.errorMessage.observe(this, {
            it.getContentIfNotHandled()?.let { message ->
                Snacky.builder()
                    .setActivity(activity)
                    .setText(message)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setActionText(getString(R.string.close))
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_favorite_true
                        )
                    )
                    .error()
                    .show()
            }
        })

        viewModel.showPaymentDetail.observe(this, {
            viewModel.orderId.observe(viewLifecycleOwner, { id ->
                it.getContentIfNotHandled()?.let {
                    val url =
                        getString(R.string.linkTransactionDetailJcoR).replace("{id}", id.toString())
                    val uri = Uri.parse(url)
                    navigateTo(uri)
                    sharedPreference.apply {
                        removeCodeVoucher()
                        removeVoucher()
                        removeAddress()
                        removePickUpJcoR()
                        removeOutletCode()
                        removeItemDetailJcoR()
                    }
                    viewModel.doDeleteAllCart()
                    viewModel.doDeleteAllCartPickUp()
                }
            })

        })
    }

    private fun initTransactionRequest(): TransactionRequest {
        // Create new Transaction Request
        var orderId = 0
        var grossAmount = 0.0
        var itemDetails: MutableList<ItemDetails> = mutableListOf()
        viewModel.orderId.observe(this, {
            orderId = it ?: 0
        })

        viewModel.grossAmount.observe(this, {
            grossAmount = it
        })

        viewModel.cartOrders.observe(this, {
            itemDetails = it
        })

        val transactionRequestNew = TransactionRequest(orderId.toString() + "", grossAmount)
//        transactionRequestNew.gopay = Gopay("mysamplesdk:://midtrans")
//        transactionRequestNew.shopeepay = Shopeepay("mysamplesdk:://midtrans")
        transactionRequestNew.customerDetails = initCustomerDetails()
        transactionRequestNew.itemDetails = itemDetails as ArrayList<ItemDetails>?
        val expiryModel = ExpiryModel()
        expiryModel.unit = ExpiryModel.UNIT_MINUTE
        expiryModel.duration = 60
        transactionRequestNew.expiry = expiryModel
        val creditCard = CreditCard()
        if (sharedPreference.loadPaymentMethod() == "credit_cards") {
            creditCard.apply {
                bank = "mandiri"
                authentication = Authentication.AUTH_3DS
            }
            transactionRequestNew.creditCard = creditCard
        }

        return transactionRequestNew
    }

    private fun initCustomerDetails(): CustomerDetails {
        //define customer detail (mandatory for coreflow)
        val mCustomerDetails = CustomerDetails()
        mCustomerDetails.phone = sharedPreference.loadPhoneNumber()
        mCustomerDetails.firstName = sharedPreference.loadName()
        mCustomerDetails.email = sharedPreference.loadEmail()
        mCustomerDetails.customerIdentifier = sharedPreference.loadEmail()
        mCustomerDetails.shippingAddress = initShippingAddress()
        mCustomerDetails.billingAddress = initBillingAddress()
        return mCustomerDetails
    }

    private fun initBillingAddress(): BillingAddress {
        val mBillingAddress = BillingAddress()
        val billingAddress = "${sharedPreference.loadAddress()} ${sharedPreference.loadDetailAddress()}"
        mBillingAddress.apply {
            address = billingAddress.take(255)
            city = sharedPreference.loadSelectCity()
            postalCode = sharedPreference.loadPostCode()
            countryCode = "IDN"
        }
        return mBillingAddress
    }

    private fun initShippingAddress(): ShippingAddress {
        val mShippingAddress = ShippingAddress()
        mShippingAddress.apply {
            address = sharedPreference.loadPickUpAddress()?.take(255)
            city = sharedPreference.loadPickUpCity()
            postalCode = sharedPreference.loadPostCode()
            countryCode = "IDN"
        }
        return mShippingAddress
    }

    private fun initMidtransSdk() {
        val sdkUIFlowBuilder: SdkUIFlowBuilder = SdkUIFlowBuilder.init()
            .setClientKey(BuildConfig.CLIENT_KEY) // client_key is mandatory
            .setContext(context) // context is mandatory
            .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
            .setMerchantBaseUrl(BuildConfig.BASE_URL_MIDTRANS) //set merchant url
            .setUIkitCustomSetting(uiKitCustomSetting())
            .enableLog(true) // enable sdk log
            .setColorTheme(
                CustomColorTheme(
                    "#86754D",
                    "#86754D",
                    "#86754D"
                )
            ) // will replace theme on snap theme on MAP
            .setLanguage("id")
        sdkUIFlowBuilder.buildSDK()
    }

    private fun uiKitCustomSetting(): UIKitCustomSetting {
        val uIKitCustomSetting = UIKitCustomSetting()
        uIKitCustomSetting.isSkipCustomerDetailsPages = true
        uIKitCustomSetting.isShowPaymentStatus = true
        return uIKitCustomSetting
    }

    override fun onTransactionFinished(result: TransactionResult?) {
        if (result?.response != null) {
            when (result.status) {
                TransactionResult.STATUS_SUCCESS -> {
                    sharedPreference.apply {
                        removeCodeVoucher()
                        removeVoucher()
                        removeAddress()
                        removePickUpJcoR()
                        removeOutletCode()
                        removeItemDetailJcoR()
                    }
                    viewModel.doDeleteAllCart()
                    viewModel.doDeleteAllCartPickUp()
                    Snacky.builder()
                        .setActivity(activity)
                        .setText("Transaksi Berhasil")
                        .setDuration(Snacky.LENGTH_SHORT)
                        .setActionText(getString(R.string.close))
                        .setBackgroundColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.c_text_secondary
                            )
                        )
                        .success()
                        .show()

                    viewModel.orderId.observe(this, {
                        viewModel.midtransPayment(it.toString())
                        viewModel.getOrderPayment(it.toString())
                        val url =
                            getString(R.string.linkTransactionDetailJcoR).replace(
                                "{id}",
                                it.toString()
                            )
                        val uri = Uri.parse(url)
                        Handler(Looper.getMainLooper()).postDelayed({
                            navigateTo(uri)
                        }, 2000)
                    })

                }
                TransactionResult.STATUS_PENDING -> {
                    sharedPreference.apply {
                        removeCodeVoucher()
                        removeVoucher()
                        removeAddress()
                        removePickUpJcoR()
                        removeOutletCode()
                        removeItemDetailJcoR()
                    }
                    viewModel.doDeleteAllCart()
                    viewModel.doDeleteAllCartPickUp()
                    Snacky.builder()
                        .setActivity(activity)
                        .setText("Transaksi Berhasil")
                        .setDuration(Snacky.LENGTH_SHORT)
                        .setActionText(getString(R.string.close))
                        .setBackgroundColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.c_text_secondary
                            )
                        )
                        .success()
                        .show()

                    viewModel.orderId.observe(this, {
                        viewModel.midtransPayment(it.toString())
                        viewModel.getOrderPayment(it.toString())
                        val url =
                            getString(R.string.linkTransactionDetailJcoR).replace(
                                "{id}",
                                it.toString()
                            )
                        val uri = Uri.parse(url)
                        Handler(Looper.getMainLooper()).postDelayed({
                            navigateTo(uri)
                        }, 2000)
                    })
                }
                TransactionResult.STATUS_FAILED -> Toast.makeText(
                    context,
                    "Transaction Failed. ID: " + result.response.transactionId.toString() + ". Message: " + result.response.statusMessage,
                    Toast.LENGTH_LONG
                ).show()
            }
            result.response.validationMessages
        } else if (result?.isTransactionCanceled == true) {
            Toast.makeText(context, "Transaction Canceled", Toast.LENGTH_LONG).show()
        } else {
            if (result?.status.equals(TransactionResult.STATUS_INVALID, true)) {
                Toast.makeText(context, "Transaction Invalid", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, "Transaction Finished with failure.", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    override fun onBackPress() {
        val navhost = (parentFragment as InjectingNavHostFragment)
        (navhost.parentFragment as MainJcoRFragment).backToHome()
    }
}