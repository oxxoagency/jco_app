package com.jcodonuts.app.ui.jcor.main.recent_order

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.RecentOrderReq
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class RecentOrderJcoRViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val jcoDatabase: JcoDatabase,
    private val app: Application
) : BaseViewModel(), RecentOrderJcoRListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _showDetail = MutableLiveData<SingleEvents<RecentOrder>>()
    val showDetail: LiveData<SingleEvents<RecentOrder>>
        get() = _showDetail

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private fun insertToCart(cartProduct: CartProductJcoR): Completable =
        jcoDatabase.cartDao().insertCartProductJcoR(cartProduct)

    private fun doInsertToCart(cartProduct: CartProductJcoR) {
        compositeDisposable.add(
            insertToCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun loadData() {
        val temp = mutableListOf<BaseCell>()
        temp.add(LoadingPage())
        val body = RecentOrderReq(2, sharedPreference.loadEmail(), sharedPreference.loadSelectCity())
        lastDisposable = homeRepository.getRecentOrder(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                temp.removeAll { it is LoadingPage }
                if (response.data.isNullOrEmpty()){
                    temp.add(EmptyCart(app.getString(R.string.you_havent_ordered)))
                }

                response.data?.map { product ->
                    temp.add(
                        RecentOrder(
                            orderId = product.order_id,
                            outletName = product.order_outlet_code,
                            menuImage = product.menu_img,
                            orderTime = product.order_time,
                            orderDetail = product.order_detail,
                            price = Converter.rupiah(product.order_total.toString()),
                            menuName = product.menu_list,
                            orderStatus = product.order_status,
                            totalOrder = product.order_total_item,
                            orderPaymentStatus = product.order_payment_status
                        )
                    )

                }
                _datas.postValue(temp)
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onRecentOrderClick(position: Int) {
        _datas.value.let {
            _showDetail.value = SingleEvents(it?.get(position) as RecentOrder)
        }
    }

    override fun onReOrderClick(position: Int) {
        _datas.value.let {
            val recentOrder = (it?.get(position) as RecentOrder).copy()
            recentOrder.orderDetail?.map { detail ->
                doInsertToCart(
                    CartProductJcoR(
                        menuCode = detail.menu_code?.toInt(),
                        name = detail.menu_name,
                        imgURL = detail.menu_image[0].image,
                        price = detail.menu_price?.toDouble() ?: 0.0,
                        productType = "",
                        notes = "",
                        qty = detail.menu_quantity?.toInt() ?: 0,
                        priceOriginal = detail.menu_unitprice?.toDouble() ?: 0.0
                    )
                )
            }
            _addToCart.value = SingleEvents("add-to-cart")
            _datas.value = it
        }
    }
}