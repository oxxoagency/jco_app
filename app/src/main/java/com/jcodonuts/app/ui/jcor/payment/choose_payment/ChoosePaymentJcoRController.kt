package com.jcodonuts.app.ui.jcor.payment.choose_payment

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.choosePaymentJcoR
import com.jcodonuts.app.data.local.*

class ChoosePaymentJcoRController(private val listener: ChoosePaymentJcoRControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is Payment -> choosePayment(cellData,listener,index)
            }
        }
    }

    private fun choosePayment(
        cellData: Payment,
        listener: ChoosePaymentJcoRControllerListener,
        position: Int
    ) {
        choosePaymentJcoR {
            id(cellData.paymentMethod)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}