package com.jcodonuts.app.ui.main.recent_order

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.RecentOrder
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.LoadingPage

class RecentOrderController(private val recentOrderListener: RecentOrderListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is RecentOrder -> addRecentOrder(cellData, index, recentOrderListener)
                is EmptyCart -> addEmptyCart(cellData)
                is LoadingPage -> addLoadingPage(cellData)
            }
        }
    }

    private fun addRecentOrder(
        cellData: RecentOrder,
        position: Int,
        listener: RecentOrderListener
    ) {
        recentOrder {
            id(cellData.orderId)
            data(cellData)
            position(position)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart){
        cartEmpty {
            id("empty-cart")
            data(cellData)
        }
    }

    private fun addLoadingPage(cellData: LoadingPage) {
        lytLoadingShimmer {
            id(cellData.hashCode())
        }
    }
}