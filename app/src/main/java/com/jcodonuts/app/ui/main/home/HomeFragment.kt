package com.jcodonuts.app.ui.main.home

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.airbnb.epoxy.Carousel
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentMainHomeBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject


class
HomeFragment @Inject constructor() : BaseFragment<FragmentMainHomeBinding, HomeViewModel>() {
    private val TAG = "HomeFragment"

    override fun getViewModelClass(): Class<HomeViewModel> {
        return HomeViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_main_home
    }

    override fun onViewReady(savedInstance: Bundle?) {
        permissionLocation()

        if (!isFragmentFromPaused) {
            viewModel.fetchHomeFromAPI()
            viewModel.loadLocation()
        }
//            viewModel.checkFromLogin(
//                phone,
//                sharedPreference.loadLatitude(),
//                sharedPreference.loadLongitude()
//            )

        binding.refresh.setOnRefreshListener {
            viewModel.fetchHomeFromAPI()
        }
    }

    private fun initRecyclerview() {
        val controller = HomeController(viewModel)
        controller.spanCount = 2
        Carousel.setDefaultGlobalSnapHelperFactory(null)
        binding.homeRecyclerview.setController(controller)
        binding.homeRecyclerview.addItemDecoration(HomeSpacingDecoration())
        viewModel.datas.observe(this, Observer {
            controller.data = it
            binding.refresh.isRefreshing = false
        })

        viewModel.menuSelected.observe(this, {
            controller.menuSelected = it
        })

        viewModel.promoSelected.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                controller.promoSelected = data
                val url = getString(R.string.linkBannerDetailFragment).replace("{id}", data)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }

        })
    }

    private fun initObserver() {
        viewModel.showDialogCannotOrder.observe(this, {
            it.getContentIfNotHandled()?.let {

                val dlg = DialogCannotOrder()
                dlg.showDialog(
                    requireActivity().supportFragmentManager,
                    "HomeFragment",
                    object : DialogCannotOrder.OnDialogClickListener {
                        override fun onLoginClick() {
                            dlg.dissmissDialog()
                            navigateTo(R.string.linkLogin)
                        }

                        override fun onRegisterClick() {
                            dlg.dissmissDialog()
                            navigateTo(R.string.linkRegister)
                        }
                    })
            }
        })

        viewModel.showLinkaja.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkLinkaja)
            }
        })

        viewModel.openProductDetail.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkProductDetail).replace("{id}", data.menuCode)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })

        viewModel.showQRcode.observe(this, {
            it.getContentIfNotHandled()?.let {
                val dlg = DialogQrCode()
                dlg.showDialog(requireActivity().supportFragmentManager)
            }
        })

        viewModel.showPickup.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkPickupFragment)
            }
        })

        viewModel.showMenuSearch.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkMenuSearchFragment)
            }
        })

        viewModel.showChangeApp.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateToWithAnimation(R.string.linkMainJcoRFragment)
            }
        })

        viewModel.showHotPromo.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkHotPromoFragment)
            }
        })

        viewModel.showNotification.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkNotificationFragment)
            }
        })

        viewModel.showUpdatePopUpMandatory.observe(this, {
            it.getContentIfNotHandled()?.let {
                onMandatoryUpdate()
            }
        })
        viewModel.showUpdatePopUp.observe(this, {
            it.getContentIfNotHandled()?.let {
                onUpdateNeeded()
            }
        })

        viewModel.showJPoint.observe(this, {
            it.getContentIfNotHandled()?.let { token ->
                val baseUrl = "https://test.jcodelivery.com/loyalty/detail?point_status=1&token="
                val url = getString(R.string.linkWebViewJPointFragment).replace("{url}", baseUrl + token)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }

    private fun permissionLocation() {
        val permissionListener: PermissionListener = object : PermissionListener {
            override fun onPermissionGranted() {
                initObserver()
                initRecyclerview()
                getLocation()
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                Toast.makeText(
                    context,
                    "Permission Denied\n" + deniedPermissions.toString(),
                    Toast.LENGTH_SHORT
                ).show()
                activity?.finishAndRemoveTask()
            }
        }

        TedPermission.with(context)
            .setPermissionListener(permissionListener)
            .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
            .setPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_PHONE_STATE
            ).check()
    }

    private fun getLocation() {
        val mLocationRequest = LocationRequest.create()
        mLocationRequest.interval = 60000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mLocationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                for (location in locationResult.locations) {
                    if (location != null) {
                        Log.d("ceklocation", location.latitude.toString())
                    }
                }
            }
        }

        val mFusedLocation = LocationServices.getFusedLocationProviderClient(activity ?: return)
        if (ActivityCompat.checkSelfPermission(
                context ?: return,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context ?: return,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context ?: return,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

        } else {
            LocationServices.getFusedLocationProviderClient(requireActivity())
                .requestLocationUpdates(mLocationRequest, mLocationCallback, null)
            mFusedLocation.lastLocation.addOnSuccessListener(
                activity ?: return
            ) { loc ->
                if (loc != null) {
                    sharedPreference.apply {
                        saveLatitude(loc.latitude.toString())
                        saveLongitude(loc.longitude.toString())
                    }
                }
            }
            mFusedLocation.lastLocation.addOnFailureListener {
                it.printStackTrace()
            }
        }
    }


    private fun onMandatoryUpdate() {
        val dlg = DialogUpdateApp(requireContext())
        viewModel.updateVersion.observe(viewLifecycleOwner, {
            dlg.showPopup(
                image = it.update_img.toString(),
                onClickListenerUpdate = {
                    openAppOnPlayStore(requireContext(), null)
                },
                onClickListenerPass = { dlg.dismissPopup() },
                mandatory = true
            )
        })

    }

    private fun onUpdateNeeded() {
        val dlg = DialogUpdateApp(requireContext())
        viewModel.updateVersion.observe(viewLifecycleOwner, {
            dlg.showPopup(
                image = it.update_img.toString(),
                onClickListenerUpdate = {
                    openAppOnPlayStore(requireContext(), null)
                },
                onClickListenerPass = { dlg.dismissPopup() },
                mandatory = false
            )
        })
    }

    private fun openAppOnPlayStore(ctx: Context, packageName: String?) {
        var packageName = packageName
        if (packageName == null) {
            packageName = ctx.packageName
        }
        val uri = Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
        openURI(ctx, uri, "Play Store not found in your device")
    }


    private fun openURI(
        ctx: Context,
        uri: Uri?,
        errorMessage: String?,
    ) {
        val i = Intent(Intent.ACTION_VIEW, uri)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (ctx.packageManager.queryIntentActivities(i, 0).size > 0) {
            ctx.startActivity(i)
        } else if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onBackPress() {
        requireActivity().finish()
    }
}