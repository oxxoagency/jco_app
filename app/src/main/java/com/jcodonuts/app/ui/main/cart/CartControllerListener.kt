package com.jcodonuts.app.ui.main.cart

import com.jcodonuts.app.data.local.CartRecommendation


interface CartControllerListener {
    fun onClick(index: Int)
    fun onSwitchChange(checked: Boolean)
    fun onPickupAddressClick(index:Int)
    fun onDeliveryAddressClick(index:Int)
    fun onOrderClick(index:Int)
    fun onChangePaymentClick(index:Int)
    fun onProductClick(index:Int)
    fun onProductPlusClick(index:Int)
    fun onProductMinusClick(index:Int)
    fun onProductPlusPickUpClick(index:Int)
    fun onProductMinusPickUpClick(index:Int)
    fun onProductNotesClick(index:Int)
    fun onAddOrderClick()
    fun onDeleteAllClick()
    fun onVoucherClick()
    fun onMenuItemClick(cartRecommendation: CartRecommendation)
    fun onUsedJPoint(checked: Boolean)
}