package com.jcodonuts.app.ui.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentLoginBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.utils.DialogError
import com.jcodonuts.app.utils.KeyboardUtil
import javax.inject.Inject
import android.content.pm.PackageManager

import android.content.pm.PackageInfo
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import com.bumptech.glide.Glide


class LoginFragment @Inject constructor() : BaseFragment<FragmentLoginBinding, LoginViewModel>() {
    private val TAG = "LoginFragment"

    override fun getViewModelClass(): Class<LoginViewModel> {
        return LoginViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_login

    }

    override fun onViewReady(savedInstance: Bundle?) {
        sharedPreference.apply {
            removeAddress()
            removeCity()
            removePickUp()
            removeSelectCity()
            removeSelectLatitudeAddress()
            removeSelectLongitudeAddress()
            removeSelectPhoneNumber()
        }
        binding.viewModel = viewModel
        binding.executePendingBindings()

        KeyboardUtil(requireActivity(), binding.root)
        binding.btnForgotPassword.setOnClickListener {
            navigateTo(R.string.linkForgot)
        }

        binding.btnRegister.setOnClickListener {
            navigateTo(R.string.linkRegister)
        }

        binding.btnLogin.setOnClickListener {
            binding.edtPhoneNo.error = null
            binding.edtPassword.error = null
            viewModel.login(binding.edtPhoneNo.editText?.text.toString(), binding.edtPassword.editText?.text.toString())
        }

        binding.btnHelpCenter.setOnClickListener {

            val url = getString(R.string.linkWebViewFragment).replace("{url}", "https://order.jcodelivery.com/help/android")
            val uri = Uri.parse(url)
            navigateTo(uri)
        }

        Glide.with(binding.imgJag.context)
            .load("https://cdn.app.jcodelivery.com/img/jagroup.jpg")
            .into(binding.imgJag)

        initObserver()

        try {
            val pInfo = requireContext().packageManager.getPackageInfo(requireContext().packageName, 0)
            val version = pInfo.versionName
            binding.versionName.text = "V$version(${pInfo.versionCode})"
        } catch (e: NameNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onBackPress() {
        goToMainPage()
    }

    private fun initObserver(){
        viewModel.errorPhone.observe(this, {
            it.getContentIfNotHandled()?.let {error->
                binding.edtPhoneNo.error = error
            }
        })
        viewModel.errorPassword.observe(this, {
            it.getContentIfNotHandled()?.let {error->
                binding.edtPassword.error = error
            }
        })

        viewModel.closeLoginPage.observe(this, {
            it.getContentIfNotHandled()?.let {
                goToMainPage()
            }
        })

        viewModel.loginFailed.observe(this, {
            it.getContentIfNotHandled()?.let {message->
                val dlg = DialogError(requireContext())
                dlg.showPopup(
                    message,
                    View.OnClickListener{
                        dlg.dismissPopup()
                    })
            }
        })
    }

    private fun goToMainPage(){
        KeyboardUtil.hideKeyboard(requireActivity())
        navigatePopupInclusiveTo(R.id.loginFragment, R.string.linkMainFragment)
    }
}