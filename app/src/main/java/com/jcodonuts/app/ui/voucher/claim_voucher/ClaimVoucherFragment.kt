package com.jcodonuts.app.ui.voucher.claim_voucher

import android.net.Uri
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentClaimVoucherBinding
import com.jcodonuts.app.ui.base.BaseFragmentWithoutBackPressDispatcher
import de.mateware.snacky.Snacky
import javax.inject.Inject


class ClaimVoucherFragment @Inject constructor() :
    BaseFragmentWithoutBackPressDispatcher<FragmentClaimVoucherBinding, ClaimVoucherViewModel>() {

    override fun getViewModelClass(): Class<ClaimVoucherViewModel> =
        ClaimVoucherViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_claim_voucher

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.topBar.btnBack.setOnClickListener {
            val url = getString(R.string.linkVoucherFragment)
            val uri = Uri.parse(url)
            navigatePopupInclusiveTo(R.id.voucherFragment, uri)
        }


        initRecyclerView()
        initObserver()
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val url = getString(R.string.linkVoucherFragment)
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.voucherFragment, uri)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun initRecyclerView() {
        val controller = ClaimVoucherController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.claimVoucher.observe(this, {
            it.getContentIfNotHandled()?.let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText(R.string.claimed)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_text_secondary
                        )
                    )
                    .success()
                    .show()
            }
        })
    }
}