package com.jcodonuts.app.ui.jcor.payment.choose_payment

interface ChoosePaymentJcoRControllerListener {
    fun onClick(index: Int)
}