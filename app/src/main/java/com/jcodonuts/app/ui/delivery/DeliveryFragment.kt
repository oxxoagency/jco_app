package com.jcodonuts.app.ui.delivery

import android.net.Uri
import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentDeliveryBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.utils.DlgLoadingProgressBar
import javax.inject.Inject

class DeliveryFragment @Inject constructor() :
    BaseFragment<FragmentDeliveryBinding, DeliveryViewModel>() {

    var dialog : DlgLoadingProgressBar? = null

    override fun getViewModelClass(): Class<DeliveryViewModel> {
        return DeliveryViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_delivery
    }

    override fun onViewReady(savedInstance: Bundle?) {
        dialog = DlgLoadingProgressBar(requireContext())
        initActionBar()
        initRecyclerview()
        initObserver()

        if (!isFragmentFromPaused) {
            viewModel.loadLocations()
        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initRecyclerview() {
        val controller = DeliveryController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.openAddAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { data ->
                val url =
                    getString(R.string.linkAddAddressFragment).replace("{id}", data.id.toString())
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.deliveryFragment, uri)
            }
        })

        viewModel.addNewAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val url =
                    getString(R.string.linkAddAddressFragment).replace("{id}", "0")
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.deliveryFragment, uri)
            }
        })

        viewModel.openCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val action =
                    DeliveryFragmentDirections.actionFromDeliveryToMainFragment("cart")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })

        viewModel.showLoading.observe(this, {
            it.getContentIfNotHandled()?.let {state ->
                if (state){
                    dialog?.showPopUp()
                } else {
                    dialog?.dismissPopup()
                }
            }
        })
    }
}