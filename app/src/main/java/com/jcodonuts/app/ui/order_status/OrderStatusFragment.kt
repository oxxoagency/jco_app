package com.jcodonuts.app.ui.order_status

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.TransactionDetailShippingInformation
import com.jcodonuts.app.databinding.FragmentOrderStatusBinding
import javax.inject.Inject


class OrderStatusFragment @Inject constructor() : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentOrderStatusBinding
    private lateinit var listener: DialogOrderStatusListener

    override fun getTheme(): Int {
        return R.style.DialogFullWidth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentOrderStatusBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initComponent()
        when {
            listener.orderStatus().orderStatus.equals("1") -> {
                secondComponent(View.GONE)
                thirdComponent(View.GONE)
                fourthComponent(View.GONE)
            }
            listener.orderStatus().orderStatus.equals("2") || listener.orderStatus().orderStatus.equals(
                "3") || listener.orderStatus().orderStatus.equals("4") || listener.orderStatus().orderStatus.equals(
                "5") -> {
                secondComponent(View.VISIBLE)
                thirdComponent(View.VISIBLE)
                fourthComponent(View.GONE)
            }
            listener.orderStatus().orderStatus.equals("6") -> {
                secondComponent(View.VISIBLE)
                thirdComponent(View.VISIBLE)
                fourthComponent(View.VISIBLE)
            }
        }
    }

    private fun secondComponent(visibility: Int) {
        binding.apply {
            secondCircle.visibility = visibility
            firsLine.visibility = visibility
            secondText.visibility = visibility
            secondTextDetail.visibility = visibility
        }
    }

    private fun thirdComponent(visibility: Int) {
        binding.apply {
            thirdCircle.visibility = visibility
            secondLine.visibility = visibility
            thirdText.visibility = visibility
            thirdTextDetail.visibility = visibility
        }
    }

    private fun fourthComponent(visibility: Int) {
        binding.apply {
            fourthCircle.visibility = visibility
            thirdLine.visibility = visibility
            fourthText.visibility = visibility
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initComponent() {
        binding.apply {
            var textDetail = ""
            btnDlgClose.setOnClickListener { dissmissDialog() }
            secondTextDetail.text =
                "Pembayaran Lunas dengan ${listener.orderStatus().paymentName} \nWaktu Transaksi: ${listener.orderStatus().orderTime}"
            thirdText.text = listener.orderStatus().orderStatusName

            if (listener.orderStatus().orderDelivery?.isEmpty() == false) {
                listener.orderStatus().orderDelivery?.forEach { orderDelivery ->
                    textDetail += if (orderDelivery.courier?.name?.isEmpty() == true) {
                        "${orderDelivery.status_name}"
                    } else {
                        "${orderDelivery.status_name} \nDiantar oleh: ${orderDelivery.courier?.name} (${orderDelivery.courier?.licensePlate}) \nNo.HP: ${orderDelivery.courier?.phone}\n\n"
                    }
                    thirdTextDetail.text = textDetail
                }
            }
        }
    }

    fun showDialog(
        fragmentManager: FragmentManager,
        tag: String?,
        listener: DialogOrderStatusListener,
    ) {
        show(fragmentManager, tag)
        this.listener = listener

    }

    fun dissmissDialog() {
        dialog?.cancel()
    }


    interface DialogOrderStatusListener {
        fun orderStatus(): TransactionDetailShippingInformation
    }
}