package com.jcodonuts.app.ui.jcor.hot_promo

import android.net.Uri
import android.os.Bundle
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentHotPromoJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject


class HotPromoJcoRFragment @Inject constructor() : BaseFragment<FragmentHotPromoJcoRBinding, HotPromoJcoRViewModel>() {

    override fun getViewModelClass(): Class<HotPromoJcoRViewModel> = HotPromoJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_hot_promo_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        setupActionBar()
        initRecycler()
    }


    private fun setupActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initRecycler() {
        val controller = HotPromoJcoRController(viewModel)
        binding.recyclerView.setController(controller)

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.promoSelected.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                controller.promoSelected = data
                val url = getString(R.string.linkBannerDetailJcoRFragment).replace("{id}", data)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }

        })
    }
}