package com.jcodonuts.app.ui.jpoint.jpoint_receive

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jcodonuts.app.data.remote.model.res.JPointRes
import com.jcodonuts.app.databinding.ItemHistoryReceivedJpointBinding

class JPointReceiveAdapter : RecyclerView.Adapter<JPointReceiveAdapter.JPointReceiveViewHolder>() {

    private var jpoints = ArrayList<JPointRes>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): JPointReceiveAdapter.JPointReceiveViewHolder {
        return JPointReceiveViewHolder(ItemHistoryReceivedJpointBinding.inflate(LayoutInflater.from(
            parent.context)))
    }

    override fun onBindViewHolder(
        holder: JPointReceiveAdapter.JPointReceiveViewHolder,
        position: Int,
    ) {
        val jpoint = jpoints[position]
        holder.bind(jpoint)
    }

    override fun getItemCount(): Int {
        return jpoints.size
    }

    internal fun setJPoints(jpoints: ArrayList<JPointRes>){
        this.jpoints = jpoints
        notifyDataSetChanged()
    }

    inner class JPointReceiveViewHolder(private val binding: ItemHistoryReceivedJpointBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(jPointRes: JPointRes) {
            binding.apply {
                data = jPointRes
                executePendingBindings()
            }
        }
    }
}