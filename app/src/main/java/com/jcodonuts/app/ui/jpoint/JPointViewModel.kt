package com.jcodonuts.app.ui.jpoint

import com.jcodonuts.app.data.repository.DirectJcoRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import javax.inject.Inject

class JPointViewModel @Inject constructor(
    private val repository: DirectJcoRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
) : BaseViewModel() {


}