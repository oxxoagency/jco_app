package com.jcodonuts.app.ui.banner_detail

interface BannerDetailControllerListener {
    fun onAddToCart(position: Int)
    fun onClickMenu(position: Int)
}