package com.jcodonuts.app.ui.jcor.main.home

import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.carousel
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*

class HomeController(private val listener: HomeControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var promoSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var menuSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is HomeHeadSectionJcoR -> addHomeHeadSection(cellData, listener)
                is HomePromoHeader -> addHomePromoHeader(listener)
                is HomePromos -> addHomePromos(cellData, listener, index)
//                is HomeMenuHeader -> addHomeMenuHeader()
                is HomeMenuCategories -> addHomeMenuCategory(cellData, listener)
                is HomeMenuItem -> addHomeMenuItem(cellData, listener, index)
                is LoadingProductGrid -> addLoadingProductGrid(cellData)
                is Divider16 -> addLoadingShimmer()
                is LoadingPage -> addLoadingPage(cellData)
            }
        }
    }

    private fun addHomeHeadSection(
        cellData: HomeHeadSectionJcoR,
        listener: HomeControllerListener
    ) {
        homeHeaderJcoR {
            id("header")
            spanSizeOverride { _, _, _ -> 2 }
            onClickListener(listener)
            data(cellData)
        }
    }

    private fun addHomePromoHeader(listener: HomeControllerListener) {
        homePromoHeaderJcoR {
            id("carousel-header")
            spanSizeOverride { _, _, _ -> 2 }
            onClickListener(listener)
        }
    }

    private fun addHomePromos(cellData: HomePromos, listener: HomeControllerListener, index: Int) {

        if (!cellData.promos.isNullOrEmpty()) {
            homePromoJcoR {
                cellData.promos.map {
                    this.id(it.imgURL)
                        .onClickListener(listener)
                        .index(index)
                        .selected(it.menuCode == promoSelected)
                }
                this.data(cellData)
                spanSizeOverride { _, _, _ -> 2 }
            }
        }

    }

    private fun addHomeMenuHeader() {
        homeMenuHeaderJcoR {
            id("menu_header")
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addHomeMenuCategory(
        cellData: HomeMenuCategories,
        listener: HomeControllerListener
    ) {
        val models = cellData.homeMenus.map {
            HomeMenuJcoRBindingModel_()
                .id(it.name)
                .data(it)
                .selected(it.name == menuSelected)
                .onClickListener(listener)
        }

        carousel {
            id("menuHeader")
            padding(Carousel.Padding.dp(16, 4, 16, 16, 8))
            models(models)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addHomeMenuItem(
        cellData: HomeMenuItem,
        listener: HomeControllerListener,
        index: Int
    ) {
        homeMenuItemJcoR {
            id(cellData.imgURL)
            data(cellData)
            index(index)
            onClickListener(listener)
        }
    }

    private fun addLoadingProductGrid(cellData: LoadingProductGrid) {
        lytLoadingProductGrid {
            id(cellData.hashCode())
        }
    }

    private fun addLoadingShimmer(){
        lytLoadingShimmer {
            id("loading")
        }
    }

    private fun addLoadingPage(cellData: LoadingPage) {
        lytLoadingShimmer {
            id(cellData.hashCode())
            spanSizeOverride { _, _, _ -> 2 }
        }
    }
}