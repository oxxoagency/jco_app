package com.jcodonuts.app.ui.jcor.banner_detail

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.bannerDetailPromoJcoR
import com.jcodonuts.app.data.local.Banner
import com.jcodonuts.app.data.local.BaseCell

class BannerDetailJcoRController(private val listener: BannerDetailJcoRControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is Banner -> addDetailBannerPromo(cellData, listener, index)
            }
        }
    }

    private fun addDetailBannerPromo(
        cellData: Banner,
        listener: BannerDetailJcoRControllerListener,
        position: Int
    ) {
        bannerDetailPromoJcoR {
            id(cellData.id)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}