package com.jcodonuts.app.ui.transaction_detail

import com.jcodonuts.app.data.local.TransactionDetailOrderSummary
import com.jcodonuts.app.data.local.TransactionDetailShippingInformation

interface TransactionDetailControllerListener {
    fun onReOrderClick(position: Int)
    fun onCopyClick(transactionDetailOrderSummary: TransactionDetailOrderSummary)
    fun onCopyVAClick(transactionDetailOrderSummary: TransactionDetailOrderSummary)
    fun onHotlineClick()
    fun onCheckOrderStatus(data: TransactionDetailShippingInformation)
}