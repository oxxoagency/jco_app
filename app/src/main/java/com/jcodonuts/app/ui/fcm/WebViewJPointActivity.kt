package com.jcodonuts.app.ui.fcm

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import com.jcodonuts.app.BuildConfig
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.ActivityWebViewJpointBinding
import com.jcodonuts.app.ui.MainActivity
import com.jcodonuts.app.ui.base.BaseActivity
import com.jcodonuts.app.utils.SharedPreference

class WebViewJPointActivity : BaseActivity<ActivityWebViewJpointBinding>() {

    private lateinit var sharedPreference: SharedPreference

    override fun getLayoutId(): Int = R.layout.activity_web_view_jpoint

    override fun onViewReady(savedInstance: Bundle?) {
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorAccent)
        sharedPreference = SharedPreference(this)
        binding.topBar.btnBack.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            finish()
            startActivity(intent)
        }

        binding.webView.apply {
            settings.javaScriptEnabled = true
            webViewClient = CustomWebViewClient()
            loadUrl("${BuildConfig.BASE_URL_LOYALTY}loyalty/detail?point_status=1&token=${
                sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)
            }&comp_id=JID")
        }
        val settings: WebSettings = binding.webView.settings
        settings.domStorageEnabled = true

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val intent = Intent(this@WebViewJPointActivity, MainActivity::class.java)
                finish()
                startActivity(intent)
            }
        }
        this.onBackPressedDispatcher.addCallback(this, callback)
    }

    inner class CustomWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            binding.webView.loadUrl(url.toString())
            return false
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            binding.apply {
                progressBar.visibility = View.VISIBLE
                webView.visibility = View.GONE
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            binding.apply {
                progressBar.visibility = View.GONE
                webView.visibility = View.VISIBLE
            }
        }
    }

}