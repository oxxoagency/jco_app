package com.jcodonuts.app.ui.jcor.main.cart

import android.util.Log
import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.carousel
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.ui.main.cart.CartControllerListener

class CartJcoRController(private val listener: CartJcoRControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var pickUp = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var paymentMethod = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var paymentIcon = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var note = ""
        set(value) {
            field = value
            requestModelBuild()
        }


    var menuSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }


    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is CartSwitch -> addCartSwitch(listener, cellData)
                is CartDeliveryAddress -> addCartDeliveryAddress(cellData, index, listener)
                is CartPickupAddress -> addCartPickupAddress(cellData, index, listener)
                is CartProductJcoR -> addCartProduct(cellData, index, listener)
                is CartRecommendations -> addRecommendation(cellData, listener)
                is EmptyCart -> addEmptyCart(cellData)
                is CartDetail -> addCartDetail(cellData, index, listener)
                is CartTotal -> addCartTotal(cellData, index, listener)
            }
        }
    }

    private fun addCartSwitch(listener: CartJcoRControllerListener, cartSwitch: CartSwitch) {
        cartSwitchJcoR {
            id("switch_cart")
            data(cartSwitch)
            listener(listener)
        }
    }

    private fun addCartDeliveryAddress(
        cellData: CartDeliveryAddress,
        index: Int,
        listener: CartJcoRControllerListener
    ) {
        cartDeliveryAddressJcoR {
            id("delivery_address")
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCartPickupAddress(
        cellData: CartPickupAddress,
        index: Int,
        listener: CartJcoRControllerListener
    ) {
        cartPickupAddressJcoR {
            id("pickup_address")
            data(cellData)
            index(index)
            listener(listener)
            pickUp(pickUp)
        }
    }

    private fun addCartProduct(
        cellData: CartProductJcoR,
        index: Int,
        listener: CartJcoRControllerListener
    ) {
        cartProductJcoR {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart) {
        emptyJcoR {
            id("empty-cart")
            data(cellData)
        }
    }

    private fun addCartDetail(
        cellData: CartDetail,
        index: Int,
        listener: CartJcoRControllerListener
    ) {
        cartDetailJcoR {
            id("cart_detail")
            data(cellData)
            index(index)
            listener(listener)
            paymentMethod(paymentMethod)
            paymentIcon(paymentIcon)
            note(note)
        }
    }

    private fun addRecommendation(
        cellData: CartRecommendations,
        listener: CartJcoRControllerListener,
    ) {
        val models = cellData.recommendations.map {
            CartRecommendationJcoRBindingModel_()
                .id(it.menuCode)
                .data(it)
                .listener(listener)
                .selected(it.menuCode == menuSelected)
        }
        carousel {
            id("menuHeader")
            padding(Carousel.Padding.dp(16, 4, 16, 16, 8))
            models(models)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addCartTotal(cellData: CartTotal, index: Int, listener: CartJcoRControllerListener) {
        cartTotalJcoR {
            id("cart_detail_jco_r")
            data(cellData)
            index(index)
            listener(listener)
        }
    }
}