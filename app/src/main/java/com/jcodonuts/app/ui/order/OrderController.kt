package com.jcodonuts.app.ui.order

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.cartEmpty
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.orderHeader
import com.jcodonuts.app.orderItem
import com.jcodonuts.app.recentOrder
import com.jcodonuts.app.ui.main.recent_order.RecentOrderListener

class OrderController(private val listener: RecentOrderListener) : AsyncEpoxyController() {
    private val TAG = "HomeController"

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when(cellData) {
                is RecentOrder -> addRecentOrder(cellData, index, listener)
                is EmptyCart -> addEmptyCart(cellData)
            }
        }
    }

    private fun addRecentOrder(
        cellData: RecentOrder,
        position: Int,
        listener: RecentOrderListener
    ) {
        recentOrder {
            id(cellData.orderId)
            data(cellData)
            position(position)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart){
        cartEmpty {
            id("empty-cart")
            data(cellData)
        }
    }
}