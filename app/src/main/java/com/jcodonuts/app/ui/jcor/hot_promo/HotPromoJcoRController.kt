package com.jcodonuts.app.ui.jcor.hot_promo

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.ModelHotPromo
import com.jcodonuts.app.emptyJcoR
import com.jcodonuts.app.hotPromoJcoR

class HotPromoJcoRController(private val listener: HotPromoJcoRControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var promoSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is ModelHotPromo -> addModelHotPromo(cellData, listener, index)
                is EmptyCart -> addEmptyCart(cellData)
            }
        }
    }

    private fun addModelHotPromo(
        cellData: ModelHotPromo,
        listener: HotPromoJcoRControllerListener,
        index: Int
    ) {
        hotPromoJcoR {
            id(cellData.img)
            data(cellData)
            selected(cellData.menuCode == promoSelected)
            index(index)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart){
        emptyJcoR {
            id("empty-cart")
            data(cellData)
        }
    }
}