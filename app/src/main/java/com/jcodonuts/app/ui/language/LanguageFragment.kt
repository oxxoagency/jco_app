package com.jcodonuts.app.ui.language

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentLanguageBinding
import com.jcodonuts.app.ui.base.BaseFragmentWithoutBackPressDispatcher
import javax.inject.Inject

class LanguageFragment @Inject constructor() :
    BaseFragmentWithoutBackPressDispatcher<FragmentLanguageBinding, LanguageViewModel>() {

    override fun getViewModelClass(): Class<LanguageViewModel> {
        return LanguageViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_language
    }

    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        binding.apply {
            if (sharedPreference.loadLanguage()?.contains(getString(R.string.indonesia)) == true) {
                radioIndonesia.isChecked = true
            } else {
                radioEnglish.isChecked = true
            }
            radioGroup.setOnCheckedChangeListener { _, i ->
                activity?.recreate()
                when (i) {
                    R.id.radioIndonesia -> sharedPreference.saveLanguage(radioIndonesia.text.toString())
                    R.id.radioEnglish -> sharedPreference.saveLanguage(radioEnglish.text.toString())
                }
            }
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val action =
                    LanguageFragmentDirections.actionFromLanguageToMainFragment("profile")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            val action =
                LanguageFragmentDirections.actionFromLanguageToMainFragment("profile")
            findNavController()
                .navigate(
                    action,
                    FragmentNavigator.Extras.Builder()
                        .build()
                )
        }
    }
}