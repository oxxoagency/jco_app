package com.jcodonuts.app.ui.jcor.voucher

import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentVoucherJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject


class VoucherJcoRFragment @Inject constructor() :
    BaseFragment<FragmentVoucherJcoRBinding, VoucherJcoRViewModel>() {

    override fun getViewModelClass(): Class<VoucherJcoRViewModel> = VoucherJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_voucher_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = VoucherJcoRController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.moreVoucher.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkClaimVoucherFragmentJcoR)
            }
        })

        viewModel.useVoucher.observe(this, {
            it.getContentIfNotHandled()?.let {
                val action =
                    VoucherJcoRFragmentDirections.actionFromVoucherToMainFragment("cart")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })
    }
}