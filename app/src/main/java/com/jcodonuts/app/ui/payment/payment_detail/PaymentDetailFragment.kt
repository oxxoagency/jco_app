package com.jcodonuts.app.ui.payment.payment_detail

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentPaymentDetailBinding
import com.jcodonuts.app.ui.base.BaseFragmentWithoutBackPressDispatcher
import javax.inject.Inject

class PaymentDetailFragment @Inject constructor() : BaseFragmentWithoutBackPressDispatcher<FragmentPaymentDetailBinding, PaymentDetailViewModel>() {

    override fun getViewModelClass(): Class<PaymentDetailViewModel> {
        return PaymentDetailViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_payment_detail
    }

    override fun onViewReady(savedInstance: Bundle?) {
        setupActionBar()
        setupRecyclerview()
        setupObserver()

        arguments?.let {
            it.getString("id")?.let { it1 -> viewModel.loadData(it1) }
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val action =
                        PaymentDetailFragmentDirections.actionFromPaymentDetailToMainFragment("cart")
                findNavController()
                        .navigate(
                                action,
                                FragmentNavigator.Extras.Builder()
                                        .build()
                        )
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun setupRecyclerview() {
        val controller = PaymentDetailController(viewModel)
        binding.recyclerview.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.trackingClick.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkTrackingFragment)
            }
        })
    }

    private fun setupObserver() {
        viewModel.trackingClick.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkTrackingFragment)
            }
        })

        viewModel.orderAgainClick.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkMainFragment)
            }
        })
    }

    private fun setupActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            val action =
                    PaymentDetailFragmentDirections.actionFromPaymentDetailToMainFragment("cart")
            findNavController()
                    .navigate(
                            action,
                            FragmentNavigator.Extras.Builder()
                                    .build()
                    )
        }
    }
}