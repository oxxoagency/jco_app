package com.jcodonuts.app.ui.product_detail

import com.jcodonuts.app.data.local.SizeBeverage
import com.jcodonuts.app.data.local.Variant

interface ProductDetailControllerListener {
    fun onPlusClick(position: Int)
    fun onMinusClick(position: Int)
    fun onDonutPlusClick(positionDetail: Int, positionDonut: Int)
    fun onDonutMinusClick(positionDetail: Int, positionDonut: Int)
    fun onAddToCart()
    fun onBtnFavoriteClick()
    fun onSizeBeverageClick(sizeBeverage: SizeBeverage)
    fun onVariantBeverageClick(variant: Variant)
    fun onMixVariantPlusClick(position: Int)
    fun onMixVariantMinusClick(position: Int)
    fun onVariantDonutPlusClick(position: Int)
    fun onVariantDonutMinusClick(position: Int)
    fun onPackagePlusClick(position: Int)
    fun onPackageMinusClick(position: Int)
    fun onDonutCustomPlusClick(position: Int)
    fun onDonutCustomMinusClick(position: Int)
}