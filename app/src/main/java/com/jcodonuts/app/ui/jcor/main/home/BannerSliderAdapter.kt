package com.jcodonuts.app.ui.jcor.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import com.jcodonuts.app.data.local.PromoBanner
import com.jcodonuts.app.databinding.ItemImagePromoJcoRBinding
import com.smarteist.autoimageslider.SliderViewAdapter

class BannerSliderAdapter(
    private val listener: HomeControllerListener,
    private val selected: Boolean
) : SliderViewAdapter<BannerSliderAdapter.BannerSliderViewHolder>() {

    private var slides: MutableList<PromoBanner> = ArrayList()

    override fun getCount(): Int {
        return slides.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?): BannerSliderAdapter.BannerSliderViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val binding = ItemImagePromoJcoRBinding.inflate(inflater, parent, false)
        return BannerSliderViewHolder(binding)

    }

    override fun onBindViewHolder(
        viewHolder: BannerSliderAdapter.BannerSliderViewHolder?,
        position: Int
    ) {
        viewHolder?.bind(slides[position])
    }

    inner class BannerSliderViewHolder(val binding: ItemImagePromoJcoRBinding) :
        SliderViewAdapter.ViewHolder(binding.root) {
        fun bind(promoBanner: PromoBanner) {
            binding.apply {
                data = promoBanner
                onClickListener = listener
                selected = this@BannerSliderAdapter.selected
                    executePendingBindings()
            }
        }
    }

    internal fun setSlider(slides: MutableList<PromoBanner>) {
        this.slides = slides
        notifyDataSetChanged()
    }

    internal fun addSliderItem(promoBanner: PromoBanner) {
        slides.add(promoBanner)
        notifyDataSetChanged()
    }
}