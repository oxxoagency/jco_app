package com.jcodonuts.app.ui.jcor.banner_detail

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.Banner
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.CartProductJcoR
import com.jcodonuts.app.data.local.RelatedMenu
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.BannerReq
import com.jcodonuts.app.data.remote.model.res.RelatedMenuRes
import com.jcodonuts.app.data.repository.ProductRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.*
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class BannerDetailJcoRViewModel @Inject constructor(
    private val repository: ProductRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val jcoDatabase: JcoDatabase,
    private val app: Application
) : BaseViewModel(), BannerDetailJcoRControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _relatedMenus = MutableLiveData<List<RelatedMenuRes>>()
    val relaatedMenus: LiveData<List<RelatedMenuRes>>
        get() = _relatedMenus

    private val _showMenu = MutableLiveData<SingleEvents<String>>()
    val showMenu: LiveData<SingleEvents<String>>
        get() = _showMenu

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private fun insertToCart(cartProductJcoR: CartProductJcoR): Completable =
        jcoDatabase.cartDao().insertCartProductJcoR(cartProductJcoR)

    fun doInsertToCart(cartProductJcoR: CartProductJcoR) {
        compositeDisposable.add(
            insertToCart(cartProductJcoR).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun loadData(id: String?) {
        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionCode
        val body = BannerReq(2, id, "android", version)

        lastDisposable = repository.getBannerDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                val temp = mutableListOf<BaseCell>()
                val banner = response.data
                temp.add(
                    Banner(
                        id = banner.banner_id,
                        bannerDescription = banner.banner_description,
                        bannerImage = banner.banner_img,
                        promoPeriod = "${
                            banner.start_date?.parseDateAndTimeWithFormat("dd MMM yyyy").toString()
                        } - ${
                            banner.end_date?.parseDateAndTimeWithFormat("dd MMM yyyy").toString()
                        }",
                        relatedMenu = banner.related_menu,
                        version = sharedPreference.loadVersion()
                    )
                )
                banner.related_menu?.map { relatedMenu ->
                    RelatedMenu(
                        id = relatedMenu.id_menu,
                        menuCode = relatedMenu.menu_code,
                        menuName = localizationMenuName(relatedMenu),
                        menuImage = relatedMenu.menu_image,
                        isPromo = relatedMenu.is_promo == "1",
                        price = relatedMenu.menu_price,
                        priceText = Converter.rupiah(relatedMenu.menu_price.toString()),
                    )
                }
                _datas.postValue(temp)
                _relatedMenus.postValue(banner.related_menu ?: mutableListOf())
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationMenuName(product: RelatedMenuRes): String =
        if (sharedPreference.loadLanguage()
                ?.contains("indonesia", true) == true
        ) product.menu_name.toString()
        else product.menu_name_en.toString()

    override fun onAddToCart(position: Int) {
        _datas.value.let {
            val banner = (it)?.get(position) as Banner
            if (banner.relatedMenu?.size ?: 0 <= 1) {
                banner.relatedMenu?.map { relatedMenu ->
                    _addToCart.value = SingleEvents("add-to-cart")
                    doInsertToCart(
                        CartProductJcoR(
                            menuCode = relatedMenu.menu_code?.toInt(),
                            name = relatedMenu.menu_name,
                            imgURL = relatedMenu.menu_image,
                            price = relatedMenu.menu_price?.toDouble() ?: 0.0,
                            productType = "",
                            notes = "",
                            qty = 1,
                            priceOriginal = relatedMenu.menu_price?.toDouble() ?: 0.0
                        )
                    )
                }
            } else {
                _showMenu.value = SingleEvents("show-menu")
            }

        }
    }

    override fun onClickMenu(position: Int) {}
}