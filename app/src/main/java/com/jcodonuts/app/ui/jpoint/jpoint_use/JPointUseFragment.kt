package com.jcodonuts.app.ui.jpoint.jpoint_use

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jcodonuts.app.data.remote.model.res.JPointRes
import com.jcodonuts.app.databinding.FragmentJPointUseBinding
import com.jcodonuts.app.utils.SharedPreference

class JPointUseFragment : Fragment() {

    private lateinit var binding: FragmentJPointUseBinding
    private val adapter by lazy { JPointUsedAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentJPointUseBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreference = SharedPreference(requireContext())
        if (sharedPreference.loadJPointUsed() != null) {
            adapter.setJPoints(sharedPreference.loadJPointUsed()?.data as ArrayList<JPointRes>)
        } else {
            binding.labelNotFound.visibility = View.VISIBLE
        }
        binding.apply {
            recyclerView.apply {
                adapter = this@JPointUseFragment.adapter
                layoutManager = LinearLayoutManager(context)
            }
        }
    }

}