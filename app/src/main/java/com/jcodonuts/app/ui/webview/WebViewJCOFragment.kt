package com.jcodonuts.app.ui.webview

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentWebViewBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject

class WebViewJCOFragment @Inject constructor() :
    BaseFragment<FragmentWebViewBinding, WebViewViewModel>() {

    override fun getViewModelClass(): Class<WebViewViewModel> = WebViewViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_web_view

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        binding.webView.apply {
            settings.javaScriptEnabled = true
            webViewClient = CustomWebViewClient()
            if (!isFragmentFromPaused) {
                arguments?.let {
                    it.getString("url")?.let { it1 -> loadUrl(it1) }
                }
            }
        }
        val settings: WebSettings = binding.webView.settings
        settings.domStorageEnabled = true

        binding.topBar.title = R.string.app_name
    }

    inner class CustomWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            binding.webView.loadUrl(url.toString())
            return false
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            binding.apply {
                progressBar.visibility = View.VISIBLE
                webView.visibility = View.GONE
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            binding.apply {
                progressBar.visibility = View.GONE
                webView.visibility = View.VISIBLE
            }
        }
    }
}