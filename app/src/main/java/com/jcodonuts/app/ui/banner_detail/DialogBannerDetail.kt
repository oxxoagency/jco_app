package com.jcodonuts.app.ui.banner_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jcodonuts.app.R
import com.jcodonuts.app.data.remote.model.res.RelatedMenuRes
import com.jcodonuts.app.databinding.DlgBannerDetailBinding
import javax.inject.Inject

class DialogBannerDetail @Inject constructor() : BottomSheetDialogFragment() {

    private val TAG = DialogBannerDetail::class.java.simpleName

    private lateinit var binding: DlgBannerDetailBinding
    private lateinit var listener: DialogBannerDetailListener
    private val adapter by lazy { BannerMenuAdapter { addToCart(it) } }

    override fun getTheme(): Int {
        return R.style.DialogFullWidth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DlgBannerDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.setData(listener.promoMenu())

        binding.apply {
            recyclerViewBanner.apply {
                adapter = this@DialogBannerDetail.adapter
                layoutManager = LinearLayoutManager(context)
            }
            btnDlgClose.setOnClickListener { dissmissDialog() }
        }
        isCancelable = false
    }

    private fun addToCart(relatedMenuRes: RelatedMenuRes) {
        binding.btnSave.setOnClickListener {
            listener.getPromo(relatedMenuRes)
            dissmissDialog()
        }
    }

    fun showDialog(
        fragmentManager: FragmentManager,
        tag: String?,
        listener: DialogBannerDetailListener
    ) {
        show(fragmentManager, tag)
        this.listener = listener

    }

    fun dissmissDialog() {
        dialog?.cancel()
    }

    interface DialogBannerDetailListener {
        fun promoMenu(): List<RelatedMenuRes>
        fun getPromo(relatedMenuRes: RelatedMenuRes)
    }
}