package com.jcodonuts.app.ui.hot_promo

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.ModelHotPromo
import com.jcodonuts.app.data.remote.model.req.ProductPromoReq
import com.jcodonuts.app.data.remote.model.res.Promo
import com.jcodonuts.app.data.repository.ProductPromoRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import com.jcodonuts.app.utils.parseDateWithFormat
import javax.inject.Inject

class HotPromoViewModel @Inject constructor(
    private val promoRepository: ProductPromoRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : HotPromoControllerListener, BaseViewModel() {

    private val _datas = MutableLiveData<List<BaseCell>>()
    val datas: LiveData<List<BaseCell>>
        get() = _datas

    private var _promoSelected = MutableLiveData<SingleEvents<String>>()
    val promoSelected: LiveData<SingleEvents<String>>
        get() = _promoSelected

    @SuppressLint("CheckResult")
    fun loadData() {
        val temp = _datas.value?.toMutableList() ?: mutableListOf()
        val body = ProductPromoReq(1, "Jakarta Barat", sharedPreference.loadPhoneNumber())
        lastDisposable = promoRepository.getHotPromo(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                model.data?.map {
                    temp.add(
                        ModelHotPromo(
                            menuCode = it.menu_code,
                            img = it.menu_image,
                            productName = localizationPromoName(it),
                            period = "${it.start_date.parseDateWithFormat("dd MMM yyyy")} - ${
                                it.end_date.parseDateWithFormat("dd MMM yyyy")
                            }"
                        )
                    )
                }
                _datas.postValue(temp)
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationPromoName(promo: Promo): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) promo.menu_name
        else promo.menu_name_en

    override fun onClick(modelHotPromo: ModelHotPromo) {
        datas.value?.let {
            _promoSelected.postValue(SingleEvents(modelHotPromo.menuCode ?: ""))
        }
    }
}