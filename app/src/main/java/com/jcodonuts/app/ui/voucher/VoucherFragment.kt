package com.jcodonuts.app.ui.voucher

import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentVoucherBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.delivery.DeliveryFragmentDirections
import javax.inject.Inject

class VoucherFragment @Inject constructor() :
    BaseFragment<FragmentVoucherBinding, VoucherViewModel>() {

    override fun getViewModelClass(): Class<VoucherViewModel> = VoucherViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_voucher

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = VoucherController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.moreVoucher.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkClaimVoucherFragment)
            }
        })

        viewModel.useVoucher.observe(this, {
            it.getContentIfNotHandled()?.let {
                val action =
                    VoucherFragmentDirections.actionFromVoucherToMainFragment("cart")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })
    }

}