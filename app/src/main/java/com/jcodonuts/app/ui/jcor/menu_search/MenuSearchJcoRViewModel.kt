package com.jcodonuts.app.ui.jcor.menu_search

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.req.ProductByCategoryReq
import com.jcodonuts.app.data.remote.model.req.ProductCategoryReq
import com.jcodonuts.app.data.remote.model.req.ProductSearchReq
import com.jcodonuts.app.data.remote.model.res.Product
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.ui.jcor.main.home.HomeControllerListener
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class MenuSearchJcoRViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val app:Application
) : BaseViewModel(), HomeControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _openProductDetail = MutableLiveData<SingleEvents<HomeMenuItem>>()
    val openProductDetail: LiveData<SingleEvents<HomeMenuItem>>
        get() = _openProductDetail

    val menuSelected = MutableLiveData<String>()

    fun loadData() {
        lastDisposable = homeRepository.getProductCategory(ProductCategoryReq(2, "Jakarta Barat"))
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                val temp = mutableListOf<BaseCell>()
                temp.add(CommonSearch("temp"))
                temp.add(
                    MenuSearchTagName(
                        data.data.map { category ->
                            category.category_title
                        },
                        data.data.map { category ->
                            category.category_name
                        },
                    )
                )
                _datas.postValue(temp)

            }, {

            })
    }

    private fun getProductByCategory(category: String) {
        _datas.value?.let {
            it.removeAll { data -> data is HomeMenuItem }
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            _datas.value = it
        }
        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionCode
        val body = ProductByCategoryReq(
            category,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber() ?: "",
            2,
            "android",
            version
        )
        lastDisposable = homeRepository.getProductByCategory(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) {
                    val temp: MutableList<BaseCell> =
                        datas.value?.toMutableList() ?: mutableListOf()
                    temp.removeAll { it is LoadingProductGrid }

                    model.data.map {
                        temp.add(
                            HomeMenuItem(
                                localizationMenuName(it),
                                it.menu_image,
                                it.menu_price,
                                Converter.rupiah(it.menu_price),
                                false,
                                it.is_promo == "1",
                                it.is_freedelivery == "1",
                                false,
                                it.menu_code,
                                normalPrice = Converter.thousandSeparator(it.menu_normalprice.toString())
                            )
                        )
                    }
                    _datas.value = temp
                }
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadSearch(search: String?) {
        _datas.value.let {
            it?.removeAll { data -> data is HomeMenuItem }
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            lastDisposable =
                homeRepository.getProductSearch(ProductSearchReq(2, "Jakarta Barat", search))
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe({ data ->
                        it?.removeAll { loading -> loading is LoadingProductGrid }
                        data.data.map { product ->
                            it?.add(
                                HomeMenuItem(
                                    name = localizationMenuName(product),
                                    imgURL = product.menu_image,
                                    price = product.menu_price,
                                    priceText = Converter.rupiah(product.menu_price),
                                    isStartFrom = false,
                                    isFreeDelivery = product.is_freedelivery == "1",
                                    isFavorite = product.is_favorite == "1",
                                    menuCode = product.menu_code,
                                    isPromo = product.is_promo == "1",
                                    normalPrice = Converter.thousandSeparator(product.menu_normalprice.toString())
                                )
                            )
                        }
                        _datas.postValue(it)
                    }, {

                    })
        }
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    override fun onSwitchAppClick() {}

    override fun onNotificationClick() {}

    override fun onSearchClick() {}

    override fun onPromoSeeAllClick() {}

    override fun onPromosItemClick(promoBanner: PromoBanner) {}

    override fun onMenuCategoryClick(menuCategory: MenuCategory) {}

    override fun onMenuItemClick(index: Int) {
        datas.value?.let {
            _openProductDetail.value = SingleEvents(it[index] as HomeMenuItem)
        }
    }

    override fun onMenuItemFavoriteClick(index: Int) {}

    override fun onSearchItem(query: String) {
        loadSearch(query)
    }

    override fun onSearchMenuCategoryClick(menuSearchTagName: MenuSearchTagName, position: Int) {
        if (menuSearchTagName.name[position] != menuSelected.value) {
            menuSelected.postValue(menuSearchTagName.name[position])
            if (menuSearchTagName.name[position] != "all") {
                getProductByCategory(menuSearchTagName.name[position])
            } else {
                getProductByCategory("")
            }
        }
    }

    override fun onJPointClick() {}
}