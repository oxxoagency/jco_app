package com.jcodonuts.app.ui.order

import android.net.Uri
import android.os.Bundle
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentOrderBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject

class OrderFragment @Inject constructor() : BaseFragment<FragmentOrderBinding, OrderViewModel>() {

    override fun getViewModelClass(): Class<OrderViewModel> {
        return OrderViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_order
    }

    override fun onViewReady(savedInstance: Bundle?) {
        setupActionBar()
        setupRecyclerview()
        initObserver()
    }

    private fun setupRecyclerview(){
        val controller = OrderController(viewModel)
        binding.recyclerview.setController(controller)

        if(!isFragmentFromPaused){
            viewModel.loadOrder()
        }

        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.orderClick.observe(this, {
            it.getContentIfNotHandled()?.let {
//                navigateTo(R.string.linkTopup)
            }
        })
    }

    private fun setupActionBar(){
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initObserver(){
        viewModel.showDetail.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkTransactionDetail).replace("{id}", data.orderId.toString())
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }
}