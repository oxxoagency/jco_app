package com.jcodonuts.app.ui.voucher

interface VoucherControllerListener {
    fun onUsedVoucherClick(position:Int)
    fun moreVoucherClick()
}