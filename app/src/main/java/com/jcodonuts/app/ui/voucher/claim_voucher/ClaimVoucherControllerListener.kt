package com.jcodonuts.app.ui.voucher.claim_voucher

interface ClaimVoucherControllerListener {
    fun onClaimClick(position: Int)
}