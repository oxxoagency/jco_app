package com.jcodonuts.app.ui.location

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jcodonuts.app.BuildConfig
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentLocationBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject


class LocationFragment @Inject constructor() :
    BaseFragment<FragmentLocationBinding, LocationViewModel>() {

    private val TAG = LocationFragment::class.java.simpleName

    private lateinit var request: FindAutocompletePredictionsRequest

    private lateinit var result: StringBuilder

    private val adapter by lazy {
        LocationAdapter {
            getAddress(it)
        }
    }

    override fun getViewModelClass(): Class<LocationViewModel> = LocationViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_location

    @SuppressLint("CheckResult")
    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), BuildConfig.API_KEY_MAPS)
        }

        val placesClient = Places.createClient(requireContext())

        val searchStream = RxTextView.textChanges(binding.searchLocation)
            .map {
                it.toString()
            }

        searchStream.subscribe { query ->
            val token = AutocompleteSessionToken.newInstance()
            val bounds = RectangularBounds.newInstance(
                LatLng(
                    sharedPreference.loadLatitude()?.toDouble() ?: 0.0,
                    sharedPreference.loadLongitude()?.toDouble() ?: 0.0
                ),
                LatLng(
                    sharedPreference.loadLatitude()?.toDouble() ?: 0.0,
                    sharedPreference.loadLongitude()?.toDouble() ?: 0.0
                )
            )

            request =
                FindAutocompletePredictionsRequest.builder() // Call either setLocationBias() OR setLocationRestriction().
                    .setLocationBias(bounds) //.setLocationRestriction(bounds)
                    .setOrigin(
                        LatLng(
                            sharedPreference.loadLatitude()?.toDouble() ?: 0.0,
                            sharedPreference.loadLongitude()?.toDouble() ?: 0.0
                        )
                    )
                    .setCountry("ID")
                    .setTypeFilter(TypeFilter.GEOCODE)
                    .setSessionToken(token)
                    .setQuery(query.toString())
                    .build()
            placesClient.findAutocompletePredictions(request).addOnSuccessListener {
                result = StringBuilder()
                adapter.setPrograms(it.autocompletePredictions)

                for (prediction in it.autocompletePredictions) {
                    result.append(" ").append(prediction.getFullText(null).toString() + "\n\n")
                    Log.i(TAG, prediction.placeId)
                    Log.i(TAG, prediction.getPrimaryText(null).toString())
                    Log.i(TAG, prediction.distanceMeters.toString())
                }
//                binding.result.text = result
                binding.recyclerViewLocation.apply {
                    adapter = this@LocationFragment.adapter
                    layoutManager = LinearLayoutManager(context)
                }
                Log.d("cekresult", result.toString())
            }.addOnFailureListener {
                if (it is ApiException) {
                    val apiException = it
                    Log.e(TAG, "Place not found: " + apiException.statusCode)
                }
            }
        }

//        val autocompleteFragment =
//            childFragmentManager.findFragmentById(R.id.autocomplete_fragment)
//                    as? AutocompleteSupportFragment
//
//        autocompleteFragment?.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME))
//
//        autocompleteFragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
//            override fun onPlaceSelected(place: Place) {
//                Log.i(TAG, "Place: ${place.name}, ${place.id}")
//            }
//
//            override fun onError(status: Status) {
//                // TODO: Handle the error.
//                Log.i(TAG, "An error occurred: $status")
//            }
//        })
        binding.cancel.setOnClickListener {
            binding.searchLocation.text?.clear()
        }
    }

    private fun getAddress(autocompletePrediction: AutocompletePrediction): String =
        "${autocompletePrediction.getFullText(null)}"

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }
}