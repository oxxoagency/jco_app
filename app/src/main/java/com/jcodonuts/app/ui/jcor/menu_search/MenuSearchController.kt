package com.jcodonuts.app.ui.jcor.menu_search

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.ui.jcor.main.home.HomeControllerListener

class MenuSearchController(private val listener: HomeControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var menuSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is CommonSearch -> addCommonSearch(listener)
                is MenuSearchTagName -> addMenuSearchTagName(cellData, index, listener)
                is LoadingProductGrid -> addLoadingProductGrid(cellData)
                is HomeMenuItem -> addHomeMenuItem(cellData, listener, index)
            }
        }
    }

    private fun addCommonSearch(listener: HomeControllerListener) {
        searchviewJcoR {
            id("menuSearchTagview")
            listener(listener)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addMenuSearchTagName(cellData: MenuSearchTagName, position: Int, listener: HomeControllerListener){
        searchTagviewJcoR {
            id(cellData.name[position])
            data(cellData)
            spanSizeOverride { _, _, _ -> 2 }
            selected(cellData.name[position] == menuSelected)
            onClickListener(listener)
            position(position)
        }
    }

    private fun addLoadingProductGrid(cellData: LoadingProductGrid) {
        lytLoadingProductGrid {
            id(cellData.hashCode())
        }
    }

    private fun addHomeMenuItem(cellData: HomeMenuItem, listener: HomeControllerListener, index:Int) {
        homeMenuItemJcoR {
            id(cellData.menuCode)
            data(cellData)
            index(index)
            onClickListener(listener)
        }
    }
}