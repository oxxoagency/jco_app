package com.jcodonuts.app.ui.jcor.product_detail

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.DlgAddToCartJcoRBinding

class DialogAddToCartJcoR(private val context: Context) {
    var dialog: Dialog? = null

    lateinit var binding: DlgAddToCartJcoRBinding

    init {
        dialog = Dialog(context, R.style.AppTheme_AppCompat_Dialog_Alert_NoFloating)
    }

    fun showPopup(
        onClickListenerHome: View.OnClickListener,
        onClickListenerCart: View.OnClickListener,
    ) {

        binding = DlgAddToCartJcoRBinding.inflate(LayoutInflater.from(context))

        binding.dlgButtonHome.setOnClickListener(onClickListenerHome)
        binding.dlgButtonCart.setOnClickListener(onClickListenerCart)
        //initializing dialog screen

        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(false)
        dialog?.setContentView(binding.root)
//        dialog?.window!!.attributes.windowAnimations = R.style.Theme_MaterialComponents_BottomSheetDialog
        dialog?.show()

    }

    fun dismissPopup() = dialog?.let { dialog?.dismiss() }
}