package com.jcodonuts.app.ui.pickup

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.data.repository.AddAddressRepository
import com.jcodonuts.app.data.repository.PickupRepository
import com.jcodonuts.app.data.repository.ProductRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Flowable
import java.math.RoundingMode
import javax.inject.Inject

class PickupViewModel @Inject constructor(
    private val homeRepository: PickupRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val addAddressRepository: AddAddressRepository,
    private val jcoDatabase: JcoDatabase,
    private val productRepository: ProductRepository,
) : BaseViewModel(), PickupControllerListener {
    private val TAG = "PickupViewModel"

    val datas = MutableLiveData<MutableList<BaseCell>>()

    private val _openToCart = MutableLiveData<SingleEvents<String>>()
    val openToCart: LiveData<SingleEvents<String>>
        get() = _openToCart

    private val _cities = MutableLiveData<List<CityDataRes>>()
    val cities: LiveData<List<CityDataRes>>
        get() = _cities

    private val _showCity = MutableLiveData<SingleEvents<String>>()
    val showCity: LiveData<SingleEvents<String>>
        get() = _showCity

    private val _showLoading = MutableLiveData<SingleEvents<Boolean>>()
    val showLoading: LiveData<SingleEvents<Boolean>>
        get() = _showLoading

    fun cartProduct(): Flowable<List<CartProductPickUp>> =
        jcoDatabase.cartDao().getAllCartProductPickUp()

    private fun updatePrice(priceOriginal: Double, price: Double, id: Int): Completable =
        jcoDatabase.cartDao().updatePricePickUp(priceOriginal, price, id)

    init {
        datas.value = mutableListOf()
    }

    @SuppressLint("CheckResult")
    fun loadLocations() {
        val body =
            OutletReq(1, sharedPreference.loadLatitude(), sharedPreference.loadLongitude(), 1)
        lastDisposable = homeRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                datas.value?.let {
                    it.add(LocationSearch("temp"))
                    if (data.status_code == 200) {
                        data.data.map { model ->
                            it.add(
                                PickupItem(
                                    _id = model.outlet_id.toString(),
                                    address = model.outlet_address.toString(),
                                    distance = "${
                                        model.distance?.toDouble()?.toBigDecimal()
                                            ?.setScale(1, RoundingMode.UP)
                                            ?.setScale(1, RoundingMode.UP)
                                    }Km",
                                    placeName = model.outlet_name,
                                    postCode = model.outlet_postcode.toString(),
                                    outletCode = model.outlet_code,
                                    city = model.outlet_city.toString(),
                                    latitude = model.outlet_latitude.toString(),
                                    longiutde = model.outlet_longitude.toString()
                                )
                            )
                        }

                    } else {
                        it.add(EmptyCart(data.error ?: ""))
                    }
                    datas.postValue(it)
                }

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getCity() {
        val body = CityReq(1, "indonesia")
        lastDisposable = addAddressRepository.getCity(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _cities.postValue(it.data)
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getSearchCity(city: String?) {
        val body = CitySearchReq(1, "indonesia", city)
        lastDisposable = addAddressRepository.getCitySearch(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({
                _cities.postValue(it.data)
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getOutletByCity(city: String?) {
        datas.value?.let {
            val body = OutletCityReq(1, city)
            lastDisposable = homeRepository.getOutletByCity(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ res ->
                    it.removeAll { data -> data is PickupItem }
                    res.data.map { model ->
                        it.add(
                            PickupItem(
                                _id = model.outlet_id.toString(),
                                address = model.outlet_address.toString(),
                                distance = "${
                                    model.distance?.toDouble()?.toBigDecimal()
                                        ?.setScale(1, RoundingMode.UP)?.setScale(1, RoundingMode.UP)
                                }Km",
                                placeName = model.outlet_name,
                                postCode = model.outlet_postcode.toString(),
                                outletCode = model.outlet_code,
                                city = model.outlet_city.toString(),
                                latitude = model.outlet_latitude.toString(),
                                longiutde = model.outlet_longitude.toString()
                            )
                        )
                    }
                    datas.value = it
                }, {

                })
        }
    }

    private fun getItemCarts(city: String) {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({
                it.forEach { cartProduct ->
                    loadProductDetail(
                        cartProduct.menuCode.toString(),
                        city,
                        cartProduct.qty,
                        cartProduct.id
                    )
                }
            }, {

            })
        Handler(Looper.getMainLooper()).postDelayed({
            _showLoading.value = SingleEvents(false)
            _openToCart.value = SingleEvents("open-cart")
        }, 1000)
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadProductDetail(menuCode: String, city: String, qty: Int, id: Int) {
        val body = ProductDetailReq(
            "1",
            city,
            menuCode,
            sharedPreference.loadPhoneNumber().toString()
        )

        lastDisposable = productRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                val data = response.data
                val price = data.menu_price?.toInt()?.times(qty)

                doUpdateCart(
                    data.menu_price?.toDouble() ?: 0.0,
                    price?.toDouble() ?: 0.0,
                    id
                )


//                priceList.add(data.menu_price.toDouble())
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doUpdateCart(priceOriginal: Double, price: Double, id: Int) {
        lastDisposable =
            updatePrice(priceOriginal, price, id)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe()
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onClick(index: Int) {
        datas.value.let {
            _showLoading.value = SingleEvents(true)
            val pickUp = (it?.get(index) as PickupItem).copy()
            sharedPreference.savePickUp(pickUp.placeName)
            sharedPreference.savePostCode(pickUp.postCode)
            sharedPreference.saveOutletCode(pickUp.outletCode)
            sharedPreference.saveOutletId(pickUp._id)
            sharedPreference.savePickUpAddress(pickUp.address)
            sharedPreference.savePickUpCity(pickUp.city)
            sharedPreference.saveSelectLatitudePickUp(pickUp.latitude)
            sharedPreference.saveSelectLongitudePickup(pickUp.longiutde)
            Log.d("cekpickupcity", pickUp.city)
            sharedPreference.saveStatePickup(true)
//            _openToCart.value = SingleEvents("open-cart")
            getItemCarts(sharedPreference.loadPickUpCity().toString())
        }
    }

    override fun onFavoriteClick(index: Int) {

    }

    override fun onSearchClick() {
        _showCity.value = SingleEvents("show-city")
    }
}