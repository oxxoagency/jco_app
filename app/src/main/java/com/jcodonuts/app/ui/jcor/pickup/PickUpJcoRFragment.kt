package com.jcodonuts.app.ui.jcor.pickup

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.databinding.FragmentPickUpJcoRBinding
import com.jcodonuts.app.ui.add_address.AddAddressFragment
import com.jcodonuts.app.ui.add_address.DialogCity
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.jcor.add_address.DialogCityJcoR
import com.jcodonuts.app.ui.pickup.PickupController
import com.jcodonuts.app.ui.pickup.PickupFragmentDirections
import javax.inject.Inject


class PickUpJcoRFragment @Inject constructor() :
    BaseFragment<FragmentPickUpJcoRBinding, PickUpJcoRViewModel>() {

    override fun getViewModelClass(): Class<PickUpJcoRViewModel> = PickUpJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_pick_up_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        initRecyclerview()
        initObserver()

        if (!isFragmentFromPaused) {
            viewModel.loadLocations()
        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initRecyclerview() {
        val controller = PickUpJcoRController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.openToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                parentFragmentManager.setFragmentResult(
                    "pickup", // Same request key FragmentA used to register its listener
                    bundleOf("pickup" to sharedPreference.loadPickUpJcoR()) // The data to be passed to FragmentA
                )
                parentFragmentManager.popBackStack()
            }
        })

        viewModel.showCity.observe(this, {
            it.getContentIfNotHandled()?.let {
                viewModel.getCity()
                var searchCity = emptyList<CityDataRes>()
                val dlg = DialogCityJcoR()
                viewModel.cities.observe(this, { cities ->
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        AddAddressFragment::class.java.simpleName,
                        object : DialogCityJcoR.DialogCityListener {
                            override fun getCity(): List<CityDataRes> = cities
                            override fun saveCity(city: CityDataRes): String {
                                viewModel.getOutletByCity(city.city)
                                return city.city.toString()
                            }
                            override fun searchCity(city: String): List<CityDataRes> {
                                if (city.isNotEmpty()) {
                                    viewModel.getSearchCity(city)
                                }

                                viewModel.cities.observe(viewLifecycleOwner, {
                                    searchCity = it
                                })
                                return searchCity
                            }
                        })
                })
            }
        })
    }
}