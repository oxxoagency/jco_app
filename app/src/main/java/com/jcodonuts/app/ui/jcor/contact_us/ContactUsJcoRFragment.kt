package com.jcodonuts.app.ui.jcor.contact_us

import android.os.Bundle
import androidx.lifecycle.observe
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentContactUsJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject


class ContactUsJcoRFragment @Inject constructor() :
    BaseFragment<FragmentContactUsJcoRBinding, ContactUsJcoRViewModel>() {

    override fun getViewModelClass(): Class<ContactUsJcoRViewModel> =
        ContactUsJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_contact_us_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        initRecyclerview()
        initObserver()

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }
    }


    private fun initRecyclerview() {
        val controller = ContactUsJcoRController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.setData(it)
        })
    }

    private fun initObserver() {

    }
}