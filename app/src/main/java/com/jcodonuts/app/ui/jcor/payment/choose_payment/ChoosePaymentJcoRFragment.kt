package com.jcodonuts.app.ui.jcor.payment.choose_payment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentChoosePaymentJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.payment.choose_payment.ChoosePaymentController
import com.jcodonuts.app.ui.payment.choose_payment.ChoosePaymentFragmentDirections
import javax.inject.Inject


class ChoosePaymentJcoRFragment @Inject constructor() :
    BaseFragment<FragmentChoosePaymentJcoRBinding, ChoosePaymentJcoRViewModel>() {

    override fun getViewModelClass(): Class<ChoosePaymentJcoRViewModel> =
        ChoosePaymentJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_choose_payment_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        setupActionBar()
        setupRecyclerview()
        initObserver()
    }

    private fun setupActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun setupRecyclerview() {
        val controller = ChoosePaymentJcoRController(viewModel)
        binding.recyclerview.setController(controller)

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.openCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                parentFragmentManager.popBackStack()
            }
        })
    }
}