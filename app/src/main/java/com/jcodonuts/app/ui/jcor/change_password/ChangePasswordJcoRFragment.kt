package com.jcodonuts.app.ui.jcor.change_password

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentChangePasswordJcoRBinding
import com.jcodonuts.app.ui.auth.change_password.ChangePasswordViewModel
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.utils.KeyboardUtil
import de.mateware.snacky.Snacky
import javax.inject.Inject


class ChangePasswordJcoRFragment @Inject constructor() :
    BaseFragment<FragmentChangePasswordJcoRBinding, ChangePasswordViewModel>() {

    override fun getViewModelClass(): Class<ChangePasswordViewModel> =
        ChangePasswordViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_change_password_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        KeyboardUtil(requireActivity(), binding.root)
        initObserver()
        initActionBar()

        binding.apply {
            btnSave.setOnClickListener {
                edtConfirmPassword.error = null
                edtNewPassword.error = null
                edtConfirmPassword.error = null

                viewModel.changePassword(
                    edtCurrentPassword.editText?.text.toString(),
                    edtNewPassword.editText?.text.toString(),
                    edtConfirmPassword.editText?.text.toString())
            }

            btnCancel.setOnClickListener { onBackPress() }
        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }


    private fun initObserver() {
        viewModel.successChangePassword.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled().let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText(getString(R.string.success_change_password))
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_text_secondary
                        )
                    )
                    .success()
                    .show()

                onBackPress()
            }
        })

        viewModel.failChangePassword.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled().let { message ->
                Snacky.builder()
                    .setActivity(activity)
                    .setText(message)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.c_text_secondary
                        )
                    )
                    .success()
                    .show()
            }
        })
    }
}