package com.jcodonuts.app.ui.main.home

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.DlgUpdateAppBinding
import com.jcodonuts.app.utils.dpToPx

class DialogUpdateApp(private val context: Context) {
    var dialog: Dialog? = null

    lateinit var binding: DlgUpdateAppBinding

    init {
        dialog = Dialog(context, R.style.AppTheme_AppCompat_Dialog_Alert_NoFloating)
    }

    fun showPopup(
        image: String,
        onClickListenerUpdate: View.OnClickListener,
        onClickListenerPass: View.OnClickListener,
        mandatory: Boolean
    ) {

        binding = DlgUpdateAppBinding.inflate(LayoutInflater.from(context))
        Glide.with(binding.imgTop.context)
            .load(image)
            .transform(CenterInside(), RoundedCorners(dpToPx(16)))
            .into(binding.imgTop)
        if (mandatory) binding.dlgButtonPass.visibility =
            View.GONE else binding.dlgButtonPass.visibility = View.VISIBLE
        binding.dlgButton.setOnClickListener(onClickListenerUpdate)
        binding.dlgButtonPass.setOnClickListener(onClickListenerPass)
        //initializing dialog screen

        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(false)
        dialog?.setContentView(binding.root)
//        dialog?.window!!.attributes.windowAnimations = R.style.Theme_MaterialComponents_BottomSheetDialog
        dialog?.show()

    }

    fun dismissPopup() = dialog?.let { dialog?.dismiss() }

}