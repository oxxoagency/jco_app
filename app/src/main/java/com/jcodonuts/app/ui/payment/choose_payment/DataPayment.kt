package com.jcodonuts.app.ui.payment.choose_payment

import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.EWallet
import com.jcodonuts.app.data.local.OtherPayment
import com.jcodonuts.app.data.local.TransferVirtualAccount

object DataPayment {

    fun eWallet(): List<EWallet> {
        val eWallets = ArrayList<EWallet>()

        eWallets.add(EWallet("Gopay", "gopay", R.drawable.logo_gopay))
        eWallets.add(EWallet("Shopee Pay", "shopeepay", R.drawable.logo_shopee_pay))

        return eWallets
    }

    fun transferVirtualAccount(): List<TransferVirtualAccount> {
        val transferVirtualAccounts = ArrayList<TransferVirtualAccount>()

        transferVirtualAccounts.add(
            TransferVirtualAccount(
                "BCA Virtual Account",
                "bca_va",
                R.drawable.logo_bca
            )
        )
        return transferVirtualAccounts
    }

    fun otherPayment(): List<OtherPayment> {
        val otherPayments = ArrayList<OtherPayment>()

        otherPayments.add(OtherPayment("Credit Cart", "credit_card", R.drawable.logo_visa))

        return otherPayments
    }
}