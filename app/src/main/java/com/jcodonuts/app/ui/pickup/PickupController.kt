package com.jcodonuts.app.ui.pickup

import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.EpoxyController
import com.jcodonuts.app.cartEmpty
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.LocationSearch
import com.jcodonuts.app.data.local.PickupItem
import com.jcodonuts.app.locationSearchview
import com.jcodonuts.app.pickupItem

/**
 * Showcases [EpoxyController] with sticky header support
 */
class PickupController(
    private val listener: PickupControllerListener
) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when(cellData) {
                is LocationSearch -> addLocationSearch(listener)
                is PickupItem -> addPickupItem(cellData, listener, index)
                is EmptyCart -> addEmptyCart(cellData)
            }
        }
    }

    private fun addLocationSearch(listener: PickupControllerListener){
        locationSearchview {
            id("locationSearch")
            listener(listener)
        }
    }

    private fun addPickupItem(cellData: PickupItem, listener: PickupControllerListener, index:Int){
        pickupItem {
            id(cellData._id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart){
        cartEmpty {
            id("empty-cart")
            data(cellData)
        }
    }
}
