package com.jcodonuts.app.ui.splash

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.BuildConfig
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.repository.AuthRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.EspressoIdlingResource
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val jcoDatabase: JcoDatabase
): BaseViewModel() {

    private val _complete = MutableLiveData<SingleEvents<Boolean>>()

    val complete : LiveData<SingleEvents<Boolean>>
        get() = _complete

    private fun deleteAll(): Completable =
        jcoDatabase.cartDao().deleteAll()

    private fun deleteAllPickUp(): Completable =
        jcoDatabase.cartDao().deleteAllPickUp()

    lateinit var disposable:Disposable

    fun countSplash(){
        disposable = Observable.timer(3, TimeUnit.SECONDS)
            .doOnSubscribe { EspressoIdlingResource.increment() }
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally {
                if(!EspressoIdlingResource.getIdlingResource().isIdleNow){
                    EspressoIdlingResource.decrement()
                }
            }
            .subscribe{
                _complete.value = SingleEvents(true)
            }
    }

    fun loadData() {
        lastDisposable = authRepository.getRefreshToken()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                countSplash()
                if (it.status == 200) {
                    sharedPreference.save(SharedPreference.ACCESS_TOKEN, it.access_token)
                }
            }, {
                _complete.value = SingleEvents(true)
            })
    }

    fun setCleanDB() {
        if (sharedPreference.loadVersion() != BuildConfig.VERSION_CODE) {
            lastDisposable = deleteAll().subscribeOn(schedulers.io())
                .observeOn(schedulers.io())
                .subscribe({}, {})
            lastDisposable?.let { compositeDisposable.add(it) }

            lastDisposable = deleteAllPickUp().subscribeOn(schedulers.io())
                .observeOn(schedulers.io())
                .subscribe({}, {})
            lastDisposable?.let { compositeDisposable.add(it) }

            sharedPreference.removeItemDetail()
        }
        Log.d("CHECK VERSION", "CHECK ${sharedPreference.loadVersion()}")
    }

    fun dispose(){
        disposable.dispose()
    }
}