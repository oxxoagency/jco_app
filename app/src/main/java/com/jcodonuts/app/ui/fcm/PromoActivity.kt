package com.jcodonuts.app.ui.fcm

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.CartProduct
import com.jcodonuts.app.data.local.CartProductPickUp
import com.jcodonuts.app.data.remote.model.res.RelatedMenuRes
import com.jcodonuts.app.databinding.ActivityPromoBinding
import com.jcodonuts.app.ui.MainActivity
import com.jcodonuts.app.ui.banner_detail.BannerDetailController
import com.jcodonuts.app.ui.banner_detail.BannerDetailViewModel
import com.jcodonuts.app.ui.banner_detail.DialogBannerDetail
import com.jcodonuts.app.ui.base.BaseActivity
import de.mateware.snacky.Snacky

class PromoActivity : BaseActivity<ActivityPromoBinding>() {

    private val viewModel by viewModels<BannerDetailViewModel> { viewModelFactory }

    override fun getLayoutId(): Int = R.layout.activity_promo

    override fun onViewReady(savedInstance: Bundle?) {
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorAccent)
        viewModel.loadData(intent.getStringExtra("id"))
        binding.topBar.btnBack.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            finish()
            startActivity(intent)
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val intent = Intent(this@PromoActivity, MainActivity::class.java)
                finish()
                startActivity(intent)
            }
        }
        this.onBackPressedDispatcher.addCallback(this, callback)

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = BannerDetailController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.addToCart.observe(this, {
            it.getContentIfNotHandled()?.let {
                Snacky.builder()
                    .setActivity(this)
                    .setText("Sukses ditambahkan ke keranjang")
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.c_text_secondary
                        )
                    )
                    .success()
                    .show()
            }
        })

        viewModel.showMenu.observe(this, { showMenu ->
            viewModel.relaatedMenus.observe(this, { data ->
                showMenu.getContentIfNotHandled()?.let {
                    val dlg = DialogBannerDetail()
                    dlg.showDialog(
                        this.supportFragmentManager,
                        "BannerDetailFragment",
                        object : DialogBannerDetail.DialogBannerDetailListener {
                            override fun promoMenu(): List<RelatedMenuRes> {
                                return data
                            }

                            override fun getPromo(relatedMenuRes: RelatedMenuRes) {
                                viewModel.doInsertToCart(
                                    CartProduct(
                                        menuCode = relatedMenuRes.menu_code?.toInt(),
                                        name = relatedMenuRes.menu_name,
                                        imgURL = relatedMenuRes.menu_image,
                                        price = relatedMenuRes.menu_price?.toDouble() ?: 0.0,
                                        productType = "",
                                        notes = "",
                                        qty = 1,
                                        priceOriginal = relatedMenuRes.menu_price?.toDouble()
                                            ?: 0.0,
                                        detailVariant = mutableListOf()
                                    )
                                )

                                viewModel.doInsertToCartPickUp(
                                    CartProductPickUp(
                                        menuCode = relatedMenuRes.menu_code?.toInt(),
                                        name = relatedMenuRes.menu_name,
                                        imgURL = relatedMenuRes.menu_image,
                                        price = relatedMenuRes.menu_price?.toDouble() ?: 0.0,
                                        productType = "",
                                        notes = "",
                                        qty = 1,
                                        priceOriginal = relatedMenuRes.menu_price?.toDouble()
                                            ?: 0.0,
                                        detailVariant = mutableListOf()
                                    )
                                )
                                Snacky.builder()
                                    .setActivity(this@PromoActivity)
                                    .setText("Sukses ditambahkan ke keranjang")
                                    .setDuration(Snacky.LENGTH_SHORT)
                                    .setActionText(R.string.close)
                                    .setBackgroundColor(
                                        ContextCompat.getColor(
                                            this@PromoActivity,
                                            R.color.c_text_secondary
                                        )
                                    )
                                    .success()
                                    .show()
                            }
                        }
                    )
                }
            })
        })
        viewModel.update.observe(this, {
            openAppOnPlayStore(this, null)
        })
    }

    private fun openAppOnPlayStore(ctx: Context, packageName: String?) {
        var packageName = packageName
        if (packageName == null) {
            packageName = ctx.packageName
        }
        val uri = Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
        openURI(ctx, uri, "Play Store not found in your device")
    }

    private fun openURI(
        ctx: Context,
        uri: Uri?,
        errorMessage: String?,
    ) {
        val i = Intent(Intent.ACTION_VIEW, uri)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (ctx.packageManager.queryIntentActivities(i, 0).size > 0) {
            ctx.startActivity(i)
        } else if (errorMessage != null) {
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
        }
    }
}