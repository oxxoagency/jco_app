package com.jcodonuts.app.ui.delivery

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.DeleteAddressReq
import com.jcodonuts.app.data.remote.model.req.ProductDetailReq
import com.jcodonuts.app.data.repository.DeliveryRepository
import com.jcodonuts.app.data.repository.ProductRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class DeliveryViewModel @Inject constructor(
    private val deliveryRepository: DeliveryRepository,
    private val schedulers: SchedulerProvider,
    private val gson: Gson,
    private val sharedPreference: SharedPreference,
    private val productRepository: ProductRepository,
    private val jcoDatabase: JcoDatabase
) : BaseViewModel(), DeliveryControllerListener {

    val datas = MutableLiveData<MutableList<BaseCell>>()
//    val datas = MutableLiveData<SingleEvents<MemberAddress>>()

    private val _phoneNumber = MutableLiveData<String>()
    val phoneNumber: LiveData<String>
        get() = _phoneNumber

    private val _openAddAddress = MutableLiveData<SingleEvents<AddressItem>>()
    val openAddAddress: LiveData<SingleEvents<AddressItem>>
        get() = _openAddAddress

    private val _addNewAddress = MutableLiveData<SingleEvents<String>>()
    val addNewAddress: LiveData<SingleEvents<String>>
        get() = _addNewAddress

    private val _openCart = MutableLiveData<SingleEvents<String>>()
    val openCart: LiveData<SingleEvents<String>>
        get() = _openCart

    private val _showLoading = MutableLiveData<SingleEvents<Boolean>>()
    val showLoading: LiveData<SingleEvents<Boolean>>
        get() = _showLoading

    fun cartProduct(): Flowable<List<CartProduct>> =
        jcoDatabase.cartDao().getAllCartProduct()

    private fun updatePrice(priceOriginal: Double, price: Double, id: Int): Completable =
        jcoDatabase.cartDao().updatePrice(priceOriginal, price, id)

    val priceList = mutableListOf<Double>()

    init {
        datas.value = mutableListOf()
    }

    @SuppressLint("CheckResult")
    fun loadLocations() {
        val temp = mutableListOf<BaseCell>()
//        temp.add(DeliveryWebView("http://192.168.1.12:8888/oxxo/jco/product_detail?code=1112004"))

        val body = MemberAddressReq(sharedPreference.loadPhoneNumber().toString())
        lastDisposable = deliveryRepository.getMemberAddress(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                //val temp = datas.value?.toMutableList() ?: mutableListOf()

                model.data?.map {
                    temp.add(
                        AddressItem(
                            id = it.member_address_id,
                            labelAddress = it.address_label,
                            address = it.address,
                            detailAddress = it.address_details,
                            phoneNumber = it.recipient_phone,
                            name = it.recipient_name,
                            city = it.cityordistrict,
                            latitude = it.latitude,
                            longitude = it.longitude
                        )
                    )
                }

                temp.add(SavePlace(""))
                datas.value = temp
                Log.d("DATA__", gson.toJson(model))
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    private fun loadProductDetail(menuCode: String, city: String, qty: Int, id: Int) {
        val body = ProductDetailReq(
            "1",
            city,
            menuCode,
            sharedPreference.loadPhoneNumber().toString()
        )

        lastDisposable = productRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                val data = response.data
                val price: Int?
                if (response.status == 200) {
                    price = data.menu_price?.toInt()?.times(qty)
                    doUpdateCart(
                        data.menu_price?.toDouble() ?: 0.0,
                        price?.toDouble() ?: 0.0,
                        id
                    )
                }

//                priceList.add(data.menu_price.toDouble())
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun getItemCarts(city: String) {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({
                it.forEach { cartProduct ->
                    loadProductDetail(
                        cartProduct.menuCode.toString(),
                        city,
                        cartProduct.qty,
                        cartProduct.id
                    )
                }
                Log.d("cekpricelist", priceList.toString())
            }, {

            })
        Handler(Looper.getMainLooper()).postDelayed({
            _showLoading.value = SingleEvents(false)
            _openCart.value = SingleEvents("open-cart")
        }, 1000)
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doUpdateCart(priceOriginal: Double, price: Double, id: Int) {
        lastDisposable =
            updatePrice(priceOriginal, price, id)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe()
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onClick(index: Int) {

    }

    override fun onFavoriteClick(index: Int) {

    }

    override fun onClickUpdateAddress(index: Int) {
        datas.value?.let {
            _openAddAddress.value = SingleEvents(it[index] as AddressItem)
        }
    }

    override fun onClickAddAddress() {
        datas.value?.let {
            _addNewAddress.value = SingleEvents("add-address")
        }
    }

    override fun onClickSaveDeliveryAddress(index: Int) {
        datas.value.let {
            _showLoading.value = SingleEvents(true)
            val address = it?.get(index) as AddressItem
            sharedPreference.saveAddress(address.address.toString())
            sharedPreference.saveDetailAddress(address.detailAddress.toString())
            sharedPreference.saveSelectCity(address.city.toString())
            sharedPreference.saveSelectLatitudeAddress(address.latitude)
            sharedPreference.saveSelectLongitudeAddress(address.longitude)
            sharedPreference.saveSelectPhoneNumber(address.phoneNumber.toString())
            sharedPreference.saveRecipientName(address.name.toString())
            getItemCarts(sharedPreference.loadSelectCity().toString())
        }
    }

    override fun onDeleteAddress(index: Int) {
        datas.value.let { datas ->
            val address = datas?.get(index) as AddressItem
            lastDisposable = deliveryRepository.deleteAddress(DeleteAddressReq(address.id))
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    datas.removeAll { data -> data is AddressItem }
                    loadLocations()
                }, {

                })
        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }
}