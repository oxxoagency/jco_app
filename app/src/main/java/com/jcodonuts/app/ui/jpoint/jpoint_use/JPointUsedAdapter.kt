package com.jcodonuts.app.ui.jpoint.jpoint_use

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jcodonuts.app.data.remote.model.res.JPointRes
import com.jcodonuts.app.databinding.ItemHistoryUsedJpointBinding

class JPointUsedAdapter : RecyclerView.Adapter<JPointUsedAdapter.JPointUsedViewHolder>() {

    private var jpoints = ArrayList<JPointRes>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): JPointUsedAdapter.JPointUsedViewHolder {
        return JPointUsedViewHolder(ItemHistoryUsedJpointBinding.inflate(LayoutInflater.from(
            parent.context)))
    }

    override fun onBindViewHolder(
        holder: JPointUsedAdapter.JPointUsedViewHolder,
        position: Int,
    ) {
        val jpoint = jpoints[position]
        holder.bind(jpoint)
    }

    override fun getItemCount(): Int {
        return jpoints.size
    }

    internal fun setJPoints(jpoints: ArrayList<JPointRes>) {
        this.jpoints = jpoints
        notifyDataSetChanged()
    }

    inner class JPointUsedViewHolder(private val binding: ItemHistoryUsedJpointBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(jPointRes: JPointRes) {
            binding.apply {
                data = jPointRes
                executePendingBindings()
            }
        }
    }
}