package com.jcodonuts.app.ui.transaction_detail

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*

class TransactionDetailController(private val listener: TransactionDetailControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is TransactionDetailHeader -> addTransactionDetailHeader(cellData)
                is TransactionDetailShippingInformation -> addTransactionDetailShippingInformation(
                    cellData,
                    listener
                )
                is TransactionDetailOrderSummary -> addTransactionDetailOrderSummary(
                    cellData,
                    listener
                )
                is TransactionDetailProduct -> addTransactionDetailProduct(cellData)
                is TransactionDetailTotal -> addTransactionDetailTotal(cellData, listener, index)
            }
        }
    }

    private fun addTransactionDetailHeader(cellData: TransactionDetailHeader) {
        transactionDetailHeader {
            id("transaction-detail-header")
            data(cellData)
        }
    }

    private fun addTransactionDetailShippingInformation(
        cellData: TransactionDetailShippingInformation,
        listener: TransactionDetailControllerListener,
    ) {
        transactionDetailShippingInformation {
            id(cellData.id)
            data(cellData)
            listener(listener)
        }
    }

    private fun addTransactionDetailOrderSummary(
        cellData: TransactionDetailOrderSummary,
        listener: TransactionDetailControllerListener,
    ) {
        transactionDetailOrderSummary {
            id(cellData.orderId)
            data(cellData)
            listener(listener)
        }
    }

    private fun addTransactionDetailProduct(
        cellData: TransactionDetailProduct,
    ) {
        transactionDetailProduct {
            id(cellData.nameProduct)
            data(cellData)
        }
    }

    private fun addTransactionDetailTotal(
        cellData: TransactionDetailTotal,
        listener: TransactionDetailControllerListener,
        position: Int,
    ) {
        transactionDetailTotal {
            id(cellData.total)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}