package com.jcodonuts.app.ui.edit_profile

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.ProfileData
import com.jcodonuts.app.data.remote.model.req.UpdateProfileReq
import com.jcodonuts.app.data.repository.AuthRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class EditProfileViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val schedulers: SchedulerProvider,
    private val app: Application,
    private val sharedPreference: SharedPreference
) : BaseViewModel() {

    private val _datas = MutableLiveData<ProfileData>()
    val datas: LiveData<ProfileData>
        get() = _datas

    private val _showProfile = MutableLiveData<SingleEvents<String>>()
    val showProfile: LiveData<SingleEvents<String>>
        get() = _showProfile

    fun loadData() {
        lastDisposable = authRepository.memberMe()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                val data = ProfileData(
                    "https://drive.google.com/uc?id=1gtrQXUFKsrkexSwWA9eN3Mh2Xtau3S3p",
                    model.data.member_name.toString(),
                    model.data.member_phone,
                    if (sharedPreference.loadLanguage()
                            ?.contains("indonesia", true) == true && model.data.member_gender == 1
                    ) "Pria"
                    else if (sharedPreference.loadLanguage()
                            ?.contains("indonesia", true) == true && model.data.member_gender == 2
                    ) "Perempuan"
                    else if (sharedPreference.loadLanguage()
                            ?.contains("indonesia", true) == false && model.data.member_gender == 1
                    ) "Male"
                    else if (sharedPreference.loadLanguage()
                            ?.contains("indonesia", true) == false && model.data.member_gender == 2
                    ) "Female"
                    else "Pilih Jenis Kelamin",
                    model.data.member_dob
                )
                _datas.postValue(data)

            }, { throwable ->
                handleError(throwable)
            })


        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun editProfile(data: ProfileData) {
        val body = UpdateProfileReq(
            member_name = data.name,
            member_phone = data.phone,
            member_phone2 = "",
            member_gender = if (data.gender?.equals(
                    "pria",
                    true
                ) == true
            ) 1
            else if (data.gender?.equals("perempuan", true) == true) 2
            else if (data.gender?.equals("male", true) == true) 1
            else 2,
            member_dob = data.dateOfBirth,
            member_photo = "https://drive.google.com/uc?id=1gtrQXUFKsrkexSwWA9eN3Mh2Xtau3S3p"
        )

        lastDisposable = authRepository.updateProfile(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status_code == 200) _showProfile.value = SingleEvents("show-profile")
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }
}