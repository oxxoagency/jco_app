package com.jcodonuts.app.ui.jcor.banner_detail

interface BannerDetailJcoRControllerListener {
    fun onAddToCart(position: Int)
    fun onClickMenu(position: Int)
}