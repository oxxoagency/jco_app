package com.jcodonuts.app.ui.jcor.main.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.DlgCartProdcutNoteJcoRBinding

class DialogNoteJcoR : BottomSheetDialogFragment() {
    private lateinit var listener: OnDialogClickListener
    private lateinit var binding: DlgCartProdcutNoteJcoRBinding
    override fun getTheme(): Int {
        return R.style.DialogFullWidth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DlgCartProdcutNoteJcoRBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        isCancelable = false
        binding.btnDlgClose.setOnClickListener {
            dialog?.cancel()
        }
        binding.edtNote.setText(listener.onNotes())
        binding.btnSave.setOnClickListener {
            listener.onSaveClick(binding.edtNote.text.toString())
        }

    }

    fun showDialog(
        fragmentManager: FragmentManager,
        tag: String?,
        listener: OnDialogClickListener
    ) {
        show(fragmentManager, tag)
        this.listener = listener
    }

    fun dissmissDialog() {
        dialog?.cancel()
    }

    interface OnDialogClickListener {
        fun onSaveClick(notes: String)
        fun onNotes(): String?
    }

}