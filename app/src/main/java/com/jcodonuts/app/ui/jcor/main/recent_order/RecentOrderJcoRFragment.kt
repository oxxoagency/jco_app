package com.jcodonuts.app.ui.jcor.main.recent_order

import android.net.Uri
import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentRecentOrderJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.base.InjectingNavHostFragment
import com.jcodonuts.app.ui.jcor.base.MainJcoRFragment
import javax.inject.Inject


class RecentOrderJcoRFragment @Inject constructor() :
    BaseFragment<FragmentRecentOrderJcoRBinding, RecentOrderJcoRViewModel>() {

    override fun getViewModelClass(): Class<RecentOrderJcoRViewModel> =
        RecentOrderJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_recent_order_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = RecentOrderJcoRController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.showDetail.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkTransactionDetailJcoR).replace(
                    "{id}",
                    data.orderId.toString()
                )
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })

        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val action = RecentOrderJcoRFragmentDirections.actionRecentOrderToCart()
                findNavController().navigate(
                    action,
                    FragmentNavigator.Extras.Builder()
                        .build()
                )
            }
        })
    }

    override fun onBackPress() {
        val navhost = (parentFragment as InjectingNavHostFragment)
        (navhost.parentFragment as MainJcoRFragment).backToHome()
    }
}