package com.jcodonuts.app.ui.detail_promo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.CartProduct
import com.jcodonuts.app.data.local.CartProductPickUp
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.ProductDetailReq
import com.jcodonuts.app.data.remote.model.res.ProductDetailRes
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.data.repository.ProductRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class DetailPromoViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val jcoDatabase: JcoDatabase
) : BaseViewModel() {

    private val _detail = MutableLiveData<ProductDetailRes>()
    val detail: LiveData<ProductDetailRes> get() = _detail

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>> get() = _addToCart

    private fun insertToCart(cartProduct: CartProduct): Completable =
        jcoDatabase.cartDao().insertCart(cartProduct)

    private fun insertToCartPickUp(cartProduct: CartProductPickUp): Completable =
        jcoDatabase.cartDao().insertCartPickUp(cartProduct)

    fun loadDetail(id: String) {
        val body = ProductDetailReq(
            "1",
            "Jakarta Barat",
            id,
            sharedPreference.loadPhoneNumber().toString()
        )

        lastDisposable = productRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                _detail.postValue(data)
            }, {
                handleError(it)
            })
    }

    fun doInsertToCart(cartProduct: CartProduct) {
        compositeDisposable.add(
            insertToCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun doInsertToCartPickUp(cartProduct: CartProductPickUp) {
        compositeDisposable.add(
            insertToCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun onAddToCart() {
        _addToCart.value = SingleEvents("addToCart")
    }

}