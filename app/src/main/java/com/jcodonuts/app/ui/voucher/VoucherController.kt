package com.jcodonuts.app.ui.voucher

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.Voucher
import com.jcodonuts.app.data.local.VoucherHeader
import com.jcodonuts.app.voucherHeader
import com.jcodonuts.app.voucherItems

class VoucherController(private val listener: VoucherControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is VoucherHeader -> addCouponHeader(listener)
                is Voucher -> addCouponList(cellData, index, listener)
            }
        }
    }

    private fun addCouponList(cellData: Voucher, index: Int, listener: VoucherControllerListener) {
        voucherItems {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCouponHeader(listener: VoucherControllerListener) {
        voucherHeader {
            id("header")
            listener(listener)
        }
    }
}