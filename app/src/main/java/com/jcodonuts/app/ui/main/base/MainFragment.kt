package com.jcodonuts.app.ui.main.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentMainBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.main.cart.CartFragment
import javax.inject.Inject

class MainFragment @Inject constructor() : BaseFragment<FragmentMainBinding, MainViewModel>() {
    private val TAG = "MainFragment"

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_main
    }

    override fun onViewReady(savedInstance: Bundle?) {
        setUpNavigation()
        initObserver()
        viewModel.loginCheck()

        setFragmentResultListener("pickup") { requestKey, bundle ->
            // We use a String here, but any type that can be put in a Bundle is supported
            val result = bundle.getString("pickup")
            Log.d("cekpickup", "check $result")
            childFragmentManager.setFragmentResult(
                "pickupB", // Same request key FragmentA used to register its listener
                bundleOf("pickupB" to result) // The data to be passed to FragmentA
            )
        }
    }

    private fun setUpNavigation() {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.navHostFragmentMain) as NavHostFragment

        val navController = navHostFragment.navController
        binding.bottomNavigation.setupWithNavController(navController)
        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            NavigationUI.onNavDestinationSelected(item, navController)
        }
        binding.bottomNavigation.setOnNavigationItemReselectedListener {

        }

        arguments?.let {
            if (it.getString("menu").equals("cart")) {
                binding.bottomNavigation.selectedItemId = R.id.cart
                it.putString("menu", "")
            } else if (it.getString("menu").equals("profile")) {
                binding.bottomNavigation.selectedItemId = R.id.profile
                it.putString("menu", "")
            }
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.home) showStatusBar() else transparentStatusBar()
        }
    }

    private fun initObserver() {
        viewModel.showBottomNavigation.observe(this, {
            it.getContentIfNotHandled()?.let {
                binding.bottomNavigation.visibility = View.VISIBLE
            }
        })
        viewModel.hideBottomNavigation.observe(this, {
            it.getContentIfNotHandled()?.let {
                binding.bottomNavigation.visibility = View.GONE
            }
        })

        viewModel.sizeCart.observe(viewLifecycleOwner, {
            val badge = binding.bottomNavigation.getOrCreateBadge(R.id.cart)
            badge.isVisible = it > 0
            badge.number = it
        })
    }

    fun backToHome() {
        binding.bottomNavigation.selectedItemId = R.id.home
    }

    private fun showStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window? = activity?.window
            window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window?.statusBarColor = resources.getColor(R.color.colorPrimary)
        }
    }

    private fun transparentStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window? = activity?.window
            window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window?.statusBarColor = Color.TRANSPARENT
        }
    }
}