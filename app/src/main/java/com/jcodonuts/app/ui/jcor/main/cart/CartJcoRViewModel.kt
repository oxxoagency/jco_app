package com.jcodonuts.app.ui.jcor.main.cart

import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.webkit.WebView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.CartOrderRes
import com.jcodonuts.app.data.remote.model.res.Product
import com.jcodonuts.app.data.repository.CartRepository
import com.jcodonuts.app.data.repository.DirectJcoRepository
import com.jcodonuts.app.data.repository.PaymentRepository
import com.jcodonuts.app.data.repository.TransactionRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.*
import com.midtrans.sdk.corekit.models.ItemDetails
import io.reactivex.Completable
import io.reactivex.Flowable
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class CartJcoRViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val jcoDatabase: JcoDatabase,
    private val schedulers: SchedulerProvider,
    private val transactionRepository: TransactionRepository,
    private val directJcoRepository: DirectJcoRepository,
    private val app: Application,
    private val paymentRepository: PaymentRepository,
    private val cartRepository: CartRepository,
    private val context: Context,
) : BaseViewModel(), CartJcoRControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _grossAmount = MutableLiveData<Double>()
    val grossAmount: LiveData<Double>
        get() = _grossAmount

    private val _cartOrders = MutableLiveData<MutableList<ItemDetails>>()
    val cartOrders: LiveData<MutableList<ItemDetails>>
        get() = _cartOrders

    private val _errorMessage = MutableLiveData<SingleEvents<String>>()
    val errorMessage: LiveData<SingleEvents<String>>
        get() = _errorMessage

    private val _showDelivery = MutableLiveData<SingleEvents<String>>()
    val showDelivery: LiveData<SingleEvents<String>>
        get() = _showDelivery

    private val _reFetchCart = MutableLiveData<SingleEvents<String>>()
    val reFetchCart: LiveData<SingleEvents<String>>
        get() = _reFetchCart

    private val _removeFromCart = MutableLiveData<SingleEvents<String>>()
    val removeFromCart: LiveData<SingleEvents<String>>
        get() = _removeFromCart

    private val _showDialogNote = MutableLiveData<SingleEvents<String>>()
    val showDialogNote: LiveData<SingleEvents<String>>
        get() = _showDialogNote

    private val _showChangePayment = MutableLiveData<SingleEvents<String>>()
    val showChangePayment: LiveData<SingleEvents<String>>
        get() = _showChangePayment

    private val _showVoucher = MutableLiveData<SingleEvents<String>>()
    val showVoucher: LiveData<SingleEvents<String>>
        get() = _showVoucher

    private val _showPickup = MutableLiveData<SingleEvents<String>>()
    val showPickup: LiveData<SingleEvents<String>>
        get() = _showPickup

    private val _showLoading = MutableLiveData<SingleEvents<Boolean>>()
    val showLoading: LiveData<SingleEvents<Boolean>>
        get() = _showLoading

    private val _orderId = MutableLiveData<Int?>()
    val orderId: LiveData<Int?>
        get() = _orderId

    private val _showMidtrans = MutableLiveData<SingleEvents<String>>()
    val showMidtrans: LiveData<SingleEvents<String>>
        get() = _showMidtrans

    private val _alertTime = MutableLiveData<SingleEvents<String>>()
    val alertTIme: LiveData<SingleEvents<String>>
        get() = _alertTime

    private val _showAddOrder = MutableLiveData<SingleEvents<String>>()
    val showAddOrder: LiveData<SingleEvents<String>>
        get() = _showAddOrder

    private val _openProductDetail = MutableLiveData<SingleEvents<String>>()
    val openProductDetail: LiveData<SingleEvents<String>>
        get() = _openProductDetail

    private val _showPaymentDetail = MutableLiveData<SingleEvents<String>>()
    val showPaymentDetail: LiveData<SingleEvents<String>>
        get() = _showPaymentDetail

    fun cartProduct(): Flowable<List<CartProductJcoR>> =
        jcoDatabase.cartDao().getAllCartProductJcoR()

    private fun updateCart(cartProduct: CartProductJcoR): Completable =
        jcoDatabase.cartDao().updateCartJcoR(cartProduct)

    private fun deleteCart(cartProduct: CartProductJcoR): Completable =
        jcoDatabase.cartDao().deleteCartJcoR(cartProduct)

    private fun deleteAll(): Completable =
        jcoDatabase.cartDao().deleteAllJcoR()

    private fun deleteAllPickUp(): Completable =
        jcoDatabase.cartDao().deleteAllPickUp()

    private var deliveryType = 0
    private var orderDetail: MutableList<CreateOrderDetailReq> = mutableListOf()
    private var isUseJPoint = false
    val menuSelected = MutableLiveData<String>()

    fun loadData() {
        val temp = mutableListOf<BaseCell>()
        temp.add(CartSwitch(false))
        temp.add(
            CartDeliveryAddress(
                sharedPreference.loadAddress().toString(),
                sharedPreference.loadPickUpJcoR().toString()
            )
        )
        _datas.postValue(temp)
        Log.d("tag", temp.toString())
    }

    private fun loadCreateOrder(deliveryType: Int?) {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val currentDateAndTime: String = sdf.format(Date())

        val body = if (deliveryType == 0) {
            CreateOrderReq(
                brand = 2,
                order_type = "8",
                payment_type = "3",
                delivery_type = deliveryType,
                member_name = sharedPreference.loadName(),
                member_email = sharedPreference.loadEmail(),
                member_phone = sharedPreference.loadPhoneNumber(),
                member_phone2 = sharedPreference.loadPhoneNumberTwo(),
                order_trx_time = currentDateAndTime,
                order_city = sharedPreference.loadSelectCity(),
                order_address = sharedPreference.loadAddress(),
                order_address_info = sharedPreference.loadDetailAddress(),
                order_postcode = sharedPreference.loadOrderPosttCodeForDelivery(), //hardcode for test emu
                order_latitude = sharedPreference.loadSelectLatitudeAddress(), //hardcode for test emu
                order_longitude = sharedPreference.loadSelectLongitudeAddress(), //hardcode for test emu
                order_note = sharedPreference.loadNotes(),
                order_company = "JID",
                order_outlet_ID = sharedPreference.loadOutletIdForDeliveryJcoR(), //hardcode for test emu
                order_outlet_code = sharedPreference.loadOutletCodeForDeliveryJcoR(), //hardcode for test emu
                order_ip = NetworkUtils.getIPAddress(true),
                order_useragent = WebView(context).settings.userAgentString,
                order_detail = orderDetail,
                coupon_code = sharedPreference.loadCodeVoucher(),
                order_payment = sharedPreference.loadPaymentMethod(),
                order_brand = "JCOR",
                order_recipient_name = sharedPreference.loadRecipientName(),
                order_recipient_phone = sharedPreference.loadSelectPhoneNumber(),
                use_jpoint = if (isUseJPoint) 1 else 0
            )
        } else {
            CreateOrderReq(
                brand = 2,
                order_type = "8",
                payment_type = "3",
                delivery_type = deliveryType,
                member_name = sharedPreference.loadName(),
                member_email = sharedPreference.loadEmail(),
                member_phone = sharedPreference.loadPhoneNumber(),
                member_phone2 = sharedPreference.loadPhoneNumberTwo(),
                order_trx_time = currentDateAndTime,
                order_city = sharedPreference.loadSelectCity(),
                order_address = sharedPreference.loadPickUpAddress(),
                order_address_info = sharedPreference.loadPickUpAddress(),
                order_postcode = sharedPreference.loadPostCode(), //hardcode for test emu
                order_latitude = sharedPreference.loadSelectLatitudePickUp(), //hardcode for test emu
                order_longitude = sharedPreference.loadSelectLongitudePickup(), //hardcode for test emu
                order_note = sharedPreference.loadNotes(),
                order_company = "JID",
                order_outlet_ID = sharedPreference.loadOutletId(), //hardcode for test emu
                order_outlet_code = sharedPreference.loadOutletCode(), //hardcode for test emu
                order_ip = NetworkUtils.getIPAddress(true),
                order_useragent = WebView(context).settings.userAgentString,
                order_detail = orderDetail,
                coupon_code = sharedPreference.loadCodeVoucher(),
                order_payment = sharedPreference.loadPaymentMethod(),
                order_brand = "JCOR",
                order_recipient_name = sharedPreference.loadName(),
                order_recipient_phone = sharedPreference.loadPhoneNumber(),
                use_jpoint = if (isUseJPoint) 1 else 0
            )
        }

        lastDisposable = transactionRepository.getCreateOrder(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _showLoading.value = SingleEvents(false)
                try {
                    _orderId.value = it.data.order_id
                } catch (e: Exception) {
                    _orderId.value = 0
                }
                if (it.status_code == 200) {
                    Log.d("CHECK", sharedPreference.loadJPoint().toString() + _grossAmount)
                    if (isUseJPoint && sharedPreference.loadJPoint()
                            ?.toInt() ?: 0 >= _grossAmount.value?.toInt() ?: 0
                    ) {
                        _showPaymentDetail.value = SingleEvents("show-payment-detail")
                    } else {
                        _showMidtrans.value = SingleEvents("show-midtrans")
                    }
                } else if (it.status_code == 404) {
                    _errorMessage.value =
                        SingleEvents("Alamat yang dipilih di luar radius pengantaran kami")
                } else {
                    if (!it.error?.order_address.isNullOrEmpty()) {
                        _errorMessage.value =
                            SingleEvents("Tidak tersedia di tempat anda atau ${it.error?.order_address}")
                    } else if (!it.error?.order_outlet_code.isNullOrEmpty()) {
                        _errorMessage.value =
                            SingleEvents("Tidak tersedia di tempat anda atau ${it.error?.order_outlet_code}")
                    } else if (!it.error?.order_outlet_ID.isNullOrEmpty()) {
                        _errorMessage.value =
                            SingleEvents("Tidak tersedia di tempat anda atau ${it.error?.order_outlet_ID}")
                    } else if (!it.error?.order_payment.isNullOrEmpty()) {
                        _errorMessage.value =
                            SingleEvents("Tidak tersedia di tempat anda atau ${it.error?.order_payment}")
                    } else if (!it.error?.message.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.message.toString())
                    }
                }

            }, {
                _showLoading.value = SingleEvents(false)
                _errorMessage.value =
                    SingleEvents("Alamat yang dipilih di luar radius pengantaran kami")
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun getAllItemCartForSwitch() {

        datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            it?.removeAll { data -> data is EmptyCart }
            it?.removeAll { data -> data is CartRecommendations }
            it?.removeAll { data -> data is CartTotal }
            it?.removeAll { data -> data is HeaderRecommendationMenu }
            _datas.value = it
        }
        if (deliveryType == 0) {
            lastDisposable = cartProduct().subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .take(1)
                .subscribe({ data ->
                    datas.value.let { temp ->
                        if (data.isNullOrEmpty()) {
                            temp?.add(EmptyCart(app.getString(R.string.cart_is_empty)))
                        }
                        data.map { cartProduct ->
                            temp?.add(
                                CartProductJcoR(
                                    cartProduct.id,
                                    cartProduct.menuCode,
                                    cartProduct.name,
                                    cartProduct.imgURL,
                                    cartProduct.price,
                                    cartProduct.productType,
                                    cartProduct.notes,
                                    cartProduct.qty,
                                    cartProduct.priceOriginal,
                                    cartProduct.variantname
                                )
                            )
                        }
                        val cartInfo = mutableListOf<CartInfo>()
                        data.map { cartProduct ->
                            cartInfo.add(
                                CartInfo(
                                    cartProduct.menuCode.toString(),
                                    cartProduct.qty.toString()
                                )
                            )
                        }
                        orderDetail.clear()
                        data.map { cartProduct ->
                            orderDetail.add(
                                CreateOrderDetailReq(
                                    menu_code = cartProduct.menuCode.toString(),
                                    menu_name = cartProduct.name,
                                    menu_quantity = cartProduct.qty.toString(),
                                    material_unit_price = cartProduct.priceOriginal.toString(),
                                    material_sub_total = cartProduct.price.toString(),
                                    menu_detail = cartProduct.variantname
                                )
                            )
                        }
                        loadCart(cartInfo, sharedPreference.loadSelectCity() ?: "")

                        _datas.value = temp
                        Log.d("tag", temp.toString())
                    }
                }, {
                    handleError(it)
                })
        } else {
            lastDisposable = cartProduct().subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .take(1)
                .subscribe({ data ->
                    datas.value.let { temp ->
                        if (data.isNullOrEmpty()) {
                            temp?.add(EmptyCart(app.getString(R.string.cart_is_empty)))
                        }
                        data.map { cartProduct ->
                            temp?.add(
                                CartProductJcoR(
                                    cartProduct.id,
                                    cartProduct.menuCode,
                                    cartProduct.name,
                                    cartProduct.imgURL,
                                    cartProduct.price,
                                    cartProduct.productType,
                                    cartProduct.notes,
                                    cartProduct.qty,
                                    cartProduct.priceOriginal,
                                    cartProduct.variantname
                                )
                            )
                        }
                        val cartInfo = mutableListOf<CartInfo>()
                        data.map { cartProduct ->
                            cartInfo.add(
                                CartInfo(
                                    cartProduct.menuCode.toString(),
                                    cartProduct.qty.toString()
                                )
                            )
                        }
                        orderDetail.clear()
                        data.map { cartProduct ->
                            orderDetail.add(
                                CreateOrderDetailReq(
                                    menu_code = cartProduct.menuCode.toString(),
                                    menu_name = cartProduct.name,
                                    menu_quantity = cartProduct.qty.toString(),
                                    material_unit_price = cartProduct.priceOriginal.toString(),
                                    material_sub_total = cartProduct.price.toString(),
                                    menu_detail = cartProduct.variantname
                                )
                            )
                        }
                        loadCart(cartInfo, sharedPreference.loadPickUpCity() ?: "")

                        _datas.value = temp
                        Log.d("tag", temp.toString())
                    }
                }, {
                    handleError(it)
                })
        }


        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getAllItemCart() {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({ data ->
                Log.d("tag", data.toString())
                sharedPreference.saveDeliveryType("0")
                val temp = mutableListOf<BaseCell>()
                temp.add(CartSwitch(false))
                temp.add(
                    CartDeliveryAddress(
                        sharedPreference.loadAddress().toString(),
                        sharedPreference.loadPickUpJcoR().toString()
                    )
                )
                if (data.isNullOrEmpty()) {
                    temp.add(EmptyCart(app.getString(R.string.cart_is_empty)))
                }
                data.map { cartProduct ->
                    temp.add(
                        CartProductJcoR(
                            cartProduct.id,
                            cartProduct.menuCode,
                            cartProduct.name,
                            cartProduct.imgURL,
                            cartProduct.price,
                            cartProduct.productType,
                            cartProduct.notes,
                            cartProduct.qty,
                            cartProduct.priceOriginal,
                            cartProduct.variantname
                        )
                    )
                }
                val cartInfo = mutableListOf<CartInfo>()
                data.map { cartProduct ->
                    cartInfo.add(
                        CartInfo(
                            cartProduct.menuCode.toString(),
                            cartProduct.qty.toString()
                        )
                    )
                }
                orderDetail.clear()
                data.map { cartProduct ->
                    orderDetail.add(
                        CreateOrderDetailReq(
                            menu_code = cartProduct.menuCode.toString(),
                            menu_name = cartProduct.name,
                            menu_quantity = cartProduct.qty.toString(),
                            material_unit_price = cartProduct.priceOriginal.toString(),
                            material_sub_total = cartProduct.price.toString(),
                            menu_detail = cartProduct.variantname
                        )
                    )
                }
                loadCart(cartInfo, sharedPreference.loadSelectCity() ?: "")

                _datas.postValue(temp)
                Log.d("tag", temp.toString())

            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadCart(cartInfo: MutableList<CartInfo>, city: String) {
        val body = CartOrder(
            2,
            sharedPreference.loadSelectCity(),
            deliveryType,
            sharedPreference.loadCodeVoucher(),
            sharedPreference.loadPaymentMethod(),
            "JCOR",
            cartInfo,
            0,
            sharedPreference.loadAddress(),
            sharedPreference.loadDetailAddress(),
            sharedPreference.loadSelectLatitudeAddress(),
            sharedPreference.loadSelectLongitudeAddress()
        )

        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = pInfo.versionCode
        val bodyRecommendation = MenuRecommendationReq(
            city = city,
            order_brand = "JCOR",
            cart_info = cartInfo,
            app_os = "android",
            app_version = version
        )
        lastDisposable = cartRepository.getCartOrders(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .flatMap { model ->
                Log.d("cekmodel", model.toString())
                _datas.value.let {
                    if (model.status_code == 200) {
                        saveCartDetail(model)
                        it?.add(
                            CartDetail(
                                paymentName = sharedPreference.loadPaymentName().toString(),
                                paymentIcon = sharedPreference.loadPaymentIcon().toString(),
                                price = Converter.rupiah(
                                    model.data.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    model.data.delivery_fee?.toDouble() ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    model.data.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                notes = sharedPreference.loadNotes(),
                                ecobag = model.data.ecoBag?.ecobag_name,
                                ecobagPrice = Converter.rupiah(
                                    model.data.ecoBag?.ecobag_price.toString(),
                                ),
                                ecobagSubTotal = Converter.rupiah(
                                    model.data.ecoBag?.ecobag_subtotal?.toDouble() ?: 0.0
                                ),
                                ecobagQty = model.data.ecoBag?.ecobag_quantity.toString(),
                                voucher = sharedPreference.loadVoucher(),
                                promo = model.data.promo?.promo_value,
                                freeDeliveryText = Converter.rupiah(
                                    model.data.freeDelivery?.toDouble() ?: 0.0
                                ),
                                freeDelivery = model.data.freeDelivery,
                                deliveryService = model.data.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    model.data.total_delivery_fee?.toDouble() ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jPoint = Converter.thousandSeparator(sharedPreference.loadJPoint() ?: ""),
                                jPointInt = sharedPreference.loadJPoint()?.toInt() ?: 0
                            )
                        )
                        _grossAmount.value = model.data.grandtotal?.toDouble()
                        val tempCart = mutableListOf<ItemDetails>()
                        model.data.cart_orders.map { cartProduct ->
                            tempCart.add(
                                ItemDetails(
                                    cartProduct.menu_code.toString(),
                                    cartProduct.menu_price?.toDouble() ?: 0.0,
                                    cartProduct.menu_quantity?.toInt() ?: 0,
                                    cartProduct.menu_name
                                )
                            )
                        }
                        tempCart.add(
                            ItemDetails(
                                "1111111111-000-1",
                                model.data.delivery_fee?.toDouble() ?: 0.0,
                                1,
                                "Delivery Fee"
                            )
                        )

                        if (model.data.ecoBag?.ecobag_subtotal ?: 0 > 0) {
                            tempCart.add(
                                ItemDetails(
                                    model.data.ecoBag?.ecobag_code,
                                    model.data.ecoBag?.ecobag_price?.toDouble() ?: 0.0,
                                    model.data.ecoBag?.ecobag_quantity ?: 0,
                                    model.data.ecoBag?.ecobag_name
                                )
                            )
                        }

                        if (model.data.promo?.promo_value != 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-2",
                                    model.data.promo?.promo_value?.toDouble() ?: 0.0,
                                    1,
                                    "Promo"
                                )
                            )
                        }

                        if (model.data.cart_orders.isNullOrEmpty()) {
                            _errorMessage.value =
                                SingleEvents("Harap ubah alamat pengiriman karena berada di luar jangkauan")
                        } else if (!model.data.invalid_menu.isNullOrEmpty()) {
                            var items = ""
                            model.data.invalid_menu.map { invalidMenu ->
                                items = invalidMenu.menu_name.toString()
                            }
                            _errorMessage.value =
                                SingleEvents("Harap ubah pesanan Anda karena produk ini tidak tersedia $items")
                        }

                        _cartOrders.value = tempCart
                    } else {
                        sharedPreference.removeCodeVoucher()
                        sharedPreference.removeVoucher()
                        it?.add(
                            CartDetail(
                                paymentName = sharedPreference.loadPaymentName().toString(),
                                paymentIcon = sharedPreference.loadPaymentIcon().toString(),
                                price = sharedPreference.loadPriceCart(),
                                deliveryServiceText = sharedPreference.loadDeliveryFeeCart(),
                                totalPrice = sharedPreference.loadTotalPriceCart(),
                                notes = sharedPreference.loadNotes(),
                                ecobag = sharedPreference.loadEcobagCart(),
                                ecobagPrice = sharedPreference.loadEcobagPriceCart(),
                                ecobagSubTotal = sharedPreference.loadEcobagSubtotalCart(),
                                ecobagQty = sharedPreference.loadEcobagQtyCart(),
                                voucher = sharedPreference.loadVoucher(),
                                promo = 0,
                                freeDeliveryText = sharedPreference.loadFreeDelivery(),
                                freeDelivery = sharedPreference.loadFreeDelivery()?.toInt(),
                                deliveryService = sharedPreference.loadDeliveryFeeCart()?.toInt(),
                                totalDeliveryFee = sharedPreference.loadTotalDeliveryFee(),
                                isDelivery = deliveryType == 0,
                                jPoint = Converter.thousandSeparator(sharedPreference.loadJPoint() ?: ""),
                                jPointInt = sharedPreference.loadJPoint()?.toInt() ?: 0
                            )
                        )

                        _errorMessage.value = SingleEvents(model.error.toString())
                        _grossAmount.value = sharedPreference.loadGrossAmount()?.toDouble()
                        Log.d("cekitemdetails", sharedPreference.loadItemDetailJcoR().toString())
                        val tempCart = mutableListOf<ItemDetails>()
                        sharedPreference.loadItemDetailJcoR()?.cart_orders?.map { cartProduct ->
                            tempCart.add(
                                ItemDetails(
                                    cartProduct.menu_code.toString(),
                                    cartProduct.menu_price?.toDouble() ?: 0.0,
                                    cartProduct.menu_quantity?.toInt() ?: 0,
                                    cartProduct.menu_name
                                )
                            )
                        }
                        tempCart.add(
                            ItemDetails(
                                "1111111111-000-1",
                                sharedPreference.loadItemDetailJcoR()?.delivery_fee?.toDouble() ?: 0.0,
                                1,
                                "Delivery Fee"
                            )
                        )

                        if (!sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name.isNullOrEmpty()) {
                            tempCart.add(
                                ItemDetails(
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_code,
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_price?.toDouble()
                                        ?: 0.0,
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_quantity ?: 0,
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name
                                )
                            )
                        }

                        if (sharedPreference.loadItemDetailJcoR()?.promo?.promo_value != 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-2",
                                    sharedPreference.loadItemDetailJcoR()?.promo?.promo_value?.toDouble()
                                        ?: 0.0,
                                    1,
                                    "Promo"
                                )
                            )
                        }

                        _cartOrders.value = tempCart
                    }
                    _datas.value = it
                }
                directJcoRepository.getMenuRecommendation(bodyRecommendation)
            }
            .subscribe({ data ->
                val response = data.data
                val recommendation = mutableListOf<CartRecommendation>()
                _datas.value.let {
                    response?.map { product ->
                        recommendation.add(CartRecommendation(
                            menuCode = product.menu_code,
                            name = localizationMenuName(product),
                            image = product.menu_img,
                            price = Converter.thousandSeparator(product.menu_price),
                            isFavorite = false
                        ))
                    }
                    if (!recommendation.isNullOrEmpty()) {
                        it?.add(HeaderRecommendationMenu())
                        it?.add(CartRecommendations(recommendation))
                    }

                    if (sharedPreference.loadItemDetailJcoR() != null) {
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetailJcoR()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetailJcoR()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetailJcoR()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointUsed = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_used
                                    ?: 0,
                                jpointEarn = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                            )
                        )
                    }
                    Log.d("cekrecom", it.toString())
                    _datas.value = it
                }

            }, {
                handleError(it)
                datas.value.let {
                    if (sharedPreference.loadItemDetailJcoR() != null) {
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetailJcoR()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetailJcoR()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetailJcoR()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointUsed = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_used
                                    ?: 0,
                                jpointEarn = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                            )
                        )
                        _datas.value = it
                    }
                }
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun saveCartDetail(model: CartOrderRes) {
        sharedPreference.apply {
            savePriceCart(
                Converter.rupiah(
                    model.data.subtotal?.toDouble()
                        ?: 0.0
                )
            )
            saveDeliveryFeeCart(
                Converter.rupiah(
                    model.data.delivery_fee?.toDouble() ?: 0.0
                )
            )
            saveTotalPriceCart(
                Converter.rupiah(
                    model.data.grandtotal?.toDouble()
                        ?: 0.0
                )
            )
            saveEcobagCart(model.data.ecoBag?.ecobag_name.toString())
            saveEcobagPriceCart(
                Converter.rupiah(
                    model.data.ecoBag?.ecobag_price.toString(),
                )
            )
            saveEcobagSubtotalCart(
                Converter.rupiah(
                    model.data.ecoBag?.ecobag_subtotal?.toDouble() ?: 0.0
                )
            )
            saveEcobagQtyCart(model.data.ecoBag?.ecobag_quantity.toString())
            saveGrossAmount(model.data.grandtotal ?: 0)
            saveItemDetailJcoR(model.data)
            saveFreeDelivery(Converter.rupiah(model.data.freeDelivery?.toDouble() ?: 0.0))
            saveTotalDeliveryFee(Converter.rupiah(model.data.total_delivery_fee?.toDouble() ?: 0.0))
        }
    }

    override fun onClick(index: Int) {}

    override fun onSwitchChange(checked: Boolean) {
        _datas.value.let {
            if (checked) {
                it?.set(0, CartSwitch(true))
                it?.set(1, CartPickupAddress(sharedPreference.loadPickUpJcoR().toString()))
                deliveryType = 1
                sharedPreference.saveDeliveryType("1")
                getAllItemCartForSwitch()
                _datas.value.let {
                    it?.removeAll { data -> data is CartDetail }
                    it?.removeAll { data -> data is CartProductJcoR }
                    it?.removeAll { data -> data is EmptyCart }
                    _datas.value = it
                }
                _datas.postValue(it)
            } else {
                it?.set(0, CartSwitch(false))
                it?.set(
                    1, CartDeliveryAddress(
                        sharedPreference.loadAddress().toString(),
                        sharedPreference.loadPickUpJcoR().toString()
                    )
                )
                deliveryType = 0
                sharedPreference.saveDeliveryType("0")
                getAllItemCartForSwitch()
                _datas.value.let {
                    it?.removeAll { data -> data is CartDetail }
                    it?.removeAll { data -> data is CartProductJcoR }
                    it?.removeAll { data -> data is EmptyCart }
                    _datas.value = it
                }
                _datas.postValue(it)
            }
        }
    }

    private fun doUpdateCart(cartProduct: CartProductJcoR) {
        lastDisposable =
            updateCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
//                    getAllItemCartForReloadDetail()
                }, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doDeleteCart(cartProduct: CartProductJcoR) {
        lastDisposable =
            deleteCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({}, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    fun doDeleteAllCart() {
        val temp = mutableListOf<BaseCell>()
        _datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            it?.removeAll { data -> data is CartRecommendations }
            it?.removeAll { data -> data is CartTotal }
            it?.removeAll { data -> data is HeaderRecommendationMenu }

            _datas.value = it
        }
        lastDisposable = deleteAll().subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({}, {})
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun doDeleteAllCartPickUp() {
        datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            it?.removeAll { data -> data is CartRecommendations }
            it?.removeAll { data -> data is CartTotal }
            it?.removeAll { data -> data is HeaderRecommendationMenu }
            _datas.value = it
        }
        lastDisposable = deleteAllPickUp().subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({}, {})
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onPickupAddressClick(index: Int) {
        _showPickup.value = SingleEvents("pickup")
    }

    override fun onDeliveryAddressClick(index: Int) {
        _showDelivery.value = SingleEvents("delivery")
    }

    override fun onOrderClick(index: Int) {
        if (deliveryType == 1) {
            val currentTime = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
            if (currentTime in 22 downTo 9) {
                _showLoading.value = SingleEvents(true)
                loadCreateOrder(deliveryType)
                sharedPreference.apply {
                    removeCodeVoucher()
                    removeVoucher()
                }
            } else {
                _alertTime.value = SingleEvents("Outlet sedang tutup pada jam 21:00 - 10:00")
            }

        } else {
            _showLoading.value = SingleEvents(true)
            loadCreateOrder(deliveryType)
            sharedPreference.apply {
                removeCodeVoucher()
                removeVoucher()
            }
        }
    }

    override fun onChangePaymentClick(index: Int) {
        _showChangePayment.value = SingleEvents("change_payment")
    }

    override fun onProductClick(index: Int) {}

    override fun onProductPlusClick(index: Int) {
        _datas.value?.let {
            val content = (it[index] as CartProductJcoR).copy()
            content.qty++
            content.price = content.priceOriginal * content.qty
            it[index] = content
            doUpdateCart(content)
            Handler(Looper.getMainLooper()).postDelayed({
                _reFetchCart.value = SingleEvents("removeFromCart")
            }, 3000)
            _datas.value = it
        }
    }

    override fun onProductMinusClick(index: Int) {
        _datas.value?.let {
            val content = (it[index] as CartProductJcoR).copy()
            if (content.qty > 1) {
                content.qty--
                content.price = content.priceOriginal * content.qty
                val price = content.priceOriginal.times(content.qty)
                it[index] = content
                doUpdateCart(
                    CartProductJcoR(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        priceOriginal = content.priceOriginal
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _reFetchCart.value = SingleEvents("removeFromCart")
                }, 1500)
            } else {
                doDeleteCart(
                    CartProductJcoR(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        notes = "",
                        qty = content.qty,
                        variantname = content.variantname,
                        priceOriginal = content.priceOriginal
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _removeFromCart.value = SingleEvents("removeFromCart")
                }, 0)
            }
            _datas.value = it
        }
    }

    private fun loadCartUseJpoint(cartInfo: MutableList<CartInfo>, city: String, jPoint: Int) {
        val body = CartOrder(
            2,
            city,
            deliveryType,
            sharedPreference.loadCodeVoucher(),
            sharedPreference.loadPaymentMethod(),
            "JCOR",
            cartInfo,
            jPoint,
            sharedPreference.loadAddress(),
            sharedPreference.loadDetailAddress(),
            sharedPreference.loadSelectLatitudeAddress(),
            sharedPreference.loadSelectLongitudeAddress()
        )
        val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val version = pInfo.versionCode
        val bodyRecommendation = MenuRecommendationReq(
            city = city,
            order_brand = "JCOR",
            cart_info = cartInfo,
            app_os = "android",
            app_version = version
        )
        lastDisposable = cartRepository.getCartOrders(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .flatMap { model ->
                datas.value.let {
                    if (model.status_code == 200) {
                        if (!model.data.error.coupon_code.isNullOrEmpty()) {
                            _errorMessage.value = SingleEvents(model.data.error.coupon_code)
                            sharedPreference.removeCodeVoucher()
                            sharedPreference.removeVoucher()
                        }
                        saveCartDetail(model)
                        _grossAmount.value = model.data.grandtotal?.toDouble()
                        val tempCart = mutableListOf<ItemDetails>()
                        model.data.cart_orders.map { cartProduct ->
                            tempCart.add(
                                ItemDetails(
                                    cartProduct.menu_code.toString(),
                                    cartProduct.menu_price?.toDouble() ?: 0.0,
                                    cartProduct.menu_quantity?.toInt() ?: 0,
                                    cartProduct.menu_name?.take(50)
                                )
                            )
                        }
                        if (model.data.delivery_fee ?: 0 >= 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-1",
                                    model.data.delivery_fee?.toDouble() ?: 0.0,
                                    1,
                                    "Delivery Fee"
                                )
                            )
                        }

                        if (!model.data.ecoBag?.ecobag_name.isNullOrEmpty()) {
                            tempCart.add(
                                ItemDetails(
                                    model.data.ecoBag?.ecobag_code,
                                    model.data.ecoBag?.ecobag_price?.toDouble() ?: 0.0,
                                    model.data.ecoBag?.ecobag_quantity ?: 0,
                                    model.data.ecoBag?.ecobag_name
                                )
                            )
                        }

                        if (model.data.promo?.promo_value != 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-2",
                                    model.data.promo?.promo_value?.toDouble() ?: 0.0,
                                    1,
                                    "Promo"
                                )
                            )
                        }

                        if (isUseJPoint) {
                            val point = sharedPreference.loadJPoint()?.toDouble() ?: 0.0
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-3",
                                    -point,
                                    1,
                                    "Potongan JPoint"
                                )
                            )
                        }

                        if (!model.data.invalid_menu.isNullOrEmpty()) {
                            var items = ""
                            model.data.invalid_menu.map { invalidMenu ->
                                items = invalidMenu.menu_name.toString()
                            }
                            _errorMessage.value =
                                SingleEvents("Harap ubah pesanan Anda karena produk ini tidak tersedia: $items")
                        }

                        _cartOrders.value = tempCart
                    } else {
                        sharedPreference.removeCodeVoucher()
                        sharedPreference.removeVoucher()
                        _errorMessage.value =
                            SingleEvents("Pesanan kamu belum memenuhi syarat dan ketentuan voucher yang berlaku")
                        _grossAmount.value = sharedPreference.loadGrossAmount()?.toDouble()
                        Log.d("cekitemdetails", sharedPreference.loadItemDetailJcoR().toString())
                        val tempCart = mutableListOf<ItemDetails>()
                        sharedPreference.loadItemDetailJcoR()?.cart_orders?.map { cartProduct ->
                            tempCart.add(
                                ItemDetails(
                                    cartProduct.menu_code.toString(),
                                    cartProduct.menu_price?.toDouble() ?: 0.0,
                                    cartProduct.menu_quantity?.toInt() ?: 0,
                                    cartProduct.menu_name
                                )
                            )
                        }
                        if (sharedPreference.loadItemDetailJcoR()?.freeDelivery == 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-1",
                                    sharedPreference.loadItemDetailJcoR()?.delivery_fee?.toDouble()
                                        ?: 0.0,
                                    1,
                                    "Delivery Fee"
                                )
                            )
                        }

                        if (!sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name.isNullOrEmpty()) {
                            tempCart.add(
                                ItemDetails(
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_code,
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_price?.toDouble()
                                        ?: 0.0,
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_quantity ?: 0,
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name
                                )
                            )
                        }

                        if (sharedPreference.loadItemDetailJcoR()?.promo?.promo_value != 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-2",
                                    sharedPreference.loadItemDetailJcoR()?.promo?.promo_value?.toDouble()
                                        ?: 0.0,
                                    1,
                                    "Promo"
                                )
                            )
                        }

                        _cartOrders.value = tempCart
                    }
                    _datas.value = it
                }
                directJcoRepository.getMenuRecommendation(bodyRecommendation)
            }
            .subscribe({ data ->
                val response = data.data
                val recommendation = mutableListOf<CartRecommendation>()
                datas.value.let {
                    response?.map { product ->
                        recommendation.add(CartRecommendation(
                            menuCode = product.menu_code,
                            name = localizationMenuName(product),
                            image = product.menu_img,
                            price = Converter.thousandSeparator(product.menu_price),
                            isFavorite = false
                        ))
                    }

                    if (sharedPreference.loadItemDetailJcoR() != null) {
                        it?.removeAll { data -> data is CartTotal }
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetailJcoR()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetailJcoR()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetailJcoR()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointEarn = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                                jpointUsed = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_used
                                    ?: 0,
                            )
                        )
                    }
                    _datas.value = it
                }

            }, {
                handleError(it)
                datas.value.let {
                    if (sharedPreference.loadItemDetailJcoR() != null) {
                        it?.removeAll { data -> data is CartTotal }
                        it?.add(
                            CartTotal(
                                price = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.total_delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobag = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_name,
                                ecobagSubTotal = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                ecobagQty = sharedPreference.loadItemDetailJcoR()?.ecoBag?.ecobag_quantity.toString(),
                                promo = sharedPreference.loadItemDetailJcoR()?.promo?.promo_value,
                                freeDelivery = sharedPreference.loadItemDetailJcoR()?.freeDelivery,
                                deliveryService = sharedPreference.loadItemDetailJcoR()?.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    sharedPreference.loadItemDetailJcoR()?.delivery_fee?.toDouble()
                                        ?: 0.0
                                ),
                                isDelivery = deliveryType == 0,
                                jpointUsed = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_used
                                    ?: 0,
                                jpointEarn = sharedPreference.loadItemDetailJcoR()?.jpoint_info?.jpoint_earn
                                    ?: 0,
                            )
                        )
                        _datas.value = it
                    }
                }
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun getAllItemCartUseJpoint(jPoint: Int) {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({ data ->
                Log.d("tag", data.toString())
                datas.value?.let {
                    val cartInfo = mutableListOf<CartInfo>()
                    data.map { cartProduct ->
                        cartInfo.add(
                            CartInfo(
                                cartProduct.menuCode.toString(),
                                cartProduct.qty.toString()
                            )
                        )
                    }

                    orderDetail.clear()
                    data.map { cartProduct ->
                        orderDetail.add(
                            CreateOrderDetailReq(
                                menu_code = cartProduct.menuCode.toString(),
                                menu_name = cartProduct.name,
                                menu_quantity = cartProduct.qty.toString(),
                                material_unit_price = cartProduct.priceOriginal.toString(),
                                material_sub_total = cartProduct.price.toString(),
                                menu_detail = cartProduct.variantname
                            )
                        )
                    }
//                    it.removeAll { data -> data is CartTotal }
                    loadCartUseJpoint(cartInfo, sharedPreference.loadSelectCity() ?: "", jPoint)

                    _datas.value = it
                }
            }, {
                handleError(it)
            })


    }

    fun midtransPayment(transactionId: String?) {
        if (deliveryType == 0) {
            val body = PaymentReq(
                2,
                transactionId,
                sharedPreference.loadEmail(),
                sharedPreference.loadPhoneNumber(),
                sharedPreference.loadFcmToken(),
                sharedPreference.loadOutletIdForDeliveryJcoR()
            )
            lastDisposable = paymentRepository.getMidtransPayment(body).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({

                }, {

                })
        } else {
            val body = PaymentReq(
                2,
                transactionId,
                sharedPreference.loadEmail(),
                sharedPreference.loadPhoneNumber(),
                sharedPreference.loadFcmToken(),
                sharedPreference.loadOutletId()
            )
            lastDisposable = paymentRepository.getMidtransPayment(body).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({

                }, {

                })
        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getOrderPayment(orderId: String) {
        val body = OrderPaymentReq(
            brand = 2,
            order_id = orderId
        )

        lastDisposable = paymentRepository.orderPayment(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    override fun onProductNotesClick(index: Int) {
        _showDialogNote.value = SingleEvents("notes")
    }

    override fun onAddOrderClick() {
        _showAddOrder.value = SingleEvents("showAddOrder")
    }

    override fun onDeleteAllClick() {
        doDeleteAllCart()
        doDeleteAllCartPickUp()
        Handler(Looper.getMainLooper()).postDelayed({
            _removeFromCart.value = SingleEvents("removeFromCart")
        }, 1)
    }

    override fun onVoucherClick() {
        _showVoucher.value = SingleEvents("show-voucher")
    }

    override fun onUsedJPoint(checked: Boolean) {
        _datas.value.let {
            if (checked) {
                isUseJPoint = true
                getAllItemCartUseJpoint(1)
            } else {
                isUseJPoint = false
                getAllItemCartUseJpoint(0)
            }

            _datas.postValue(it)
        }
    }

    override fun onMenuItemClick(cartRecommendation: CartRecommendation) {
        _datas.value.let {
            menuSelected.postValue(cartRecommendation.menuCode ?: "")
            _openProductDetail.value = SingleEvents(cartRecommendation.menuCode ?: "")
            Log.d("recomend", "clicked")
        }
    }
}