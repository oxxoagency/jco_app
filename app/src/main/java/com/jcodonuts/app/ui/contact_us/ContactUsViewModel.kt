package com.jcodonuts.app.ui.contact_us

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.ui.main.profile.ProfileControllerListener
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class ContactUsViewModel @Inject constructor() : ProfileControllerListener, BaseViewModel() {

    val datas = MutableLiveData<MutableList<BaseCell>>()

    private val _showMenuDetail = MutableLiveData<SingleEvents<Int>>()
    val showMenuDetail: LiveData<SingleEvents<Int>>
        get() = _showMenuDetail

    private val _showWebView = MutableLiveData<SingleEvents<String>>()
    val showWebView: LiveData<SingleEvents<String>>
        get() = _showWebView

    private val _openMail = MutableLiveData<SingleEvents<String>>()
    val openMail: LiveData<SingleEvents<String>>
        get() = _openMail

    private val _openFacebook = MutableLiveData<SingleEvents<String>>()
    val openFacebook: LiveData<SingleEvents<String>>
        get() = _openFacebook

    private val _openInstagram = MutableLiveData<SingleEvents<String>>()
    val openInstagram: LiveData<SingleEvents<String>>
        get() = _openInstagram

    init {
        datas.value = mutableListOf()
    }

    fun loadData() {
        datas.value?.let {
            it.add(ProfileMenuHeader("Kontak"))
            it.add(ProfileMenu(R.drawable.ic_baseline_language, R.string.website_jco, WEB))
            it.add(ProfileMenu(R.drawable.ic_mail, R.string.email_jco, EMAIL))
            it.add(ProfileMenu(R.drawable.ic_call, R.string.number_jco, PHONE))
            it.add(ProfileMenuHeader("Social Media"))
            it.add(SocmedMenu(R.drawable.ic_facebook, "iamjcolovers", FACEBOOK))
            it.add(SocmedMenu(R.drawable.ic_instagram, "jcoindonesia", INSTAGRAM))
            it.add(SocmedMenu(R.drawable.ic_youtube, "J.CO Donuts & Coffee", YOUTUBE))
            it.add(ProfileMenuHeader("Alamat"))
            it.add(AddressMenu("Mal Kelapa Gading 1 & 2 Lantai Dasar Unit G-140 C & D \n Jalan Boulevar Kelapa Gading \n Kelapa Gading \n Jakarta Utara"))
            it.add(ProfileMenuHeader("LAYANAN PENGADUAN KONSUMEN"))
            it.add(AddressMenu("DITJEN PERLINDUNGAN KONSUMEN & TERTIB NIAGA KEMENDAG RI \n 0853-1111-1010"))
            datas.postValue(it)
        }
    }

    override fun onMenuClick(index: Int) {
        datas.value?.let {
            val data = (it[index] as ProfileMenu).copy()
            if (data.type == PHONE) {
                _showMenuDetail.value = SingleEvents(data.title)
            } else if (data.type == WEB) {
                _showWebView.value = SingleEvents("https://jcodonuts.com")
            } else if (data.type == EMAIL) {
                _openMail.value = SingleEvents("hello@jcodelivery.com")
            }
        }
    }

    override fun onSignOut() {

    }

    override fun onEditProfile() {

    }

    override fun onLinkAjaClick() {

    }

    override fun onSocialMediaClick(index: Int) {
        datas.value.let {
            val data = (it?.get(index) as SocmedMenu).copy()
            if (data.type == FACEBOOK) {
                _openFacebook.value = SingleEvents("iamjcolovers")
            } else if(data.type == INSTAGRAM) {
                _openInstagram.value = SingleEvents("jcoindonesia")
            }
        }
    }

    companion object {
        const val WEB = 1
        const val EMAIL = 2
        const val PHONE = 3
        const val FACEBOOK = 4
        const val INSTAGRAM = 5
        const val YOUTUBE = 5
    }
}