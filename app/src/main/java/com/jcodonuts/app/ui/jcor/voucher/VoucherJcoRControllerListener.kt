package com.jcodonuts.app.ui.jcor.voucher

interface VoucherJcoRControllerListener {
    fun onUsedVoucherClick(position: Int)
    fun moreVoucherClick()
}