package com.jcodonuts.app.ui.add_address

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jcodonuts.app.BuildConfig
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentDialogSearchLocationBinding
import com.jcodonuts.app.ui.MainActivity
import com.jcodonuts.app.ui.location.LocationAdapter
import com.jcodonuts.app.utils.KeyboardUtil
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class DialogSearchLocation @Inject constructor() : BottomSheetDialogFragment() {

    private val TAG = DialogSearchLocation::class.java.simpleName

    private lateinit var binding: FragmentDialogSearchLocationBinding
    private lateinit var listener: DialogSearchLocationListener
    private val adapter by lazy {
        LocationAdapter {
            getAddress(it)
        }
    }
    private lateinit var request: FindAutocompletePredictionsRequest

    override fun getTheme(): Int {
        return R.style.DialogFullWidth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDialogSearchLocationBinding.inflate(inflater)
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        KeyboardUtil(requireActivity(), binding.root)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), BuildConfig.API_KEY_MAPS)
        }

        val placesClient = Places.createClient(requireContext())

        val searchStream = RxTextView.textChanges(binding.searchLocation)
            .map {
                it.toString()
            }
            .debounce(1, TimeUnit.SECONDS)

        searchStream.subscribe { query ->
            val token = AutocompleteSessionToken.newInstance()
            val bounds = RectangularBounds.newInstance(
                LatLng(
                    listener.latitude().toDouble(),
                    listener.longitude().toDouble()
                ),
                LatLng(
                    listener.latitude().toDouble(),
                    listener.longitude().toDouble()
                )
            )

            request =
                FindAutocompletePredictionsRequest.builder() // Call either setLocationBias() OR setLocationRestriction().
                    .setLocationBias(bounds) //.setLocationRestriction(bounds)
                    .setOrigin(
                        LatLng(
                            listener.latitude().toDouble(),
                            listener.longitude().toDouble()
                        )
                    )
                    .setCountry("ID")
                    .setSessionToken(token)
                    .setQuery(query.toString())
                    .build()

            placesClient.findAutocompletePredictions(request).addOnSuccessListener {
                binding.loading.loading.visibility = View.GONE
                adapter.setPrograms(it.autocompletePredictions)
                Log.i(TAG, it.autocompletePredictions.toString())
                for (prediction in it.autocompletePredictions) {
                    Log.i(TAG, prediction.placeId)
                    Log.i(TAG, prediction.getPrimaryText(null).toString())
                    Log.i(TAG, prediction.distanceMeters.toString())
                }
//                binding.result.text = result
                binding.recyclerViewLocation.apply {
                    adapter = this@DialogSearchLocation.adapter
                    layoutManager = LinearLayoutManager(context)
                }
            }.addOnFailureListener {
                if (it is ApiException) {
                    val apiException = it
                    Log.e(TAG, "Place not found: " + apiException.statusCode)
                }
            }

            if (!query.isNullOrEmpty()) binding.btnSave.visibility =
                View.GONE else binding.btnSave.visibility = View.GONE

        }
        binding.close.setOnClickListener { dissmissDialog() }
        binding.chooseByMap.setOnClickListener {
            navigateTo(R.string.linkMapFragment)
            dissmissDialog()
        }

    }

    private fun getAddress(autocompletePrediction: AutocompletePrediction) {
        listener.address(autocompletePrediction)
        dissmissDialog()

    }


    private fun navigateTo(link: Int) {
        val uri = Uri.parse(getString(link))
        Navigation.findNavController((activity as MainActivity), R.id.nav_host_fragment)
            .navigate(uri)
    }

    fun showDialog(
        fragmentManager: FragmentManager,
        tag: String?,
        listener: DialogSearchLocationListener
    ) {
        show(fragmentManager, tag)
        this.listener = listener

    }

    fun dissmissDialog() {
        dialog?.cancel()
    }

    interface DialogSearchLocationListener {
        fun latitude(): String
        fun longitude(): String
        fun address(autocompletePrediction: AutocompletePrediction): String
    }
}