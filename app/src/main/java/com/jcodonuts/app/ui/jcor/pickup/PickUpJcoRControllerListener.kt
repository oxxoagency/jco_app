package com.jcodonuts.app.ui.jcor.pickup

interface PickUpJcoRControllerListener {
    fun onClick(index: Int)
    fun onSearchClick()
}