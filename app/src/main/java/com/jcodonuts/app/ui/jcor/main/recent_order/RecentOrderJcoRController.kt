package com.jcodonuts.app.ui.jcor.main.recent_order

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.cartEmpty
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.LoadingPage
import com.jcodonuts.app.data.local.RecentOrder
import com.jcodonuts.app.emptyJcoR
import com.jcodonuts.app.lytLoadingShimmer
import com.jcodonuts.app.recentOrderJcoR

class RecentOrderJcoRController(private val recentOrderJcoRListener: RecentOrderJcoRListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is RecentOrder -> addRecentOrder(cellData, index, recentOrderJcoRListener)
                is EmptyCart -> addEmptyCart(cellData)
                is LoadingPage -> addLoadingPage(cellData)
            }
        }
    }

    private fun addRecentOrder(
        cellData: RecentOrder,
        position: Int,
        listener: RecentOrderJcoRListener
    ) {
        recentOrderJcoR {
            id(cellData.orderId)
            data(cellData)
            position(position)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart){
        emptyJcoR {
            id("empty-cart")
            data(cellData)
        }
    }

    private fun addLoadingPage(cellData: LoadingPage) {
        lytLoadingShimmer {
            id(cellData.hashCode())
        }
    }
}