package com.jcodonuts.app.ui.jcor.main.profile

import android.os.Bundle
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentProfileJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.base.InjectingNavHostFragment
import com.jcodonuts.app.ui.main.base.MainFragment
import com.jcodonuts.app.ui.main.profile.ProfileViewModel
import javax.inject.Inject


class ProfileJcoRFragment @Inject constructor() :
    BaseFragment<FragmentProfileJcoRBinding, ProfileJcoRViewModel>() {

    override fun getViewModelClass(): Class<ProfileJcoRViewModel> = ProfileJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_profile_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        initRecyclerview()
        initObserver()

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }
    }

    private fun initRecyclerview() {
        val controller = ProfileJcoRController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.setData(it)
        })
    }

    private fun initObserver() {
        viewModel.showEditProfile.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkEditProfileFragmentJcoR)
            }
        })

        viewModel.showMenuDetail.observe(this, {
            it.getContentIfNotHandled()?.let { index ->
                when (index) {
                    ProfileViewModel.CHANGE_PASSWORD -> {
                        navigateTo(R.string.linkChangePasswordFragmentJcoR)
                    }
//                    ProfileViewModel.ORDER -> {
//                        navigateTo(R.string.linkOrderFragment)
//                    }
                    ProfileViewModel.LANGUAGE -> {
                        navigateTo(R.string.linkLanguageFragmentJcoR)
                    }
                    ProfileViewModel.CONTACT_US -> {
                        navigateTo(R.string.linkContactUsFragmentJcoR)
                    }
                }
            }
        })
    }

    override fun onBackPress() {
        val navhost = (parentFragment as InjectingNavHostFragment)
        (navhost.parentFragment as MainFragment).backToHome()
    }
}