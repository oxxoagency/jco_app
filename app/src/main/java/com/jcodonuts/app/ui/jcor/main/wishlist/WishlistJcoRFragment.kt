package com.jcodonuts.app.ui.jcor.main.wishlist

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentWishlistJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.base.InjectingNavHostFragment
import com.jcodonuts.app.ui.main.base.MainFragment
import com.jcodonuts.app.ui.main.home.HomeSpacingDecoration
import javax.inject.Inject


class WishlistJcoRFragment @Inject constructor() :
    BaseFragment<FragmentWishlistJcoRBinding, WishlistJcoRViewModel>() {

    override fun getViewModelClass(): Class<WishlistJcoRViewModel> =
        WishlistJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_wishlist_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        initRecyclerview()
        initObserver()

        if (!isFragmentFromPaused) {
            viewModel.loadLocations()
        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initRecyclerview() {
        val controller = WishlistJcoRController(viewModel)
        binding.recyclerview.setController(controller)
        binding.recyclerview.addItemDecoration(HomeSpacingDecoration())
        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.whistList.observe(viewLifecycleOwner, {
            if (it.isNullOrEmpty()) {
                binding.empty.visibility = View.VISIBLE
                binding.recyclerview.visibility = View.GONE
            } else {
                binding.empty.visibility = View.GONE
                binding.recyclerview.visibility = View.VISIBLE
            }
        })

        viewModel.openProductDetail.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkProductDetailJcoR).replace("{id}", data.menuCode)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }

    override fun onBackPress() {
        val navhost = (parentFragment as InjectingNavHostFragment)
        (navhost.parentFragment as MainFragment).backToHome()
    }
}