package com.jcodonuts.app.ui.jcor.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.CartProduct
import com.jcodonuts.app.data.local.CartProductJcoR
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import io.reactivex.Flowable
import javax.inject.Inject

class MainJcoRViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val schedulers: SchedulerProvider,
    private val database: JcoDatabase
) : BaseViewModel() {

    private val _sizeCart = MutableLiveData<Int>()
    val sizeCart: LiveData<Int>
        get() = _sizeCart

    private fun cartProduct(): Flowable<List<CartProductJcoR>> =
        database.cartDao().getAllCartProductJcoR()

    init {
        getAllItemCart()
    }

    private fun getAllItemCart() {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers. ui())
            .subscribe({ data ->
                var totalQty = 0
                data.forEach {
                    Log.d("tag", it.qty.toString())
                    totalQty += it.qty
                }
                _sizeCart.postValue(totalQty)
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }

    }
}