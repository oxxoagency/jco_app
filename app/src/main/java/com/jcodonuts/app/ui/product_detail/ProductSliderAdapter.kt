package com.jcodonuts.app.ui.product_detail

import android.view.LayoutInflater
import android.view.ViewGroup
import com.jcodonuts.app.data.remote.model.res.MenuImage
import com.jcodonuts.app.databinding.ItemImageProductDetailBinding
import com.smarteist.autoimageslider.SliderViewAdapter

class ProductSliderAdapter : SliderViewAdapter<ProductSliderAdapter.ProductSliderViewHolder>() {

    private var slides: MutableList<MenuImage> = ArrayList()


    override fun getCount(): Int {
        return slides.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?): ProductSliderAdapter.ProductSliderViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val binding = ItemImageProductDetailBinding.inflate(inflater, parent, false)
        return ProductSliderViewHolder(binding)

    }

    override fun onBindViewHolder(
        viewHolder: ProductSliderAdapter.ProductSliderViewHolder?,
        position: Int
    ) {
        viewHolder?.bind(slides[position])
    }

    inner class ProductSliderViewHolder(val binding: ItemImageProductDetailBinding) :
        SliderViewAdapter.ViewHolder(binding.root) {
        fun bind(menuImage: MenuImage) {
            binding.apply {
                item = menuImage
                executePendingBindings()
            }
        }
    }

    internal fun setSlider(slides: MutableList<MenuImage>) {
        this.slides = slides
        notifyDataSetChanged()
    }

    internal fun addSliderItem(menuImage: MenuImage){
        slides.add(menuImage)
        notifyDataSetChanged()
    }
}