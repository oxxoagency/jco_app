package com.jcodonuts.app.ui.midtrans

import android.os.Bundle
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentMidtransBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject

class MidtransFragment @Inject constructor() :
    BaseFragment<FragmentMidtransBinding, MidtransViewModel>(){

    override fun getViewModelClass(): Class<MidtransViewModel> = MidtransViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_midtrans

    override fun onViewReady(savedInstance: Bundle?) {

    }



}