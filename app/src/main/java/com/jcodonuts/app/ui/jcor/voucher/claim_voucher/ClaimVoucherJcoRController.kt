package com.jcodonuts.app.ui.jcor.voucher.claim_voucher

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.claimVoucherItemsJcoR
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.Voucher
import com.jcodonuts.app.emptyJcoR

class ClaimVoucherJcoRController(private val listener: ClaimVoucherJcoRControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is Voucher -> addCouponList(cellData, index, listener)
                is EmptyCart -> addEmptyCart(cellData)
            }
        }
    }

    private fun addCouponList(
        cellData: Voucher,
        index: Int,
        listener: ClaimVoucherJcoRControllerListener
    ) {
        claimVoucherItemsJcoR {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyCart) {
        emptyJcoR {
            id("empty-cart")
            data(cellData)
        }
    }
}