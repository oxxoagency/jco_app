package com.jcodonuts.app.ui.delivery
interface DeliveryControllerListener {
    fun onClick(index: Int)
    fun onFavoriteClick(index: Int)
    fun onClickUpdateAddress(index: Int)
    fun onClickAddAddress()
    fun onClickSaveDeliveryAddress(index: Int)
    fun onDeleteAddress(index: Int)
}