package com.jcodonuts.app.ui.jcor.menu_search

import android.net.Uri
import android.os.Bundle
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentMenuSearchJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.main.home.HomeSpacingDecoration
import javax.inject.Inject


class MenuSearchJcoRFragment @Inject constructor() :
    BaseFragment<FragmentMenuSearchJcoRBinding, MenuSearchJcoRViewModel>() {

    override fun getViewModelClass(): Class<MenuSearchJcoRViewModel> =
        MenuSearchJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_menu_search_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        initRecyclerview()

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initRecyclerview() {
        val controller = MenuSearchController(viewModel)
        binding.recyclerview.setController(controller)
        binding.recyclerview.addItemDecoration(HomeSpacingDecoration())
        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.openProductDetail.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkProductDetailJcoR).replace("{id}", data.menuCode)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })

        viewModel.menuSelected.observe(viewLifecycleOwner, {
            controller.menuSelected = it
        })
    }
}