package com.jcodonuts.app.ui.main.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.CartProduct
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Flowable
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val schedulers: SchedulerProvider,
    private val database: JcoDatabase
) : BaseViewModel() {

    private val _showBottomNavigation = MutableLiveData<SingleEvents<String>>()
    val showBottomNavigation: LiveData<SingleEvents<String>>
        get() = _showBottomNavigation

    private val _hideBottomNavigation = MutableLiveData<SingleEvents<String>>()
    val hideBottomNavigation: LiveData<SingleEvents<String>>
        get() = _hideBottomNavigation

    private val _sizeCart = MutableLiveData<Int>()
    val sizeCart: LiveData<Int>
        get() = _sizeCart

    private fun cartProduct(): Flowable<List<CartProduct>> =
        database.cartDao().getAllCartProduct()

    init {
        getAllItemCart()
    }

    fun loginCheck() {
        val data = sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)
        if (data != null && data != "") {
            _showBottomNavigation.value = SingleEvents("show buttomNavigation")
        } else {
            _hideBottomNavigation.value = SingleEvents("hide buttomNavigation")
        }
    }

    private fun getAllItemCart() {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers. ui())
            .subscribe({ data ->
                var totalQty = 0
                data.forEach {
                    Log.d("tag", it.qty.toString())
                    totalQty += it.qty
                }
                _sizeCart.postValue(totalQty)
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }

    }
}