package com.jcodonuts.app.ui.jcor.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentMainJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import javax.inject.Inject


class MainJcoRFragment @Inject constructor() :
    BaseFragment<FragmentMainJcoRBinding, MainJcoRViewModel>() {

    override fun getViewModelClass(): Class<MainJcoRViewModel> = MainJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_main_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        setUpNavigation()
        initObserver()
    }

    private fun setUpNavigation() {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.navHostFragmentMain) as NavHostFragment
        val navController = navHostFragment.navController
        binding.bottomNavigation.setupWithNavController(navController)
        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            NavigationUI.onNavDestinationSelected(item, navController)
        }
        binding.bottomNavigation.setOnNavigationItemReselectedListener {

        }

        arguments?.let {
            if (it.getString("menu").equals("cart")) {
                binding.bottomNavigation.selectedItemId = R.id.cart
                it.putString("menu", "")
            } else if (it.getString("menu").equals("profile")) {
                binding.bottomNavigation.selectedItemId = R.id.profile
                it.putString("menu", "")
            }
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.home) showStatusBar() else transparentStatusBar()
        }
    }

    private fun initObserver() {
        viewModel.sizeCart.observe(viewLifecycleOwner, {
            val badge = binding.bottomNavigation.getOrCreateBadge(R.id.cart)
            badge.isVisible = it > 0
            badge.number = it
        })
    }

    fun backToHome() {
        binding.bottomNavigation.selectedItemId = R.id.home
    }

    private fun showStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window? = activity?.window
            window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window?.statusBarColor = resources.getColor(R.color.jco_r_secondary)
        }
    }

    private fun transparentStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window? = activity?.window
            window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window?.statusBarColor = Color.TRANSPARENT
        }
    }
}