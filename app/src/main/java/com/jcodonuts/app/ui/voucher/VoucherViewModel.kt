package com.jcodonuts.app.ui.voucher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.Voucher
import com.jcodonuts.app.data.local.VoucherHeader
import com.jcodonuts.app.data.remote.model.req.CouponMemberReq
import com.jcodonuts.app.data.remote.model.req.DirectCouponMemberReq
import com.jcodonuts.app.data.repository.CouponRepository
import com.jcodonuts.app.data.repository.DirectJcoRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import com.jcodonuts.app.utils.parseDateWithFormat
import javax.inject.Inject

class VoucherViewModel @Inject constructor(
    private val couponRepository: CouponRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val directJcoRepository: DirectJcoRepository
) : BaseViewModel(), VoucherControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _moreVoucher = MutableLiveData<SingleEvents<String>>()
    val moreVoucher: LiveData<SingleEvents<String>>
        get() = _moreVoucher

    private val _useVoucher = MutableLiveData<SingleEvents<String>>()
    val useVoucher: LiveData<SingleEvents<String>>
        get() = _useVoucher

    fun loadData() {
        val body = DirectCouponMemberReq("2","2","JCO","JID", sharedPreference.loadDeliveryType())
        lastDisposable = directJcoRepository.couponMember(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                val data = it.data
                val temp = mutableListOf<BaseCell>()
                temp.add(VoucherHeader(""))
                if (it.status_code == 200) {
                data.map { coupon ->
                        temp.add(
                            Voucher(
                                id = coupon.coupon_id,
                                name = coupon.coupon_name,
                                image = coupon.image,
                                date = "${coupon.start_time?.parseDateWithFormat("dd MMM yyyy")} - ${
                                    coupon.end_time?.parseDateWithFormat("dd MMM yyyy")
                                }",
                                description = coupon.description?.replace("\\n", "\n"),
                                code = coupon.code,
                                isClaim = coupon.is_claim,
                                isUsed = coupon.is_used
                            )
                        )
                    }
                }
                _datas.postValue(temp)
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onUsedVoucherClick(position: Int) {
        _datas.value.let {
            val coupon = (it?.get(position) as Voucher)

            if (coupon.isUsed != 1) {
                sharedPreference.apply {
                    saveCodeVoucher(coupon.code.toString())
                    saveVoucher(coupon.name.toString())
                }
                _useVoucher.value = SingleEvents("use-voucher")
            }
        }
    }

    override fun moreVoucherClick() {
        _moreVoucher.value = SingleEvents("more-voucher")
    }
}