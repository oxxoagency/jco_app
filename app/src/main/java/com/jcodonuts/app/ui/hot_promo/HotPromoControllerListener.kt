package com.jcodonuts.app.ui.hot_promo

import com.jcodonuts.app.data.local.ModelHotPromo

interface HotPromoControllerListener {
    fun onClick(modelHotPromo: ModelHotPromo)
}