package com.jcodonuts.app.ui.pickup

import android.os.Bundle
import android.util.Log
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.PickupItem
import com.jcodonuts.app.data.remote.model.res.CityDataRes
import com.jcodonuts.app.databinding.FragmentPickupBinding
import com.jcodonuts.app.ui.add_address.AddAddressFragment
import com.jcodonuts.app.ui.add_address.DialogCity
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.delivery.DeliveryFragmentDirections
import com.jcodonuts.app.utils.DlgLoadingProgressBar
import javax.inject.Inject

class PickupFragment @Inject constructor() :
    BaseFragment<FragmentPickupBinding, PickupViewModel>() {

    var dialog : DlgLoadingProgressBar? = null

    override fun getViewModelClass(): Class<PickupViewModel> {
        return PickupViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_pickup
    }

    override fun onViewReady(savedInstance: Bundle?) {
        dialog = DlgLoadingProgressBar(requireContext())
        initActionBar()
        initRecyclerview()
        initObserver()

        if (!isFragmentFromPaused) {
            viewModel.loadLocations()
        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initRecyclerview() {
        val controller = PickupController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.openToCart.observe(this, {
            onBackPress()
        })

        viewModel.showCity.observe(this, {
            it.getContentIfNotHandled()?.let {
                viewModel.getCity()
                var searchCity = emptyList<CityDataRes>()
                val dlg = DialogCity()
                viewModel.cities.observe(this, { cities ->
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        AddAddressFragment::class.java.simpleName,
                        object : DialogCity.DialogCityListener {
                            override fun getCity(): List<CityDataRes> = cities
                            override fun saveCity(city: CityDataRes): String {
                                viewModel.getOutletByCity(city.city)
                                return city.city.toString()
                            }

                            override fun searchCity(city: String): List<CityDataRes> {
                                if (city.isNotEmpty()) {
                                    viewModel.getSearchCity(city)
                                }

                                viewModel.cities.observe(viewLifecycleOwner, {
                                    searchCity = it
                                })
                                return searchCity
                            }
                        })
                })
            }
        })

        viewModel.showLoading.observe(this, {
            it.getContentIfNotHandled()?.let {state ->
                if (state){
                    dialog?.showPopUp()
                } else {
                    dialog?.dismissPopup()
                }
            }
        })
    }
}