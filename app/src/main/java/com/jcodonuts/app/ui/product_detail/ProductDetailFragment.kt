package com.jcodonuts.app.ui.product_detail

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.data.remote.model.res.DetailVariant
import com.jcodonuts.app.data.remote.model.res.MenuImage
import com.jcodonuts.app.databinding.FragmentProductDetailBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.utils.Converter
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import javax.inject.Inject

class ProductDetailFragment @Inject constructor() :
    BaseFragment<FragmentProductDetailBinding, ProductDetailViewModel>() {

    private val adapter by lazy { ProductSliderAdapter() }


    override fun getViewModelClass(): Class<ProductDetailViewModel> {
        return ProductDetailViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_product_detail
    }

    override fun onViewReady(savedInstance: Bundle?) {
        binding.btnBack.setOnClickListener {
            onBackPress()
        }

        binding.viewModel = viewModel
        binding.executePendingBindings()


        initRecyclerview()
        initObserver()

        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadDetail(it1) }
            }

        }

        binding.slider.apply {
            setSliderAdapter(adapter)
            startAutoCycle()
            setInfiniteAdapterEnabled(true)
            setIndicatorAnimation(IndicatorAnimationType.WORM)
            setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        }

        binding.home.setOnClickListener {
            val action = ProductDetailFragmentDirections.actionFromProductToMainFragment("")
            findNavController().navigate(
                action,
                FragmentNavigator.Extras.Builder()
                    .build()
            )
        }

        binding.cart.setOnClickListener {
            val action = ProductDetailFragmentDirections.actionFromProductToMainFragment("cart")
            findNavController().navigate(
                action,
                FragmentNavigator.Extras.Builder()
                    .build()
            )
        }
    }

    private fun initRecyclerview() {
        val controller = ProductDetailController(viewModel)
        binding.recyclerview.setController(controller)
        binding.recyclerview.itemAnimator = null

        viewModel.datas.observe(viewLifecycleOwner, {
            controller.data = it
//            Log.d("cekdatas", controller.data.toString())
        })

        viewModel.sizeSelected.observe(viewLifecycleOwner, {
            controller.sizeSelected = it
        })

        viewModel.variantSelected.observe(viewLifecycleOwner, {
            controller.variantSelected = it
        })

        viewModel.price.observe(viewLifecycleOwner, {
            if (!it.isNullOrEmpty()) {
                controller.price = Converter.thousandSeparator(it)
                Log.d("cekprice", it)
            }
        })

        viewModel.productDetail.observe(viewLifecycleOwner, { detail ->
            adapter.setSlider((detail.menu_image ?: mutableListOf()) as MutableList<MenuImage>)
            binding.slider.apply {
                setSliderAdapter(adapter)
                setIndicatorAnimation(IndicatorAnimationType.WORM)
                setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            }
        })
        viewModel.positionVariantSelected.observe(viewLifecycleOwner, { position ->
            binding.slider.apply {
                currentPagePosition = position
            }
        })
        viewModel.max.observe(viewLifecycleOwner, {
            controller.max = it
        })

        viewModel.mainQty.observe(viewLifecycleOwner, {
            controller.mainQty = it
        })

        viewModel.qty.observe(viewLifecycleOwner, {
            controller.qtyDonut = it

            Log.d("cekdonut", "quantity = ${controller.qtyDonut}")
        })

        viewModel.maxDonutCustom.observe(viewLifecycleOwner, {
            controller.maxCustomDonut = it
        })

        viewModel.totalAmount.observe(viewLifecycleOwner, {
            controller.totalAmount = it
        })
//
//        viewModel.notifyContentUpdate.observe(this, {
//            it.getContentIfNotHandled()?.let { position ->
//                adapter.notifyItemChanged(position)
//            }
//        })
    }

    private fun initObserver() {
        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { _ ->
                val dlg = DialogAddToCart(requireContext())
                dlg.showPopup({
                    dlg.dismissPopup()
                    onBackPress()
                },
                    {
                        dlg.dismissPopup()
                        val action =
                            ProductDetailFragmentDirections.actionFromProductToMainFragment("cart")
                        findNavController().navigate(
                            action,
                            FragmentNavigator.Extras.Builder()
                                .build()
                        )
                    })
//                    Snacky.builder()
//                        .setActivity(activity)
//                        .setText("$nameProduct Sukses ditambahkan ke keranjang")
//                        .setDuration(Snacky.LENGTH_SHORT)
//                        .setActionText(getString(R.string.close))
//                        .setActionTextSize(12f)
//                        .setBackgroundColor(
//                            ContextCompat.getColor(
//                                requireContext(),
//                                R.color.c_text_secondary
//                            )
//                        )
//                        .success()
//                        .show()
            }
        })

        viewModel.showAlert.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { message ->
                Toast.makeText(
                    context,
                    message,
                    Toast.LENGTH_LONG
                ).show()
            }
        })

        viewModel.showAlertDonutCustomize.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                Toast.makeText(
                    context,
                    getString(R.string.alert_donut_customize),
                    Toast.LENGTH_LONG
                ).show()
            }
        })

        viewModel.showVariantDonut.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val dlg = DialogVariantDonut()
                viewModel.productDetail.observe(viewLifecycleOwner, { productDetail ->
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        ProductDetailFragment::class.java.simpleName,
                        object : DialogVariantDonut.DialogVariantListener {
                            override fun onClose() {
                                TODO("Not yet implemented")
                            }

                            override fun nameDonut(): String =
                                productDetail.menu_name

                            override fun sizeDonut(): String =
                                productDetail.menu_amount

                            override fun variantDonut(): List<DetailVariant> =
                                productDetail.details ?: emptyList()
                        }
                    )
                })
            }
        })

        viewModel.sizeCart.observe(viewLifecycleOwner, {
            binding.apply {
                if (it <= 0) {
                    bgQtyCart.visibility = View.GONE
                    qtyCart.visibility = View.GONE
                } else {
                    bgQtyCart.visibility = View.VISIBLE
                    qtyCart.visibility = View.VISIBLE
                    qtyCart.text = it.toString()
                }
            }
        })
    }

    private fun transparentStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window? = activity?.window
            window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window?.statusBarColor = Color.TRANSPARENT
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.maxDonutCustomize = 0
        val controller = ProductDetailController(viewModel)
        controller.data = mutableListOf()
        controller.qtyDonut = mutableListOf()
        controller.mainQty = 0
        controller.max = 0
        controller.maxCustomDonut = 0
        controller.price = ""
//        Log.d("cekdatas", controller.data.toString())
    }
}