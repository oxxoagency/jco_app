package com.jcodonuts.app.ui.product_detail

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.local.room.JcoDatabase
import com.jcodonuts.app.data.remote.model.req.ProductDetailReq
import com.jcodonuts.app.data.remote.model.req.ProductFavoriteReq
import com.jcodonuts.app.data.remote.model.res.DetailVariant
import com.jcodonuts.app.data.remote.model.res.MenuImage
import com.jcodonuts.app.data.remote.model.res.ProductDetail
import com.jcodonuts.app.data.remote.model.res.ProductDetailRes
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.data.repository.ProductRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

@SuppressLint("NullSafeMutableLiveData")
class ProductDetailViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val jcoDatabase: JcoDatabase,
    private val sharedPreference: SharedPreference,
    private val context: Context,
) : BaseViewModel(), ProductDetailControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _productDetail = MutableLiveData<ProductDetail>()
    val productDetail: LiveData<ProductDetail>
        get() = _productDetail

    private val _donut = MutableLiveData<List<DetailVariant>>()
    val donut: LiveData<List<DetailVariant>>
        get() = _donut

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private val _showVariantDonut = MutableLiveData<SingleEvents<String>>()
    val showVariantDonut: LiveData<SingleEvents<String>>
        get() = _showVariantDonut

    private val _nameProduct = MutableLiveData<String>()
    val nameProduct: LiveData<String>
        get() = _nameProduct

    private val _showAlert = MutableLiveData<SingleEvents<String>>()
    val showAlert: LiveData<SingleEvents<String>>
        get() = _showAlert

    private val _showAlertDonutCustomize = MutableLiveData<SingleEvents<String>>()
    val showAlertDonutCustomize: LiveData<SingleEvents<String>>
        get() = _showAlertDonutCustomize

    private var _qty = MutableLiveData<MutableList<Int>>()
    val qty: LiveData<MutableList<Int>>
        get() = _qty


    private val _sizeCart = MutableLiveData<Int>()
    val sizeCart: LiveData<Int>
        get() = _sizeCart

    private val _menuImage = MutableLiveData<MenuImage>()
    var temp = mutableListOf<BaseCell>()

    val sizeSelected = MutableLiveData<String>()
    val variantSelected = MutableLiveData<String>()
    val positionVariantSelected = MutableLiveData<Int>()
    var price = MutableLiveData<String>()
    var max = MutableLiveData<Int>()
    var mainQty = MutableLiveData<Int>()
    var maxDonutCustom = MutableLiveData<Int>()
    var totalAmount = MutableLiveData<Int>()
    var sizeDefault = ""

    var maxDonutCustomize = 0
    var tempClick = 0
    var variantDonut = StringBuilder()
    var qtyVariant: Int = 0

    // Maximum limit amount for custom donut
    var donutCustomSelected: Int = 0
    var limitDonutCustom: Int = 0

    private lateinit var menuCode: String

    private var tempCartProduct: CartProduct? = null

    private var tempCartProducts: MutableList<CartProduct>? = mutableListOf()
    private var tempCartProductsPickUp: MutableList<CartProductPickUp>? = mutableListOf()

    private var tempCartDonut: MutableList<CartDetailVariant>? = mutableListOf()

    // Container to store variant qty
    private var tempCartVariant: MutableList<CartTempItem> = mutableListOf()
    private var tempCartVariantGrouped: MutableList<CartTempItem> = mutableListOf()
    private var tempDonutCustom: MutableList<CartTempDonutCustom> = mutableListOf()

    private fun insertToCart(cartProduct: CartProduct): Completable =
        jcoDatabase.cartDao().insertCart(cartProduct)

    private fun insertToCartPickUp(cartProduct: CartProductPickUp): Completable =
        jcoDatabase.cartDao().insertCartPickUp(cartProduct)

    private fun insertToCarts(cartProduct: MutableList<CartProduct>): Completable =
        jcoDatabase.cartDao().insertCarts(cartProduct)

    private fun insertToCartsPickUp(cartProduct: MutableList<CartProductPickUp>): Completable =
        jcoDatabase.cartDao().insertCartsPickUp(cartProduct)

    private fun cartProduct(): Flowable<List<CartProduct>> =
        jcoDatabase.cartDao().getAllCartProduct()


    init {
        getAllItemCart()
    }

    @SuppressLint("CheckResult")
    fun loadDetail(id: String) {
        sharedPreference.removeCodeVoucher()
        sharedPreference.removeVoucher()
        menuCode = id

        //set loading page
        val loading = mutableListOf<BaseCell>()
        loading.add(LoadingPage())
        _datas.postValue(loading)

        // Reset data
        Log.d("RESET", "RESET | maxDonutCustomize: " + maxDonutCustomize + " | max: " + max)
        maxDonutCustomize = 0
        max.setValue(0)
        Log.d("RESET", "RESET | maxDonutCustomize: " + maxDonutCustomize + " | max: " + max)

        val body = ProductDetailReq(
            "1",
            sharedPreference.loadSelectCity().toString(),
            menuCode,
            sharedPreference.loadPhoneNumber().toString()
        )
        lastDisposable = productRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                _productDetail.value = model.data
                _nameProduct.value = localizationMenuName(model)
                totalAmount.value = model.data.menu_amount.toInt()
                model.data.menu_image?.map {
                    _menuImage.postValue(it)
                }
                Log.d("DATA__", "is favorite " + model.data.is_favorite)

                if (model.data.category_name.contains(
                        "beverage",
                        true
                    ) && !model.data.category_name.contains("uncustom", true)
                ) {
                    price.postValue(model.data.details?.get(0)?.menu_price)
                    temp.add(
                        ProductDetailContent(
                            model.data.menu_code,
                            model.data.id_menu.toInt(),
                            "#FAC1A8",
                            "#F25A17",
                            model.data.menu_desc,
                            "",
                            model.data.menu_price,
                            price.value ?: "",
                            localizationMenuName(model),
                            1,
                            0,
                            model.data.menu_amount.toInt(),
                            model.data.category_title,
                            model.data.menu_image?.get(0)?.image,
                            model.data.details,
                            model.data.variant_num,
                            Converter.thousandSeparator(model.data.menu_normalprice.toString())
                        )
                    )
                } else {
                    price.postValue(model.data.menu_price)
                    defaultTempCartProduct(model, model.data.menu_price.toString(), "")
                    max.value = model.data.menu_amount.toInt()
                    temp.add(
                        ProductDetailContent(
                            model.data.menu_code,
                            model.data.id_menu.toInt(),
                            "#FAC1A8",
                            "#F25A17",
                            model.data.menu_desc,
                            "",
                            model.data.menu_price,
                            price.value ?: "",
                            localizationMenuName(model),
                            1,
                            0,
                            model.data.menu_amount.toInt(),
                            model.data.category_title,
                            model.data.menu_image?.get(0)?.image,
                            model.data.details,
                            model.data.variant_num,
                            Converter.thousandSeparator(model.data.menu_normalprice.toString())
                        )
                    )
                }

                if (model.data.category_name.contains(
                        "beverage",
                        true
                    ) && !model.data.category_name.contains("uncustom", true)
                ) {
                    temp.add(ProductDetailChooseItemHeader("test"))
                }

                if (model.data.category_name == "package" && model.data.details != null) {
                    model.data.details.map { detail ->
                        if (model.data.category_name == "package" && !(detail.package_name?.contains(
                                "sendiri",
                                true
                            ) ?: false)
                        ) {
                            // Package donut
                            temp.add(
                                ProductDetailPackage(
                                    0,
                                    detail.package_name.toString(),
                                    detail.package_image.toString(),
                                    detail.package_description.toString().replace("\\n", "\n"),
                                )
                            )
                            // Store variant in temp container
                            var cartTempItem = CartTempItem(
                                variantName = detail.package_name.toString(),
                                variantQty = 0
                            )
                            tempCartVariant?.add(cartTempItem)
                            Log.d("TEMP", tempCartVariant.toString())
                        } else {
                            // Custom variant
                            temp.add(
                                ProductDetailVariant(
                                    0,
                                    detail.package_name.toString(),
                                    detail.package_donut_list ?: mutableListOf(),
                                    max = model.data.menu_amount.toInt()
                                )
                            )

                            var cartTempItem = CartTempItem(
                                variantName = detail.package_name.toString(),
                                variantQty = 0
                            )
                            tempCartVariant?.add(cartTempItem)
//                            _qty.value =
//                                detail.package_donut_list?.size?.let { List(it) { 0 } } as MutableList<Int>?
//                            detail.package_donut_list?.map { packageDonut ->
//                                temp.add(
//                                    PackageDonut(
//                                        menu_name = packageDonut.menu_name,
//                                        menu_img = packageDonut.menu_img,
//                                        quantity = 12
//                                    )
//                                )
//                            }

                            detail.package_donut_list?.map { donut ->
                                temp.add(
                                    DonutCustom(
                                        donut.menu_img ?: "",
                                        donut.menu_name ?: "",
                                        0
                                    )
                                )
                                var initDonutCustom = CartTempDonutCustom(
                                    donutName = donut.menu_name ?: "",
                                    donutQty = 0
                                )
                                tempDonutCustom?.add(initDonutCustom)
                                Log.d("TEMP", tempDonutCustom.toString())
                            }
                        }
                    }
                }

                if (model.data.category_name == "package") {
                    temp.add(
                        ProductDetailVariantDonut(
                            model.data.details?.map { detailVariant ->
                                detailVariant.package_name.toString()
                            }
                        )
                    )
                }

                if (model.data.category_name == "beverage" && !model.data.category_name.contains(
                        "uncustom",
                        true
                    )
                ) {
                    val beverages = mutableListOf<SizeBeverage>()
                    model.data.details?.map {
                        beverages.add(
                            SizeBeverage(it.size, it.menu_code, false, it.menu_price)
                        )
                        sizeSelected.postValue(beverages[0].size)
                        sizeDefault = beverages[0].size.toString()
                    }
                    temp.add(SizeVariantBeverage(beverages.distinctBy {
                        it.size
                    }))
                }

                if (model.data.category_name.contains(
                        "beverage",
                        true
                    ) && !model.data.category_name.contains("uncustom", true)
                ) {
                    temp.add(HeaderVariantBeverage("test"))
                    val beverages = mutableListOf<Variant>()
                    model.data.details?.map {
                        beverages.add(
                            Variant(it.temp)
                        )
                        variantSelected.postValue(beverages[0].variant)

                    }
                    defaultTempCartProductBeverage(
                        model,
                        model.data.details?.get(0)?.menu_price ?: "",
                        "$sizeDefault - ${beverages[0].variant}"
                    )
                    temp.add(VariantBeverage(beverages.distinct()))
                    tempCartDonut?.add(CartDetailVariant(beverages[0].variant.toString()))
                }

                temp.add(Divider16(""))
                temp.add(Divider16(""))
                temp.add(Divider16(""))
                temp.add(Divider16(""))
                temp.add(Divider16(""))
                temp.add(Divider16(""))
                _donut.postValue(model.data.details)
//                    temp.add(DeliveryWebView("http://192.168.1.12:8888/oxxo/jco/product_detail?code=" + menuCode))
                _datas.postValue(temp)
                Log.d("cektemp", temp.toString())
            }, {
                handleError(it)
            })


//                    {
//                    data ->
//                    val temp = _datas.value?.toMutableList() ?: mutableListOf()
//                    temp.add(data.content)
//                    temp.addAll(data.donuts)

//                    _datas.postValue(temp)
//                },{
//
//                }
//                )
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun defaultTempCartProduct(
        content: ProductDetailRes,
        price: String,
        variantName: String?,
    ) {
        tempCartProduct = CartProduct(
            menuCode = content.data.menu_code.toInt(),
            name = localizationMenuName(content),
            imgURL = content.data.menu_image?.get(0)?.image,
            price = price.toDouble(),
            productType = content.data.category_title,
            notes = "",
            qty = 1,
            detailVariant = tempCartDonut,
            priceOriginal = price.toDouble(),
            variantname = variantName.toString()
        )
        Log.d("cekqty", tempCartProduct.toString())
    }

    private fun defaultTempCartProductBeverage(
        content: ProductDetailRes,
        price: String,
        variantName: String?,
    ) {
        tempCartProduct = CartProduct(
            menuCode = content.data.details?.get(0)?.menu_code?.toInt(),
            name = localizationMenuName(content),
            imgURL = content.data.menu_image?.get(0)?.image,
            price = price.toDouble(),
            productType = content.data.category_title,
            notes = "",
            qty = 1,
            detailVariant = tempCartDonut,
            priceOriginal = price.toDouble(),
            variantname = variantName.toString()
        )
        Log.d("cekqty", tempCartProduct.toString())
    }

    private fun doInserTotCart(cartProduct: CartProduct, quantity: Int) {
        if (quantity > 0) {
            compositeDisposable.add(
                insertToCart(cartProduct).subscribeOn(schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {
                        handleError(it)
                    })
            )
        }
    }

    private fun doInserTotCartPickUp(cartProduct: CartProductPickUp, quantity: Int) {
        if (quantity > 0) {
            compositeDisposable.add(
                insertToCartPickUp(cartProduct).subscribeOn(schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {
                        handleError(it)
                    })
            )
        }
    }

    private fun doInsertToCarts(cartProduct: MutableList<CartProduct>) {
        var isCustom = true
        cartProduct.forEach {
            Log.d("tag", "${it.variantname} ${it.getMenuName()}")
            if (it.variantname.contains("sendiri", true) && it.getMenuName().isEmpty()) {
//                isCustom = false
            }

        }
        if (isCustom && donutCustomSelected == limitDonutCustom) {
            compositeDisposable.add(
                insertToCarts(cartProduct).subscribeOn(schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {
                        handleError(it)
                    })
            )
        } else {
            _showAlert.value = SingleEvents(context.getString(R.string.alert_donut))
        }
    }

    private fun doInsertToCartsPickUp(cartProduct: MutableList<CartProductPickUp>) {
        var isCustom = true
        cartProduct.forEach {
            Log.d("tag", "${it.variantname} ${it.getMenuName()}")
            if (it.variantname.contains("sendiri", true) && it.getMenuName().isEmpty()) {
//                isCustom = false
            }

        }
        if (isCustom && donutCustomSelected == limitDonutCustom) {
            compositeDisposable.add(
                insertToCartsPickUp(cartProduct).subscribeOn(schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {
                        handleError(it)
                    })
            )
        } else {
            _showAlert.value = SingleEvents(context.getString(R.string.alert_donut))
        }
    }

    private fun localizationMenuName(product: ProductDetailRes): String =
        if (sharedPreference.loadLanguage()
                ?.contains("indonesia", true) == true
        ) product.data.menu_name
        else product.data.menu_name_en

    /**
    logic for make new line every variant
     */
    private fun addNewLineProduct(header: ProductDetailContent, donut: ProductDetailVariant) {
        if (header.productName?.contains("1") == true) {
            val fixPrice = header.price?.toInt()?.times(donut.quantity)
            if (donut.quantity <= 1) {
                tempCartProducts?.add(
                    CartProduct(
                        menuCode = header.menuCode?.toInt(),
                        name = header.productName,
                        imgURL = header.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = header.type,
                        notes = "",
                        qty = donut.quantity,
                        detailVariant = tempCartDonut,
                        variantname = donut.variant,
                        priceOriginal = header.price?.toDouble() ?: 0.0,
                    )
                )
            } else {
                tempCartProducts?.removeFirstOrNull()
                tempCartProducts?.add(
                    CartProduct(
                        menuCode = header.menuCode?.toInt(),
                        name = header.productName,
                        imgURL = header.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = header.type,
                        notes = "",
                        qty = donut.quantity,
                        detailVariant = tempCartDonut,
                        variantname = donut.variant,
                        priceOriginal = header.price?.toDouble() ?: 0.0,
                    )
                )
            }
        } else {
            val fixPrice = header.price?.toInt()?.times(1)
            tempClick++
            if (tempClick < 2) {
                variantDonut.append("${donut.variant}, ")
            } else if (!variantDonut.contains(donut.variant)) {
                variantDonut.append(donut.variant).toString()
            }

            if (tempClick >= 2) {
                tempCartProducts?.add(
                    CartProduct(
                        menuCode = header.menuCode?.toInt(),
                        name = header.productName,
                        imgURL = header.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = header.type,
                        notes = "",
                        qty = 1,
                        detailVariant = tempCartDonut,
                        variantname = variantDonut.toString().removeSuffix(", "),
                        priceOriginal = header.price?.toDouble() ?: 0.0,
                    )
                )
                tempClick = 0
                variantDonut = StringBuilder()
            }
        }

    }

    /**
     * logic for new line variant for 6,3 2 pcs
     */

    private fun addNewLineOther(header: ProductDetailContent, donut: ProductDetailVariant) {
        val fixPrice = header.price?.toInt()?.times(donut.quantity)
        if (donut.quantity <= 1) {
            tempCartProducts?.add(
                CartProduct(
                    menuCode = header.menuCode?.toInt(),
                    name = header.productName,
                    imgURL = header.menu_image,
                    price = fixPrice?.toDouble() ?: 0.0,
                    productType = header.type,
                    notes = "",
                    qty = donut.quantity,
                    detailVariant = tempCartDonut,
                    variantname = donut.variant,
                    priceOriginal = header.price?.toDouble() ?: 0.0,
                )
            )
        } else {
            tempCartProducts?.removeFirstOrNull()
            tempCartProducts?.add(
                CartProduct(
                    menuCode = header.menuCode?.toInt(),
                    name = header.productName,
                    imgURL = header.menu_image,
                    price = fixPrice?.toDouble() ?: 0.0,
                    productType = header.type,
                    notes = "",
                    qty = donut.quantity,
                    detailVariant = tempCartDonut,
                    variantname = donut.variant,
                    priceOriginal = header.price?.toDouble() ?: 0.0,
                )
            )
        }
    }

    private fun addVariantCostumeDonut(
        tempQty: MutableList<Int>,
        positionDonut: Int,
        donut: ProductDetailVariant,
    ) {
        if (tempQty[positionDonut] <= 1) {
            tempCartDonut?.add(
                CartDetailVariant(
                    donut.variantDonut[positionDonut].menu_name.toString(),
                    tempQty[positionDonut]
                )
            )
            Log.d("cektempqty", "true" + tempQty[positionDonut])
        } else {
//            tempCartDonut?.find {true}?.qty = tempQty[positionDonut]
            tempCartDonut?.add(
                CartDetailVariant(
                    donut.variantDonut[positionDonut].menu_name.toString(),
                    tempQty[positionDonut]
                )
            )
            Log.d("cektempqty", "false" + tempQty[positionDonut] + tempCartDonut)
        }
    }

    private fun getAllItemCart() {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                var totalQty = 0
                data.forEach {
                    Log.d("tag", it.qty.toString())
                    totalQty += it.qty
                }
                _sizeCart.postValue(totalQty)
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    override fun onPlusClick(position: Int) {
        _datas.value?.let {
            val content = (it[position] as ProductDetailContent).copy()
            content.quantity++
            max.value = content.quantity * content.totalPerPack
            val size = sizeSelected.value
            val variant = variantSelected.value

            content.variant?.map { data ->
                if (data.size?.contains(
                        size.toString(),
                        true
                    ) == true && data.temp?.contains(variant.toString(), true) == true
                ) {
                    price.postValue(data.menu_price)

                    val fixPrice = data.menu_price?.toInt()?.times(content.quantity)
                    tempCartProduct = CartProduct(
                        menuCode = data.menu_code?.toInt(),
                        name = content.productName,
                        imgURL = content.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = content.type,
                        notes = "",
                        qty = content.quantity,
                        detailVariant = tempCartDonut,
                        priceOriginal = data.menu_price?.toDouble() ?: 0.0,
                        variantname = "$size - $variant"
                    )
                }
            }

            if (content.variant.isNullOrEmpty()) {
                val fixPrice = content.price?.toInt()?.times(content.quantity)
                tempCartProduct = CartProduct(
                    menuCode = content.menuCode?.toInt(),
                    name = content.productName,
                    imgURL = content.menu_image,
                    price = fixPrice?.toDouble() ?: 0.0,
                    productType = content.type,
                    notes = "",
                    qty = content.quantity,
                    detailVariant = tempCartDonut,
                    priceOriginal = content.price?.toDouble() ?: 0.0,
                )
            }

            it[position] = content
            _datas.postValue(it)
            Log.d("cekqty", tempCartProduct.toString())
        }
    }

    override fun onMinusClick(position: Int) {
        _datas.value?.let {
            val content = (it[position] as ProductDetailContent).copy()

            if (content.quantity > 0) {
                if (content.quantity <= qtyVariant) {
                    _showAlert.value =
                        SingleEvents(context.getString(R.string.please_check_your_box_quantity))
                } else {
                    content.quantity--
                    max.value = content.quantity * content.totalPerPack
                    it[position] = content
                    _datas.postValue(it)
                    val size = sizeSelected.value
                    val variant = variantSelected.value
                    content.variant?.map { data ->
                        if (data.size?.contains(
                                size.toString(),
                                true
                            ) == true && data.temp?.contains(variant.toString(), true) == true
                        ) {
                            price.postValue(data.menu_price)

                            val fixPrice = data.menu_price?.toInt()?.times(content.quantity)
                            tempCartProduct = CartProduct(
                                menuCode = content.menuCode?.toInt(),
                                name = content.productName,
                                imgURL = content.menu_image,
                                price = fixPrice?.toDouble() ?: 0.0,
                                productType = content.type,
                                notes = "",
                                qty = content.quantity,
                                detailVariant = tempCartDonut,
                                priceOriginal = data.menu_price?.toDouble() ?: 0.0,
                            )
                        }
                    }
                }
            }
        }
    }

    //TODO cek donut qty
    override fun onDonutPlusClick(positionDetail: Int, positionDonut: Int) {
        Log.d("CLICK", "DONUT | limitDonutCustom: " + limitDonutCustom)
        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            val donut = (it[positionDetail] as ProductDetailVariant).copy()
            val max = header.quantity * header.totalPerPack

//            Log.d("cekbasecell", it.toString())

            if (donut.quantity > 0) {
                Log.d(
                    "CHECKDONUT",
                    "Step 1 | maxDonutCustomize:" + maxDonutCustomize + " | limitDonutCustom: " + limitDonutCustom
                )
//                if (maxDonutCustomize < header.totalPerPack) {
                if (maxDonutCustomize < limitDonutCustom) {
                    Log.d("CHECKDONUT", "Step 2")
                    _qty.value = MutableList(donut.variantDonut.size) { index ->
                        val tempQty: MutableList<Int> =
                            MutableList(donut.variantDonut.size) {
                                _qty.value?.get(index) ?: 0
                            }
//                        _showAlertDonutCustomize.value = SingleEvents("showAlertDonutCustomize")
                        Log.d("CHECKDONUT", "Step 3")
                        if (positionDonut == index) {
                            Log.d("CHECKDONUT", "Step 4")
                            if (maxDonutCustomize >= limitDonutCustom) {
                                Log.d("CHECKDONUT", "Step 5A")
                                tempQty[positionDonut] = _qty.value?.get(index)?.plus(1) ?: 0
//                                _showAlertDonutCustomize.value = SingleEvents("showAlertDonutCustomize")
                                maxDonutCustomize++
                                donut.quantity++
                                header.quantity++
                                maxDonutCustom.value = maxDonutCustomize
                                addVariantCostumeDonut(tempQty, positionDonut, donut)
                            } else {
                                Log.d("CHECKDONUT", "Step 5B")
                                tempQty[positionDonut] = _qty.value?.get(index)?.plus(1) ?: 0
                                maxDonutCustomize++
                                maxDonutCustom.value = maxDonutCustomize
//                                _showAlertDonutCustomize.value = SingleEvents("showAlertDonutCustomize")
                                addVariantCostumeDonut(tempQty, positionDonut, donut)
                            }

                        }
                        tempQty[positionDonut]
                    }
                }
            } else {
                _showAlertDonutCustomize.value = SingleEvents("showAlertDonutCustomize")
            }
            it[0] = header
            it[positionDetail] = donut
            _datas.postValue(it)
//            Log.d("cekcustomdonut",donut.toString())
        }
    }

    override fun onDonutMinusClick(positionDetail: Int, positionDonut: Int) {
        _datas.value?.let {

            val donut = (it[positionDetail] as ProductDetailVariant).copy()

            if (donut.quantity > 0) {
                _qty.value = MutableList(donut.variantDonut.size) { index ->
                    val tempQty: MutableList<Int> =
                        MutableList(donut.variantDonut.size) { _qty.value?.get(index) ?: 0 }
                    if (_qty.value?.get(index) ?: 0 > 0) {
                        if (positionDonut == index) {
                            tempQty[positionDonut] = _qty.value?.get(index)?.minus(1) ?: 0
                            maxDonutCustomize--
                            maxDonutCustom.value = maxDonutCustomize
                        }
                    }
                    tempQty[positionDonut]
                }

            } else {
                _showAlertDonutCustomize.value = SingleEvents("showAlertDonutCustomize")
            }



            it[positionDetail] = donut
            Log.d("cekdonut", "position = $positionDonut , donut = $donut")
            _datas.postValue(it)

        }
    }

    override fun onMixVariantPlusClick(position: Int) {
        Log.d("CLICK", "MIX")
        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            val max = header.quantity * header.totalPerPack
            val total = (header.quantityInPcs + header.totalPerPack) - 1
            if (header.quantityInPcs < max && total < max) {
                val mix = (it[position] as MixVariant).copy()
                mix.qty++

                header.quantityInPcs += header.totalPerPack

                it[0] = header
                it[position] = mix
                _datas.postValue(it)

            }
        }
    }

    override fun onMixVariantMinusClick(position: Int) {
        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            if (header.quantityInPcs > 0) {
                val mix = (it[position] as MixVariant).copy()
                mix.qty--

                header.quantityInPcs -= header.totalPerPack

                it[0] = header
                it[position] = mix
                _datas.postValue(it)
            }
        }
    }

    override fun onDonutCustomPlusClick(position: Int) {
        // Customdonut
        Log.d("POSITION: ", position.toString())
//        Log.d("DATA: ", _datas.value.toString())
        _datas.value?.let {
            val donut = (it[position] as DonutCustom).copy()

            if (donutCustomSelected < limitDonutCustom) {
                // Update qty at temp container
                tempDonutCustom[position - tempCartVariant.size - 1].donutQty += 1

                donut.quantity++
                donutCustomSelected++

                it[position] = donut
                _datas.postValue(it)
            } else {
                _showAlert.value = SingleEvents(context.getString(R.string.alert_donut_custom))
            }
        }

        Log.d("CURRENTTEMP: ", tempDonutCustom.toString())
    }

    override fun onDonutCustomMinusClick(position: Int) {
        // Customdonut
        Log.d("POSITION: ", position.toString())
//        Log.d("DATA: ", _datas.value.toString())
        _datas.value?.let {
            val donut = (it[position] as DonutCustom).copy()

            if (donut.quantity > 0) {
                // Update qty at temp container
                tempDonutCustom[position - tempCartVariant.size - 1].donutQty -= 1

                donut.quantity--
                donutCustomSelected--

                it[position] = donut
                _datas.postValue(it)
            }
        }

        Log.d("CURRENTTEMP: ", tempDonutCustom.toString())
    }

    override fun onSizeBeverageClick(sizeBeverage: SizeBeverage) {
        _datas.value.let {
            sizeSelected.postValue(sizeBeverage.size)
            val detail = (it?.get(0) as ProductDetailContent).copy()

            val variant = variantSelected.value

            detail.variant?.map { data ->
                if (data.size?.contains(
                        sizeBeverage.size.toString(),
                        true
                    ) == true && data.temp?.contains(variant.toString(), true) == true
                ) {
                    price.postValue(data.menu_price)

                    val fixPrice = data.menu_price?.toInt()?.times(detail.quantity)
                    tempCartProduct = CartProduct(
                        menuCode = detail.menuCode?.toInt(),
                        name = detail.productName,
                        imgURL = detail.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = detail.type,
                        notes = "",
                        qty = detail.quantity,
                        detailVariant = tempCartDonut,
                        priceOriginal = data.menu_price?.toDouble() ?: 0.0,
                        variantname = "${sizeBeverage.size} $variant"
                    )
                }

            }
            _datas.postValue(it)
        }
    }

    private fun groupVariant() {
        Log.d("CHECK", tempCartVariant.toString())

        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            val fixPrice = header.price?.toInt()

            // Loop through temp container
            var cartAddVariant = ""
            var cartAddCount = 0
            var previousVariant = ""

            tempCartVariant.map { t ->
                if (t.variantQty > 0) {
                    repeat(t.variantQty) {
                        // Add variant to cart
                        if (cartAddCount == 1) {
                            cartAddVariant += ", "
                        }
                        cartAddVariant += t.variantName

                        // Donut Custom
                        if (t.variantName == "Pilih Sendiri") {
                            var donutCustomCount = 0
                            cartAddVariant += ": "
                            tempDonutCustom.map { c ->
                                if (c.donutQty > 0) {
                                    if (donutCustomCount > 0) {
                                        cartAddVariant += ", "
                                    }
                                    cartAddVariant += c.donutQty.toString() + " " + c.donutName
                                    donutCustomCount++
                                }
                            }
                        }

                        cartAddCount += 1
                        var varNum = header.variantNum?.toInt()

                        Log.d("TESTVARNUM", varNum.toString())
                        if (cartAddCount == varNum) {
                            // Insert into temp container grouped
                            if (cartAddVariant == previousVariant) {
                                // If the same group, add quantity
                                tempCartVariantGrouped[tempCartVariantGrouped.lastIndex].variantQty += 1
                            } else {
                                var currentVariant = CartTempItem(
                                    variantName = cartAddVariant,
                                    variantQty = 1
                                )
                                tempCartVariantGrouped.add(currentVariant)
                                previousVariant = cartAddVariant
                            }

                            Log.d(
                                "CHECKCART",
                                t.toString() + " | " + cartAddCount.toString() + " | " + cartAddVariant
                            )

                            // Reset count
                            cartAddCount = 0
                            cartAddVariant = ""
                        }
                    }
                }
            }

            // After finish group, loop through container grouped
            Log.d("GROUPEDCART", tempCartVariantGrouped.toString())
            tempCartVariantGrouped.map { g ->
                // Add to temp cart
                tempCartProducts?.add(
                    CartProduct(
                        menuCode = header.menuCode?.toInt(),
                        name = header.productName,
                        imgURL = header.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = header.type,
                        notes = "",
                        qty = g.variantQty,
                        detailVariant = tempCartDonut,
                        variantname = g.variantName.toString(),
                        priceOriginal = header.price?.toDouble() ?: 0.0,
                    )
                )

                tempCartProductsPickUp?.add(
                    CartProductPickUp(
                        menuCode = header.menuCode?.toInt(),
                        name = header.productName,
                        imgURL = header.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = header.type,
                        notes = "",
                        qty = g.variantQty,
                        detailVariant = tempCartDonut,
                        variantname = g.variantName.toString(),
                        priceOriginal = header.price?.toDouble() ?: 0.0,
                    )
                )

            }

//            tempCartProducts?.removeFirstOrNull()
        }
    }

    override fun onAddToCart() {
        // Group variant for each lines
        groupVariant()

        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            val max = header.quantity * header.totalPerPack

            val tempCartProductPickUp = CartProductPickUp(
                id = tempCartProduct?.id ?: 0,
                menuCode = tempCartProduct?.menuCode,
                name = tempCartProduct?.name,
                imgURL = tempCartProduct?.imgURL,
                price = tempCartProduct?.price ?: 0.0,
                productType = tempCartProduct?.productType,
                notes = "",
                qty = tempCartProduct?.qty ?: 0,
                detailVariant = tempCartDonut,
                priceOriginal = tempCartProduct?.priceOriginal ?: 0.0,
                variantname = tempCartProduct?.variantname.toString()
            )


            if (header.quantity <= 0) {
                _showAlert.value = SingleEvents("Harap tambah quantity Anda")
            } else {
                if (header.type?.contains(
                        "package",
                        true
                    ) == true && !header.variant.isNullOrEmpty()
                ) {
                    if (header.quantityInPcs < max) {
                        if (header.totalPerPack == 24) {
                            _showAlert.value =
                                SingleEvents("Harap pilih ${header.quantity * 2} box donut terlebih dahulu")
                            tempCartProducts?.clear()
                            tempCartVariantGrouped.clear()
                        } else {
                            _showAlert.value =
                                SingleEvents("Harap pilih ${header.quantity} box donut terlebih dahulu")
                            tempCartProducts?.clear()
                            tempCartVariantGrouped.clear()
                        }
                    } else {
                        if (header.variant.isNullOrEmpty()) {
                            doInserTotCart(tempCartProduct ?: return, tempCartProduct?.qty ?: 0)
                            doInserTotCartPickUp(
                                tempCartProductPickUp,
                                tempCartProductPickUp.qty
                            )
                            _addToCart.value = SingleEvents("addToCart")
                        } else {
                            doInsertToCarts(tempCartProducts ?: mutableListOf())
                            doInsertToCartsPickUp(tempCartProductsPickUp ?: mutableListOf())
                            _addToCart.value = SingleEvents("addToCart")
                        }
                    }
                } else {
                    _addToCart.value = SingleEvents("addToCart")
                    doInserTotCart(tempCartProduct ?: return, tempCartProduct?.qty ?: 0)
                    doInserTotCartPickUp(tempCartProductPickUp, tempCartProductPickUp.qty)
                }
            }
        }
    }

    override fun onBtnFavoriteClick() {
        productDetail.value?.let { modelData ->
            val favorite = if (modelData.is_favorite == "1") "0" else "1"
            val body = ProductFavoriteReq(
                favorite,
                sharedPreference.loadPhoneNumber().toString(),
                menuCode
            )
            lastDisposable = homeRepository.setProductFavorite(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    modelData.is_favorite = favorite
                    _productDetail.postValue(modelData)
                }, {
                    handleError(it)
                })

            lastDisposable?.let { compositeDisposable.add(it) }
        }
    }

    override fun onVariantBeverageClick(variant: Variant) {
        _datas.value.let {
            variantSelected.postValue(variant.variant)
            tempCartDonut?.removeFirst()
            tempCartDonut?.add(CartDetailVariant(variant.variant.toString()))

            val detail = (it?.get(0) as ProductDetailContent).copy()
            val size = sizeSelected.value

            detail.variant?.mapIndexed { index, data ->
                if (data.size?.contains(
                        size.toString(),
                        true
                    ) == true && data.temp?.contains(variant.variant.toString(), true) == true
                ) {
                    price.postValue(data.menu_price)
                    positionVariantSelected.postValue(index)
                    val fixPrice = data.menu_price?.toInt()?.times(detail.quantity)
                    tempCartProduct = CartProduct(
                        menuCode = data.menu_code?.toInt(),
                        name = detail.productName,
                        imgURL = data.menu_image,
                        price = fixPrice?.toDouble() ?: 0.0,
                        productType = detail.type,
                        notes = "",
                        qty = detail.quantity,
                        detailVariant = tempCartDonut,
                        priceOriginal = data.menu_price?.toDouble() ?: 0.0,
                        variantname = "$size - ${variant.variant}"
                    )
                }

            }
            _datas.postValue(it)
        }
    }

    override fun onVariantDonutPlusClick(position: Int) {
        Log.d("CLICK", "VARIANT " + position.toString() + " limit: " + limitDonutCustom)
        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            val max = header.quantity * header.totalPerPack
            limitDonutCustom += header.totalPerPack
            if (header.quantityInPcs < max) {
                when {
                    header.totalPerPack <= 2 -> {
                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++
                        qtyVariant++
                        header.quantityInPcs += 2
                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = max
                        mainQty.value = header.quantityInPcs

                        addNewLineOther(header, donut)

                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                    header.totalPerPack <= 3 -> {
                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++
                        qtyVariant++
                        header.quantityInPcs += 3
                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = max
                        mainQty.value = header.quantityInPcs

                        addNewLineOther(header, donut)

                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                    header.totalPerPack <= 6 -> {
                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++
                        qtyVariant++
                        header.quantityInPcs += 6
                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = max
                        mainQty.value = header.quantityInPcs

                        if (donut.quantity > 0 && !donut.variant.contains("sendiri", true)) {
                            tempCartDonut?.add(CartDetailVariant(donut.variant, donut.quantity))
                        }

                        tempCartVariant[position - 1].variantQty += 1
//                        addNewLineOther(header, donut)

                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                    else -> {
//                        tempCartVariant[position].variantQty += 1
                        Log.d("NEW " + position.toString(), tempCartVariant.toString())

                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++
                        qtyVariant++
                        header.quantityInPcs += 12
                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = max
                        mainQty.value = header.quantityInPcs

                        tempCartVariant[position - 1].variantQty += 1
//                        addNewLineProduct(header, donut)

                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                }
            } else {
                when {
                    header.totalPerPack <= 2 -> {
                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++

                        header.quantityInPcs += 2
                        header.quantity++
                        val maximum = header.quantity * header.totalPerPack
                        val fixPrice = header.price?.toInt()?.times(header.quantity)
                        qtyVariant++
//                        if (donut.quantity > 0) {
//                            tempCartDonut?.add(CartDetailVariant(donut.variant))
//                        }

                        tempCartProduct = CartProduct(
                            menuCode = header.menuCode?.toInt(),
                            name = header.productName,
                            imgURL = header.menu_image,
                            price = fixPrice?.toDouble() ?: 0.0,
                            productType = header.type,
                            notes = "",
                            qty = header.quantity,
                            detailVariant = tempCartDonut,
                            priceOriginal = header.price?.toDouble() ?: 0.0,
                        )

                        addNewLineOther(header, donut)

                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = maximum
                        mainQty.value = header.quantityInPcs
                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                    header.totalPerPack <= 3 -> {
                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++
                        qtyVariant++
                        header.quantityInPcs += 3
                        header.quantity++
                        val maximum = header.quantity * header.totalPerPack
                        val fixPrice = header.price?.toInt()?.times(header.quantity)

//                        if (donut.quantity > 0) {
//                            tempCartDonut?.add(CartDetailVariant(donut.variant))
//                        }

                        tempCartProduct = CartProduct(
                            menuCode = header.menuCode?.toInt(),
                            name = header.productName,
                            imgURL = header.menu_image,
                            price = fixPrice?.toDouble() ?: 0.0,
                            productType = header.type,
                            notes = "",
                            qty = header.quantity,
                            detailVariant = tempCartDonut,
                            priceOriginal = header.price?.toDouble() ?: 0.0,
                        )

                        addNewLineOther(header, donut)

                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = maximum
                        mainQty.value = header.quantityInPcs
                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                    header.totalPerPack <= 6 -> {
                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++

                        header.quantityInPcs += 6
                        header.quantity++
                        qtyVariant++
                        val maximum = header.quantity * header.totalPerPack
                        val fixPrice = header.price?.toInt()?.times(header.quantity)

                        if (donut.quantity > 0 && !donut.variant.contains("sendiri", true)) {
                            tempCartDonut?.add(CartDetailVariant(donut.variant, donut.quantity))
                        }

//                        addNewLineOther(header, donut)
                        tempCartVariant[position - 1].variantQty += 1

                        tempCartProduct = CartProduct(
                            menuCode = header.menuCode?.toInt(),
                            name = header.productName,
                            imgURL = header.menu_image,
                            price = fixPrice?.toDouble() ?: 0.0,
                            productType = header.type,
                            notes = "",
                            qty = header.quantity,
                            detailVariant = tempCartDonut,
                            priceOriginal = header.price?.toDouble() ?: 0.0,
                        )
                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = maximum
                        mainQty.value = header.quantityInPcs
                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                    else -> {
                        val donut = (it[position] as ProductDetailVariant).copy()
                        donut.quantity++
                        qtyVariant++
                        header.quantityInPcs += 12
                        header.quantity++
                        val maximum = header.quantity * header.totalPerPack
                        header.price?.toInt()?.times(header.quantity)

//                        addNewLineProduct(header, donut)
                        tempCartVariant[position - 1].variantQty += 1

                        donut.max = max
                        donut.mainQuantity = header.quantityInPcs
                        this.max.value = maximum
                        mainQty.value = header.quantityInPcs
                        it[0] = header
                        it[position] = donut
                        _datas.postValue(it)
//                        Log.d("cekcustomdonut",donut.toString())
                    }
                }
            }
        }
        Log.d("CLICK", "VARIANT-END, limit: " + limitDonutCustom)
    }

    override fun onVariantDonutMinusClick(position: Int) {
        Log.d("CUSTOMPACKAGEMINUS", "1")
        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()

            // Check if donut custom is more than limit
            if (donutCustomSelected <= limitDonutCustom - header.totalPerPack) {
                limitDonutCustom -= header.totalPerPack

                if (header.quantityInPcs > 0) {
                    when {
                        header.totalPerPack <= 2 -> {
                            val donut = (it[position] as ProductDetailVariant).copy()
                            donut.quantity--

                            header.quantityInPcs -= 2
                            qtyVariant--
                            tempCartDonut?.remove(CartDetailVariant(donut.variant))


                            it[0] = header
                            it[position] = donut
                            _datas.postValue(it)
                        }
                        header.totalPerPack <= 3 -> {
                            val donut = (it[position] as ProductDetailVariant).copy()
                            donut.quantity--
                            qtyVariant--
                            tempCartDonut?.remove(CartDetailVariant(donut.variant))

                            header.quantityInPcs -= 3
                            it[0] = header
                            it[position] = donut
                            _datas.postValue(it)
                        }
                        header.totalPerPack <= 6 -> {
                            val donut = (it[position] as ProductDetailVariant).copy()
                            donut.quantity--
                            header.quantityInPcs -= 6
                            qtyVariant--
                            tempCartDonut?.remove(CartDetailVariant(donut.variant))

                            tempCartVariant[position - 1].variantQty -= 1

                            it[0] = header
                            it[position] = donut
                            _datas.postValue(it)
                        }
                        else -> {
                            val donut = (it[position] as ProductDetailVariant).copy()
                            donut.quantity--
                            qtyVariant--
                            header.quantityInPcs -= 12
                            mainQty.value = header.quantityInPcs

                            tempCartDonut?.remove(CartDetailVariant(donut.variant))

                            tempCartVariant[position - 1].variantQty -= 1

                            it[0] = header
                            it[position] = donut
                            _datas.postValue(it)
                        }
                    }
                }
            } else {
                _showAlert.value =
                    SingleEvents(context.getString(R.string.alert_donut_custom_pass_limit))
            }
        }
    }

    override fun onPackagePlusClick(position: Int) {
        Log.d("CLICK", "clickPACKAGE " + position.toString())
        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            val max = header.quantity * header.totalPerPack

            if (header.quantityInPcs < max) {
                // Update qty at temp container
                tempCartVariant[position - 1].variantQty += 1

                val donut = (it[position] as ProductDetailPackage).copy()
                donut.quantity++
                qtyVariant++
                header.quantityInPcs += 12
//                donut.max = max
//                donut.mainQuantity = header.quantityInPcs
                this.max.value = max
                mainQty.value = header.quantityInPcs

                it[0] = header
                it[position] = donut
                _datas.postValue(it)
            } else {
                tempCartVariant[position - 1].variantQty += 1

                val donut = (it[position] as ProductDetailPackage).copy()
                donut.quantity++
                header.quantity++
                qtyVariant++
                header.quantityInPcs += 12
//                donut.max = max
//                donut.mainQuantity = header.quantityInPcs
                this.max.value = max
                mainQty.value = header.quantityInPcs

                it[0] = header
                it[position] = donut
                _datas.postValue(it)
            }
        }
    }

    override fun onPackageMinusClick(position: Int) {
        _datas.value?.let {
            val header = (it[0] as ProductDetailContent).copy()
            if (header.quantityInPcs > 0) {
                // Update qty at temp container
                if (header.totalPerPack == 24) {
                    tempCartVariant[position - 1].variantQty -= 1

                    val donut = (it[position] as ProductDetailPackage).copy()
                    Log.d("cekdonutqty", donut.quantity.toString())
                    donut.quantity--
                    qtyVariant--
                    header.quantityInPcs -= 12
                    mainQty.value = header.quantityInPcs

//                tempCartDonut?.remove(CartDetailVariant(donut.variant))

                    it[0] = header
                    it[position] = donut
                    _datas.postValue(it)

                    if ((donut.quantity % 2) == 0) {
                        header.quantity--
                    }
                } else {
                    tempCartVariant[position - 1].variantQty -= 1

                    val donut = (it[position] as ProductDetailPackage).copy()
                    Log.d("cekdonutqty", donut.quantity.toString())
                    donut.quantity--
                    qtyVariant--
                    header.quantityInPcs -= 12
                    header.quantity--
                    mainQty.value = header.quantityInPcs

//                tempCartDonut?.remove(CartDetailVariant(donut.variant))

                    it[0] = header
                    it[position] = donut
                    _datas.postValue(it)
                }
            }
        }
    }

    //TODO check it again
    override fun onCleared() {
        super.onCleared()
        temp = mutableListOf()
        Log.d("tag", temp.toString())
//        _datas.value.let {
//            it?.removeAll { data -> data is ProductDetailContent }
//            it?.removeAll { data -> data is PackageDonut }
//            it?.removeAll { data -> data is ProductDetailVariant }
//            it?.removeAll { data -> data is ProductDetailVariantDonut }
//            it?.removeAll { data -> data is Divider16 }
//            _datas.value = it
//            Log.d("runnn", it.toString())
//        }
//        _datas.postValue(temp)
    }
}