package com.jcodonuts.app.ui.payment.choose_payment

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*

class ChoosePaymentController(private val listener: ChoosePaymentControllerListener) :
    AsyncEpoxyController() {
    private val TAG = "HomeController"

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is PointHeader -> addPointHeader()
                is Point -> addPointPayment(cellData)
                is EWalletHeader -> addEWalletHeader()
                is EWallet -> addEWallet(cellData, listener, index)
                is TransferVirtualAccountHeader -> addTransferVirtualAccountHeader()
                is TransferVirtualAccount -> addTransferVirtualAccount(cellData, listener, index)
                is OtherPaymentHeader -> addOtherPaymentHeader()
                is OtherPayment -> addOtherPayment(cellData, listener, index)
                is Payment -> choosePayment(cellData,listener,index)
            }
        }
    }

    private fun addPointHeader() {
        pointHeader {
            id("point-header")
        }
    }

    private fun addPointPayment(cellData: Point) {
        pointPayment {
            id("point-payment")
            data(cellData)
        }
    }

    private fun addEWalletHeader() {
        eWalletHeader {
            id("e-wallet-header")
        }
    }

    private fun addEWallet(
        cellData: EWallet,
        listener: ChoosePaymentControllerListener,
        position: Int
    ) {
        eWallet {
            id("e-wallet")
            data(cellData)
            listener(listener)
            position(position)
        }
    }

    private fun addTransferVirtualAccountHeader() {
        transferVirtualAccountHeader {
            id("transfer-virtual-accunt-header")
        }
    }

    private fun addTransferVirtualAccount(
        cellData: TransferVirtualAccount,
        listener: ChoosePaymentControllerListener,
        position: Int
    ) {
        transferVirtualAccount {
            id("transfer-virtual-account")
            data(cellData)
            listener(listener)
            position(position)
        }
    }

    private fun addOtherPaymentHeader() {
        otherPaymentHeader {
            id("other-payment-header")
        }
    }

    private fun addOtherPayment(
        cellData: OtherPayment,
        listener: ChoosePaymentControllerListener,
        position: Int
    ) {
        otherPayment {
            id("other-payment")
            data(cellData)
            listener(listener)
            position(position)
        }
    }

    private fun choosePayment(
        cellData: Payment,
        listener: ChoosePaymentControllerListener,
        position: Int
    ) {
        choosePayment {
            id(cellData.paymentMethod)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}