package com.jcodonuts.app.ui.detail_promo

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.CartProduct
import com.jcodonuts.app.data.local.CartProductPickUp
import com.jcodonuts.app.data.remote.model.res.MenuImage
import com.jcodonuts.app.databinding.FragmentDetailPromoBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.ui.product_detail.ProductSliderAdapter
import com.jcodonuts.app.utils.parseDateWithFormat
import com.smarteist.autoimageslider.SliderAnimations
import de.mateware.snacky.Snacky
import javax.inject.Inject

class DetailPromoFragment @Inject constructor() :
        BaseFragment<FragmentDetailPromoBinding, DetailPromoViewModel>() {

    override fun getViewModelClass(): Class<DetailPromoViewModel> = DetailPromoViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_detail_promo

    private val adapter by lazy { ProductSliderAdapter() }

    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        initObserver()
        binding.apply {
            viewmodel = viewModel
            executePendingBindings()
            btnAddToCart.setOnClickListener { viewModel.onAddToCart() }
        }

        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadDetail(it1) }
            }

        }
        binding.slider.apply {
            setSliderAdapter(adapter)
            startAutoCycle()
            setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            scrollTimeInSec = 3

        }
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initObserver() {
        viewModel.detail.observe(viewLifecycleOwner, {
            binding.promoPeriod.text =
                    "${it.data.start_date.parseDateWithFormat("dd MMM yyyy")} - ${
                        it.data.end_date.parseDateWithFormat("dd MMM yyyy")
                    }"
            adapter.setSlider((it.data.menu_image ?: mutableListOf()) as MutableList<MenuImage>)
        })

        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { _ ->
                viewModel.detail.observe(viewLifecycleOwner, { data ->
                    viewModel.doInsertToCart(
                            CartProduct(
                                    menuCode = data.data.menu_code.toInt(),
                                    id = data.data.id_menu.toInt(),
                                    name = data.data.menu_name,
                                    imgURL = data.data.menu_image?.get(0)?.image,
                                    price = data.data.menu_price?.toDouble() ?: 0.0,
                                    productType = data.data.category_name,
                                    notes = "",
                                    qty = 1,
                                    detailVariant = mutableListOf(),
                                    priceOriginal = data.data.menu_price?.toDouble() ?: 0.0
                            )
                    )
                    viewModel.doInsertToCartPickUp(
                        CartProductPickUp(
                            menuCode = data.data.menu_code.toInt(),
                            id = data.data.id_menu.toInt(),
                            name = data.data.menu_name,
                            imgURL = data.data.menu_image?.get(0)?.image,
                            price = data.data.menu_price?.toDouble() ?: 0.0,
                            productType = data.data.category_name,
                            notes = "",
                            qty = 1,
                            detailVariant = mutableListOf(),
                            priceOriginal = data.data.menu_price?.toDouble() ?: 0.0
                        )
                    )
                    Snacky.builder()
                            .setActivity(activity)
                            .setText("${data.data.menu_name} Sukses ditambahkan ke keranjang")
                            .setDuration(Snacky.LENGTH_SHORT)
                            .setBackgroundColor(
                                    ContextCompat.getColor(
                                            requireContext(),
                                            R.color.c_text_secondary
                                    )
                            )
                            .success()
                            .show()
                })
            }
        })
    }
}