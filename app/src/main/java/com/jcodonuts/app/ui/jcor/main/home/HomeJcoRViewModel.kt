package com.jcodonuts.app.ui.jcor.main.home

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.req.HomeReq
import com.jcodonuts.app.data.remote.model.req.OutletReq
import com.jcodonuts.app.data.remote.model.req.ProductByCategoryReq
import com.jcodonuts.app.data.remote.model.req.ProductFavoriteReq
import com.jcodonuts.app.data.remote.model.res.HomeRes
import com.jcodonuts.app.data.remote.model.res.Product
import com.jcodonuts.app.data.repository.HomeRepository
import com.jcodonuts.app.data.repository.PickupRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.Converter
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SharedPreference
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class HomeJcoRViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val app: Application,
    private val homeRepository: HomeRepository,
    private val pickupRepository: PickupRepository,
    private val schedulers: SchedulerProvider,
    private val gson: Gson,
) :
    BaseViewModel(), HomeControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    val menuSelected = MutableLiveData<String>()

    private val _changeApp = MutableLiveData<SingleEvents<String>>()
    val changeApp: LiveData<SingleEvents<String>>
        get() = _changeApp

    private val _openProductDetail = MutableLiveData<SingleEvents<HomeMenuItem>>()
    val openProductDetail: LiveData<SingleEvents<HomeMenuItem>>
        get() = _openProductDetail

    private var _promoSelected = MutableLiveData<SingleEvents<String>>()
    val promoSelected: LiveData<SingleEvents<String>>
        get() = _promoSelected

    private val _showHotPromo = MutableLiveData<SingleEvents<String>>()
    val showHotPromo: LiveData<SingleEvents<String>>
        get() = _showHotPromo

    private val _showMenuSearch = MutableLiveData<SingleEvents<String>>()
    val showMenuSearch: LiveData<SingleEvents<String>>
        get() = _showMenuSearch

    private val _showJPoint = MutableLiveData<SingleEvents<String>>()
    val showJPoint: LiveData<SingleEvents<String>>
        get() = _showJPoint

    fun fetchHomeFromAPI() {
        val temp = mutableListOf<BaseCell>()
        temp.add(LoadingPage())
        _datas.value = temp

        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionName
        val body = HomeReq(2,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber(),
            "android",
            pInfo.versionCode)
        lastDisposable = homeRepository.fetchHome(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                parseFetchHome(model)

                //save json to sharepreference
                val data = gson.toJson(model)
                sharedPreference.save(SharedPreference.DATA_HOME, data)
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
        menuSelected.postValue("")
    }

    private fun parseFetchHome(model: HomeRes) {
        val temp = mutableListOf<BaseCell>()
        temp.removeAll { it is LoadingPage }
        temp.add(HomeHeadSectionJcoR(model.user.member_name,
            Converter.thousandSeparator(model.jpoint?.point.toString()),
            model.jpoint?.point_tier?.name))

        temp.add(
            HomeSearchSection(
                "test",
                sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN) == null
            )
        )
        temp.add(HomePromoHeader("test"))
        val promos = mutableListOf<PromoBanner>()
        model.promos?.map {
            promos.add(PromoBanner(it.banner_id.toString(), it.banner_img.toString()))
        }
        temp.add(HomePromos(promos))

        temp.add(HomeMenuHeader(app.getString(R.string.select_coffees)))

        val menus = mutableListOf<MenuCategory>()
        model.category.map {
            if (it.category_name == "all") {
                menus.add(
                    MenuCategory(
                        it.category_name, it.category_title, it.category_img,
                        true
                    )
                )
                menuSelected.postValue(it.category_name)
            } else {
                menus.add(
                    MenuCategory(
                        it.category_name, it.category_title, it.category_img,
                        false
                    )
                )
            }
        }

        temp.add(HomeMenuCategories(menus))

        model.products.map {
            temp.add(
                HomeMenuItem(
                    localizationMenuName(it),
                    it.menu_image,
                    it.menu_price,
                    Converter.thousandSeparator(it.menu_price),
                    false,
                    it.is_promo == "1",
                    it.is_freedelivery == "1",
                    it.is_favorite == "1",
                    it.menu_code,
                    Converter.thousandSeparator(it.menu_normalprice.toString())
                )
            )
            if (model.user.status_code != 401) {
                sharedPreference.saveJPoint(model.jpoint?.point.toString())
            }
        }

        _datas.value = temp
    }

    private fun getProductByCategory(category: String) {
        _datas.value?.let {
            it.removeAll { data -> data is HomeMenuItem }
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            _datas.value = it
        }
        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionCode
        val body = ProductByCategoryReq(
            category,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber() ?: "",
            2,
            "android",
            version
        )
        lastDisposable = homeRepository.getProductByCategory(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) {
                    val temp: MutableList<BaseCell> =
                        datas.value?.toMutableList() ?: mutableListOf()
                    temp.removeAll { it is LoadingProductGrid }

                    model.data.map {
                        temp.add(
                            HomeMenuItem(
                                localizationMenuName(it),
                                it.menu_image,
                                it.menu_price,
                                Converter.thousandSeparator(it.menu_price),
                                false,
                                it.is_promo == "1",
                                it.is_freedelivery == "1",
                                false,
                                it.menu_code,
                                Converter.thousandSeparator(it.menu_normalprice.toString())
                            )
                        )
                    }
                    _datas.value = temp
                }
                Log.d("DATA__", gson.toJson(model))
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun loadLocation() {
        val body = OutletReq(2, sharedPreference.loadLatitude(), sharedPreference.loadLongitude())
        lastDisposable = pickupRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status_code == 200) {
                    sharedPreference.saveOutletCodeForDeliveryJcoR(it.data[0].outlet_code)
                    sharedPreference.saveOutletIdForDeliveryJcoR(it.data[0].outlet_id.toString())
                    sharedPreference.saveOrderPostCodeForDelivery(it.data[0].outlet_postcode.toString())
                }
            }, {

            })
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    override fun onSwitchAppClick() {
        _changeApp.value = SingleEvents("change-app")
    }

    override fun onNotificationClick() {}

    override fun onSearchClick() {
        _showMenuSearch.value = SingleEvents("menu_search")
    }

    override fun onPromoSeeAllClick() {
        _showHotPromo.value = SingleEvents("hot_promo")
    }

    override fun onPromosItemClick(promoBanner: PromoBanner) {
        _promoSelected.postValue(SingleEvents(promoBanner.menuCode ?: ""))
    }

    override fun onMenuCategoryClick(menuCategory: MenuCategory) {
        if (menuCategory.name != menuSelected.value) {
            menuSelected.postValue(menuCategory.name)
            if (menuCategory.name != "all") {
                getProductByCategory(menuCategory.name)
            } else {
                getProductByCategory("")
            }
        }
    }

    override fun onMenuItemClick(index: Int) {
        datas.value?.let {
            _openProductDetail.value = SingleEvents(it[index] as HomeMenuItem)
        }
    }

    override fun onMenuItemFavoriteClick(index: Int) {
        _datas.value?.let {
            val temp = (it[index] as HomeMenuItem).copy()
            val favorite = if (temp.isFavorite) "0" else "1"
            val body = ProductFavoriteReq(
                favorite,
                sharedPreference.loadPhoneNumber().toString(),
                temp.menuCode
            )
            lastDisposable = homeRepository.setProductFavorite(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ model ->
                    temp.isFavorite = !temp.isFavorite
                    it[index] = temp
                    _datas.postValue(it)
                }, {
                    handleError(it)
                })

            lastDisposable?.let { compositeDisposable.add(it) }
        }
    }

    override fun onSearchItem(query: String) {}

    override fun onSearchMenuCategoryClick(menuSearchTagName: MenuSearchTagName, position: Int) {}
    override fun onJPointClick() {
        _showJPoint.value =
            SingleEvents(sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN) ?: "")
    }
}