package com.jcodonuts.app.ui.auth.change_password

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jcodonuts.app.data.remote.model.req.ChangePasswordReq
import com.jcodonuts.app.data.repository.AuthRepository
import com.jcodonuts.app.ui.base.BaseViewModel
import com.jcodonuts.app.utils.SchedulerProvider
import com.jcodonuts.app.utils.SingleEvents
import javax.inject.Inject

class ChangePasswordViewModel @Inject constructor(
        private val authRepository: AuthRepository,
        private val schedulers: SchedulerProvider) : BaseViewModel() {

    private var _successChangePassword = MutableLiveData<SingleEvents<String>>()
    val successChangePassword: LiveData<SingleEvents<String>>
        get() = _successChangePassword

    private var _failChangePassword = MutableLiveData<SingleEvents<String>>()
    val failChangePassword: LiveData<SingleEvents<String>>
        get() = _failChangePassword

    fun changePassword(oldPassword: String, newPassword: String, confirmPassword: String) {
        lastDisposable = authRepository.changePassword(ChangePasswordReq(oldPassword, newPassword, confirmPassword))
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    when (it.status_code) {
                        200 -> _successChangePassword.value = SingleEvents("success-change-password")
                        else -> _failChangePassword.value = SingleEvents(it.error.toString())

                    }
                }, {

                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }
}