package com.jcodonuts.app.ui.jcor.product_detail

import com.jcodonuts.app.data.local.SizeBeverage
import com.jcodonuts.app.data.local.Variant

interface ProductDetailJcoRControllerListener {
    fun onPlusClick(position: Int)
    fun onMinusClick(position: Int)
    fun onAddToCart()
    fun onBtnFavoriteClick()
    fun onSizeBeverageClick(sizeBeverage: SizeBeverage)
    fun onVariantBeverageClick(variant: Variant)
}