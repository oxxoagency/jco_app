package com.jcodonuts.app.ui.add_address

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.core.util.Preconditions
import com.google.android.gms.common.api.ApiException
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FetchPlaceResponse
import com.jcodonuts.app.R
import com.jcodonuts.app.data.remote.model.req.AddressReq
import com.jcodonuts.app.data.remote.model.req.UpdateAddressReq
import com.jcodonuts.app.databinding.FragmentAddAddressBinding
import com.jcodonuts.app.ui.base.BaseFragment
import de.mateware.snacky.Snacky
import javax.inject.Inject

class AddAddressFragment @Inject constructor() :
    BaseFragment<FragmentAddAddressBinding, AddAddressViewModel>() {

    private var memberId = 0

    override fun getViewModelClass(): Class<AddAddressViewModel> = AddAddressViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_add_address

    override fun onViewReady(savedInstance: Bundle?) {
        initActionBar()
        initObserver()

        arguments?.let {
            it.getString("id")?.let { it1 -> viewModel.getAddress(it1.toInt()) }
        }

        binding.apply {
            btnSave.setOnClickListener { save() }
            address.setOnClickListener {
                val dlg = DialogSearchLocation()
                dlg.showDialog(
                    requireActivity().supportFragmentManager,
                    AddAddressFragment::class.java.simpleName,
                    object : DialogSearchLocation.DialogSearchLocationListener {
                        override fun latitude(): String =
                            sharedPreference.loadLatitude().toString()

                        override fun longitude(): String =
                            sharedPreference.loadLongitude().toString()

                        override fun address(autocompletePrediction: AutocompletePrediction): String {
                            Log.d("cekaddress", autocompletePrediction.getFullText(null).toString())
                            binding.address.text = "${autocompletePrediction.getFullText(null)}"
                            val placeField =
                                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
                            val request = FetchPlaceRequest.newInstance(
                                autocompletePrediction.placeId,
                                placeField
                            )
                            val placesClient = Places.createClient(requireContext())
                            placesClient.fetchPlace(request)
                                .addOnSuccessListener { response: FetchPlaceResponse ->
                                    val place = response.place
                                    val latlng = place.latLng.toString().split(",")
                                    val lat = latlng[0].replace("lat/lng: (", "")
                                    val long = latlng[1].replace(")", "")
                                    sharedPreference.apply {
                                        saveLatitudeAddress(lat)
                                        saveLongitudeAddress(long)
                                    }
                                    viewModel.loadLocation(lat, long)
                                }.addOnFailureListener { exception: Exception ->
                                    if (exception is ApiException) {
                                        Log.e("TAG", "Place not found: ${exception.message}")
                                        val statusCode = exception.statusCode
                                        TODO("Handle error with given status code")
                                    }
                                }
                            return "${autocompletePrediction.getFullText(null)}"
                        }
                    }
                )
            }
        }

        resultAddressFromMap()

    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun save() {
        binding.apply {
            if (arguments?.getString("id") == "0") {
                if (fullName.text.isNullOrEmpty()
                    || phoneNumber.text.isNullOrEmpty()
                    || address.text.isNullOrEmpty()
                    || detailAddress.text.isNullOrEmpty()
                ) {
                    Snacky.builder()
                        .setActivity(activity)
                        .setText("Mohon isi alamat dengan lengkap")
                        .setDuration(Snacky.LENGTH_SHORT)
                        .error()
                        .show()
                } else {
                    viewModel.setAddress(
                        AddressReq(
                            member_phone = sharedPreference.loadPhoneNumber(),
                            address_label = binding.label.text.toString(),
                            recipient_name = binding.fullName.text.toString(),
                            recipient_postcode = sharedPreference.loadPostCode(),
                            recipient_phone = binding.phoneNumber.text.toString(),
                            cityordistrict = sharedPreference.loadCity(),
                            address = binding.address.text.toString(),
                            address_details = binding.detailAddress.text.toString(),
                            latitude = sharedPreference.loadLatitudeAddress(),
                            longitude = sharedPreference.loadLongitudeAddress()
                        )
                    )
                }
            } else {
                if (binding.address.text.toString() == context?.getString(R.string.choose_your_location)) {
                    viewModel.updateAddress(
                        UpdateAddressReq(
                            member_address_id = memberId,
                            member_phone = sharedPreference.loadPhoneNumber(),
                            address_label = binding.label.text.toString(),
                            recipient_name = binding.fullName.text.toString(),
                            recipient_postcode = sharedPreference.loadPostCode(),
                            recipient_phone = binding.phoneNumber.text.toString(),
                            cityordistrict = sharedPreference.loadCity(),
                            address = "",
                            address_details = binding.detailAddress.text.toString(),
                            latitude = sharedPreference.loadLatitudeAddress(),
                            longitude = sharedPreference.loadLongitudeAddress()
                        )
                    )
                } else {
                    viewModel.updateAddress(
                        UpdateAddressReq(
                            member_address_id = memberId,
                            member_phone = sharedPreference.loadPhoneNumber(),
                            address_label = binding.label.text.toString(),
                            recipient_name = binding.fullName.text.toString(),
                            recipient_postcode = sharedPreference.loadPostCode(),
                            recipient_phone = binding.phoneNumber.text.toString(),
                            cityordistrict = sharedPreference.loadCity(),
                            address = binding.address.text.toString(),
                            address_details = binding.detailAddress.text.toString(),
                            latitude = sharedPreference.loadLatitudeAddress(),
                            longitude = sharedPreference.loadLongitudeAddress()
                        )
                    )
                }

                sharedPreference.saveSelectPhoneNumber(binding.phoneNumber.text.toString())
            }
        }
    }

    @SuppressLint("RestrictedApi")
    private fun resultAddressFromMap() {
        parentFragmentManager.setFragmentResultListener("address", this,
            { requestKey, result ->
                Preconditions.checkState("address" == requestKey)
                binding.address.text =
                    result.toString().removePrefix("Bundle[{address=").removeSuffix("}]")
            })
    }

    private fun initObserver() {
        viewModel.successAddAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                navigatePopupInclusiveTo(R.id.addAddressFragment, R.string.linkDeliveryFragment)
            }
        })

        viewModel.memberAddress.observe(viewLifecycleOwner, {
            binding.apply {
                fullName.setText(it.data.recipient_name)
                phoneNumber.setText(it.data.recipient_phone)
//                address.text = it.data.address
                detailAddress.setText(it.data.address_details)
                memberId = it.data.member_address_id ?: 0
                label.setText(it.data.address_label.toString())
            }
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { message ->
                Snacky.builder()
                    .setActivity(activity)
                    .setText(message)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .error()
                    .show()
            }
        })
    }
}