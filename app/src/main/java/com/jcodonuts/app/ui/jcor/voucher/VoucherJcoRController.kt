package com.jcodonuts.app.ui.jcor.voucher

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.Voucher
import com.jcodonuts.app.data.local.VoucherHeader
import com.jcodonuts.app.voucherHeaderJcoR
import com.jcodonuts.app.voucherItemsJcoR

class VoucherJcoRController(private val listener : VoucherJcoRControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is VoucherHeader -> addCouponHeader(listener)
                is Voucher -> addCouponList(cellData, index, listener)
            }
        }
    }

    private fun addCouponHeader(listener: VoucherJcoRControllerListener) {
        voucherHeaderJcoR {
            id("header")
            listener(listener)
        }
    }

    private fun addCouponList(cellData: Voucher, index: Int, listener: VoucherJcoRControllerListener) {
        voucherItemsJcoR {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }
}