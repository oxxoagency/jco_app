package com.jcodonuts.app.ui.jcor.transaction_detail

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.*
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.ui.transaction_detail.TransactionDetailControllerListener

class TransactionDetailJcoRController(private val listener: TransactionDetailControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is TransactionDetailHeader -> addTransactionDetailHeader(cellData)
                is TransactionDetailShippingInformation -> addTransactionDetailShippingInformation(
                    cellData
                )
                is TransactionDetailOrderSummary -> addTransactionDetailOrderSummary(
                    cellData,
                    listener
                )
                is TransactionDetailProduct -> addTransactionDetailProduct(cellData)
                is TransactionDetailTotal -> addTransactionDetailTotal(cellData, listener, index)
            }
        }
    }

    private fun addTransactionDetailHeader(cellData: TransactionDetailHeader) {
        transactionDetailHeaderJcoR {
            id("transaction-detail-header")
            data(cellData)
        }
    }

    private fun addTransactionDetailShippingInformation(cellData: TransactionDetailShippingInformation) {
        transactionDetailShippingInformationJcoR {
            id(cellData.id)
            data(cellData)
        }
    }

    private fun addTransactionDetailOrderSummary(
        cellData: TransactionDetailOrderSummary,
        listener: TransactionDetailControllerListener
    ) {
        transactionDetailOrderSummaryJcoR {
            id(cellData.orderId)
            data(cellData)
            listener(listener)
        }
    }

    private fun addTransactionDetailProduct(
        cellData: TransactionDetailProduct
    ) {
        transactionDetailProductJcoR {
            id(cellData.nameProduct)
            data(cellData)
        }
    }

    private fun addTransactionDetailTotal(
        cellData: TransactionDetailTotal,
        listener: TransactionDetailControllerListener,
        position: Int
    ) {
        transactionDetailTotalJcoR {
            id(cellData.total)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}