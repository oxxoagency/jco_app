package com.jcodonuts.app.ui.jcor.delivery

import android.net.Uri
import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.FragmentDeliveryJcoRBinding
import com.jcodonuts.app.ui.base.BaseFragment
import com.jcodonuts.app.utils.DlgLoadingProgressBarJcoR
import javax.inject.Inject


class DeliveryJcoRFragment @Inject constructor() :
    BaseFragment<FragmentDeliveryJcoRBinding, DeliveryJcoRViewModel>() {

    var dialog: DlgLoadingProgressBarJcoR? = null

    override fun getViewModelClass(): Class<DeliveryJcoRViewModel> =
        DeliveryJcoRViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_delivery_jco_r

    override fun onViewReady(savedInstance: Bundle?) {
        dialog = DlgLoadingProgressBarJcoR(requireContext())
        if (!isFragmentFromPaused) {
            viewModel.loadLocations()
        }

        initActionBar()
        initRecyclerview()
        initObserver()
    }

    private fun initActionBar() {
        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }
    }

    private fun initRecyclerview() {
        val controller = DeliveryJcoRController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.addNewAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val url =
                    getString(R.string.linkAddAddressJcoRFragment).replace("{id}", "0")
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.deliveryJcoRFragment, uri)
            }
        })

        viewModel.openAddAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { data ->
                val url =
                    getString(R.string.linkAddAddressJcoRFragment).replace(
                        "{id}",
                        data.id.toString()
                    )
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.deliveryJcoRFragment, uri)
            }
        })

        viewModel.openCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val action =
                    DeliveryJcoRFragmentDirections.actionFromDeliveryToMainFragment("cart")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })

        viewModel.showLoading.observe(this, {
            it.getContentIfNotHandled()?.let { state ->
                if (state) {
                    dialog?.showPopUp()
                } else {
                    dialog?.dismissPopup()
                }
            }
        })
    }
}