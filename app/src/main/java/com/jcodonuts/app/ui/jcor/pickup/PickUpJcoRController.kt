package com.jcodonuts.app.ui.jcor.pickup

import com.airbnb.epoxy.AsyncEpoxyController
import com.jcodonuts.app.data.local.BaseCell
import com.jcodonuts.app.data.local.EmptyCart
import com.jcodonuts.app.data.local.LocationSearch
import com.jcodonuts.app.data.local.PickupItem
import com.jcodonuts.app.emptyJcoR
import com.jcodonuts.app.locationSearchviewJcoR
import com.jcodonuts.app.pickupItemJcoR

class PickUpJcoRController(private val listener: PickUpJcoRControllerListener): AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when(cellData) {
                is LocationSearch -> addLocationSearch(listener)
                is PickupItem -> addPickupItem(cellData, listener, index)
                is EmptyCart -> addEmptyCart(cellData)
            }
        }
    }

    private fun addLocationSearch(listener: PickUpJcoRControllerListener){
        locationSearchviewJcoR{
            id("locationSearch")
            listener(listener)
        }
    }

    private fun addPickupItem(cellData: PickupItem, listener: PickUpJcoRControllerListener, index:Int){
        pickupItemJcoR {
            id(cellData._id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }
    private fun addEmptyCart(cellData: EmptyCart){
        emptyJcoR {
            id("empty-cart")
            data(cellData)
        }
    }

}