package com.jcodonuts.app.data.repository

import android.app.Application
import com.jcodonuts.app.data.remote.api.JcoApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.CouponClaimReq
import com.jcodonuts.app.data.remote.model.req.CouponMemberReq
import com.jcodonuts.app.data.remote.model.req.CouponReq
import com.jcodonuts.app.data.remote.model.res.CouponRes
import io.reactivex.Single
import javax.inject.Inject

interface CouponRepository {
    fun getCouponList(body: CouponReq): Single<CouponRes>
    fun claimCoupon(body: CouponClaimReq): Single<CouponRes>
    fun couponMember(body: CouponMemberReq): Single<CouponRes>
}

class CouponRepositoryImpl @Inject constructor(
    private val service: JcoApi,
    private val application: Application
) : CouponRepository, BaseRepository() {

    override fun getCouponList(body: CouponReq): Single<CouponRes> =
        composeSingle { service.showCouponList(body) }
            .compose(ErrorNetworkHandler())

    override fun claimCoupon(body: CouponClaimReq): Single<CouponRes> =
        composeSingle { service.claimCoupon(body) }
            .compose(ErrorNetworkHandler())

    override fun couponMember(body: CouponMemberReq): Single<CouponRes> =
        composeSingle { service.couponMember(body) }
            .compose(ErrorNetworkHandler())
}