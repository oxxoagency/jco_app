package com.jcodonuts.app.data.local.room

import androidx.room.*
import com.jcodonuts.app.data.local.CartProduct
import com.jcodonuts.app.data.local.CartProductJcoR
import com.jcodonuts.app.data.local.CartProductPickUp
import com.jcodonuts.app.data.local.CartProductPickUpJcoR
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface CartDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCart(cartProduct: CartProduct): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartPickUp(cartProduct: CartProductPickUp): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCarts(cartProduct: MutableList<CartProduct>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartsPickUp(cartProduct: MutableList<CartProductPickUp>): Completable

    @Query("select * from cart_product")
    fun getAllCartProduct(): Flowable<List<CartProduct>>

    @Query("select * from cart_product_pickup")
    fun getAllCartProductPickUp(): Flowable<List<CartProductPickUp>>

    @Update
    fun updateCart(cartProduct: CartProduct): Completable

    @Update
    fun updateCartPickUp(cartProduct: CartProductPickUp): Completable

    @Delete
    fun deleteCart(cartProduct: CartProduct): Completable

    @Delete
    fun deleteCartPickUp(cartProduct: CartProductPickUp): Completable

    @Query("delete from cart_product")
    fun deleteAll(): Completable

    @Query("delete from cart_product_pickup")
    fun deleteAllPickUp(): Completable

    @Query("update cart_product set priceOriginal = :priceOriginal, price = :price where id = :id")
    fun updatePrice(priceOriginal: Double, price: Double, id: Int): Completable

    @Query("update cart_product_pickup set priceOriginal = :priceOriginal, price = :price where id = :id")
    fun updatePricePickUp(priceOriginal: Double, price: Double, id: Int): Completable

    // FOR JCO R

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartProductJcoR(cartProductJcoR: CartProductJcoR): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartProductPickUpJcoR(cartProductJcoR: CartProductPickUpJcoR): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartsJcoR(cartProduct: MutableList<CartProductJcoR>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartsPickUpJcoR(cartProduct: MutableList<CartProductPickUpJcoR>): Completable

    @Query("select * from cart_product_jco_r")
    fun getAllCartProductJcoR(): Flowable<List<CartProductJcoR>>

    @Query("select * from cart_product_pick_pickup_jco_r")
    fun getAllCartProductPickUpJcoR(): Flowable<List<CartProductPickUpJcoR>>

    @Update
    fun updateCartJcoR(cartProduct: CartProductJcoR): Completable

    @Update
    fun updateCartPickUpJcoR(cartProduct: CartProductPickUpJcoR): Completable

    @Delete
    fun deleteCartJcoR(cartProduct: CartProductJcoR): Completable

    @Delete
    fun deleteCartPickUpJcoR(cartProduct: CartProductPickUpJcoR): Completable

    @Query("delete from cart_product_jco_r")
    fun deleteAllJcoR(): Completable

    @Query("delete from cart_product_pick_pickup_jco_r")
    fun deleteAllPickUpJcoR(): Completable

    @Query("update cart_product_jco_r set priceOriginal = :priceOriginal, price = :price where id = :id")
    fun updatePriceJcoR(priceOriginal: Double, price: Double, id: Int): Completable

    @Query("update cart_product_pick_pickup_jco_r set priceOriginal = :priceOriginal, price = :price where id = :id")
    fun updatePricePickUpJcoR(priceOriginal: Double, price: Double, id: Int): Completable
}
