package com.jcodonuts.app.data.remote.model.req

data class ProductFavoriteReq(
    val favorited: String,
    val member_phone: String,
    val menu_code: String
)

data class ProductByCategoryReq(
    val category: String,
    val city: String,
    val member_phone: String,
    val brand: Int,
    val app_os:String,
    val app_version:Int
)

data class ProductDetailReq(
    val brand: String,
    val city: String,
    val menu_code: String,
    val member_phone: String,
)

data class ProductFavReq(
    val member_phone: String,
    val city: String,
    val brand: Int?
)

data class ProductPromoReq(
    val brand: Int?,
    val city: String?,
    val member_phone: String?,
)

data class ProductSearchReq(
    val brand: Int?,
    val city: String?,
    val menu_name: String?,
)

data class ProductCategoryReq(
    val brand: Int?,
    val city: String?
)