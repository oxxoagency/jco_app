package com.jcodonuts.app.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CartDetailVariant(
    @PrimaryKey
    val name:String,
    var qty:Int = 0
)