package com.jcodonuts.app.data.repository

import com.jcodonuts.app.data.remote.api.DirectJcoApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.*
import io.reactivex.Single
import javax.inject.Inject

interface DirectJcoRepository {
    fun getMenuRecommendation(body: MenuRecommendationReq): Single<MenuRecommendationRes>
    fun getJPointMutation( pointStatus: Int, page: Int): Single<JPointsRes>
    fun login(body: LoginReq): Single<LoginRes>
    fun createOrder(body: DirectCreateOrderReq): Single<CreateOrderRes>
    fun couponMember(body: DirectCouponMemberReq): Single<CouponRes>
}

class DirectJcoRepositoryImpl @Inject constructor(private val service: DirectJcoApi) :
    DirectJcoRepository, BaseRepository() {
    override fun getMenuRecommendation(body: MenuRecommendationReq): Single<MenuRecommendationRes> =
        composeSingle { service.getMenuRecommendation(body) }.compose(ErrorNetworkHandler())

    override fun getJPointMutation( pointStatus: Int, page: Int): Single<JPointsRes> =
        composeSingle { service.getJpointMutation(pointStatus, page) }.compose(
            ErrorNetworkHandler())

    override fun login(body: LoginReq): Single<LoginRes> {
        return composeSingle { service.login(body) }.compose(ErrorNetworkHandler())
    }

    override fun createOrder(body: DirectCreateOrderReq): Single<CreateOrderRes> =
        composeSingle { service.createOrder(body) }

    override fun couponMember(body: DirectCouponMemberReq): Single<CouponRes> =
        composeSingle { service.couponMember(body) }
            .compose(ErrorNetworkHandler())
}


