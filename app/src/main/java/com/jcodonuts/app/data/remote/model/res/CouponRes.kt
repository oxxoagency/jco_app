package com.jcodonuts.app.data.remote.model.res

data class CouponRes(
    val status_code: Int?,
    val error:String?,
    val data: List<Coupon>
)

data class Coupon(
    val coupon_id: String?,
    val code: String?,
    val coupon_name: String?,
    val description: String?,
    val image: String?,
    val coupon_value: String?,
    val coupon_type: String?,
    val min_cart: String?,
    val max_discount: String?,
    val coupon_limit_user: String?,
    val coupon_limit_type: String?,
    val coupon_limit: String?,
    val payment_type: String?,
    val end_time: String?,
    val start_time:String?,
    val is_claim:Int?,
    val is_used:Int?,
    val claim_at:String?,
    val coupon_description:String?
)