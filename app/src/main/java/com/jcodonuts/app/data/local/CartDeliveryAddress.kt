package com.jcodonuts.app.data.local

data class CartDeliveryAddress(var address: String, var pickup:String) : BaseCell()
