package com.jcodonuts.app.data.local

data class AddressItem(
    val id: Int?,
    val labelAddress: String?,
    val detailAddress: String?,
    val address: String?,
    val phoneNumber: String?,
    val name: String?,
    val city: String?,
    val latitude:String?,
    val longitude:String?
) : BaseCell()