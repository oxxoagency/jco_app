package com.jcodonuts.app.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jcodonuts.app.data.remote.model.res.DetailVariant
import com.jcodonuts.app.data.remote.model.res.PackageDonut


data class ProductDetail(
    val content: ProductDetailContent,
    val donuts: List<ProductDetailDonut>
)

data class ProductDetailContent(
    var menuCode: String?,
    var id_menu: Int?,
    val colorType: String?,
    val colorTypeText: String?,
    val desc: String?,
    val maxDesc: String?,
    val price: String?,
    val priceText: String?,
    val productName: String?,
    var quantity: Int,
    var quantityInPcs: Int,
    var totalPerPack: Int,
    val type: String?,
    val menu_image: String?,
    val variant: List<DetailVariant>?,
    val variantNum: String?,
    val normalPrice: String?
) : BaseCell()

data class ProductDetailDonut(
    val imgURL: String,
    val name: String,
    val price: String,
    var quantity: Int
) : BaseCell()

data class ProductDetailVariants(
    val productDetailVariant: List<ProductDetailVariant>
)

data class ProductDetailVariant(
    var quantity: Int,
    var variant: String,
    var variantDonut: List<PackageDonut>,
    var mainQuantity: Int? = 0,
    var max: Int? = 0
) : BaseCell() {
    fun desc(): String {
        val desc = mutableListOf<String>()
        variantDonut.forEachIndexed { index, packageDonut ->
            if ((index + 1) % 4 == 0) {
                desc.add(packageDonut.menu_name.toString() + "\n")
            } else {
                desc.add(packageDonut.menu_name.toString() )
            }
        }
        return desc.toString().removePrefix("[").removeSuffix("]").replace("\n, ", ",\n")
    }
}

data class DonutCustom(
    var menuImg: String,
    var menuName: String,
    var quantity: Int
) : BaseCell()

data class ProductDetailPackage(
    var quantity: Int,
    var packageName: String,
    var packageImage: String,
    var packageDescription: String,
) : BaseCell()

data class ProductDetailVariantCustom(
    var quantity: Int,
    var variant: String,
    var variantDonut: List<PackageDonut>,
    var mainQuantity: Int? = 0,
    var max: Int? = 0
):BaseCell()

data class VariantDonut(
    val menuImg: String,
    var menuName: String
) : BaseCell()

data class ProductDetailVariantDonut(
    val packageName: List<String>?
) : BaseCell()

data class ProductDetailVariantBeverage(
    val menuCode: String?,
    val menuSize: String?,
    val menuTemp: String?,
    val menuPrice: String?,
    var textColor: String?
) : BaseCell()

data class SizeVariantBeverage(
    val listSize: List<SizeBeverage>
) : BaseCell()

data class SizeBeverage(
    val size: String?,
    val menuCode: String?,
    var isColorText: Boolean,
    var menuPrice: String?
) : BaseCell()

data class VariantBeverage(
    val listVariant: List<Variant>
) : BaseCell()

data class Variant(
    val variant: String?
) : BaseCell()

data class MixVariant(
    val menu_code: String?,
    val menu_name: String?,
    var qty: Int
) : BaseCell()

data class HeaderVariantBeverage(val title: String?) : BaseCell()

data class PackageDonutLocal(
    val menu_img:String?,
    val menu_name:String?,
    val quantity: Int?
): BaseCell()

class DataConverter {

    @TypeConverter
    fun fromDetailVariantList(value: List<CartDetailVariant>): String {
        val gson = Gson()
        val type = object : TypeToken<List<CartDetailVariant>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toCountryLangList(value: String): List<CartDetailVariant> {
        val gson = Gson()
        val type = object : TypeToken<List<CartDetailVariant>>() {}.type
        return gson.fromJson(value, type)
    }
}