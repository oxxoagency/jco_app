package com.jcodonuts.app.data.remote.model.res

data class OutletRes(
    val status_code: Int?,
    val data: List<PickupFrom>,
    val error:String?
)