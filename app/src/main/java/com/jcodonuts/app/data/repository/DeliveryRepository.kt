package com.jcodonuts.app.data.repository

import android.app.Application
import android.content.res.AssetManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jcodonuts.app.data.local.DeliveryItem
import com.jcodonuts.app.data.local.MemberAddress
import com.jcodonuts.app.data.local.MemberAddressReq
import com.jcodonuts.app.data.local.MemberAddressRes
import com.jcodonuts.app.data.remote.api.JcoApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.DeleteAddressReq
import com.jcodonuts.app.data.remote.model.res.SuccessRes
import io.reactivex.Single
import javax.inject.Inject

interface DeliveryRepository {
    fun getListLocations(): Single<List<DeliveryItem>>
    fun getMemberAddress(body: MemberAddressReq): Single<MemberAddressRes>
    fun deleteAddress(body: DeleteAddressReq): Single<SuccessRes>
}

class DeliveryRepositoryImpl @Inject constructor(
    private val service: JcoApi,
    private val application: Application
) : DeliveryRepository, BaseRepository() {

    override fun getMemberAddress(body: MemberAddressReq): Single<MemberAddressRes> {
        return composeSingle { service.getMemberAddress(body) }
            .compose(ErrorNetworkHandler())
    }

    override fun getListLocations(): Single<List<DeliveryItem>> {
        return composeSingle {
            Single.fromCallable {
                val jsonString = application.baseContext.assets.readFile("pickup_places.json")
                val itemType = object : TypeToken<List<DeliveryItem>>() {}.type
                Gson().fromJson(jsonString, itemType)
            }
        }
    }

    override fun deleteAddress(body: DeleteAddressReq): Single<SuccessRes> {
        return composeSingle { service.deleteAddress(body) }
            .compose(ErrorNetworkHandler())
    }

    private fun AssetManager.readFile(fileName: String) = open(fileName)
        .bufferedReader()
        .use { it.readText() }


}