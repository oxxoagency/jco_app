package com.jcodonuts.app.data.local

data class ChoosePaymentData(val icon: String, val name: String, val credit: String) : BaseCell()

data class PointHeader(val title: String?) : BaseCell()

data class Point(val point: String?) : BaseCell()

data class EWalletHeader(val title: String?) : BaseCell()

data class EWallet(val name: String?, val payment: String?, val image: Int?) : BaseCell()

data class TransferVirtualAccountHeader(val title: String?) : BaseCell()

data class TransferVirtualAccount(val name: String?, val payment: String?, val image: Int?) :
    BaseCell()

data class OtherPaymentHeader(val title: String?) : BaseCell()

data class OtherPayment(val name: String?, val payment: String?, val image: Int?) :
    BaseCell()

data class Payment(val paymentMethod: String?, val paymentName: String?, val paymentIcon: String?): BaseCell()