package com.jcodonuts.app.data.local

data class MemberAddressItem(
    val _id: String,
    val recipient_name: String,
    val address_label: String,
    val recipient_address: String,
):BaseCell()