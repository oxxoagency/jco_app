package com.jcodonuts.app.data.repository

import android.app.Application
import android.content.res.AssetManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jcodonuts.app.data.local.PickupItem
import com.jcodonuts.app.data.remote.api.JcoApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.OutletCityReq
import com.jcodonuts.app.data.remote.model.req.OutletReq
import com.jcodonuts.app.data.remote.model.res.OutletRes
import io.reactivex.Single
import javax.inject.Inject

interface PickupRepository {
    fun getListLocations(): Single<List<PickupItem>>
    fun getOutletLocation(body: OutletReq): Single<OutletRes>
    fun getOutletByCity(body: OutletCityReq): Single<OutletRes>
}

class PickupRepositoryImpl @Inject constructor(
    private val application: Application,
    private val service: JcoApi
) : PickupRepository, BaseRepository() {


    override fun getListLocations(): Single<List<PickupItem>> {
        return composeSingle {
            Single.fromCallable {
                val jsonString = application.baseContext.assets.readFile("pickup_places.json")
                val itemType = object : TypeToken<List<PickupItem>>() {}.type
                Gson().fromJson(jsonString, itemType)
            }
        }
    }

    override fun getOutletLocation(body: OutletReq): Single<OutletRes> =
        composeSingle { service.showOutlet(body) }
            .compose(ErrorNetworkHandler())

    override fun getOutletByCity(body: OutletCityReq): Single<OutletRes> =
        composeSingle { service.showOutletByCity(body) }

    private fun AssetManager.readFile(fileName: String) = open(fileName)
        .bufferedReader()
        .use { it.readText() }


}