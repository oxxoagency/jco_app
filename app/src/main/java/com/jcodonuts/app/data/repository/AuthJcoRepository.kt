package com.jcodonuts.app.data.repository

import com.jcodonuts.app.data.remote.api.DirectJcoApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.CheckMemberReq
import com.jcodonuts.app.data.remote.model.req.LoginReq
import com.jcodonuts.app.data.remote.model.req.PaymentListReq
import com.jcodonuts.app.data.remote.model.res.LoginRes
import com.jcodonuts.app.data.remote.model.res.MemberRes
import com.jcodonuts.app.data.remote.model.res.PaymentListRes
import io.reactivex.Single
import javax.inject.Inject

interface AuthJcoRepository {
    fun checkMember(body: CheckMemberReq): Single<MemberRes>
    fun paymentList(company: String?): Single<PaymentListRes>
}

class AuthJcoRepositoryImpl @Inject constructor(private val service: DirectJcoApi) :
    AuthJcoRepository, BaseRepository() {

    override fun checkMember(body: CheckMemberReq): Single<MemberRes> {
        return composeSingle { service.checkMember(body) }.compose(ErrorNetworkHandler())
    }

    override fun paymentList(company: String?): Single<PaymentListRes> {
        return composeSingle { service.paymentList(company) }.compose(ErrorNetworkHandler())
    }

}