package com.jcodonuts.app.data.remote.model.res

data class BannerRes(
    val status_code: Int?,
    val data: DataBanner
)

data class DataBanner(
    val banner_id: String?,
    val banner_img: String?,
    val banner_description: String?,
    val start_date: String?,
    val end_date: String?,
    val related_menu: List<RelatedMenuRes>?
)

data class RelatedMenuRes(
    val id_menu: String?,
    val menu_code: String?,
    val menu_name: String?,
    val menu_name_en: String?,
    val menu_image: String?,
    val is_promo: String?,
    val menu_price: String?
)

