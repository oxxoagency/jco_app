package com.jcodonuts.app.data.local

data class ModelOrder(
    val id: String?,
    val name: String,
    val desc: String,
    val time: String,
    val price: String,
    val status: String,
    val orderStatus: String
) : BaseCell() {
    fun getStatusLabel() = when {
        orderStatus.contains("6", true) -> {
            "Selesai"
        }
        orderStatus.contains("7", true) -> {
            "Gagal"
        }
        else -> {
            "Diproses"
        }
    }

    fun getColor() = when{
        orderStatus.contains("6", true) -> {
            "#F25A17"
        }
        orderStatus.contains("7", true) -> {
            "#F30000"
        }
        else -> {
            "#D3BD00"
        }
    }

}

data class ModelOrderHeader(val date: String) : BaseCell()
