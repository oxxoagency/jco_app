package com.jcodonuts.app.data.local

data class PickupItem(
    val _id: String,
    val outletCode:String,
    val address: String,
    val distance: String,
    var isFavorite: Boolean = false,
    val placeName: String,
    val postCode:String,
    val city:String,
    val latitude: String,
    val longiutde: String
):BaseCell()

data class DeliveryWebView(
    val url: String,
):BaseCell()