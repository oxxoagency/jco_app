package com.jcodonuts.app.data.remote.model.req

data class RecentOrderReq(
    val brand: Int?,
    val member_email: String?,
    val city: String?
)

data class DetailRecentOrderReq(
    val brand:Int?,
    val order_id:String?
)