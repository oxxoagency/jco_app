package com.jcodonuts.app.data.remote.model.res

data class MenuRecommendationRes(
    val status_code:Int?,
    val data: List<Product>?
)