package com.jcodonuts.app.data.remote.api

import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.*
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface JcoUserApi {

    @POST("member/login")
    fun login(
        @Body body: LoginReq
    ): Single<LoginRes>

    @POST("member/register")
    fun register(
        @Body body: RegisterReq
    ): Single<RegisterRes>

    @GET("member/logout")
    fun logout(): Single<SuccessRes>

    @PUT("member/password/reset")
    fun resetPassword(@Body body: ChangePsswordReq): Single<SuccessRes>

    @GET("member/me")
    fun memberMe(): Single<MemberRes>

    @PUT("member/password/change")
    fun changePassword(@Body body: ChangePasswordReq): Single<SuccessResChangePassword>

    @PUT("member/update")
    fun updateProfile(@Body body:UpdateProfileReq): Single<MemberRes>

    @GET("member/refresh")
    fun getRefreshToken(): Single<RefreshTokenRes>
}