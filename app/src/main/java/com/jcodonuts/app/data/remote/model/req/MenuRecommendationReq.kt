package com.jcodonuts.app.data.remote.model.req

data class MenuRecommendationReq(
    val city: String?,
    val order_brand: String?,
    val cart_info: List<CartInfo>?,
    val app_os: String?,
    val app_version: Int?,
)