package com.jcodonuts.app.data.remote.model.res

data class ProductFavoriteRes(
    val status: Int?,
    val message: String?,
    val data: List<ProductFavorite>
)

data class ProductFavorite(
    val menu_code: String?,
    val menu_ordering: String?,
    val category_name: String?,
    val category_title: String?,
    val menu_name: String?,
    val menu_desc: String?,
    val menu_image: String?,
    val min_quantity: String?,
    val max_quantity: String?,
    val max_delivery: String?,
    val start_date: String?,
    val end_date: String?,
    val is_promo: String?,
    val is_active: String?,
    val is_freedelivery: String?,
    val menu_amount: String?,
    val menu_price: String?,
    val menu_URL: String?,
    val is_favorite: Int?,
    val menu_name_en: String?,
    val menu_normalprice: Int?
)