package com.jcodonuts.app.data.repository

import android.app.Application
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.api.JcoApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.AddressReq
import com.jcodonuts.app.data.remote.model.req.CityReq
import com.jcodonuts.app.data.remote.model.req.CitySearchReq
import com.jcodonuts.app.data.remote.model.req.UpdateAddressReq
import com.jcodonuts.app.data.remote.model.res.CityRes
import com.jcodonuts.app.data.remote.model.res.SuccessRes
import io.reactivex.Single
import javax.inject.Inject

interface AddAddressRepository {

    fun setAddress(body: AddressReq): Single<AddMemberRes>
    fun getAddress(id: Int): Single<AddressRes>
    fun updateAddress(body: UpdateAddressReq): Single<SuccessRes>
    fun getCity(body: CityReq): Single<CityRes>
    fun getCitySearch(body: CitySearchReq): Single<CityRes>
}

class AddAddressRepositoryImpl @Inject constructor(
    private val service: JcoApi,
    private val application: Application
) : AddAddressRepository, BaseRepository() {

    override fun setAddress(body: AddressReq): Single<AddMemberRes> =
        composeSingle { service.setAddress(body) }
            .compose(ErrorNetworkHandler())

    override fun getAddress(id: Int): Single<AddressRes> =
        composeSingle { service.getAddress(id) }
            .compose(ErrorNetworkHandler())

    override fun updateAddress(body: UpdateAddressReq): Single<SuccessRes> =
        composeSingle { service.updateAddress(body) }
            .compose(ErrorNetworkHandler())

    override fun getCity(body: CityReq): Single<CityRes> =
        composeSingle { service.getCity(body) }
            .compose(ErrorNetworkHandler())

    override fun getCitySearch(body: CitySearchReq): Single<CityRes> =
        composeSingle { service.getCitySearch(body) }
            .compose(ErrorNetworkHandler())
}
