package com.jcodonuts.app.data.local

data class ProfileHeader(
    val img: String,
    val name: String,
    val balance: String,
    val point: String,
    val phoneNumber: String
) : BaseCell()

data class ProfileMenuHeader(val title: String) : BaseCell()

data class ProfileMenu(val icon: Int, val title: Int, val type: Int) : BaseCell()

data class SocmedMenu(val icon: Int, val title: String, val type: Int) : BaseCell()

data class ProfileFooter(var showLoading: Boolean = false) : BaseCell()

data class ProfileData(
    val img: String,
    val name: String,
    val phone: String?,
    val gender: String?,
    val dateOfBirth: String?
) : BaseCell()

data class AddressMenu(val title:String) : BaseCell()