package com.jcodonuts.app.data.local

import com.jcodonuts.app.data.remote.model.res.RelatedMenuRes

data class Banner(
    val id: String?,
    val bannerImage: String?,
    val bannerDescription: String?,
    val promoPeriod: String?,
    val version : Int?,
    val relatedMenu : List<RelatedMenuRes>?
) : BaseCell()

data class RelatedMenu(
    val id: String?,
    val menuCode: String?,
    val menuName: String?,
    val menuImage: String?,
    val isPromo: Boolean?,
    val price: String?,
    val priceText: String?
) : BaseCell()