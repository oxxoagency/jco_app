package com.jcodonuts.app.data.local


data class PaymentDetail(
    val orderDetail: OrderDetail,
    val orderProducts: List<OrderProduct>,
    val orderTotal: OrderTotal
)

data class OrderDetail(
    val addressName:String,
    val addressDetail:String,
    val orderNumber:String,
    val purchaseDate:String,
    val deliveryType:String
):BaseCell()

data class OrderProduct(
        val name:String,
        val qty:String
):BaseCell()

data class OrderTotal(
        val subTotal:String,
        val deliveryFee:String,
        val total:String
):BaseCell()