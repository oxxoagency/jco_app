package com.jcodonuts.app.data.local

data class MenuSearchTagName(val tags:List<String>, val name:List<String>):BaseCell()
