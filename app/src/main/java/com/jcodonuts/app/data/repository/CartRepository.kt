package com.jcodonuts.app.data.repository

import android.app.Application
import com.jcodonuts.app.data.remote.api.JcoAdminApi
import com.jcodonuts.app.data.remote.api.JcoApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.CartOrder
import com.jcodonuts.app.data.remote.model.res.CartOrderRes
import io.reactivex.Single
import javax.inject.Inject

interface CartRepository {
    fun getCartOrders(body: CartOrder): Single<CartOrderRes>
}

class CartRepositoryImpl @Inject constructor(
    private val service: JcoAdminApi,
    private val application: Application
) : CartRepository, BaseRepository() {

    override fun getCartOrders(body: CartOrder): Single<CartOrderRes> =
        composeSingle { service.getCartOrders(body) }
            .compose(ErrorNetworkHandler())
}