package com.jcodonuts.app.data.remote.api

import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.*
import io.reactivex.Single
import retrofit2.http.*

interface JcoApi {

    @POST("home/app")
    fun fetchAppHome(
        @Body body: HomeReq
    ): Single<HomeRes>

    @POST("product/category")
    fun getProductByCategory(
        @Body body: ProductByCategoryReq
    ): Single<ProductsByCategoryRes>

    @POST("product/favorite")
    fun setProductFavorite(
        @Body body: ProductFavoriteReq
    ): Single<SuccessRes>

    @POST("product/detail")
    fun getProductDetail(
        @Body body: ProductDetailReq
    ): Single<ProductDetailRes>

    @POST("member/address/showall")
    fun getMemberAddress(
        @Body body: MemberAddressReq
    ): Single<MemberAddressRes>

    @POST("member/address/store")
    fun setAddress(@Body body: AddressReq): Single<AddMemberRes>

    @GET("member/address/show/{id}")
    fun getAddress(@Path("id") id: Int): Single<AddressRes>

    @PUT("member/address/update")
    fun updateAddress(@Body body: UpdateAddressReq): Single<SuccessRes>

    @POST("product/show/favorite")
    fun showProductFavorite(@Body body: ProductFavReq): Single<ProductFavoriteRes>

    @POST("product/promo")
    fun showHotPromo(@Body body: ProductPromoReq): Single<ProductPromoRes>

    @POST("product/search")
    fun showSearchProduct(@Body body: ProductSearchReq): Single<ProductSearchRes>

    @POST("product/category/all")
    fun showAllCategory(@Body body: ProductCategoryReq): Single<ProductCategoryRes>

    @POST("member/address/destroy")
    fun deleteAddress(@Body body: DeleteAddressReq): Single<SuccessRes>

    @POST("outlet/location")
    fun showOutlet(@Body body: OutletReq): Single<OutletRes>

    @POST("outlet/city")
    fun showOutletByCity(@Body body: OutletCityReq): Single<OutletRes>

    @POST("home/order/recent")
    fun showRecentOrder(@Body body: RecentOrderReq): Single<RecentOrderRes>

    @POST("coupon/list")
    fun showCouponList(@Body body: CouponReq): Single<CouponRes>

    @POST("coupon/claim")
    fun claimCoupon(@Body body: CouponClaimReq): Single<CouponRes>

    @POST("coupon/member")
    fun couponMember(@Body body: CouponMemberReq): Single<CouponRes>

    @POST("product/promo/detail")
    fun getBannerDetail(@Body body: BannerReq): Single<BannerRes>

    @POST("city/country")
    fun getCity(@Body body: CityReq): Single<CityRes>

    @POST("home/order/recomendation")
    fun getOrderRecommendation(@Body body: RecommendationReq): Single<ProductsByCategoryRes>

    @POST("city/search")
    fun getCitySearch(@Body body: CitySearchReq): Single<CityRes>

    @GET("home/update_notif")
    fun getUpdateNotification(): Single<UpdateVersionRes>
}