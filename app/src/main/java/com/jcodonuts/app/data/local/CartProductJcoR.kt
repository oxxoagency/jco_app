package com.jcodonuts.app.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cart_product_jco_r")
data class CartProductJcoR(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val menuCode: Int?,
    val name: String?,
    val imgURL: String?,
    var price: Double,
    val productType: String?,
    var notes: String?,
    var qty: Int,
    var priceOriginal: Double,
    var variantname: String = ""
) : BaseCell()

@Entity(tableName = "cart_product_pick_pickup_jco_r")
data class CartProductPickUpJcoR(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val menuCode: Int?,
    val name: String?,
    val imgURL: String?,
    var price: Double,
    val productType: String?,
    var notes: String?,
    var qty: Int,
    var priceOriginal: Double,
    var variantname: String = ""
) : BaseCell()