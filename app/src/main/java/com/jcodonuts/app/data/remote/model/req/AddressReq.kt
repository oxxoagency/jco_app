package com.jcodonuts.app.data.remote.model.req

data class AddressReq(
    val member_phone: String?,
    val address_label: String?,
    val recipient_name: String?,
    val recipient_phone: String?,
    val recipient_postcode: String?,
    val cityordistrict: String?,
    val address: String?,
    val address_details: String?,
    val latitude: String?,
    val longitude: String?
)

data class UpdateAddressReq(
    val member_address_id: Int?,
    val member_phone: String?,
    val address_label: String?,
    val recipient_name: String?,
    val recipient_phone: String?,
    val recipient_postcode: String?,
    val cityordistrict: String?,
    val address: String?,
    val address_details: String?,
    val latitude: String?,
    val longitude: String?
)

data class DeleteAddressReq(
    val member_address_id: Int?
)