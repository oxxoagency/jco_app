package com.jcodonuts.app.data.repository

import android.app.Application
import com.jcodonuts.app.data.remote.api.JcoAdminApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.CreateOrderReq
import com.jcodonuts.app.data.remote.model.req.DetailRecentOrderReq
import com.jcodonuts.app.data.remote.model.req.OrderListReq
import com.jcodonuts.app.data.remote.model.res.CreateOrderRes
import com.jcodonuts.app.data.remote.model.res.DetailRecentOrderRes
import com.jcodonuts.app.data.remote.model.res.OrderListRes
import io.reactivex.Single
import javax.inject.Inject

interface TransactionRepository {
    fun getTransactionDetail(body: DetailRecentOrderReq): Single<DetailRecentOrderRes>
    fun getCreateOrder(body: CreateOrderReq): Single<CreateOrderRes>
    fun getOrderList(body: OrderListReq): Single<OrderListRes>
}

class TransactionRepositoryImpl @Inject constructor(
    private val service: JcoAdminApi,
    private val application: Application
) : TransactionRepository, BaseRepository() {

    override fun getTransactionDetail(body: DetailRecentOrderReq): Single<DetailRecentOrderRes> =
        composeSingle { service.getOrderDetail(body) }
            .compose(ErrorNetworkHandler())

    override fun getCreateOrder(body: CreateOrderReq): Single<CreateOrderRes> =
        composeSingle { service.createOrder(body) }
            .compose(ErrorNetworkHandler())

    override fun getOrderList(body: OrderListReq): Single<OrderListRes> =
        composeSingle { service.getOrderList(body) }
            .compose(ErrorNetworkHandler())
}