package com.jcodonuts.app.data.local

data class JPointHistory(
    val id: Int,
    val pointStatus: String,
    val pointUsed: String,
    val pointEarn: String,
    val description: String,
    val date: String,
    val time: String,
): BaseCell()