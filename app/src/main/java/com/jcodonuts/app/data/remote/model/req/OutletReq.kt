package com.jcodonuts.app.data.remote.model.req

data class OutletReq(
    val brand:Int?,
    val latitude:String?,
    val longitude:String?,
    val delivery_type: Int? = 0
)

data class OutletCityReq(
    val brand:Int?,
    val city:String?,
    val delivery_type: Int? = 1
)