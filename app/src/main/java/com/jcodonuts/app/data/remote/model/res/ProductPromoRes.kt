package com.jcodonuts.app.data.remote.model.res

data class ProductPromoRes(
    val status: Int?,
    val message: String?,
    val data: List<Promo>?
)