package com.jcodonuts.app.data.remote.model.res

data class JPointsRes(
    val status_code: Int,
    val data: List<JPointRes>
)

data class JPointRes(
    val id: Int?,
    val point_status: String?,
    val point_used: Int?,
    val point_earn: String?,
    val description: String?,
    val created_at: String?
)