package com.jcodonuts.app.data.local

data class CartDetail(
    val paymentName: String?,
    var paymentIcon: String?,
    var price: String?,
    var deliveryServiceText: String?,
    var deliveryService:Int?,
    var totalPrice: String?,
    var notes: String?,
    val ecobag: String?,
    val ecobagPrice: String?,
    val ecobagSubTotal: String?,
    val ecobagQty: String?,
    val voucher: String?,
    val promo: Int?,
    val freeDeliveryText: String?,
    val freeDelivery: Int?,
    val totalDeliveryFee:String?,
    val isDelivery: Boolean,
    val jPoint: String,
    val jPointInt: Int
) : BaseCell()

data class CartTotal(
    val ecobag: String?,
    val ecobagSubTotal: String?,
    var price: String?,
    val freeDelivery: Int?,
    var deliveryService:Int?,
    val ecobagQty: String?,
    val totalDeliveryFee:String?,
    var deliveryServiceText: String?,
    val promo: Int?,
    var totalPrice: String?,
    val isDelivery: Boolean,
    val jpointUsed: Int,
    val jpointEarn: Int
): BaseCell()

data class CartRecommendation(
    val menuCode: String?,
    val name: String?,
    val image: String?,
    val price: String?,
    val isFavorite: Boolean?
) : BaseCell()

data class CartRecommendations(
    val recommendations: List<CartRecommendation>
): BaseCell()

data class EmptyCart(val title: String) : BaseCell()

data class CartTempItem(
    var variantName: String?,
    var variantQty: Int
) : BaseCell()

data class HeaderRecommendationMenu(val label: String = "") : BaseCell()

data class CartTempDonutCustom(
    var donutName: String?,
    var donutQty: Int
) : BaseCell()