package com.jcodonuts.app.data.remote.model.res

data class MapsRes(
    val predictions: List<Predictions>?
)

data class Predictions(
    val description: String?
)