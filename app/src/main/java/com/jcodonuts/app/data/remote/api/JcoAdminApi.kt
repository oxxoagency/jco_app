package com.jcodonuts.app.data.remote.api

import com.jcodonuts.app.data.remote.model.req.CartOrder
import com.jcodonuts.app.data.remote.model.req.CreateOrderReq
import com.jcodonuts.app.data.remote.model.req.DetailRecentOrderReq
import com.jcodonuts.app.data.remote.model.req.OrderListReq
import com.jcodonuts.app.data.remote.model.res.CartOrderRes
import com.jcodonuts.app.data.remote.model.res.CreateOrderRes
import com.jcodonuts.app.data.remote.model.res.DetailRecentOrderRes
import com.jcodonuts.app.data.remote.model.res.OrderListRes
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface JcoAdminApi {


    @POST("order/delivery")
    fun getCartOrders(
        @Body body: CartOrder
    ): Single<CartOrderRes>

    @POST("order/detail")
    fun getOrderDetail(@Body body: DetailRecentOrderReq): Single<DetailRecentOrderRes>

    @POST("order/store")
    fun createOrder(@Body body: CreateOrderReq): Single<CreateOrderRes>

    @POST("order/list")
    fun getOrderList(@Body body: OrderListReq): Single<OrderListRes>
}