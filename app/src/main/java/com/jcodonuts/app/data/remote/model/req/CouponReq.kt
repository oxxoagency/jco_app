package com.jcodonuts.app.data.remote.model.req

data class CouponReq(
    val brand: Int,
    val platform:String?,
    val coupon_type:String?
)

data class CouponClaimReq(
    val brand:Int?,
    val code:String?,
    val member_phone:String?
)

data class CouponMemberReq(
    val brand:Int?
)

data class DirectCouponMemberReq(
    val platform: String?,
    val coupon_type: String?,
    val order_brand: String?,
    val order_company: String?,
    val delivery_type: String?
)