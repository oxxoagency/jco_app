package com.jcodonuts.app.data.local

import com.jcodonuts.app.data.remote.model.res.OrderDetail

data class RecentOrder(
    val orderId: String?,
    val outletName: String?,
    val menuImage: String?,
    val orderTime: String?,
    val price: String?,
    val menuName: String?,
    val orderStatus: String?,
    val totalOrder: String?,
    val orderPaymentStatus: String?,
    val orderDetail: List<OrderDetail>?
) : BaseCell() {
    fun getStatusLabel() = when {
        orderStatus?.contains("6", true) == true -> {
            "Selesai"
        }
        orderStatus?.contains("7", true) == true -> {
            "Gagal"
        }
        orderStatus?.contains("1", true) == true -> {
            "Menunggu Pembayaran"
        }
        orderStatus?.contains("2", true) == true -> {
            "Sudah Dibayar"
        }
        orderStatus?.contains("4", true) == true -> {
            "Customer tidak dapat dihubungi"
        }
        orderStatus?.contains("3", true) == true -> {
            "Dikonfirmasi toko"
        }
        orderStatus?.contains("5", true) == true -> {
            "Sedang dikirim"
        }
        orderStatus?.contains("8", true) == true -> {
            "Expired"
        }
        else -> {
            "Diproses"
        }
    }
}