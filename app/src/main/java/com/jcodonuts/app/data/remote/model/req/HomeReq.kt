package com.jcodonuts.app.data.remote.model.req

data class HomeReq(
    val brand: Int,
    val city: String,
    val member_phone: String? = "0",
    val app_os: String?,
    val app_version: Int?
)
