package com.jcodonuts.app.data.remote.model.res

data class SuccessRes(
    val message: String,
    val status: Int
)

data class SuccessResChangePassword(
        val message: String,
        val status_code: Int,
        val error:String?
)

data class SuccessPaymentRes(
    val status:Int?,
    val message:String?,
    val data:Boolean
)