package com.jcodonuts.app.data.remote.api

import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.*
import io.reactivex.Single
import retrofit2.http.*

interface DirectJcoApi {

    @POST("user/get")
    fun checkMember(@Body body: CheckMemberReq): Single<MemberRes>

    @POST("menu/recommendation")
    fun getMenuRecommendation(@Body body: MenuRecommendationReq): Single<MenuRecommendationRes>

    @GET("payment/list")
    fun paymentList(@Query("company") company: String?): Single<PaymentListRes>

    @GET("jpoint/mutation")
    fun getJpointMutation(
        @Query("point_status") pointStatus: Int,
        @Query("page") page: Int
    ): Single<JPointsRes>

    @POST("user/login")
    fun login(
        @Body body: LoginReq
    ): Single<LoginRes>

    @POST("checkout/submit")
    fun createOrder(@Body body: DirectCreateOrderReq): Single<CreateOrderRes>

    @POST("coupon/coupon_list")
    fun couponMember(@Body body: DirectCouponMemberReq): Single<CouponRes>
}