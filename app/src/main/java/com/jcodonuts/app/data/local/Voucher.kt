package com.jcodonuts.app.data.local

data class Voucher(
    val id:String?,
    val name:String?,
    val image:String?,
    val date:String?,
    val description:String?,
    val code:String?,
    val isClaim:Int?,
    val isUsed:Int?
): BaseCell()

data class VoucherHeader(val name:String?): BaseCell()