package com.jcodonuts.app.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cart_product")
data class CartProduct(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val menuCode: Int?,
    val name: String?,
    val imgURL: String?,
    var price: Double,
    val productType: String?,
    var notes: String?,
    var qty: Int,
    var priceOriginal: Double,
    var variantname: String = "",
    var detailVariant: MutableList<CartDetailVariant>?
) : BaseCell() {

    fun getMenuName(): String {
        val names = mutableListOf<String>()
        detailVariant?.forEach { detailVariant ->
            names.add(detailVariant.name)
        }
        return names.distinct().toString().removePrefix("[").removeSuffix("]")
    }

    fun getMenuDetail(): String {
        return if (productType?.contains("beverage",true) == true && detailVariant != null) {
            "${name?.toUpperCase()} - ${variantname.toUpperCase()}"
        } else {
            variantname
        }
    }
}

@Entity(tableName = "cart_product_pickup")
data class CartProductPickUp(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val menuCode: Int?,
    val name: String?,
    val imgURL: String?,
    var price: Double,
    val productType: String?,
    var notes: String?,
    var qty: Int,
    var priceOriginal: Double,
    var variantname: String = "",
    var detailVariant: MutableList<CartDetailVariant>?
) : BaseCell() {

    fun getMenuName(): String {
        val names = mutableListOf<String>()
        detailVariant?.forEach { detailVariant ->
            names.add(detailVariant.name)
        }
        return names.distinct().toString().removePrefix("[").removeSuffix("]")
    }

    fun getMenuDetail(): String {
        return if (productType?.contains("beverage",true) == true && detailVariant != null) {
            "${name?.toUpperCase()} - ${variantname.toUpperCase()}"
        } else {
            variantname
        }
    }
}
