package com.jcodonuts.app.data.remote.model.res

import com.jcodonuts.app.data.local.BaseCell

data class RecentOrderRes(
    val status: Int?,
    val message: String?,
    val data: List<Order>?
)

data class DetailRecentOrderRes(
    val status_code: Int?,
    val data: Order?
)

data class Order(
    val order_id: String?,
    val order_status: String?,
    val payment_method: String?,
    val order_type: String?,
    val delivery_type: String?,
    val order_time: String?,
    val order_city: String?,
    val order_address: String?,
    val order_addressinfo: String?,
    val order_outlet_ID: String?,
    val order_outlet_code: String?,
    val outlet_name: String?,
    val outlet_address: String?,
    val order_payment: OrderPayment?,
    val subtotal: Int?,
    val order_subtotal: String?,
    val order_total: String?,
    val delivery_fee: Int?,
    val order_fee:Int?,
    val freeDelivery: Int?,
    val order_brand: String?,
    val order_total_item: String?,
    val menu_list: String?,
    val menu_img: String?,
    val outlet_city: String?,
    val outlet_postcode: String?,
    val outlet_province: String?,
    val outlet_country: String?,
    val promo: PromoVoucher?,
    val grandtotal: Int?,
    val outlet_phone: String?,
    val member_phone: String?,
    val order_payment_status:String?,
    val order_promo:Int?,
    val order_note:String?,
    val ecoBag: EcoBag?,
    val order_detail: List<OrderDetail>,
    val order_progress: List<OrderProgress>,
    val jpoint_info: JPointInfo?,
    val order_delivery: List<OrderDelivery>?,
    val delivery_service: Int?,
    val order_status_name: String?
) : BaseCell() {
    fun getPrice(): Int {
        var price = 0
        order_detail.forEach {
            price += it.menu_price?.toInt() ?: 0
        }
        return price
    }

    fun getMenuName(): String {
        val names = mutableListOf<String>()
        order_detail.forEach { orderDetail ->
            names.add(orderDetail.menu_name.toString())
        }
        return names.toString().removePrefix("[").removeSuffix("]")
    }
}

data class OrderDetail(
    val order_seq: String?,
    val menu_code: String?,
    val menu_name: String?,
    val menu_quantity: String?,
    val menu_unitprice: String?,
    val menu_price: String?,
    val menu_detail: String?,
    val menu_image: List<ImageOrder>
)

data class ImageOrder(
    val image: String?
)

data class OrderPayment(
    val gw_approval_code: String?,
    val gw_transaction_time: String?,
    val gw_payment_type: String?,
    val gw_status_code: String?,
    val payment_name: String?,
    val payment_icon: String?,
    val gw_va_numbers:String?
)

data class OrderProgress(
    val progress_status: String?,
    val progress_outlet_ID: String?,
    val order_store_name: String?,
    val progress_time: String?
)

data class OrderDelivery(
    val order_delivery_id: String?,
    val status: String?,
    val courier: Courier?,
    val status_name: String?,
    val trackingURL: String?,
)

data class Courier(
    val name: String?,
    val phone: String?,
    val licensePlate: String?
)