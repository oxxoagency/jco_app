package com.jcodonuts.app.data.repository

import android.app.Application
import android.content.res.AssetManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jcodonuts.app.data.local.TopupItem
import com.jcodonuts.app.data.remote.api.JcoPayment
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.OrderPaymentReq
import com.jcodonuts.app.data.remote.model.req.PaymentFinishReq
import com.jcodonuts.app.data.remote.model.req.PaymentReq
import com.jcodonuts.app.data.remote.model.res.PaymentFinishRes
import com.jcodonuts.app.data.remote.model.res.PaymentListRes
import com.jcodonuts.app.data.remote.model.res.SuccessPaymentRes
import io.reactivex.Single
import javax.inject.Inject

interface PaymentRepository {
    fun getTopupMethod(): Single<List<TopupItem>>
    fun getPaymentList(): Single<PaymentListRes>
    fun getMidtransPayment(body: PaymentReq): Single<SuccessPaymentRes>
    fun paymentFinish(body: PaymentFinishReq): Single<PaymentFinishRes>
    fun orderPayment(body: OrderPaymentReq): Single<SuccessPaymentRes>
}

class PaymentRepositoryImpl @Inject constructor(
    private val service: JcoPayment,
    private val application: Application
) : PaymentRepository, BaseRepository() {


    override fun getTopupMethod(): Single<List<TopupItem>> {
        return composeSingle {
            Single.fromCallable {
                val jsonString = application.baseContext.assets.readFile("topup.json")
                val itemType = object : TypeToken<List<TopupItem>>() {}.type
                Gson().fromJson(jsonString, itemType)
            }
        }
    }

    override fun getPaymentList(): Single<PaymentListRes> =
        composeSingle { service.getPaymentList() }
            .compose(ErrorNetworkHandler())

    override fun getMidtransPayment(body: PaymentReq): Single<SuccessPaymentRes> =
        composeSingle {
            service.getMidtransPayment(body)
        }
            .compose(ErrorNetworkHandler())

    override fun paymentFinish(body: PaymentFinishReq): Single<PaymentFinishRes> =
        composeSingle { service.paymentFinish(body) }.compose(ErrorNetworkHandler())

    override fun orderPayment(body: OrderPaymentReq): Single<SuccessPaymentRes> =
        composeSingle { service.orderPayment(body) }.compose(ErrorNetworkHandler())


    private fun AssetManager.readFile(fileName: String) = open(fileName)
        .bufferedReader()
        .use { it.readText() }


}