package com.jcodonuts.app.data.repository

import android.app.Application
import com.jcodonuts.app.data.remote.api.JcoApi
import com.jcodonuts.app.data.remote.model.req.ProductPromoReq
import com.jcodonuts.app.data.remote.model.res.ProductPromoRes
import io.reactivex.Single
import javax.inject.Inject

interface ProductPromoRepository {
    fun getHotPromo(body: ProductPromoReq): Single<ProductPromoRes>
}

class ProductPromoRepositoryImpl @Inject constructor(
    private val service: JcoApi,
    private val application: Application
) : ProductPromoRepository, BaseRepository() {

    override fun getHotPromo(body: ProductPromoReq): Single<ProductPromoRes> =
        service.showHotPromo(body)
}