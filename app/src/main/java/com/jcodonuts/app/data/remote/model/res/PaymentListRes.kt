package com.jcodonuts.app.data.remote.model.res

data class PaymentListRes(
    val status_code: Int?,
    val data: List<PaymentList>
)

data class PaymentList(
    val payment_method: String?,
    val payment_name: String?,
    val payment_icon: String?,
    val ordering: String?
)

data class PaymentFinishRes(
    val status: Int?,
    val message: String?,
    val data: PaymentFinishData?
)

data class PaymentFinishData(
    val masked_card: String?,
    val approval_code: String?,
    val bank: String?,
    val channel_response_code: String?,
    val channel_response_message: String?,
    val transaction_time: String?,
    val gross_amount: String?,
    val currency: String?,
    val order_id: String?,
    val payment_type: String?,
    val signature_key: String?,
    val status_code: String?,
    val transaction_id: String?,
    val transaction_status: String?,
    val fraud_status: String?,
    val status_message: String?,
    val merchant_id: String?,
    val card_type: String?,
    val info: String?
)