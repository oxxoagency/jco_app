package com.jcodonuts.app.data.repository


import com.jcodonuts.app.data.remote.api.JcoUserApi
import com.jcodonuts.app.data.remote.helper.ErrorNetworkHandler
import com.jcodonuts.app.data.remote.model.req.*
import com.jcodonuts.app.data.remote.model.res.*
import io.reactivex.Single
import javax.inject.Inject

interface AuthRepository {
    fun login(body: LoginReq): Single<LoginRes>
    fun register(body: RegisterReq): Single<RegisterRes>
    fun logout(): Single<SuccessRes>
    fun resetPassword(body: ChangePsswordReq): Single<SuccessRes>
    fun memberMe(): Single<MemberRes>
    fun changePassword(body: ChangePasswordReq): Single<SuccessResChangePassword>
    fun updateProfile(body: UpdateProfileReq): Single<MemberRes>
    fun getRefreshToken(): Single<RefreshTokenRes>
}

class AuthRepositoryImpl @Inject constructor(
    private val service: JcoUserApi
) : AuthRepository, BaseRepository() {

    override fun login(body: LoginReq): Single<LoginRes> {
        return composeSingle { service.login(body) }
            .compose(ErrorNetworkHandler())
    }

    override fun register(body: RegisterReq): Single<RegisterRes> {
        return composeSingle { service.register(body) }
            .compose(ErrorNetworkHandler())
    }

    override fun logout(): Single<SuccessRes> {
        return composeSingle { service.logout() }
            .compose(ErrorNetworkHandler())
    }

    override fun resetPassword(body: ChangePsswordReq): Single<SuccessRes> {
        return composeSingle { service.resetPassword(body) }
            .compose(ErrorNetworkHandler())
    }

    override fun memberMe(): Single<MemberRes> =
        composeSingle { service.memberMe() }.compose(ErrorNetworkHandler())

    override fun changePassword(body: ChangePasswordReq): Single<SuccessResChangePassword> =
        composeSingle { service.changePassword(body) }.compose(ErrorNetworkHandler())

    override fun updateProfile(body: UpdateProfileReq): Single<MemberRes> =
        composeSingle { service.updateProfile(body) }.compose(ErrorNetworkHandler())

    override fun getRefreshToken(): Single<RefreshTokenRes> =
        composeSingle { service.getRefreshToken() }.compose(ErrorNetworkHandler())
}