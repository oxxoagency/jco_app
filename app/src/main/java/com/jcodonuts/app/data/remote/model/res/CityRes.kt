package com.jcodonuts.app.data.remote.model.res

data class CityRes(
    val status_code: Int?,
    val data: List<CityDataRes>
)

data class CityDataRes(
    val city: String?,
    val province: String?,
    val delivery_fee: Int?
)