package com.jcodonuts.app.data.remote.model.req

data class CartOrder(
    val brand: Int?,
    val order_city: String?,
    val delivery_type: Int?,
    val coupon_code: String?,
    val order_payment: String?,
    val order_brand: String?,
    val cart_info: List<CartInfo>,
    val use_jpoint: Int?,
    val order_address: String?,
    val order_address_info: String?,
    val order_latitude: String?,
    val order_longitude: String?
)

data class CartInfo(
    val menu_code: String?,
    val menu_quantity: String?
)

data class RecommendationReq(
    val brand: Int?,
    val member_phone: String?,
    val city: String?,
    val limit: Int?
)