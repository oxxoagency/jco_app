package com.jcodonuts.app.data.remote.model.res

data class MemberRes(
    val `data`: Member,
    val message: String,
    val status_code: Int,
    val error:String?
)

data class Member(
    val member_email: String?,
    val member_name: String?,
    val member_phone: String?,
    val member_phone2: String?,
    val member_point: String?,
    val member_gender:Int?,
    val member_dob:String?
)