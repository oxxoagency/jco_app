package com.jcodonuts.app.data.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jcodonuts.app.data.local.*

@Database(
    entities = [CartProduct::class, CartDetailVariant::class, CartProductJcoR::class, CartProductPickUp::class, CartProductPickUpJcoR::class],
    version = 5
)
@TypeConverters(DataConverter::class)
abstract class JcoDatabase : RoomDatabase() {

    abstract fun cartDao(): CartDao
}