package com.jcodonuts.app.data.local

data class SavePlace(val title: String) : BaseCell()