package com.jcodonuts.app.data.remote.model.res

data class CreateOrderRes(
    val status_code: Int?,
    val data: OrderId,
    val error: Error?,
)

data class OrderId(
    val order_id: Int?,
    val order_detail: List<OrderDetail>?,
    val order_fee: Int?,
    val order_promo: Int?,
    val order_loyalty: Int?,
    val order_total: Int?
)

data class Error(
    val order_address: String?,
    val order_outlet_code: String?,
    val order_outlet_ID:String?,
    val member_phone2:String?,
    val order_payment: String?,
    val message: String?
)

data class OrderListRes(
    val status_code: String?,
    val data: List<OrderRes>
)

data class OrderRes(
    val order_id: String?,
    val delivery_type: String?,
    val order_status: String?,
    val order_time: String?,
    val order_total: String?,
    val order_outlet_name:String?,
    val order_detail: List<OrderDetail>
){
    fun getOrderName():String{
        val name = mutableListOf<String>()
        order_detail.forEach {
            name.add(it.menu_name.toString())
        }
        return name.toString().removePrefix("[").removeSuffix("]").replace("\n, ", ",\n")
    }
}