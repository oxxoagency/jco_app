package com.jcodonuts.app.data.local

data class DeliveryItem(
    val _id: String,
    val address: String,
    val distance: String,
    var isFavorite: Boolean,
    val placeName: String
) : BaseCell()

data class MemberAddress(
    val member_address_id: Int?,
    val address_label: String?,
    val recipient_name: String?,
    val address: String?,
    val address_details: String?,
    val recipient_phone:String?,
     val cityordistrict:String?,
    val recipient_postcode:Int?,
    val latitude:String?,
    val longitude:String?
) : BaseCell()

data class MemberAddressReq(
    val member_phone: String
)

data class MemberAddressRes(
    val `data`: List<MemberAddress>?,
    val status: Int,
    val message: String,
) : BaseCell()

data class AddMemberRes(
    val `data`: MemberAddress,
    val status: Int,
    val message: String,
) : BaseCell()

data class MemberAddressGroup(
    val memberAddress: List<MemberAddressItem>
) : BaseCell()

data class AddressRes(
    val `data`: MemberAddress,
    val status: Int,
    val message: String,
) : BaseCell()