package com.jcodonuts.app.data.remote.api

import com.jcodonuts.app.data.remote.model.req.OrderPaymentReq
import com.jcodonuts.app.data.remote.model.req.PaymentFinishReq
import com.jcodonuts.app.data.remote.model.req.PaymentReq
import com.jcodonuts.app.data.remote.model.res.PaymentFinishRes
import com.jcodonuts.app.data.remote.model.res.PaymentListRes
import com.jcodonuts.app.data.remote.model.res.SuccessPaymentRes
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface JcoPayment {

    @GET("payment/list")
    fun getPaymentList(): Single<PaymentListRes>

    @POST("midtrans/payment")
    fun getMidtransPayment(@Body body: PaymentReq): Single<SuccessPaymentRes>

    @POST("transaction/payment/finish")
    fun paymentFinish(@Body body: PaymentFinishReq): Single<PaymentFinishRes>

    @POST("order/payment")
    fun orderPayment(@Body body: OrderPaymentReq): Single<SuccessPaymentRes>
}