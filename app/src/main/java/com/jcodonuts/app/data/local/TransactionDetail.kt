package com.jcodonuts.app.data.local

import com.jcodonuts.app.data.remote.model.res.OrderDelivery
import com.jcodonuts.app.data.remote.model.res.OrderDetail

data class TransactionDetailHeader(
    val orderStatus: String?,
    val orderPaymentStatus: String?
) : BaseCell() {
    fun getStatusLabel() = when {
        orderStatus?.contains("6", true) == true -> {
            "Selesai"
        }
        orderStatus?.contains("7", true) == true -> {
            "Gagal"
        }
        orderStatus?.contains("1", true) == true -> {
            "Menunggu Pembayaran"
        }
        orderStatus?.contains("2", true) == true -> {
            "Sudah Dibayar"
        }
        orderStatus?.contains("4", true) == true -> {
            "Customer tidak dapat dihubungi"
        }
        orderStatus?.contains("3", true) == true -> {
            "Dikonfirmasi toko"
        }
        orderStatus?.contains("5", true) == true -> {
            "Sedang dikirim"
        }
        orderStatus?.contains("8", true) == true -> {
            "Expired"
        }
        else -> {
            "Diproses"
        }
    }
}

data class TransactionDetailShippingInformation(
    val id: String?,
    val orderAddress: String?,
    val orderInfo: String?,
    val outletAddress: String?,
    val outletName: String?,
    val deliveryType: String?,
    val memberPhone: String?,
    val outletPhone: String?,
    val deliveryService: Int?,
    val orderStatus: String?,
    val orderDelivery: List<OrderDelivery>?,
    val paymentName: String?,
    val orderTime: String?,
    val orderStatusName: String?
) : BaseCell()

data class TransactionDetailOrderSummary(
    val orderId: String?,
    val purchaseDate: String?,
    val vaNumber: String?,
    val instructionBCA: String?,
    val instructionMandiri: String?,
    val paymentName: String?,
    val orderStatus: String?,
    val notes:String?
) : BaseCell() {

    fun getInstruction() = when {
        paymentName?.contains(
            "BCA",
            true
        ) == true -> instructionBCA
        paymentName?.contains("Mandiri", true) == true -> instructionMandiri
        else -> null
    }
}

data class TransactionDetailProduct(
    val nameProduct: String?,
    val qty: String?,
    val menuCode: String?,
    val unitPrice: String?,
    val price: String?,
    val menuDetail: String?
) : BaseCell()

data class TransactionDetailTotal(
    val orderId:String?,
    val subtotal: String?,
    val deliveryFee: String?,
    val ecobag: String?,
    val ecobagPrice: String?,
    val total: String?,
    val paymentName: String?,
    val paymentIcon: String?,
    val freeDeliveryText: String?,
    val freeDelivery: Int?,
    val promo: Int?,
    val promoText:String?,
    val isDelivery: Boolean,
    val isPaid: Boolean,
    val orderDetail: List<OrderDetail>,
    val jPointUsed: String?,
    val jPontReceived: String?,
    val isGetJPoint: Boolean,
    val isUseJPoint: Boolean
) : BaseCell()