package com.jcodonuts.app.data.remote.api

import com.jcodonuts.app.BuildConfig
import com.jcodonuts.app.data.remote.model.res.MapsRes
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MapsApi {

    @GET("place/autocomplete/json")
    fun getPlaceAutoComplete(
        @Query("input") input: String?,
        @Query("key") key: String? = BuildConfig.API_KEY_MAPS
    ): Single<MapsRes>
}