package com.jcodonuts.app.data.remote.model.req

data class CityReq(
    val brand:Int?,
    val country:String?
)

data class CitySearchReq(
    val brand:Int?,
    val country:String?,
    val city:String?
)