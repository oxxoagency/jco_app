package com.jcodonuts.app.data.remote.model.req

data class CreateOrderReq(
    val brand: Int?,
    val order_type: String?,
    val payment_type: String?,
    val delivery_type: Int?,
    val member_name: String?,
    val member_email: String?,
    val member_phone: String?,
    val member_phone2: String?,
    val order_trx_time: String?,
    val order_city: String?,
    val order_address: String?,
    val order_address_info: String?,
    val order_postcode: String?,
    val order_latitude: String?,
    val order_longitude: String?,
    val order_note: String?,
    val order_company: String?,
    val order_outlet_ID: String?,
    val order_outlet_code: String?,
    val order_ip: String?,
    val order_useragent: String?,
    val order_detail: MutableList<CreateOrderDetailReq>?,
    val coupon_code: String?,
    val order_payment: String?,
    val order_brand: String?,
    val order_recipient_name:String?,
    val order_recipient_phone:String?,
    val use_jpoint: Int?
)

data class DirectCreateOrderReq(
    val order_type: String?,
    val payment_type: String?,
    val delivery_type: Int?,
    val member_name: String?,
    val member_email: String?,
    val member_phone: String?,
    val member_phone2: String?,
    val order_trx_time: String?,
    val order_city: String?,
    val order_address: String?,
    val order_address_info: String?,
    val order_postcode: String?,
    val order_latitude: String?,
    val order_longitude: String?,
    val order_note: String?,
    val order_company: String?,
    val order_outlet_ID: String?,
    val order_outlet_code: String?,
    val order_ip: String?,
    val order_useragent: String?,
    val order_detail: MutableList<CreateOrderDetailReq>?,
    val coupon_code: String?,
    val order_payment: String?,
    val order_brand: String?,
    val order_recipient_name:String?,
    val order_recipient_phone:String?,
    val use_jpoint: Int?
)

data class CreateOrderDetailReq(
    val menu_code: String?,
    val menu_name: String?,
    val menu_quantity: String?,
    val material_unit_price: String?,
    val material_sub_total: String?,
    val menu_detail: String?
)

data class OrderListReq(
    val brand: Int?,
    val member_email: String?,
    val order_brand: String?,
    val order_status: Int?,
    val offset: Int?,
    val limit: Int?
)