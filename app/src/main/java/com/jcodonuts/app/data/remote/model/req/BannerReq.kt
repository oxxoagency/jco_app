package com.jcodonuts.app.data.remote.model.req

data class BannerReq(
    val brand: Int?,
    val banner_id: String?,
    val app_os:String?,
    val app_version:Int?
)