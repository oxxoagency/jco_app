package com.jcodonuts.app.data.local

data class HomeHeadSection(val title:String?, val point:String?, val tier: String?):BaseCell()