package com.jcodonuts.app.data.remote.model.res

data class LoginRes(
    val accessToken: String,
    val refreshToken: String,
    val status_code: Int,
    val error: String,
)

data class RefreshTokenRes(
    val status: Int,
    val message:String,
    val access_token:String
)