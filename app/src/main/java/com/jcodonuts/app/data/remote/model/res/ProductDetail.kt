package com.jcodonuts.app.data.remote.model.res

import com.jcodonuts.app.data.local.BaseCell

data class ProductDetailRes(
    val `data`: ProductDetail,
    val message: String,
    val status: Int?,
    val status_code:Int?
)

data class ProductDetail(
    val category_name: String,
    val category_title: String,
    val id_menu: String,
    var is_favorite: String,
    val is_freedelivery: String,
    val is_promo: String,
    val material: String,
    val menu_amount: String,
    val menu_code: String,
    val menu_desc: String,
    val menu_image: List<MenuImage>?,
    val menu_name: String,
    val menu_name_en: String,
    val menu_price: String?,
    val min_quantity: String,
    val product_id: String,
    val start_date: String,
    val end_date: String,
//    val variant_group: List<VariantGroup>
    val details: List<DetailVariant>?,
    val menu_quantity: String?,
    val menu_detail: String?,
    val order_time: String?,
    val variant_num: String?,
     val menu_normalprice: Int?
)

data class DetailVariant(
    val menu_img: String?,
    val menu_image:String?,
    val menu_name: String?,
    val menu_code: String?,
    val size: String?,
    val temp: String?,
    val menu_price: String?,
    val package_name: String?,
    val package_donut_list: List<PackageDonut>?,
    val package_image: String?,
    val package_description: String?
) : BaseCell()

data class VariantGroup(
    val jco_id_menu: String,
    val jco_menu_code: String,
    val limit: String,
    val optional: String,
    val product_id: String,
    val variant: List<Variant>,
    val variant_group_desc: String,
    val variant_group_id: String,
    val variant_group_name: String,
    val variant_group_type: String
)

data class Variant(
    val variant_group_id: String,
    val variant_id: String,
    val variant_img: String,
    val variant_name: String,
    val variant_price: String
)

data class PackageDonut(
    val menu_img: String?,
    val menu_name: String?,
    var quantity: Int = 0
) : BaseCell()

data class MenuImage(
    val image: String?
)