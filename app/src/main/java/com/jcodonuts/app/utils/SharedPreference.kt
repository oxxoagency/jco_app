package com.jcodonuts.app.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.JPointHistory
import com.jcodonuts.app.data.remote.model.res.CartRes
import com.jcodonuts.app.data.remote.model.res.JPointRes
import com.jcodonuts.app.data.remote.model.res.JPointsRes


class SharedPreference(val context: Context) {

    companion object {
        val ACCESS_TOKEN = "ACCESS_TOKEN"
        val REFRESH_TOKEN = "REFRESH_TOKEN"
        val FROM_LOGIN = "FROM_LOGIN"
        val DATA_HOME = "DATA_HOME"
        val PHONE_NUMBER = "PHONE_NUMBER"
        val NOTES = "NOTES"
        val NOTES_JCO_R = "NOTES_JCO_R"
        val POINTS = "POINTS"
        val LANGUANGE = "LANGUAGE"
        val LATITUDE = "LATITUDE"
        val LONGITUDE = "LONGITUDE"
        val LATITUDE_ADDRESS = "LATITUDE_ADDRESS"
        val LONGITUDE_ADDRESS = "LONGITUDE_ADDRESS"
        val SELECT_LATITUDE_ADDRESS = "SELECT_LATITUDE_ADDRESS"
        val SELECT_LONGITUDE_ADDRESS = "SELECT_LONGITUDE_ADDRESS"
        val SELECT_LATITUDE_PICKUP = "SELECT_LATITUDE_PICKUP"
        val SELECT_LONGITUDE_PICKUP = "SELECT_LONGITUDE_PICKUP"
        val ADDRESS = "ADDRESS"
        val PICKUP = "PICKUP"
        val PICKUP_JCO_R = "PICKUP_JCO_R"
        val EMAIL = "EMAIL"
        val NAME = "NAME"
        val PHONE_NUMBER_TWO = "PHONE_NUMBER_TWO"
        val DETAIL_ADDRESS = "DETAIL_ADDRESS"
        val POSTCODE = "POSTCODE"
        val OUTLET_ID = "OUTLET_ID"
        val OUTLET_CODE = "OUTLET_CODE"
        val PAYMENT_METHOD = "PAYMENT_METHOD"
        val PAYMENT_NAME = "PAYMENT_NAME"
        val PAYMENT_ICON = "PAYMENT_ICON"
        val VOUCHER = "VOUCHER"
        val CODE_VOUCHER = "CODE_VOUCHER"
        val PRICE_CART = "PRICE_CART"
        val DELIVERY_FEE_CART = "DELIVERY_FEE_CART"
        val TOTAL_PRICE_CART = "TOTAL_PRICE_CART"
        val ECOBAG_CART = "ECOBAG_CART"
        val ECOBAG_PRICE = "ECOBAG_PRICE"
        val ECOBAG_SUB_TOTAL = "ECOBAG_SUB_TOTAL"
        val ECOBAG_QTY = "ECOBAG_QTY"
        val GROSS_AMOUT = "GROSS_AMOUNT"
        val PICKUP_ADDRESS = "PICKUP_ADDRESS"
        val PICKUP_CITY = "PICKUP_CITY"
        val ITEM_DETAILS = "ITEM_DETAILS"
        val ITEM_DETAILS_JCO_R = "ITEM_DETAILS_JCO_R"
        val JPOINT_RECEIVED = "JPOINT_RECEIVED"
        val JPOINT_USED = "JPOINT_USED"
        val OUTLET_ID_FOR_DELIVERY = "OUTLET_ID_FOR_DELIVERY"
        val OUTLET_ID_FOR_DELIVERY_JCO_R = "OUTLET_ID_FOR_DELIVERY_JCO_R"
        val OUTLET_CODE_FOR_DELIVERY = "OUTLET_CODE_FOR_DELIVERY"
        val OUTLET_CODE_FOR_DELIVERY_JCO_R = "OUTLET_CODE_FOR_DELIVERY_JCO_R"
        val ORDER_POST_CODE_FOR_DELIVERY = "ORDER_POST_CODE_FOR_DELIVERY"
        val CITY = "CITY"
        val FCM_TOKEN = "FCM_TOKEN"
        val FREE_DELIVERY = "FREE_DELIVERY"
        val SELECT_CITY = "SELECT_CITY"
        val TOTAL_DELIVERY_FEE = "TOTAL_DELIVERY_FEE"
        val SELECT_PHONE_NUMBER = "SELECT_PHONE_NUMBER"
        val REFRESH_STATE_CART_DETAIL = "REFRESH_STATE_CART_DETAIL"
        val VERSION = "VERSION"
        val RECIPIENT_NAME = "RECIPIENT_NAME"
        val STATE_PICKUP = "STATE_PICKUP"
        val JPOINT = "JPOINT"
        val DELIVERY_TYPE = "DELIVERY_TYPE"
    }

    private val PREFS_NAME = "jcoApp"
    val sharedPref: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun save(KEY_NAME: String, text: String) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, text)

        editor!!.commit()
    }

    fun save(KEY_NAME: String, value: Int) {
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putInt(KEY_NAME, value)

        editor.commit()
    }

    fun save(KEY_NAME: String, status: Boolean) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putBoolean(KEY_NAME, status!!)

        editor.commit()
    }

    fun getValueString(KEY_NAME: String): String? {

        return sharedPref.getString(KEY_NAME, null)


    }

    fun getValueInt(KEY_NAME: String): Int {

        return sharedPref.getInt(KEY_NAME, 0)
    }

    fun getValueBoolean(KEY_NAME: String, defaultValue: Boolean): Boolean {

        return sharedPref.getBoolean(KEY_NAME, defaultValue)

    }

    fun clearSharedPreference() {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        //sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        editor.clear()
        editor.commit()
    }

    fun removeValue(KEY_NAME: String) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.remove(KEY_NAME)
        editor.commit()
    }

    fun savePhoneNumber(phone: String?) {
        sharedPref.edit()
            .putString(PHONE_NUMBER, phone)
            .apply()
    }

    fun loadPhoneNumber(): String? = sharedPref.getString(PHONE_NUMBER, "0")

    fun savePhoneNumberTwo(phone: String) {
        sharedPref.edit()
            .putString(PHONE_NUMBER_TWO, phone)
            .apply()
    }

    fun loadPhoneNumberTwo(): String? = sharedPref.getString(PHONE_NUMBER_TWO, "")

    fun saveNotes(notes: String) {
        sharedPref.edit()
            .putString(NOTES, notes)
            .apply()
    }

    fun loadNotes(): String? = sharedPref.getString(NOTES, "")

    fun saveNotesJcoR(notes: String) {
        sharedPref.edit()
            .putString(NOTES_JCO_R, notes)
            .apply()
    }

    fun loadNotesJcoR(): String? = sharedPref.getString(NOTES_JCO_R, "")

    fun savePoints(points: String?) {
        sharedPref.edit()
            .putString(POINTS, points)
            .apply()
    }

    fun loadPoints(): String? = sharedPref.getString(POINTS, "")

    fun saveLanguage(language: String) {
        sharedPref.edit()
            .putString(LANGUANGE, language)
            .apply()
    }

    fun loadLanguage(): String? =
        sharedPref.getString(LANGUANGE, context.getString(R.string.indonesia))

    fun saveLongitude(longitude: String?) {
        sharedPref.edit()
            .putString(LONGITUDE, longitude)
            .apply()
    }

    fun loadLongitude(): String? = sharedPref.getString(LONGITUDE, "")

    fun saveLongitudeAddress(longitude: String?) {
        sharedPref.edit()
            .putString(LONGITUDE_ADDRESS, longitude)
            .apply()
    }

    fun loadLongitudeAddress(): String? = sharedPref.getString(LONGITUDE_ADDRESS, "")

    fun saveSelectLongitudeAddress(longitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LONGITUDE_ADDRESS, longitude)
            .apply()
    }

    fun loadSelectLongitudeAddress(): String? = sharedPref.getString(SELECT_LONGITUDE_ADDRESS, "")

    fun saveSelectLongitudePickup(longitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LONGITUDE_PICKUP, longitude)
            .apply()
    }

    fun loadSelectLongitudePickup(): String? = sharedPref.getString(SELECT_LONGITUDE_PICKUP, "")

    fun saveLatitude(latitude: String?) {
        sharedPref.edit()
            .putString(LATITUDE, latitude)
            .apply()
    }

    fun loadLatitude(): String? = sharedPref.getString(LATITUDE, "")

    fun saveLatitudeAddress(latitude: String?) {
        sharedPref.edit()
            .putString(LATITUDE_ADDRESS, latitude)
            .apply()
    }

    fun loadLatitudeAddress(): String? = sharedPref.getString(LATITUDE_ADDRESS, "")

    fun saveSelectLatitudeAddress(latitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LATITUDE_ADDRESS, latitude)
            .apply()
    }

    fun loadSelectLatitudeAddress(): String? = sharedPref.getString(SELECT_LATITUDE_ADDRESS, "")

    fun saveSelectLatitudePickUp(latitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LATITUDE_PICKUP, latitude)
            .apply()
    }

    fun loadSelectLatitudePickUp(): String? = sharedPref.getString(SELECT_LATITUDE_PICKUP, "")

    fun saveAddress(address: String) {
        sharedPref.edit()
            .putString(ADDRESS, address)
            .apply()
    }

    fun loadAddress(): String? = sharedPref.getString(ADDRESS, "")

    fun saveDetailAddress(address: String) {
        sharedPref.edit()
            .putString(DETAIL_ADDRESS, address)
            .apply()
    }

    fun loadDetailAddress(): String? = sharedPref.getString(DETAIL_ADDRESS, "")

    fun savePickUp(pickUp: String) {
        sharedPref.edit()
            .putString(PICKUP, pickUp)
            .apply()
    }

    fun loadPickUp(): String? = sharedPref.getString(PICKUP, "")

    fun savePickUpJcoR(pickUp: String) {
        sharedPref.edit()
            .putString(PICKUP_JCO_R, pickUp)
            .apply()
    }

    fun loadPickUpJcoR(): String? = sharedPref.getString(PICKUP_JCO_R, "")

    fun saveEmail(email: String?) {
        sharedPref.edit()
            .putString(EMAIL, email)
            .apply()
    }

    fun loadEmail(): String? = sharedPref.getString(EMAIL, "")

    fun saveName(name: String?) {
        sharedPref.edit()
            .putString(NAME, name)
            .apply()
    }

    fun loadName(): String? = sharedPref.getString(NAME, "")

    fun savePostCode(postCode: String) {
        sharedPref.edit()
            .putString(POSTCODE, postCode)
            .apply()
    }

    fun loadPostCode(): String? = sharedPref.getString(POSTCODE, "")

    fun saveOutletId(outletId: String) {
        sharedPref.edit()
            .putString(OUTLET_ID, outletId)
            .apply()
    }

    fun loadOutletId(): String? = sharedPref.getString(OUTLET_ID, "")

    fun saveOutletCode(outletId: String) {
        sharedPref.edit()
            .putString(OUTLET_CODE, outletId)
            .apply()
    }

    fun loadOutletCode(): String? = sharedPref.getString(OUTLET_CODE, "")

    fun savePaymentMethod(payment: String) {
        sharedPref.edit()
            .putString(PAYMENT_METHOD, payment)
            .apply()
    }

    fun loadPaymentMethod(): String? = sharedPref.getString(PAYMENT_METHOD, "")

    fun savePaymentName(payment: String) {
        sharedPref.edit()
            .putString(PAYMENT_NAME, payment)
            .apply()
    }

    fun loadPaymentName(): String? = sharedPref.getString(PAYMENT_NAME, "")

    fun savePaymentIcon(payment: String) {
        sharedPref.edit()
            .putString(PAYMENT_ICON, payment)
            .apply()
    }

    fun loadPaymentIcon(): String? = sharedPref.getString(PAYMENT_ICON, "")

    fun saveVoucher(voucher: String) {
        sharedPref.edit()
            .putString(VOUCHER, voucher)
            .apply()
    }

    fun loadVoucher(): String? =
        sharedPref.getString(VOUCHER, "")

    fun saveCodeVoucher(voucher: String) {
        sharedPref.edit()
            .putString(CODE_VOUCHER, voucher)
            .apply()
    }

    fun loadTotalDeliveryFee(): String? =
        sharedPref.getString(TOTAL_DELIVERY_FEE, "")

    fun saveTotalDeliveryFee(voucher: String) {
        sharedPref.edit()
            .putString(TOTAL_DELIVERY_FEE, voucher)
            .apply()
    }

    fun loadFreeDelivery(): String? =
        sharedPref.getString(FREE_DELIVERY, "")

    fun saveFreeDelivery(voucher: String) {
        sharedPref.edit()
            .putString(FREE_DELIVERY, voucher)
            .apply()
    }

    fun loadCodeVoucher(): String? = sharedPref.getString(CODE_VOUCHER, "")

    fun removeVoucher() {
        sharedPref.edit()
            .remove(VOUCHER)
            .apply()
    }

    fun removeCodeVoucher() {
        sharedPref.edit()
            .remove(CODE_VOUCHER)
            .apply()
    }

    fun savePriceCart(voucher: String) {
        sharedPref.edit()
            .putString(PRICE_CART, voucher)
            .apply()
    }

    fun loadPriceCart(): String? = sharedPref.getString(PRICE_CART, "")

    fun saveDeliveryFeeCart(voucher: String) {
        sharedPref.edit()
            .putString(DELIVERY_FEE_CART, voucher)
            .apply()
    }

    fun loadDeliveryFeeCart(): String? = sharedPref.getString(DELIVERY_FEE_CART, "")

    fun saveTotalPriceCart(voucher: String) {
        sharedPref.edit()
            .putString(TOTAL_PRICE_CART, voucher)
            .apply()
    }

    fun loadTotalPriceCart(): String? = sharedPref.getString(TOTAL_PRICE_CART, "")

    fun saveGrossAmount(grossAmount: Int) {
        sharedPref.edit()
            .putInt(GROSS_AMOUT, grossAmount)
            .apply()
    }

    fun loadGrossAmount(): Int? = sharedPref.getInt(GROSS_AMOUT, 0)

    fun saveEcobagCart(voucher: String) {
        sharedPref.edit()
            .putString(ECOBAG_CART, voucher)
            .apply()
    }

    fun loadEcobagCart(): String? = sharedPref.getString(ECOBAG_CART, "")

    fun saveEcobagPriceCart(voucher: String) {
        sharedPref.edit()
            .putString(ECOBAG_PRICE, voucher)
            .apply()
    }

    fun loadEcobagPriceCart(): String? = sharedPref.getString(ECOBAG_CART, "")

    fun saveEcobagSubtotalCart(voucher: String) {
        sharedPref.edit()
            .putString(ECOBAG_SUB_TOTAL, voucher)
            .apply()
    }

    fun loadEcobagSubtotalCart(): String? = sharedPref.getString(ECOBAG_SUB_TOTAL, "")

    fun saveEcobagQtyCart(voucher: String) {
        sharedPref.edit()
            .putString(ECOBAG_QTY, voucher)
            .apply()
    }

    fun loadEcobagQtyCart(): String? = sharedPref.getString(ECOBAG_QTY, "")


    fun savePickUpAddress(address: String) {
        sharedPref.edit()
            .putString(PICKUP_ADDRESS, address)
            .apply()
    }

    fun loadPickUpAddress(): String? = sharedPref.getString(PICKUP_ADDRESS, "")

    fun savePickUpCity(address: String) {
        sharedPref.edit()
            .putString(PICKUP_CITY, address)
            .apply()
    }

    fun loadPickUpCity(): String? = sharedPref.getString(PICKUP_CITY, "Jakarta Barat")

    fun saveItemDetail(cartOrders: CartRes) {
        val gson = Gson()
        val cartOrder = gson.toJson(cartOrders)
        sharedPref.edit()
            .putString(ITEM_DETAILS, cartOrder)
            .apply()
    }

    fun loadItemDetail(): CartRes? {
        val gson = Gson()
        val cartOrder = sharedPref.getString(ITEM_DETAILS, "")
        return gson.fromJson(cartOrder, CartRes::class.java)
    }

    fun saveItemDetailJcoR(cartOrders: CartRes) {
        val gson = Gson()
        val cartOrder = gson.toJson(cartOrders)
        sharedPref.edit()
            .putString(ITEM_DETAILS_JCO_R, cartOrder)
            .apply()
    }

    fun loadItemDetailJcoR(): CartRes? {
        val gson = Gson()
        val cartOrder = sharedPref.getString(ITEM_DETAILS_JCO_R, "")
        return gson.fromJson(cartOrder, CartRes::class.java)
    }

    fun saveJPointReceived(jPointRes: JPointsRes) {
        val gson = Gson()
        val jpoint = gson.toJson(jPointRes)
        sharedPref.edit()
            .putString(JPOINT_RECEIVED, jpoint)
            .apply()
    }

    fun loadJPointReceived(): JPointsRes? {
        val gson = Gson()
        val jpoint = sharedPref.getString(JPOINT_RECEIVED, "")
        return gson.fromJson(jpoint, JPointsRes::class.java)
    }

    fun saveJPointUsed(jPointRes: JPointsRes) {
        val gson = Gson()
        val jpoint = gson.toJson(jPointRes)
        sharedPref.edit()
            .putString(JPOINT_USED, jpoint)
            .apply()
    }

    fun loadJPointUsed(): JPointsRes? {
        val gson = Gson()
        val jpoint = sharedPref.getString(JPOINT_USED, "")
        return gson.fromJson(jpoint, JPointsRes::class.java)
    }

    fun removeItemDetail() {
        sharedPref.edit()
            .remove(ITEM_DETAILS)
            .apply()
    }

    fun removeItemDetailJcoR() {
        sharedPref.edit()
            .remove(ITEM_DETAILS_JCO_R)
            .apply()
    }

    fun removeJPointUsed() {
        sharedPref.edit()
            .remove(JPOINT_USED)
            .apply()
    }

    fun removeJPointReceived() {
        sharedPref.edit()
            .remove(JPOINT_RECEIVED)
            .apply()
    }

    fun saveOutletIdForDelivery(address: String?) {
        sharedPref.edit()
            .putString(OUTLET_ID_FOR_DELIVERY, address)
            .apply()
    }


    fun loadOutletIdForDelivery(): String? = sharedPref.getString(OUTLET_ID_FOR_DELIVERY, "")

    fun saveOutletIdForDeliveryJcoR(address: String?) {
        sharedPref.edit()
            .putString(OUTLET_ID_FOR_DELIVERY_JCO_R, address)
            .apply()
    }

    fun loadOutletIdForDeliveryJcoR(): String? =
        sharedPref.getString(OUTLET_ID_FOR_DELIVERY_JCO_R, "")

    fun saveOutletCodeForDelivery(address: String?) {
        sharedPref.edit()
            .putString(OUTLET_CODE_FOR_DELIVERY, address)
            .apply()
    }

    fun loadOutletCodeForDelivery(): String? = sharedPref.getString(OUTLET_CODE_FOR_DELIVERY, "")

    fun saveOutletCodeForDeliveryJcoR(address: String?) {
        sharedPref.edit()
            .putString(OUTLET_CODE_FOR_DELIVERY_JCO_R, address)
            .apply()
    }

    fun loadOutletCodeForDeliveryJcoR(): String? = sharedPref.getString(
        OUTLET_CODE_FOR_DELIVERY_JCO_R, ""
    )

    fun saveOrderPostCodeForDelivery(address: String?) {
        sharedPref.edit()
            .putString(ORDER_POST_CODE_FOR_DELIVERY, address)
            .apply()
    }

    fun loadOrderPosttCodeForDelivery(): String? =
        sharedPref.getString(ORDER_POST_CODE_FOR_DELIVERY, "")

    fun saveCity(address: String?) {
        sharedPref.edit()
            .putString(CITY, address)
            .apply()
    }

    fun loadCity(): String? =
        sharedPref.getString(CITY, "")

    fun saveSelectCity(address: String) {
        sharedPref.edit()
            .putString(SELECT_CITY, address)
            .apply()
    }

    fun loadSelectCity(): String? =
        sharedPref.getString(SELECT_CITY, "Jakarta Barat")

    fun saveSelectPhoneNumber(address: String) {
        sharedPref.edit()
            .putString(SELECT_PHONE_NUMBER, address)
            .apply()
    }

    fun loadSelectPhoneNumber(): String? =
        sharedPref.getString(SELECT_PHONE_NUMBER, "")

    fun removeAddress() {
        sharedPref.edit()
            .remove(ADDRESS)
            .apply()
    }

    fun removeSelectPhoneNumber() {
        sharedPref.edit()
            .remove(SELECT_PHONE_NUMBER)
            .apply()
    }

    fun removeSelectLongitudeAddress() {
        sharedPref.edit()
            .remove(SELECT_LONGITUDE_ADDRESS)
            .apply()
    }

    fun removeSelectLatitudeAddress() {
        sharedPref.edit()
            .remove(SELECT_LATITUDE_ADDRESS)
            .apply()
    }

    fun removeSelectCity() {
        sharedPref.edit()
            .remove(SELECT_CITY)
            .apply()
    }

    fun removePickUp() {
        sharedPref.edit()
            .remove(PICKUP)
            .apply()
    }

    fun removePickUpJcoR() {
        sharedPref.edit()
            .remove(PICKUP_JCO_R)
            .apply()
    }

    fun removeCity() {
        sharedPref.edit()
            .remove(CITY)
            .apply()
    }

    fun removeOutletCode() {
        sharedPref.edit()
            .remove(OUTLET_CODE)
            .apply()
    }

    fun saveFcmToken(token: String) {
        sharedPref.edit()
            .putString(FCM_TOKEN, token)
            .apply()
    }

    fun loadFcmToken(): String? =
        sharedPref.getString(FCM_TOKEN, "")

    fun saveRefreshStateCartDetail(state: Boolean) {
        sharedPref.edit()
            .putBoolean(REFRESH_STATE_CART_DETAIL, state)
            .apply()
    }

    fun loadRefreshStateCartDetail(): Boolean =
        sharedPref.getBoolean(REFRESH_STATE_CART_DETAIL, false)

    fun saveVersion(version: Int) {
        sharedPref.edit()
            .putInt(VERSION, version)
            .apply()
    }

    fun loadVersion(): Int? =
        sharedPref.getInt(VERSION, 0)

    fun saveRecipientName(name: String) {
        sharedPref.edit()
            .putString(RECIPIENT_NAME, name)
            .apply()
    }

    fun loadRecipientName(): String? =
        sharedPref.getString(RECIPIENT_NAME, "")
    fun saveStatePickup(state: Boolean) {
        sharedPref.edit()
            .putBoolean(STATE_PICKUP, state)
            .apply()
    }

    fun loadStatePickup(): Boolean =
        sharedPref.getBoolean(STATE_PICKUP, false)

    fun saveJPoint(jpoint: String) {
        sharedPref.edit()
            .putString(JPOINT, jpoint)
            .apply()
    }

    fun loadJPoint(): String? =
        sharedPref.getString(JPOINT, "")

    fun saveDeliveryType(deliveryType: String) {
        sharedPref.edit()
            .putString(DELIVERY_TYPE, deliveryType)
            .apply()
    }

    fun loadDeliveryType(): String? =
        sharedPref.getString(DELIVERY_TYPE, "0")

}