package com.jcodonuts.app.utils

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.Window
import android.widget.LinearLayout
import com.jcodonuts.app.R
import com.jcodonuts.app.databinding.DlgProgressBarBinding

class DlgLoadingProgressBar(private val context: Context) {
    var dialog: Dialog? = null

    lateinit var binding : DlgProgressBarBinding

    init{
        dialog = Dialog(context, R.style.AppTheme_AppCompat_Dialog_Alert_NoFloating)
    }

    fun showPopUp(){
        binding = DlgProgressBarBinding.inflate(LayoutInflater.from(context))
        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.window!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT )
//        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(false)
        dialog?.setContentView(binding.root)
//        dialog?.window!!.attributes.windowAnimations = R.style.Theme_MaterialComponents_BottomSheetDialog
        dialog?.show()
    }

    fun dismissPopup() = dialog?.dismiss()
}