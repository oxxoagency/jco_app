package com.jcodonuts.app.utils

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import co.lujun.androidtagview.TagContainerLayout
import co.lujun.androidtagview.TagView
import com.airbnb.epoxy.EpoxyRecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.jcodonuts.app.R
import com.jcodonuts.app.data.local.*
import com.jcodonuts.app.data.remote.model.res.PackageDonut
import com.jcodonuts.app.productDetailDonut
import com.jcodonuts.app.productDetailDonutCustom
import com.jcodonuts.app.ui.jcor.main.cart.CartJcoRControllerListener
import com.jcodonuts.app.ui.main.cart.CartControllerListener
import com.jcodonuts.app.ui.main.home.BannerSliderAdapter
import com.jcodonuts.app.ui.main.home.HomeControllerListener
import com.jcodonuts.app.ui.product_detail.ProductDetailControllerListener
import com.jcodonuts.app.ui.product_detail.ProductDetailDonutAdapter
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import net.glxn.qrgen.android.QRCode
import org.sufficientlysecure.htmltextview.HtmlTextView
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*


private const val TAG = "Extension"

@BindingAdapter(value = ["imgUrl", "roundCorner", "imgPlaceholder"], requireAll = false)
fun AppCompatImageView.setImgUrl(imgUrl: String?, roundCorner: Int = 0, imgPlaceholder: Int = 0) {

    var placeholder = R.drawable.img_placeholder_donut
    if (imgPlaceholder != 0) {
        placeholder = imgPlaceholder
    }

    if (imgUrl != null) {
        if (roundCorner > 0) {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .transform(CenterInside(), RoundedCorners(dpToPx(roundCorner)))
                .into(this)
        } else {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .into(this)
        }
    } else {
        setImageResource(R.drawable.img_placeholder_donut)
    }
}

@BindingAdapter(value = ["imgUrlPayment", "roundCorner", "imgPlaceholder"], requireAll = false)
fun AppCompatImageView.setImgUrlPayment(
    imgUrl: String?,
    roundCorner: Int = 0,
    imgPlaceholder: Int = 0,
) {

    var placeholder = R.drawable.ic_payment
    if (imgPlaceholder != 0) {
        placeholder = imgPlaceholder
    }

    if (imgUrl != null) {
        if (roundCorner > 0) {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .transform(CenterInside(), RoundedCorners(dpToPx(roundCorner)))
                .into(this)
        } else {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .into(this)
        }
    } else {
        setImageResource(R.drawable.ic_payment)
    }
}

@BindingAdapter(value = ["imgUrlPaymentJcoR", "roundCorner", "imgPlaceholder"], requireAll = false)
fun AppCompatImageView.setImgUrlPaymentJcoR(
    imgUrl: String?,
    roundCorner: Int = 0,
    imgPlaceholder: Int = 0,
) {

    var placeholder = R.drawable.ic_payment_jco_r
    if (imgPlaceholder != 0) {
        placeholder = imgPlaceholder
    }

    if (imgUrl != null) {
        if (roundCorner > 0) {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .transform(CenterInside(), RoundedCorners(dpToPx(roundCorner)))
                .into(this)
        } else {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .into(this)
        }
    } else {
        setImageResource(R.drawable.ic_payment_jco_r)
    }
}


@BindingAdapter("imgSVG")
fun AppCompatImageView.bindImgSVG(url: String) {
    SvgLoader.fetchSvg(context, url, this)
}

@BindingAdapter("src")
fun ImageView.bindSrc(img: Int) {
    this.setImageResource(img)
}

@BindingAdapter("isLocationFavorite")
fun ImageView.bindLocationFavorited(isFavorite: Boolean) {
    if (isFavorite) {
        ImageViewCompat.setImageTintList(
            this, ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.colorPrimary
                )
            )
        )
    } else {
        ImageViewCompat.setImageTintList(
            this, ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.c_text_secondary
                )
            )
        )
    }
}

@BindingAdapter("imgDrawable")
fun AppCompatImageView.bindImgDrawable(drawable: Int) {
    this.setImageResource(drawable)
}

@BindingAdapter("html")
fun HtmlTextView.bindText(text: String?) {
    text?.let {
        this.setHtml(it)
        this.setOnClickATagListener { widget, spannedText, href ->
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(href)
            context.startActivity(i)
            true
        }
    }
}

@BindingAdapter("isMenuItemFavorite")
fun AppCompatImageView.bindFavorite(isFavorite: Boolean) {
    if (isFavorite) {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite))
        this.setColorFilter(
            ContextCompat.getColor(context, R.color.c_favorite_true),
            android.graphics.PorterDuff.Mode.SRC_IN)
    } else {
        this.setImageDrawable(ContextCompat.getDrawable(context,
            R.drawable.ic_round_favorite_border))
        this.setColorFilter(
            ContextCompat.getColor(context, R.color.c_favorite_false),
            android.graphics.PorterDuff.Mode.SRC_IN)
    }
}

@BindingAdapter("isMenuItemFavoriteNew")
fun AppCompatImageView.bindFavoriteNew(isFavorite: Boolean) {
    if (isFavorite) {
        this.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_favorite))
        this.setColorFilter(
            ContextCompat.getColor(context, R.color.c_favorite_true),
            android.graphics.PorterDuff.Mode.SRC_IN
        )
    } else {
        this.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_favorite_border))
    }

}

@BindingAdapter("isMenuItemFavoriteJcoR")
fun AppCompatImageView.bindFavoriteJcoR(isFavorite: Boolean) {
    if (isFavorite) {
        this.setColorFilter(
            ContextCompat.getColor(context, R.color.c_favorite_true),
            android.graphics.PorterDuff.Mode.SRC_IN
        )
    } else {
        this.setColorFilter(
            ContextCompat.getColor(context, R.color.c_text_secondary),
            android.graphics.PorterDuff.Mode.SRC_IN
        )
    }

}

@BindingAdapter("isSizeBevarge")
fun TextView.bindSizBeverage(state: Boolean) {
    if (state) {
        this.setTextColor(ContextCompat.getColor(context, R.color.c_white))
        this.background = ContextCompat.getDrawable(context, R.drawable.bg_radio_button_selected)
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
        this.background = ContextCompat.getDrawable(context, R.drawable.bg_radio_button_idle)
    }
}

@BindingAdapter("isOpacityDonut")
fun TextView.bindOpacityDonut(state: Boolean) {
    if (state) {
        this.alpha = 0.5f
    } else {
        this.alpha = 1.0f
    }
}

@BindingAdapter("isOpacityDonut")
fun LinearLayout.bindOpacityDonut(state: Boolean) {
    if (state) {
        this.alpha = 0.5f
    } else {
        this.alpha = 1.0f
    }
}

@BindingAdapter("isOpacityDonut")
fun ConstraintLayout.bindOpacityDonut(state: Boolean) {
    if (state) {
        this.alpha = 0.5f
    } else {
        this.alpha = 1.0f
    }
}

@BindingAdapter("isOpacityDonut")
fun CardView.bindOpacityDonut(state: Boolean) {
    if (state) {
        this.alpha = 0.5f
    } else {
        this.alpha = 1.0f
    }
}

@BindingAdapter("hideCustomDonut")
fun CardView.bindHideCustomDonut(state: Boolean) {
    if (state) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideCustomDonut")
fun LinearLayout.bindHideCustomDonut(state: Boolean) {
    if (state) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideCustomDonut")
fun ConstraintLayout.bindHideCustomDonut(state: Boolean) {
    if (state) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideCustomDonut")
fun EpoxyRecyclerView.bindHideCustomDonut(state: Boolean) {
    if (state) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideCustomDonut")
fun TextView.bindHideCustomDonut(state: Boolean) {
    if (state) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideCustomDonut")
fun ImageView.bindHideCustomDonut(state: Boolean) {
    if (state) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideCustomDonut")
fun HorizontalScrollView.bindHideCustomDonut(state: Boolean) {
    if (state) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("isMenuCategories")
fun TextView.bindMenuCategories(state: Boolean) {
    if (state) {
        this.setTextColor(ContextCompat.getColor(context, R.color.c_white))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.black))
    }
}

@BindingAdapter("isMenuCategoriesJcoR")
fun TextView.bindMenuCategoriesJcoR(state: Boolean) {
    if (state) {
        this.setTextColor(ContextCompat.getColor(context, R.color.c_white))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.c_white))
    }
}

@BindingAdapter("isMenuCategories")
fun TagContainerLayout.bindMenuCategories(state: Boolean) {
    if (state) {
        this.tagTextColor = ContextCompat.getColor(context, R.color.c_white)
    } else {
        this.tagTextColor = ContextCompat.getColor(context, R.color.black)
    }
}

@BindingAdapter("isTagMenuCategoriesJCOR")
fun TagContainerLayout.bindMenuCategoriesJcoR(state: Boolean) {
    if (state) {
        this.tagTextColor = ContextCompat.getColor(context, R.color.c_white)
    } else {
        this.tagTextColor = ContextCompat.getColor(context, R.color.c_white)
    }
}

@BindingAdapter("selected")
fun LinearLayout.bindSelected(selected: Boolean) {
    this.isSelected = selected
}

@BindingAdapter("selected")
fun TextView.bindSelected(selected: Boolean) {
    this.isSelected = selected
}

@BindingAdapter("selected")
fun ImageView.bindSelected(selected: Boolean) {
    this.isSelected = selected
}

@BindingAdapter("selected")
fun TagContainerLayout.bindSelected(selected: Boolean) {
    this.isSelected = selected
}

@BindingAdapter("listTags")
fun TagContainerLayout.bindListTags(datas: List<String>) {
    this.tags = datas
}

@BindingAdapter("loadWeb")
fun WebView.loadWeb(url: String) {
    this.loadUrl(url)
}

@BindingAdapter(value = ["onCartChangeListener", "cartSwitch"], requireAll = false)
fun SwitchCompatEx.onCartChangeListener(listener: CartControllerListener, cartSwitch: CartSwitch) {
    this.setOnCheckedChangeListener(null)
    this.isChecked = cartSwitch.pickup
    this.setOnCheckedChangeListener { _, isChecked ->
        listener.onSwitchChange(isChecked)
        Log.d("CHECKBOX", isChecked.toString())
    }
}

@BindingAdapter(value = ["checkedCheckBox", "jpointBalance"])
fun SwitchCompat.bindCheckedCheckBox(listener: CartControllerListener, jpointBalance: Int) {
    this.setOnCheckedChangeListener { _, isChecked ->
        listener.onUsedJPoint(isChecked)
        Log.d("CHECKBOX", isChecked.toString())
    }
    this.isEnabled = jpointBalance > 0
    this.isChecked = false
    Log.d("CHECKBOX", isChecked.toString())
}

@BindingAdapter(value = ["checkedCheckBox", "jpointBalance"])
fun SwitchCompat.bindCheckedCheckBox(listener: CartJcoRControllerListener, jpointBalance: Int) {
    this.setOnCheckedChangeListener { _, isChecked ->
        listener.onUsedJPoint(isChecked)
        Log.d("CHECKBOX", isChecked.toString())
    }
    this.isEnabled = jpointBalance > 0
    this.isChecked = false
    Log.d("CHECKBOX", isChecked.toString())
}

@BindingAdapter(value = ["onCartChangeJcoRListener", "cartSwitchJcoR"], requireAll = false)
fun SwitchCompatExJcoR.onCartChangeJcoRListener(
    listener: CartJcoRControllerListener,
    cartSwitch: CartSwitch,
) {
    this.setOnCheckedChangeListener(null)
    this.isChecked = cartSwitch.pickup
    this.setOnCheckedChangeListener { _, isChecked ->
        listener.onSwitchChange(isChecked)
    }
}

@BindingAdapter("cardBackgroundColor")
fun MaterialCardView.bindCardBg(color: String) {
    this.setCardBackgroundColor(Color.parseColor(color))
}

@BindingAdapter("titleActionbar")
fun TextView.bindTitle(title: Int) {
    this.setText(title)
}

//@BindingAdapter("setWebViewClient")
//fun WebView.setWebViewClient(client: WebViewClient?) {
//    this.webViewClient = client
//}

fun dpToPx(dp: Int): Int {
    return (dp * Resources.getSystem().displayMetrics.density).toInt()
}

@BindingAdapter("hideDonutTextView")
fun hideDonutTextView(linearLayout: LinearLayout, text: String) {
    if (text.contains("Donuts", true) && !text.contains("PROMO", true)) {
        linearLayout.visibility = View.VISIBLE
    } else {
        linearLayout.visibility = View.GONE
    }
}

@BindingAdapter("hideStatementDonut")
fun hideStatementDonut(textView: TextView, text: String) {
    if (text.contains("Donuts", true) && !text.contains("PROMO", true)) {
        textView.visibility = View.VISIBLE
    } else {
        textView.visibility = View.GONE
    }
}

@BindingAdapter("showImageBalance")
fun bindImageBalance(imageView: ImageView, image: Int) {
    Glide.with(imageView.context).load(image)
        .into(imageView)
}

@BindingAdapter("convertRupiah")
fun bindConvertRupiah(textView: TextView, price: Double) {
    textView.text = Converter.rupiah(price)
}

@BindingAdapter("convertRupiahPromo")
fun bindConvertRupiahPromo(textView: TextView, price: Int) {
    textView.text = Converter.rupiah(price.toDouble())
}


@BindingAdapter("notes")
fun bindNotes(textView: TextView, notes: String?) {
    if (notes.isNullOrEmpty()) {
        textView.text = "Add Notes"
    } else {
        textView.text = notes
    }
}

@BindingAdapter("paymentMethod")
fun bindPaymentMethod(textView: TextView, paymentMethod: String?) {
    if (paymentMethod.isNullOrEmpty()) {
        textView.text = textView.context.getString(R.string.choose_payment)
    } else {
        textView.text = paymentMethod
    }
}

@BindingAdapter("voucher")
fun TextView.bindVoucher(voucher: String?) {
    if (voucher.isNullOrEmpty()) {
        this.text = context.getString(R.string.use_a_voucher_promo_code)
    } else {
        this.text = voucher
    }
}

@BindingAdapter("imageDrawable")
fun bindImageDrawable(imageView: ImageView, drawable: Int) {
    Glide.with(imageView.context)
        .load(drawable)
        .into(imageView)
}

@BindingAdapter("htmlText")
fun bindHtmlText(textView: TextView, text: String?) {
    textView.text = text?.replace("<br>", "\n", true)
}

@BindingAdapter("htmlTextView")
fun HtmlTextView.bindHtmlTexView(text: String?) {
    this.setHtml(text.toString().replace("\n", "<br>", true))
}

@BindingAdapter(value = ["search", "activity"])
fun SearchView.bindSearch(listener: HomeControllerListener, activity: Activity) {
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            listener.onSearchItem(query.toString())
            hideKeyboard(activity)
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean = false

    })
}

@BindingAdapter(value = ["searchJcoR", "activityJcoR"])
fun SearchView.bindSearch(
    listener: com.jcodonuts.app.ui.jcor.main.home.HomeControllerListener,
    activity: Activity,
) {
    val editText = this.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
    editText.setTextColor(Color.WHITE)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        editText.setTextCursorDrawable(R.drawable.custom_cursor)
    }
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            listener.onSearchItem(query.toString())
            hideKeyboard(activity)
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean = false

    })
}

fun hideKeyboard(activity: Activity) {
    val imm: InputMethodManager =
        activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = activity.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

@BindingAdapter(value = ["tagClickListener", "tagData"], requireAll = false)
fun TagContainerLayout.bindClick(
    listener: HomeControllerListener,
    menuSearchTagName: MenuSearchTagName,
) {
    this.setOnTagClickListener(object : TagView.OnTagClickListener {
        override fun onTagClick(position: Int, text: String?) {
            listener.onSearchMenuCategoryClick(menuSearchTagName, position)
        }

        override fun onTagLongClick(position: Int, text: String?) {
            TODO("Not yet implemented")
        }

        override fun onSelectedTagDrag(position: Int, text: String?) {
            TODO("Not yet implemented")
        }

        override fun onTagCrossClick(position: Int) {
            TODO("Not yet implemented")
        }
    })
}

@BindingAdapter(value = ["tagClickListenerJcoR", "tagDataJcoR"], requireAll = false)
fun TagContainerLayout.bindClickJcoR(
    listener: com.jcodonuts.app.ui.jcor.main.home.HomeControllerListener,
    menuSearchTagName: MenuSearchTagName,
) {
    this.tagTextColor = ContextCompat.getColor(context, R.color.c_white)
    this.setOnTagClickListener(object : TagView.OnTagClickListener {
        override fun onTagClick(position: Int, text: String?) {
            listener.onSearchMenuCategoryClick(menuSearchTagName, position)
        }

        override fun onTagLongClick(position: Int, text: String?) {
            TODO("Not yet implemented")
        }

        override fun onSelectedTagDrag(position: Int, text: String?) {
            TODO("Not yet implemented")
        }

        override fun onTagCrossClick(position: Int) {
            TODO("Not yet implemented")
        }
    })
}

@BindingAdapter("hidePlusMinusDonuts")
fun ImageView.bindHidePlusMinusDonuts(data: ProductDetailContent) {
    if (data.productName?.contains("donut", true) == true) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hidePlusMinusDonuts")
fun TextView.bindHidePlusMinusDonuts(data: ProductDetailContent) {
    if (data.productName?.contains("donut", true) == true) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("recyclerViewDonut")
fun RecyclerView.bindRecyclerViewDonut(data: List<PackageDonut>?) {
    val adapter = this.adapter as ProductDetailDonutAdapter?
    if (data != null) {
        adapter?.setVariant(data)
    }
    adapter?.notifyDataSetChanged()
}

@BindingAdapter(value = ["sliderPromo", "listenerPromo", "selectedPromo"])
fun SliderView.bindSliderViewPromo(
    data: HomePromos?,
    listener: HomeControllerListener,
    selected: Boolean,
) {
    val adapter = BannerSliderAdapter(listener, selected)
    adapter.setSlider(data?.promos as MutableList<PromoBanner>)
    this.setSliderAdapter(adapter)
    this.setIndicatorAnimation(IndicatorAnimationType.WORM)
    this.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
    Log.d("cekdatapromo", data.toString())

}

@BindingAdapter(value = ["sliderPromoJcoR", "listenerPromoJcoR", "selectedPromoJcoR"])
fun SliderView.bindSliderViewPromoJcoR(
    data: HomePromos?,
    listener: com.jcodonuts.app.ui.jcor.main.home.HomeControllerListener,
    selected: Boolean,
) {
    val adapter = com.jcodonuts.app.ui.jcor.main.home.BannerSliderAdapter(listener, selected)
    adapter.setSlider(data?.promos as MutableList<PromoBanner>)
    this.setSliderAdapter(adapter)
    this.setIndicatorAnimation(IndicatorAnimationType.WORM)
    this.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
    Log.d("cekdatapromo", data.toString())

}

@BindingAdapter("alphaDonut")
fun LinearLayout.bindAlphaDonut(data: ProductDetailVariant) {
    if (data.max == data.mainQuantity && data.max != 0 && data.mainQuantity != 0) {
        if (data.quantity != 0) {
            this.alpha = 1.0f
        } else {
            this.alpha = 0.5f
        }
    }
}

@BindingAdapter("hideDonutCustomize")
fun ImageView.bindHideDonutCustomize(data: ProductDetailVariant) {
    if (data.variant.contains("sendiri", true)) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideDonutCustomize")
fun TextView.bindHideDonutCustomize(data: ProductDetailVariant) {
    if (data.variant.contains("sendiri", true)) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideDonutCustomize")
fun View.bindHideDonutCustomize(data: ProductDetailVariant) {
    if (data.variant.contains("sendiri", true)) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideDonutCustomize")
fun LinearLayout.bindHideDonutCustomize(data: ProductDetailVariant) {
    if (data.variant.contains("sendiri", true)) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideDonutCustomize")
fun HorizontalScrollView.bindHideDonutCustomize(data: ProductDetailVariant) {
    if (data.variant.contains("sendiri", true)) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideDonutCustomize")
fun AppCompatImageView.bindHideDonutCustomize(data: ProductDetailVariant) {
    if (data.variantDonut.size > 12) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideDonutCustomize")
fun ConstraintLayout.bindHideDonutCustomize(data: ProductDetailVariant) {
    if (data.variantDonut.size > 12) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("epoxyDonut")
fun EpoxyRecyclerView.bindEpoxyDonut(data: List<PackageDonut>) {
    withModels {
        data.forEachIndexed { index, packageDonut ->
            productDetailDonut {
                id("$index ${packageDonut.menu_img}")
                data(packageDonut)
            }
        }
    }
}

@BindingAdapter(
    value = ["epoxyDonutCustom", "listenerCustomDonut", "indexVariantDonut", "quantityDonut", "maxCustomDonut", "totalAmount"],
    requireAll = false
)
fun EpoxyRecyclerView.bindEpoxyDonutCustom(
    data: ProductDetailVariant,
    listener: ProductDetailControllerListener,
    indexVariantDonut: Int,
    quantityDonut: List<Int>,
    maxCustomDonut: Int,
    totalAmount: Int,
) {
    withModels {
        data.variantDonut.forEachIndexed { index, packageDonut ->
            productDetailDonutCustom {
                id("$index ${packageDonut.menu_img}")
                data(packageDonut)
                quantity(quantityDonut[index])
                listener(listener)
                index(index)
                indexVariantDonut(indexVariantDonut)
                isSelected(quantityDonut[index] > 0)
                isMaxCustomDonut(quantityDonut[index] <= 0 && maxCustomDonut >= totalAmount)
            }
        }
    }
}

@BindingAdapter("selectedDonutCustom")
fun ConstraintLayout.bindSelectedDonutCustom(isSelected: Boolean) {
    if (isSelected) {
        this.background = ContextCompat.getDrawable(context, R.drawable.bg_custom_donut_selected)
    } else {
        this.background = ContextCompat.getDrawable(context, R.drawable.bg_transparent)
    }
}

@BindingAdapter("selectedDonutCustom")
fun TextView.bindSelectedDonutCustom(isSelected: Boolean) {
    if (isSelected) {
        this.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.c_text_secondary))
    }
}

@BindingAdapter("disabledRadioGroup")
fun RadioGroup.bindDisabledRadioGroup(state: Boolean) {
    if (state) {
        for (i in 0 until this.childCount) {
            this.getChildAt(i).isClickable = false
        }
    }
}

@BindingAdapter("checkedDelivery")
fun RadioButton.bindCheckedDelivery(transactionDetailShippingInformation: TransactionDetailShippingInformation) {
    if (transactionDetailShippingInformation.deliveryType?.contains("0", true) == true) {
        this.isChecked = true
    }
}

@BindingAdapter("checkedPickUp")
fun RadioButton.bindCheckedPickUp(transactionDetailShippingInformation: TransactionDetailShippingInformation) {
    if (transactionDetailShippingInformation.deliveryType?.contains("1", true) == true) {
        this.isChecked = true
    }
}

@BindingAdapter("changeToDelete")
fun ImageView.bindChangeToDelete(data: CartProduct) {
    if (data.qty == 1) {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_trash_new))
    } else {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.img_btn_minus))
    }
}

@BindingAdapter("changeToDelete")
fun ImageView.bindChangeToDelete(data: CartProductPickUp) {
    if (data.qty == 1) {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_trash_new))
    } else {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.img_btn_minus))
    }
}

@BindingAdapter("changeToDeleteJcoR")
fun ImageView.bindChangeToDelete(data: CartProductJcoR) {
    if (data.qty == 1) {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_trash_jco_r))
    } else {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.img_btn_minus_jco_r))
    }
}

@BindingAdapter("typeDelivery")
fun TextView.bindTypeDelivery(orderDetail: OrderDetail) {
    if (orderDetail.deliveryType.contains("0", true)) {
        this.text = context.getString(R.string.delivered_to)
    } else {
        this.text = context.getString(R.string.pickup_at)
    }
}

@BindingAdapter("typeDeliveryInformation")
fun TextView.bindTypeDeliveryInformation(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = context.getString(R.string.shipping_information)
    } else {
        this.text = context.getString(R.string.pick_up_information)
    }
}

@BindingAdapter("typeDelivery")
fun TextView.bindTypeDelivery(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = context.getString(R.string.delivered_to)
    } else {
        this.text = context.getString(R.string.pickup_at)
    }
}

@BindingAdapter("addressName")
fun TextView.bindAddressName(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = orderDetail.orderAddress
    } else {
        this.text = orderDetail.outletName
    }
}

@BindingAdapter("addressDetail")
fun TextView.bindAddressDetail(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = orderDetail.orderInfo
    } else {
        this.text = orderDetail.outletAddress
    }
}

@BindingAdapter("phoneTransaction")
fun TextView.bindPhoneTransaction(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = orderDetail.memberPhone
    } else {
        this.text = orderDetail.outletPhone
    }
}

@BindingAdapter("isHideSendFrom")
fun LinearLayout.bindIsHideSendFrom(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("showVariantCostume")
fun TextView.bindShowVariantCostume(cartProduct: CartProduct) {
    if (cartProduct.variantname.contains("sendiri", true)) {
//        this.text = cartProduct.getMenuName()
        this.text = cartProduct.variantname
    } else {
        this.text = cartProduct.variantname
    }
}

@BindingAdapter("showVariantCostume")
fun TextView.bindShowVariantCostume(cartProduct: CartProductPickUp) {
    if (cartProduct.variantname.contains("sendiri", true)) {
//        this.text = cartProduct.getMenuName()
        this.text = cartProduct.variantname
    } else {
        this.text = cartProduct.variantname
    }
}

@BindingAdapter("isClaimBackground")
fun AppCompatButton.bindIsClaimBackground(data: Voucher) {
    if (data.isClaim == 0) {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_orange_button)
    } else {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_grey)
    }
}

@BindingAdapter("isClaimBackgroundJcoR")
fun AppCompatButton.bindIsClaimBackgroundJcoR(data: Voucher) {
    if (data.isClaim == 0) {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_jco_r_secondary)
    } else {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_grey)
    }
}

@BindingAdapter("isClaimText")
fun TextView.bindIsClaimText(data: Voucher) {
    if (data.isClaim == 0) {
        this.text = context.getString(R.string.claim_now)
    } else {
        this.text = context.getString(R.string.claimed)
    }
}

@BindingAdapter("isUsedVoucherBackground")
fun AppCompatButton.bindIsUsedVoucherBackground(data: Voucher) {
    if (data.isUsed == 1) {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_grey)
    } else {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_orange_button)
    }
}

@BindingAdapter("isUsedVoucherBackgroundJcoR")
fun AppCompatButton.bindIsUsedVoucherBackgroundJcoR(data: Voucher) {
    if (data.isUsed == 1) {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_grey)
    } else {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_jco_r_secondary)
    }
}

@BindingAdapter("isUsedVoucherText")
fun TextView.bindIsUsedVoucherText(data: Voucher) {
    if (data.isUsed == 1) {
        this.text = context.getString(R.string.already_used)
    } else {
        this.text = context.getString(R.string.use_the_voucher)
    }
}

@BindingAdapter("hidePromoCart")
fun LinearLayout.bindHidePromoCart(data: CartDetail) {
    if (data.promo == 0) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hidePromoCart")
fun LinearLayout.bindHidePromoCart(data: CartTotal) {
    if (data.promo == 0) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("textJpoint")
fun TextView.bindTextJpoint(data: CartTotal) {
    if (data.jpointEarn == 0) {
        this.text = context.getText(R.string.jpoint_used)
    } else if (data.jpointUsed == 0) {
        this.text = context.getText(R.string.jpoint_received)
    }
}

@BindingAdapter("textUsedJpoint")
fun TextView.bindTextUsedJPoint(jpoint: Int) {
    if (jpoint <= 0) {
        this.text =
            "${context.getString(R.string.you_have)} $jpoint JPoint (${context.getString(R.string.cant_be_used)})"
    } else {
        this.text = "${context.getString(R.string.you_have)} $jpoint JPoint"
    }
}

@BindingAdapter("jpointBalance")
fun TextView.bindBalanceJpoint(data: CartTotal) {
    if (data.jpointEarn == 0) {
        this.text = "${Converter.thousandSeparator(data.jpointUsed.toString())} JPoint"
    } else if (data.jpointUsed == 0) {
        this.text = "${Converter.thousandSeparator(data.jpointEarn.toString())} JPoint"
    }
}

@BindingAdapter("hidePromo")
fun TextView.bindHidePromo(data: TransactionDetailTotal) {
    if (data.promo == 0) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideFreeDelivery")
fun LinearLayout.bindHideFreeDelivery(data: CartDetail) {
    if (data.isDelivery) {
        if (data.freeDelivery == 0 || data.deliveryService ?: 0 < 0) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
        }
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hideFreeDelivery")
fun LinearLayout.bindHideFreeDelivery(data: CartTotal) {
    if (data.isDelivery) {
        if (data.freeDelivery == 0 || data.deliveryService ?: 0 < 0) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
        }
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hideFreeDelivery")
fun TextView.bindHideFreeDelivery(data: TransactionDetailTotal) {
    if (data.freeDelivery == 0) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("distancePickup")
fun TextView.bindDistancePickup(data: PickupItem) {
    if (data.distance.contains("null")) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = data.distance
    }
}

@BindingAdapter("ecobagText")
fun TextView.bindEcobagText(data: CartDetail) {
    if (data.ecobag.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = "${data.ecobagQty}X ${data.ecobag}"
    }
}

@BindingAdapter("ecobagText")
fun TextView.bindEcobagText(data: CartTotal) {
    if (data.ecobag.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = "${data.ecobagQty}X ${data.ecobag}"
    }
}

@BindingAdapter("ecobagPrice")
fun TextView.bindEcobagPrice(data: CartDetail) {
    if (data.ecobag.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = data.ecobagSubTotal
    }
}

@BindingAdapter("ecobagPrice")
fun TextView.bindEcobagPrice(data: CartTotal) {
    if (data.ecobag.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = data.ecobagSubTotal
    }
}

@BindingAdapter("textButtonBanner")
fun MaterialButton.bindTextButtonBanner(data: Banner) {
    val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
    val version = pInfo.versionCode
    if (version < data.version ?: 0) {
        this.text = "Update Aplikasi"
    } else {
        if (data.relatedMenu?.size ?: 0 > 1) {
            this.text = context.getString(R.string.choose_promo_menu)
        } else {
            this.text = context.getString(R.string.add_to_cart)
        }

    }
}

@BindingAdapter("textButtonBanner")
fun AppCompatButton.bindTextButtonBanner(data: Banner) {
    if (data.relatedMenu?.size ?: 0 > 1) {
        this.text = context.getString(R.string.choose_promo_menu)
    } else {
        this.text = context.getString(R.string.add_to_cart)
    }
}

@BindingAdapter("textVersion")
fun TextView.bindTextVersion(data: ProfileFooter) {
    try {
        val pInfo = this.context.packageManager.getPackageInfo(this.context.packageName, 0)
        val version = pInfo.versionName
        this.text = "V$version(${pInfo.versionCode})"
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
}

@BindingAdapter("hidePlusMinus")
fun ImageView.bindHidePlusMinus(data: ProductDetailContent) {
    if (data.productName?.contains("donut", true) == true && !data.variant.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hidePlusMinus")
fun TextView.bindHidePlusMinus(data: ProductDetailContent) {
    if (data.productName?.contains("donut", true) == true && !data.variant.isNullOrEmpty()) {
        this.text = "${data.quantity}x"
    } else {
        this.text = "${data.quantity}"
    }
}

@BindingAdapter("textColorStatusOrder")
fun TextView.bindTextColorStatusOrder(data: RecentOrder) {
    if (data.orderStatus?.contains("8", true) == true) {
        this.setTextColor(ContextCompat.getColor(context, R.color.red))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.light_green))
    }
}

@BindingAdapter("textColorStatusOrder")
fun TextView.bindTextColorStatusOrder(data: TransactionDetailHeader) {
    if (data.orderStatus?.contains("8", true) == true) {
        this.setTextColor(ContextCompat.getColor(context, R.color.red))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.light_green))
    }
}

@BindingAdapter("hideVaNumber")
fun TextView.bindHideVaNumber(data: TransactionDetailOrderSummary) {
    if (data.vaNumber.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideInstruction")
fun TextView.bindHideInstruction(data: TransactionDetailOrderSummary) {
    if (data.getInstruction().isNullOrEmpty() || data.orderStatus?.contains("1") == false) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideDeliveryFee")
fun LinearLayout.bindHideDeliveryFee(data: CartDetail) {
    if (data.isDelivery) {
        if (data.freeDelivery == 0 || data.deliveryService ?: 0 < 0) {
            this.visibility = View.VISIBLE
        } else {
            this.visibility = View.GONE
        }
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hideDeliveryFee")
fun LinearLayout.bindHideDeliveryFee(data: CartTotal) {
    if (data.isDelivery) {
        if (data.freeDelivery == 0 || data.deliveryService ?: 0 < 0) {
            this.visibility = View.VISIBLE
        } else {
            this.visibility = View.GONE
        }
    } else {
        this.visibility = View.GONE
    }
}


@BindingAdapter("scratchText")
fun TextView.bindScratchText(data: CartDetail) {
    this.text = data.deliveryServiceText
    this.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
}

@BindingAdapter("scratchText")
fun TextView.bindScratchText(data: CartTotal) {
    this.text = data.deliveryServiceText
    this.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
}

@BindingAdapter("hideText")
fun TextView.bindHideText(text: String?) {
    if (text.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideText")
fun TextView.bindHideText(state: Boolean) {
    if (state) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hideImageView")
fun ImageView.bindHideImageView(state: Boolean) {
    if (state) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("qrCode")
fun ImageView.bindQrCode(orderId: String) {
    val bitmap = QRCode.from(orderId).bitmap()
    this.setImageBitmap(bitmap)
}

@BindingAdapter(value = ["scratchPrice", "priceText"])
fun TextView.bindScratchPrice(price: String, priceText: String) {
    this.text = price
    this.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
    if (price == "0"){
        this.visibility = View.GONE
    } else {
        if (priceText == price) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
        }
    }
}

@BindingAdapter(value = ["scratchPrice", "priceText"])
fun TextView.bindScratchPrice(price: Int, priceText: String) {
    this.text = price.toString()
    this.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
    if (price == 0){
        this.visibility = View.GONE
    } else {
        if (price.toString() == priceText) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
        }
    }
}

@BindingAdapter("textDate")
fun TextView.bindTextDate(date: String) {
    this.text = date.parseDateAndTimeWithFormat("dd MMM yyyy")
}

@BindingAdapter("textTime")
fun TextView.bindTextTime(date: String) {
    this.text = date.parseDateAndTimeWithFormat("HH:mm")
}

fun String.parseDateWithFormat(format: String): String? {
    val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    try {
        val result = df.parse(this) ?: return ""
        val sdf = SimpleDateFormat(format, Locale.getDefault())
        return sdf.format(result)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
    return this
}

fun String.parseDateAndTimeWithFormat(format: String): String? {
    val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    try {
        val result = df.parse(this) ?: return ""
        val sdf = SimpleDateFormat(format, Locale.getDefault())
        return sdf.format(result)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
    return this
}

fun Date.parseToTime(): String {
    val format = SimpleDateFormat("HH:mm")
    return format.format(this)
}